<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        if (!$this->session->userdata('user_id')) {
            redirect('', 'refresh');
        }
        $this->privileges = $this->MUser_privileges->get_by_ref_user($this->session->userdata('user_id'));
    }

    function index()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'settings';
        $data['content'] = 'admin/settings/home';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function init_path($string)
    {
        $array_path =  explode('.', $string);
        $realpath  = '';
        foreach ($array_path as $p) {

            $realpath .= $p;
            $realpath .= '/';
        }
        return $realpath;
    }
    public function db_backup()
    {
        $CI = &get_instance();
        $CI->load->database();
        $path = "";
        if ($CI->db->username == 'root') {
            if ($path != '') {
                $path = realpath($this->init_path($path));
            } else {
                if (!is_dir('D:/database/'))
                    mkdir('D:/database/', 0777);
                $path = realpath($this->init_path('D:/database/'));
            }
        }
        if ($host == '') {
            $host = $CI->db->hostname;
        }
        if ($user == '') {
            $user = $CI->db->username;
        }
        if ($pass == '') {
            $pass = $CI->db->password;
        }
        if ($name == '') {
            $name = $CI->db->database;
        }

        set_time_limit(3000);
        $mysqli = new mysqli($host, $user, $pass, $name);
        $mysqli->select_db($name);
        $mysqli->query("SET NAMES 'utf8mb4'");
        $queryTables = $mysqli->query('SHOW TABLES');
        while ($row = $queryTables->fetch_row()) {
            $target_tables[] = $row[0];
        }
        if ($tables !== false) {
            $target_tables = array_intersect($target_tables, $tables);
        }
        $content = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\r\nSET time_zone = \"+00:00\";\r\n\r\n\r\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\r\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\r\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\r\n/*!40101 SET NAMES utf8mb4 */;\r\n--\r\n-- Database: `" . $name . "`\r\n--\r\n\r\n\r\n";
        foreach ($target_tables as $table) {
            if (empty($table)) {
                continue;
            }
            $result = $mysqli->query('SELECT * FROM `' . $table . '`');
            $fields_amount = $result->field_count;
            $rows_num = $mysqli->affected_rows;
            $res = $mysqli->query('SHOW CREATE TABLE ' . $table);
            $TableMLine = $res->fetch_row();
            $content .= "\n\n" . $TableMLine[1] . ";\n\n";
            for ($i = 0, $st_counter = 0; $i < $fields_amount; $i++, $st_counter = 0) {
                while ($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
                    if ($st_counter % 100 == 0 || $st_counter == 0) {
                        $content .= "\nINSERT INTO " . $table . " VALUES";
                    }
                    $content .= "\n(";
                    for ($j = 0; $j < $fields_amount; $j++) {
                        $row[$j] = str_replace("\n", "\\n", addslashes($row[$j]));
                        if (isset($row[$j])) {
                            $content .= '"' . $row[$j] . '"';
                        } else {
                            $content .= '""';
                        }
                        if ($j < ($fields_amount - 1)) {
                            $content .= ',';
                        }
                    }
                    $content .= ")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ((($st_counter + 1) % 100 == 0 && $st_counter != 0) || $st_counter + 1 == $rows_num) {
                        $content .= ";";
                    } else {
                        $content .= ",";
                    }
                    $st_counter = $st_counter + 1;
                }
            }
            $content .= "\n\n\n";
        }
        $content .= "\r\n\r\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\r\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\r\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
        $backup_name = $backup_name ? $backup_name : $name . "_" . date('d-m-Y') . "_" . date('H-i-s') . ".sql";

        $fileLocation = $path . '\\' . $backup_name;
        $file = fopen($fileLocation, "w");
        fwrite($file, $content);
        fclose($file);
        header("Content-Type: text/plain");
        header("Content-Length: " . filesize($fileLocation));
        header('Content-Disposition: attachment; filename=' . $backup_name);
        readfile($fileLocation);
        // if($download_allow){
        // ob_get_clean(); header('Content-Type: application/octet-stream');   header("Content-Transfer-Encoding: Binary"); header("Content-disposition: attachment; filename=\"".$backup_name."\"");
        // }

        // echo $content; exit;
        // redirect('home/index', 'refresh');
    }

    public function basic()
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MSettings->update();
            } else {
                $this->MSettings->create($this->session->userdata('user_company'));
            }
            redirect('settings', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'settings';
            $data['content'] = 'admin/settings/basic';
            $data['settings'] = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            if ($data['settings']) {
                $data['payable_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_payable']);
                $data['receivable_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_receivable']);
                $data['cash_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_cash']);
                $data['bank_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_bank']);
                $data['sales_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_sales']);
                $data['purchase_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_purchase']);

                // $data['inventory_tree'] = $this->MAc_charts->get_basic_setting_coa_tree($data['settings']['ac_inventory']);

                $data['ac_inventory_raw'] = $this->MAc_charts->get_by_id($data['settings']['ac_inventory_raw']);
                $data['ac_inventory_wet_blue'] = $this->MAc_charts->get_by_id($data['settings']['ac_inventory_wet_blue']);
                $data['ac_inventory_chemical'] = $this->MAc_charts->get_by_id($data['settings']['ac_inventory_chemical']);
                $data['ac_inventory_fg'] = $this->MAc_charts->get_by_id($data['settings']['ac_inventory_fg']);
                // $data['project'] = $this->MAc_charts->get_by_id($data['settings']['ac_inventory_fg']);

                $data['cogs_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_cogs']);
                $data['tax_tree'] = $this->MAc_charts->get_by_id($data['settings']['ac_tax']);
            }
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function cmp_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'settings';
        $data['content'] = 'admin/settings/company/list';
        $data['companies'] = $this->MCompanies->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function cmp_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MCompanies->update();
                $company_id = $this->input->post('id');
            } else {
                $company_id = $this->MCompanies->create();
                $ref_user = $this->MUsers->create($company_id, $this->input->post('contact_person'), 'Power User', 'Active');
                $this->MUser_privileges->power_user($company_id, $ref_user);
                $this->MAc_charts->basic_setup($company_id, $ref_user);
            }

            if ($_FILES['logo']['name']) {
                $config['file_name'] = $company_id;
                $config['upload_path'] = './uploads/companies/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload("logo")) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                } else {
                    $filedata = $this->upload->data();
                    $this->MCompanies->add_logo($filedata['file_name'], $company_id);

                    $con['image_library'] = 'gd2';
                    $con['source_image'] = './uploads/companies/' . $filedata['file_name'];
                    $con['maintain_ratio'] = TRUE;
                    $con['overwrite'] = TRUE;
                    $con['width'] = 150;
                    $con['height'] = 50;
                    $this->load->library('image_lib', $con);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                }
            }

            $this->session->set_flashdata('message', 'Company Saved.');
            redirect('settings/cmp_list', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'settings';
            $data['content'] = 'admin/settings/company/save';
            $data['company'] = $this->MCompanies->get_by_id($id);
            $data['currencies'] = $this->MCurrencies->get_all();
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function cmp_delete($id)
    {
        $result = $this->MSettings->get_users($id);

        if ($result > 0) {
            $this->session->set_flashdata('error', 'Please delete ' . $result . ' users against this company.');
            redirect('settings/cmp_list', 'refresh');
        } else {
            $this->MAc_charts->delete_by_cmp($id);
            $this->MAc_journal_details->delete_by_cmp($id);
            $this->MAc_journal_master->delete_by_cmp($id);
            $this->MAc_money_receipts->delete_by_cmp($id);
            $this->MAc_payment_receipts->delete_by_cmp($id);
            $this->MCustomers->delete_by_cmp($id);
            $this->MEmps->delete_by_cmp($id);
            $this->MItems->delete_by_cmp($id);
            $this->MPurchase_details->delete_by_cmp($id);
            $this->MPurchase_master->delete_by_cmp($id);
            $this->MSales_details->delete_by_cmp($id);
            $this->MSales_master->delete_by_cmp($id);
            $this->MSettings->delete_by_cmp($id);
            $this->MSuppliers->delete_by_cmp($id);
            $this->MUser_privileges->delete_by_cmp($id);
            $this->MUsers->delete_by_cmp($id);
            $this->MCompanies->delete($id);
            redirect('settings/cmp_list', 'refresh');
        }
    }

    public function cmp_set($id)
    {
        $this->session->unset_userdata('user_company');
        $this->session->set_userdata('user_company', $id);
        redirect('settings', 'refresh');
    }

    //Chart of Accounts Start ---------------

    public function chart_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'settings';
        $data['content'] = 'admin/settings/chart/list';
        $data['charts'] = $this->MAc_default_charts->get_coa_list();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function chart_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MAc_default_charts->update();
            } else {
                $this->MAc_default_charts->create();
            }
            redirect('settings/chart_list', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'settings';
            $data['content'] = 'admin/settings/chart/save';
            $data['chart'] = $this->MAc_default_charts->get_by_id($id);
            if ($id) {
                $data['ac_chart_tree'] = $this->MAc_default_charts->get_coa_tree($data['chart']['parent_id']);
            } else {
                $data['ac_chart_tree'] = $this->MAc_default_charts->get_coa_tree();
            }
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function chart_delete($id)
    {
        $this->MAc_default_charts->delete($id);
        redirect('settings/chart_list', 'refresh');
    }

    //Chart of Accounts End ---------------

    //Currency Start ---------------

    public function currency_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'settings';
        $data['content'] = 'admin/settings/currency/list';
        $data['currencies'] = $this->MCurrencies->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function currency_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MCurrencies->update();
                $this->session->set_flashdata('success', 'Currency updated successfully.');
            } else {
                $this->MCurrencies->create();
                $this->session->set_flashdata('success', 'Currency created successfully.');
            }
            redirect('settings/currency_list', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'settings';
            $data['content'] = 'admin/settings/currency/save';
            $data['currency'] = $this->MCurrencies->get_by_id($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function currency_delete($id)
    {
        $this->MCurrencies->delete($id);
        $this->session->set_flashdata('info', 'Currency deleted successfully.');
        redirect('settings/currency_list', 'refresh');
    }

    //Currency End ---------------

}
