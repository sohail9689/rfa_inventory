<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MReports extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_purchase_date($start_date, $end_date)
    {

        // $inv_date = array();
        $this->db->where('purchase_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('status', '1');
        $this->db->group_by('purchase_date');
        $q = $this->db->get('purchase_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $inv_date['inv_date'] = $row['purchase_date'];
                // $tmp_date[] = $inv_date;
            }
        }
        return $inv_date;
    }
    public function get_production_date($start_date, $end_date)
    {
        $inv_date = array();
        $this->db->where('production_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('production_date');
        $q = $this->db->get('production');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['production_date'];
                // $tmp_date[] = $inv_date;
            }
        }
        return $inv_date;
    }

    public function get_production_selection_date($start_date, $end_date)
    {
        $inv_date = array();
        $this->db->where('selection_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('selection_date');
        $q = $this->db->get('production_selection_item');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['selection_date'];
                // $tmp_date[] = $inv_date;
            }
        }
        return $inv_date;
    }
    public function get_sales_date($start_date, $end_date)
    {
        $inv_date = array();
        $this->db->where('sales_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('sales_date');
        $q = $this->db->get('sales_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['sales_date'];
                // $tmp_date[] = $inv_date;
            }
        }
        return $inv_date;
    }

    public function get_inventory_summary()
    {

        if ($this->input->post('type') == 'Raw') {
            return  $this->get_raw_summary();
        } elseif ($this->input->post('type') == 'Chemical') {
            return $this->get_chemical_summary();
        } elseif ($this->input->post('type') == 'Wet Blue') {
            return $this->get_wet_blue_summary();
        } elseif ($this->input->post('type') == 'FG') {
            return $this->get_fg_summary();
        }
    }

    public function get_raw_summary()
    {
        $data = array();
        $tmp_date = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if ($this->input->post('item_id') != '0') {

            $items = array($this->MItems->get_by_id($this->input->post('item_id')));
        } else {

            $items = $this->MItems->get_all('', $this->input->post('type'));
        }

        $this->db->where('purchase_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('status', '1');
        $this->db->group_by('purchase_date');
        $q = $this->db->get('purchase_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $inv_date['inv_date'] = $row['purchase_date'];
                $tmp_date[] = $inv_date;
            }
        }
        // $tmp_date[] = $this->get_purchase_date($start_date, $end_date);
        // $tmp_date[] = $this->get_production_date($start_date, $end_date);
        $this->db->where('production_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('production_date');
        $q = $this->db->get('production');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['production_date'];
                $tmp_date[] = $inv_date;
            }
        }

        $prev_purchases = $this->MPurchase_master->get_before_date($start_date);
        $prev_production = $this->MProduction->get_before_date($start_date);
        foreach ($items as $item) {

            $ins = 0;
            // Purchase Quantity
            foreach ($prev_purchases as $prev_purchase) {
                $purchase_details = $this->MPurchase_details->get_by_purchase_no_item_id($prev_purchase['purchase_no'], $item['id']);
                $ins += $purchase_details['sq_weight'];
            }

            // for production
            $outs = 0;

            foreach ($prev_production as $prev) {

                $pre_production = $this->MProduction->get_by_id($prev['id'], $item['id']);
                $outs += $pre_production['raw_quantity'];
            }
            $tmp_open['name'] = $item['name'];
            $tmp_open['avco_price'] = $item['avco_price'];
            $tmp_open['open'] = $ins - $outs;
            $open[] = $tmp_open;
            unset($tmp_open);
        }
        $inv = array();

        if (count($tmp_date) > 0) {
            $sort_date = array_sort($tmp_date, 'inv_date');
            $distinct_date = array_distinct($sort_date, 'inv_date');

            foreach ($distinct_date as $date) {

                // Get Purchase by date
                $purchases = $this->MPurchase_master->get_by_date($date['inv_date']);
                $productions = $this->MProduction->get_by_date($date['inv_date']);
                $tmp_item = array();
                foreach ($items as $item) {
                    $purchase_qty = 0;
                    if (count($purchases) > 0) {
                        foreach ($purchases as $purchase) {
                            $tmp_purchase = $this->MPurchase_details->get_by_purchase_no_item_id($purchase['purchase_no'], $item['id']);

                            if ($tmp_purchase) {
                                $purchase_qty += $tmp_purchase['sq_weight'];
                            }
                        }
                    }
                    $production_qty = 0;
                    if (count($productions) > 0) {
                        foreach ($productions as $production) {

                            $pre_production = $this->MProduction->get_by_id($production['id'], $item['id']);
                            $production_qty += $pre_production['raw_quantity'];
                        }
                    }
                    $tmp['item_id'] = $item['id'];
                    $tmp['item_name'] = $item['name'];
                    $tmp['avco_price'] = $item['avco_price'];
                    $tmp['purchase_qty'] = $purchase_qty;
                    $tmp['production_qty'] = $production_qty;
                    $tmp_item[] = $tmp;
                }
                $tmp_data['date'] = $date['inv_date'];
                $tmp_data['details'] = $tmp_item;
                $inv[] = $tmp_data;
            }
        }
        $data['open'] = isset($open) ? $open : 0;
        $data['inv'] = $inv;
        return $data;
    }

    public function get_chemical_summary()
    {
        $data = array();
        $tmp_date = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if ($this->input->post('item_id') != '0') {

            $items = array($this->MItems->get_by_id($this->input->post('item_id')));
        } else {

            $items = $this->MItems->get_all('', $this->input->post('type'));
        }
        $this->db->where('purchase_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('status', '1');
        $this->db->group_by('purchase_date');
        $q = $this->db->get('purchase_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $inv_date['inv_date'] = $row['purchase_date'];
                $tmp_date[] = $inv_date;
            }
        }

        $this->db->where('production_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('production_date');
        $q = $this->db->get('production');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['production_date'];
                $tmp_date[] = $inv_date;
            }
        }

        $this->db->where('purchase_return_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('purchase_return_date');
        $q = $this->db->get('purchase_return_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $inv_date['inv_date'] = $row['purchase_return_date'];
                $tmp_date[] = $inv_date;
            }
        }
        // $tmp_date[] = $this->get_purchase_date($start_date, $end_date);
        // $tmp_date[] = $this->get_production_date($start_date, $end_date);
        $prev_purchases = $this->MPurchase_master->get_before_date($start_date);
        $prev_production = $this->MProduction->get_before_date($start_date);
        $prev_purchases_return = $this->MPurchase_return_master->get_before_date($start_date);
        foreach ($items as $item) {

            $ins = 0;
            // Purchase Quantity
            foreach ($prev_purchases as $prev_purchase) {
                $purchase_details = $this->MPurchase_details->get_by_purchase_no_item_id($prev_purchase['purchase_no'], $item['id']);
                $ins += $purchase_details['sq_weight'];
            }

            // for production
            $outs = 0;
            foreach ($prev_production as $prev) {
                $pre_production = $this->MProduction->get_recipe_chemical_by_production_id($prev['id'], $item['id']);

                foreach ($pre_production as $key => $value) {
                    $outs += $value['qty_kg'];
                }
            }
            foreach ($prev_purchases_return as $purchases_return) {

                $purchase_return_details = $this->MPurchase_return_details->get_by_purchase_no_item_id($purchase_return['purchase_return_no'], $item['id']);
                $outs += $purchase_return_details['sq_weight'];
            }

            $tmp_open['name'] = $item['name'];
            $tmp_open['avco_price'] = $item['avco_price'];
            $tmp_open['open'] = $ins - $outs;
            $open[] = $tmp_open;
            unset($tmp_open);
        }
        $inv = array();

        if (count($tmp_date) > 0) {
            $sort_date = array_sort($tmp_date, 'inv_date');
            $distinct_date = array_distinct($sort_date, 'inv_date');

            foreach ($distinct_date as $date) {

                $purchases = $this->MPurchase_master->get_by_date($date['inv_date']);
                $productions = $this->MProduction->get_by_date($date['inv_date']);
                $purchases_return = $this->MPurchase_return_master->get_by_date($date['inv_date']);
                $tmp_item = array();

                foreach ($items as $item) {
                    $purchase_qty = 0;
                    if (count($purchases) > 0) {
                        foreach ($purchases as $purchase) {

                            $tmp_purchase = $this->MPurchase_details->get_by_purchase_no_item_id($purchase['purchase_no'], $item['id']);

                            if ($tmp_purchase) {
                                $purchase_qty += $tmp_purchase['sq_weight'];
                            }
                        }
                    }

                    $production_qty = 0;
                    if (count($productions) > 0) {
                        foreach ($productions as $production) {

                            $pre_production = $this->MProduction->get_recipe_chemical_by_production_id($production['id'], $item['id']);

                            foreach ($pre_production as $key => $value) {
                                $production_qty += $value['qty_kg'];
                            }
                        }
                    }
                    $purchase_return_qty = 0;
                    if (count($purchases_return) > 0) {
                        foreach ($purchases_return as $purchase_return) {
                            $tmp_purchase_return = $this->MPurchase_return_details->get_by_purchase_no_item_id($purchase_return['purchase_return_no'], $item['id']);

                            if ($tmp_purchase_return) {
                                $purchase_return_qty += $tmp_purchase_return['sq_weight'];
                            }
                        }
                    }
                    $tmp['item_id'] = $item['id'];
                    $tmp['item_name'] = $item['name'];
                    $tmp['avco_price'] = $item['avco_price'];
                    $tmp['purchase_qty'] = $purchase_qty;
                    $tmp['production_qty'] = $production_qty;
                    $tmp['purchase_return_qty'] = $purchase_return_qty;
                    $tmp_item[] = $tmp;
                }
                $tmp_data['date'] = $date['inv_date'];
                $tmp_data['details'] = $tmp_item;
                $inv[] = $tmp_data;
            }
        }
        $data['open'] = isset($open) ? $open : 0;
        $data['inv'] = $inv;
        return $data;
    }

    public function get_wet_blue_summary()
    {

        $data = array();
        $tmp_date = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if ($this->input->post('item_id') != '0') {

            $items = array($this->MItems->get_by_id($this->input->post('item_id')));
        } else {

            $items = $this->MItems->get_all('', $this->input->post('type'));
        }

        $this->db->where('purchase_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('status', '1');
        $this->db->group_by('purchase_date');
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $inv_date['inv_date'] = $row['purchase_date'];
                $tmp_date[] = $inv_date;
            }
        }

        $this->db->where('production_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('production_date');
        $q = $this->db->get('production');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['production_date'];
                $tmp_date[] = $inv_date;
            }
        }

        $this->db->where('selection_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('selection_date');
        $q = $this->db->get('production_selection_item');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['selection_date'];
                $tmp_date[] = $inv_date;
            }
        }

        $this->db->where('purchase_return_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('purchase_return_date');
        $q = $this->db->get('purchase_return_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {

                $inv_date['inv_date'] = $row['purchase_return_date'];
                $tmp_date[] = $inv_date;
            }
        }

        // $tmp_date[] = $this->get_purchase_date($start_date, $end_date);
        // $tmp_date[] = $this->get_production_selection_date($start_date, $end_date);
        // $tmp_date[] = $this->get_production_date($start_date, $end_date);
        $prev_purchases = $this->MPurchase_master->get_before_date($start_date);
        $prev_purchases_return = $this->MPurchase_return_master->get_before_date($start_date);
        $prev_production_selection = $this->MProduction->get_before_date_selection($start_date);
        $prev_production = $this->MProduction->get_before_date($start_date);

        foreach ($items as $item) {

            $ins = 0;
            // Purchase Quantity
            foreach ($prev_purchases as $prev_purchase) {
                $purchase_details = $this->MPurchase_details->get_by_purchase_no_item_id($prev_purchase['purchase_no'], $item['id']);
                $ins += $purchase_details['sq_weight'];
            }
            // produced in production selection
            foreach ($prev_production_selection as $prev_selection) {
                if ($prev_selection['selection_item_id'] == $item['id']) {
                    $ins += $prev_selection['sq_ft'];
                }
            }

            // for production
            $outs = 0;
            foreach ($prev_production as $prev) {

                $pre_production = $this->MProduction->get_by_id($prev['id'], $item['id']);
                $outs += $pre_production['raw_quantity'];
            }
            foreach ($prev_purchases_return as $purchases_return) {

                $purchase_return_details = $this->MPurchase_return_details->get_by_purchase_no_item_id($purchase_return['purchase_return_no'], $item['id']);
                $outs += $purchase_return_details['sq_weight'];
            }

            $tmp_open['name'] = $item['name'];
            $tmp_open['avco_price'] = $item['avco_price'];
            $tmp_open['open'] = $ins - $outs;

            $open[] = $tmp_open;
            unset($tmp_open);
        }

        $inv = array();
        if (count($tmp_date) > 0) {
            $sort_date = array_sort($tmp_date, 'inv_date');
            $distinct_date = array_distinct($sort_date, 'inv_date');
            foreach ($distinct_date as $date) {
                // Get Purchase by date
                $purchases = $this->MPurchase_master->get_by_date($date['inv_date']);
                $purchases_return = $this->MPurchase_return_master->get_by_date($date['inv_date']);

                $productions = $this->MProduction->get_by_date($date['inv_date']);
                $productions_selection = $this->MProduction->get_by_date_selection($date['inv_date']);
                $tmp_item = array();

                foreach ($items as $item) {
                    $purchase_qty = 0;
                    if (count($purchases) > 0) {
                        foreach ($purchases as $purchase) {

                            $tmp_purchase = $this->MPurchase_details->get_by_purchase_no_item_id($purchase['purchase_no'], $item['id']);

                            if ($tmp_purchase) {
                                $purchase_qty += $tmp_purchase['sq_weight'];
                            }
                        }
                    }

                    $production_qty = 0;
                    if (count($productions) > 0) {
                        foreach ($productions as $production) {

                            $pre_production = $this->MProduction->get_by_id($production['id'], $item['id']);
                            $production_qty += $pre_production['raw_quantity'];
                        }
                    }
                    $production_selection_qty = 0;
                    if (count($productions_selection) > 0) {
                        foreach ($productions_selection as $selection) {
                            if ($selection['selection_item_id'] == $item['id']) {
                                $production_selection_qty += $selection['sq_ft'];
                            }
                        }
                    }
                    $purchase_return_qty = 0;
                    if (count($purchases_return) > 0) {
                        foreach ($purchases_return as $purchase_return) {
                            $tmp_purchase_return = $this->MPurchase_return_details->get_by_purchase_no_item_id($purchase_return['purchase_return_no'], $item['id']);

                            if ($tmp_purchase_return) {
                                $purchase_return_qty += $tmp_purchase_return['sq_weight'];
                            }
                        }
                    }


                    $tmp['item_id'] = $item['id'];
                    $tmp['item_name'] = $item['name'];
                    $tmp['avco_price'] = $item['avco_price'];
                    $tmp['purchase_qty'] = $purchase_qty;
                    $tmp['production_qty'] = $production_qty;
                    $tmp['purchase_return_qty'] = $purchase_return_qty;
                    $tmp['production_selection_qty'] = $production_selection_qty;
                    $tmp_item[] = $tmp;
                }
                $tmp_data['date'] = $date['inv_date'];
                $tmp_data['details'] = $tmp_item;
                $inv[] = $tmp_data;
            }
        }

        $data['open'] = isset($open) ? $open : 0;
        $data['inv'] = $inv;
        return $data;
    }

    public function get_fg_summary()
    {
        $data = array();
        $tmp_date = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if ($this->input->post('item_id') != '0') {

            $items = array($this->MItems->get_by_id($this->input->post('item_id')));
        } else {

            $items = $this->MItems->get_all('', $this->input->post('type'));
        }

        $this->db->where('selection_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('selection_date');
        $q = $this->db->get('production_selection_item');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['selection_date'];
                $tmp_date[] = $inv_date;
            }
        }
        $this->db->where('sales_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('sales_date');
        $q = $this->db->get('sales_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['sales_date'];
                $tmp_date[] = $inv_date;
            }
        }
        $this->db->where('sales_return_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('sales_return_date');
        $q = $this->db->get('sales_return_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['sales_return_date'];
                $tmp_date[] = $inv_date;
            }
        }
        $this->db->where('purchase_return_date BETWEEN "' . date_to_db($start_date) . '" AND "' . date_to_db($end_date) . '"');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->group_by('purchase_return_date');
        $q = $this->db->get('purchase_return_master');

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $inv_date['inv_date'] = $row['purchase_return_date'];
                $tmp_date[] = $inv_date;
            }
        }

        // $tmp_date[] = $this->get_production_selection_date($start_date, $end_date);
        // $tmp_date[] = $this->get_sales_date($start_date, $end_date);
        $prev_production_selection = $this->MProduction->get_before_date_selection($start_date);
        $prev_purchases_return = $this->MPurchase_return_master->get_before_date($start_date);
        $prev_sales_return = $this->MSales_return_master->get_before_date($start_date);
        $prev_sales = $this->MSales_master->get_before_date($start_date);
        foreach ($items as $item) {

            $ins = 0;
            // Purchase Quantity
            foreach ($prev_production_selection as $prev_selection) {
                if ($prev_selection['selection_item_id'] == $item['id']) {
                    $ins += $prev_selection['sq_ft'];
                }
            }

            foreach ($prev_sales_return as $sales_return) {
                $sales_return_details = $this->MSales_return_details->get_by_sales_no_item_id($sales_return['sales_return_no'], $item['id']);
                $ins += $prev_selection['sq_weight'];
            }

            $outs = 0;
            // Sales Quantity
            foreach ($prev_sales as $prev_sale) {
                $sales_details = $this->MSales_details->get_by_sales_no_item_id($prev_sale['sales_no'], $item['id']);
                $outs += $sales_details['sq_weight'];
            }

            foreach ($prev_purchases_return as $purchases_return) {

                $purchase_return_details = $this->MPurchase_return_details->get_by_purchase_no_item_id($purchases_return['purchase_return_no'], $item['id']);
                $outs += $pre_production['sq_weight'];
            }
            $tmp_open['name'] = $item['name'];
            $tmp_open['avco_price'] = $item['avco_price'];
            $tmp_open['open'] = $ins - $outs;
            $open[] = $tmp_open;
            unset($tmp_open);
            //$open[$item['name']] = $ins - $outs;
        }
        //var_dump($just_open);
        $inv = array();

        if (count($tmp_date) > 0) {
            $sort_date = array_sort($tmp_date, 'inv_date');
            $distinct_date = array_distinct($sort_date, 'inv_date');


            foreach ($distinct_date as $date) {

                $productions_selection = $this->MProduction->get_by_date_selection($date['inv_date']);
                $sales = $this->MSales_master->get_by_date($date['inv_date']);
                $sales_return = $this->MSales_return_master->get_by_date($date['inv_date']);
                $purchases_return = $this->MPurchase_return_master->get_by_date($date['inv_date']);
                $tmp_item = array();

                foreach ($items as $item) {
                    $production_selection_qty = 0;
                    if (count($productions_selection) > 0) {
                        foreach ($productions_selection as $selection) {

                            if ($selection['selection_item_id'] == $item['id']) {
                                $production_selection_qty += $selection['sq_ft'];
                            }
                        }
                    }

                    $sales_qty = 0;
                    if (count($sales) > 0) {
                        foreach ($sales as $sale) {
                            $tmp_sales = $this->MSales_details->get_by_sales_no_item_id($sale['sales_no'], $item['id']);
                            if ($tmp_sales) {
                                $sales_qty += $tmp_sales['sq_weight'];
                            }
                        }
                    }
                    $purchase_return_qty = 0;
                    if (count($purchases_return) > 0) {
                        foreach ($purchases_return as $purchase_return) {
                            $tmp_purchase_return = $this->MPurchase_return_details->get_by_purchase_no_item_id($purchase_return['purchase_return_no'], $item['id']);

                            if ($tmp_purchase_return) {
                                $purchase_return_qty += $tmp_purchase_return['sq_weight'];
                            }
                        }
                    }
                    $sales_return_qty = 0;
                    if (count($sales_return) > 0) {
                        foreach ($sales_return as $sal_return) {
                            $tmp_sales_return = $this->MSales_return_details->get_by_sales_no_item_id($sal_return['sales_return_no'], $item['id']);

                            if ($tmp_sales_return) {
                                $sales_return_qty += $tmp_sales_return['sq_weight'];
                            }
                        }
                    }
                    $tmp['item_id'] = $item['id'];
                    $tmp['item_name'] = $item['name'];
                    $tmp['avco_price'] = $item['avco_price'];
                    $tmp['production_selection_qty'] = $production_selection_qty;
                    $tmp['sales_qty'] = $sales_qty;
                    $tmp['purchase_return_qty'] = $purchase_return_qty;
                    $tmp['sales_return_qty'] = $sales_return_qty;
                    $tmp_item[] = $tmp;
                }
                $tmp_data['date'] = $date['inv_date'];
                $tmp_data['details'] = $tmp_item;
                $inv[] = $tmp_data;
            }
        }
        $data['open'] = isset($open) ? $open : 0;
        $data['inv'] = $inv;
        return $data;
    }
    // public function get_stock_balance($product_id)
    // {

    //     $this->db->select_sum('quantity');
    //     $this->db->where('id', $product_id);
    //     $puchase_q = $this->db->get('production');
    //     if ($puchase_q->num_rows() > 0) {
    //         foreach ($puchase_q->result_array() as $row) {
    //             $purchase = $row;
    //         }
    //     }

    //     $puchase_q->free_result();

    //     $this->db->select_sum('quantity');
    //     $this->db->where('item_id', $product_id);
    //     $sales_q = $this->db->get('sales_details');
    //     if ($sales_q->num_rows() > 0) {
    //         foreach ($sales_q->result_array() as $row) {
    //             $sales = $row;
    //         }
    //     }

    //     $sales_q->free_result();

    //     return (int) $purchase['quantity'] - (int) $sales['quantity'];
    // }

    public function get_stock_balance($product_id)
    {



        $this->db->select('sum(quantity) as quantity');
        $this->db->where('item_id', $product_id);
        $sales_details = $this->db->get('sales_details');
        if ($sales_details->num_rows() > 0) {
            foreach ($sales_details->result_array() as $row) {
                $sales = $row;
            }
        }
        $sales_details->free_result();

        $this->db->select('sum(quantity) as quantity');
        $this->db->where('item_id', $product_id);
        $purchase_return_details = $this->db->get('purchase_return_details');
        if ($purchase_return_details->num_rows() > 0) {
            foreach ($purchase_return_details->result_array() as $row) {
                $purchase_return = $row;
            }
        }
        $purchase_return_details->free_result();

        $this->db->select('sum(quantity) as quantity');
        $this->db->where('item_id', $product_id);
        $sales_return_details = $this->db->get('sales_return_details');
        if ($sales_return_details->num_rows() > 0) {
            foreach ($sales_return_details->result_array() as $row) {
                $sales_return = $row;
            }
        }
        $sales_return_details->free_result();

        $this->db->select('sum(quantity) as quantity');
        $this->db->where('item_id', $product_id);
        $purchase_details = $this->db->get('purchase_details');
        if ($purchase_details->num_rows() > 0) {
            foreach ($purchase_details->result_array() as $row) {
                $purchase = $row;
            }
        }
        $purchase_details->free_result();

        $ins_quantity = (int) $purchase['quantity'] + (int) $sales_return['quantity'];
        $out_quantity = (int) $sales['quantity'] + (int) $purchase_return['quantity'];

        $data['total_quantity'] = (int) $ins_quantity - (int) $out_quantity;
        return $data;
    }

    public function get_chemical_stock_balance($product_id)
    {

        $this->db->select('sum(qty_kg) as quantity');
        $this->db->where('chemical_id', $product_id);
        $production_q = $this->db->get('production_recipe_chemical');
        if ($production_q->num_rows() > 0) {
            foreach ($production_q->result_array() as $row) {
                $production = $row;
            }
        }
        $production_q->free_result();

        $this->db->select('sum(sq_weight) as quantity');
        $this->db->where('purchase_details.item_id', $product_id);
        $this->db->where('purchase_master.status', '1');
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_details.company_id', $this->session->userdata('user_company'));
        $this->db->from('purchase_master');
        $this->db->join('purchase_details', 'purchase_master.purchase_no=purchase_details.purchase_no', 'left');
        $purchase_q = $this->db->get();
        if ($purchase_q->num_rows() > 0) {
            foreach ($purchase_q->result_array() as $row) {
                $purchase = $row;
            }
        }

        $purchase_q->free_result();

        $this->db->select('sum(quantity) as quantity, sum(sq_weight) as sq_weight');
        $this->db->where('item_id', $product_id);
        $purchase_return_details = $this->db->get('purchase_return_details');
        if ($purchase_return_details->num_rows() > 0) {
            foreach ($purchase_return_details->result_array() as $row) {
                $purchase_return = $row;
            }
        }
        $purchase_return_details->free_result();

        $out_quantity = (int) $production['quantity'] + (int) $purchase_return['quantity'];
        $data['total_quantity'] = (int) $purchase['quantity'] - (int) $out_quantity;
        return $data;
    }
}
