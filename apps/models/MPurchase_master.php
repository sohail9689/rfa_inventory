<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MPurchase_master extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->select('purchase_master.*, suppliers.name, suppliers.address, suppliers.contact_person, suppliers.phone_no');
        $this->db->from('purchase_master');
        $this->db->join('suppliers', 'purchase_master.supplier_id=suppliers.id');
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_master.id', $id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $item_qty = $this->MPurchase_details->get_total_quantity($id);
                $row['item_qty'] = $item_qty;
                $amount = $this->MPurchase_details->get_total_price($id);
                $row['amount'] = $amount;
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_purchase_no($purchase_no = NULL, $stat_date  = NULL, $en_date  = NULL)
    {
        $data = array();
        $this->db->select('purchase_master.*, SUM(purchase_details.quantity) as quantity, suppliers.name as supplier_name, suppliers.address as supplier_address, suppliers.phone_no as supplier_mobile');
        $this->db->from('purchase_master');
        $this->db->join('purchase_details', 'purchase_master.id = purchase_details.master_id', 'left');
        $this->db->join('suppliers', 'purchase_master.supplier_id = suppliers.id', 'left');
        if ($purchase_no) {
            $this->db->where('purchase_master.purchase_date >= ', ($stat_date));
            $this->db->where('purchase_master.purchase_date <= ', ($en_date));
            $this->db->where('purchase_master.purchase_no', $purchase_no);
        }
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("purchase_master.purchase_no");
        $this->db->order_by('purchase_master.id', 'ASC');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_purchase_master($id)
    {
        $data = array();
        $this->db->select('purchase_master.*, suppliers.name as supplier_name, suppliers.address as supplier_address, suppliers.phone_no as supplier_mobile');
        $this->db->from('purchase_master');
        $this->db->join('suppliers', 'purchase_master.supplier_id = suppliers.id', 'left');
        $this->db->where('purchase_master.id', $id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_last_purchase_no($stat_date, $en_date)
    {
        $data = array();
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('purchase_master');
        $start_y = date('y', strtotime($stat_date));
        $end_y = date('y', strtotime($en_date));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
            $string = $data['purchase_no'];
            $delimiter = "_";
            $array = explode($delimiter, $string);
            $aa = (int) $array[1] + 1;
            $number =  $start_y . $end_y . '_' . $aa;
        } else {
            $number = $start_y . $end_y . '_1001';
        }

        $q->free_result();
        return $number;
    }

    public function get_latest()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all($stat_date, $en_date)
    {
        $data = array();
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('purchase_no', 'DESC');
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $supplier = $this->MSuppliers->get_by_id($row['supplier_id']);
                $qty = $this->MPurchase_details->get_total_quantity($row['purchase_no'], NULL, $stat_date, $en_date);
                $price = $this->MPurchase_details->get_total_price($row['purchase_no'], NULL, $stat_date, $en_date);

                $row['supplier_name'] = $supplier['name'];
                $row['total_qty'] = $qty;
                $row['total_price'] = $price;
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_supplier_id($supplier_id)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('supplier_id', $supplier_id);
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        return $data;
    }


    public function get_before_date($date)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_date <', date_to_db($date));
        $this->db->where('status', '1');
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_date($date)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_date', $date);
        $this->db->where('status', '1');
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all_between_date($items = NULL, $supplier_id = NULL, $stat_date, $en_date, $type = NULL)
    {
        $data = array();
        $start_date = date_to_db($this->input->post('start_date'));
        $end_date = date_to_db($this->input->post('end_date'));
        $this->db->where("purchase_date BETWEEN '$start_date' AND '$end_date'");
        if ($supplier_id) {
            $this->db->where('supplier_id', $supplier_id);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('purchase_no', 'ASC');
        $q = $this->db->get('purchase_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $user = $this->MSuppliers->get_by_id($row['supplier_id']);
                $row['supplier_name'] = $user['name'];
                if ($items) {
                    $row['item_quantity'] = $this->MPurchase_details->get_total_quantity($row['purchase_no'], $items, $stat_date, $en_date, $type);
                    $row['total_price'] = $this->MPurchase_details->get_total_price($row['purchase_no'], $items, $stat_date, $en_date, $type);
                } else {
                    $row['item_quantity'] = $this->MPurchase_details->get_total_quantity($row['purchase_no'], NULL, $stat_date, $en_date, $type);
                    $row['total_price'] = $this->MPurchase_details->get_total_price($row['purchase_no'], NULL, $stat_date, $en_date, $type);
                }


                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function create()
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'supplier_id' => $this->input->post('supplier_id'),
            'purchase_no' => $this->input->post('purchase_no'),
            'purchase_date' => date_to_db($this->input->post('purchase_date')),
            'notes' => $this->input->post('notes'),
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );
        $this->db->insert('purchase_master', $data);

        return $this->db->insert_id();
    }

    public function create_by_production($item_id, $type, $production_id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];

        $purchase = $this->MPurchase_master->get_last_purchase_no($stat_date, $en_date);
        if (count($purchase) > 0) {
            $purchase_no = (int) $purchase['purchase_no'] + 1;
        } else {
            $purchase_no = 1001;
        }

        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'supplier_id' => '1',
            'purchase_no' => $purchase_no,
            'purchase_date' => date_to_db($this->input->post('production_date')),
            'notes' => $this->input->post('notes'),
            'status' => '1',
            'production_id' => $production_id,
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );
        $this->db->insert('purchase_master', $data);

        if ($this->db->insert_id()) {
            $items = $this->MItems->get_by_id($item_id);
            $this->db->select('sum(total_price) as total_price');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $this->db->where('production_id', $production_id);
            $total_price = $this->db->get('production_recipe_chemical')->row_array();
            $prunit = $total_price['total_price'] / $this->input->post('raw_quantity');
            $data = array(
                'company_id' => $this->session->userdata('user_company'),
                'purchase_no' => $purchase_no,
                'item_id' => $item_id,
                'item_type' => $type,
                'ac_id' => $items['ac_id'],
                'quantity' => $this->input->post('quantit'),
                'sq_weight' => $this->input->post('raw_quantity'),
                'unit' => 'sq ft',
                'purchase_date' => date_to_db($this->input->post('production_date')),
                'purchase_price' => $prunit,
                'production_id' => $production_id,
                'total_price' => $this->input->post('raw_quantity') * $prunit,
                'created' => date('Y-m-d H:i:s', time()),
                'created_by' => $this->session->userdata('user_id'),
            );
            $this->db->insert('purchase_details', $data);
            $avco = $this->MPurchase_details->get_avco(trim($item_id));
            $this->MItems->update_field($item_id, 'avco_price', $avco);
        }
    }

    public function update($id)
    {
        $data = array(
            'supplier_id' => $this->input->post('supplier_id'),
            'purchase_date' => date_to_db($this->input->post('purchase_date')),
            'notes' => $this->input->post('notes'),
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );
        $this->db->where('id', $id);
        $this->db->update('purchase_master', $data);
    }

    public function update_voucher_id($id, $voucher_id)
    {
        $data = array(
            'ac_id' => $voucher_id
        );
        $this->db->where('id', $id);
        $this->db->update('purchase_master', $data);
    }
    public function update_draft($id)
    {
        $data = array(

            'draft' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('purchase_master', $data);
    }
    public function update_status($id)
    {
        $data = array(
            'status' => $this->input->post('status')
        );
        $this->db->where('id', $id);
        $this->db->update('purchase_master', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('purchase_master');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('purchase_master');
    }
}
