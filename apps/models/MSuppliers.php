<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MSuppliers extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit(1);
        $q = $this->db->get('suppliers');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_latest()
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('code', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('suppliers');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all()
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('suppliers');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function create($code)
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'code' => $code,
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'contact_person' => $this->input->post('contact_person'),
            'gst' => $this->input->post('gst'),
            'ntn' => $this->input->post('ntn'),
            'email' => $this->input->post('email'),
            'phone_no' => $this->input->post('phone_no'),
            'status' => $this->input->post('status'),
            'created_at' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );
        $this->db->insert('suppliers', $data);

        return $this->db->insert_id();
    }

    public function update($code)
    {

        $data = array(
            'code' => $code,
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'contact_person' => $this->input->post('contact_person'),
            'phone_no' => $this->input->post('phone_no'),
            'status' => $this->input->post('status'),
            'modified_at' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );


        $this->db->where('id', $this->input->post('id'));
        $this->db->update('suppliers', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('suppliers');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('suppliers');
    }
}
