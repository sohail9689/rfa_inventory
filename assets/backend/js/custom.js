$(document).ready(function () {
	// My POS.cc Custom js
	//finish sales add item
	$(document).on('click', '#sales_add_item', function (event) {
		var sales_no = $('#sales_no').val();
		var sales_date = $('#sales_date').val();
		var customer_id = $('#customer_id').val();
		var product_id = $('#product_id').val();
		var quantity = parseInt($('#quantity').val());
		var price = $('#price').val();
		var des = $('#des').val();
		var avail_quantity = parseInt($('#avail_quantity').val());
		if (!sales_no) {
			alert('Please give a sales no.');
			return false;
		}
		if (!product_id) {
			alert('Please select an Item.');
			return false;
		}
		if (!customer_id) {
			alert('Please select a Customer.');
			return false;
		}

		if (!quantity || quantity == 0) {
			alert('Quantity must be greated than zero.');
			return false;
		}

		if (!price || price == 0) {
			alert('Price must be greated than zero.');
			return false;
		}


		if (avail_quantity >= quantity) {
			$.ajax({
				type: "POST",
				url: "inventory/sales_save",
				data: { sales_no: sales_no, sales_date: sales_date, customer_id: customer_id, item_id: product_id, quantity: quantity, price: price, des: des },
				success: function (msg) {
					$("#sales_details").children().remove();
					$("#sales_details").html(msg);
					$("#sample_1").dataTable({
						"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
						"sPaginationType": "bootstrap",
						"oLanguage": {
							"sLengthMenu": "_MENU_ records per page",
							"oPaginate": {
								"sPrevious": "Prev",
								"sNext": "Next"
							}
						},
						"aoColumnDefs": [{
							'bSortable': false,
							'aTargets': [0]
						}]
					});
					// }
					$('#sales_no').attr('disabled', true);
					$('#sales_date').attr('disabled', true);
					$('#quantity').val('');
					$('#price').val('');
					$('#avail_quantity').val(0);
					$('#customer_id').attr('disabled', true);
					$('#product_id').val(0).trigger("change");
					$('#quantity').focus();

				}

			});
		} else {
			alert('Insufficiant Stock! Available Quantity is ' + avail_quantity);
		}


	});
	$("#parent_id").change(function () {
		var result = $(this).find("option:selected").val();
		$.ajax({
			url: "accounts/get_ac_code",
			type: "POST",
			data: { result: result },
			success: function (data) {
				var res = $.parseJSON(data);
				$('#ac_code').val(res);
			}

		})
	});

	//finish sales item delete
	$(document).on('click', '.sales_item_delete', function (event) {
		if (!confirm("Are you sure you want to delete this item?")) {
			return false;
		}
		var master_id = $('#master_id').val();
		var detail_id = $(this).prev().val();
		$.ajax({
			type: "POST",
			url: "inventory/sales_item_delete",
			data: { master_id: master_id, detail_id: detail_id },
			success: function (msg) {
				$("#sales_details").children().remove();
				$("#sales_details").html(msg);
				$("#datatables").dataTable();
			}
		});
	});
	//finish sales complete
	$(document).on('click', '#sales_complete', function (event) {
		var totl_price = $('#totl_price').val();
		if (!totl_price || totl_price == 0) {
			alert('First Add Item.');
			return false;
		}
		var customer_id = $('#customer_id').val();
		var sales_date = $('#sales_date').val();
		var des = $('#des').val();
		var id = $('#master_id').val();
		$.ajax({
			type: "POST",
			url: "inventory/sales_complete",
			data: { id: id, sales_date: sales_date, customer_id: customer_id, des: des },
			success: function (msg) {
				window.location.replace(msg);
			}
		});
	});

	//finish sales return add item
	$(document).on('click', '#sales_return_add_item', function (event) {
		var sales_return_no = $('#sales_return_no').val();
		var sales_return_date = $('#sales_return_date').val();
		var customer_id = $('#customer_id').val();
		var item_id = $('#item_id').val();
		var quantity = $('#quantity').val();
		var sq_weight = $('#sq_weight').val();
		var price = $('#price').val();
		var notes = $('#notes').val();
		if (!sales_return_no) {
			alert('Please give a sales return no.');
			return false;
		}
		if (!item_id) {
			alert('Please select an Item.');
			return false;
		}
		if (!customer_id) {
			alert('Please select a Customer.');
			return false;
		}
		if (!quantity) {
			alert('Quantity must be greated than zero.');
			return false;
		}
		if (!sq_weight) {
			alert('Total Area must be greated than zero.');
			return false;
		}

		$.ajax({
			type: "POST",
			url: "inventory/sales_return_save",
			data: { sales_return_no: sales_return_no, sq_weight: sq_weight, sales_return_date: sales_return_date, customer_id: customer_id, item_id: item_id, quantity: quantity, price: price, notes: notes },
			success: function (msg) {
				//alert(msg);
				$("#sales_return_details").children().remove();
				$("#sales_return_details").html(msg);
				$("#sample_1").dataTable();

			}
		});
		$('#sales_return_no').attr('disabled', true);
		$('#sales_return_date').attr('disabled', true);
		$('#customer_id').attr('disabled', true);
		$('#quantity').val('');
		$('#sq_weight').val('');
		$('#price').val('');
		$('#item_id').focus();
	});
	//finish sales return item delete
	$(document).on('click', '.sales_return_item_delete', function (event) {
		if (!confirm("Are you sure you want to delete this item?")) {
			return false;
		}
		var sales_return_no = $('#sales_return_no').val();
		var item_id = $(this).prev().val();
		$.ajax({
			type: "POST",
			url: "inventory/sales_return_item_delete",
			data: { sales_return_no: sales_return_no, item_id: item_id },
			success: function (msg) {
				$("#sales_return_details").children().remove();
				$("#sales_return_details").html(msg);
				$("#datatables").dataTable();
			}
		});
	});
	//finish sales return complete
	$(document).on('click', '#sales_return_complete', function (event) {
		var totl_price = $('#totl_price').val();
		if (!totl_price || totl_price == 0) {
			alert('First Add Item.');
			return false;
		}
		var sales_return_no = $('#sales_return_no').val();
		var paid_amount = $('#paid_amount').val();
		var customer_id = $('#customer_id').val();
		var sales_return_date = $('#sales_return_date').val();
		var notes = $('#notes').val();
		$.ajax({
			type: "POST",
			url: "inventory/sales_return_complete",
			data: { sales_return_no: sales_return_no, paid_amount: paid_amount, sales_return_date: sales_return_date, customer_id: customer_id, notes: notes },
			success: function (msg) {
				window.location.replace(msg);
			}
		});
	});

	//finish purchase add item
	$(document).on('click', '#purchase_add_item', function (event) {
		var purchase_no = $('#purchase_no').val();
		var purchase_date = $('#purchase_date').val();
		var item_id = $('#item_id').val();
		var supplier_id = $('#supplier_id').val();
		var price = $('#price').val();
		var quantity = $('#quantity').val();
		var notes = $('#notes').val();
		var check = true;
		var expense_name_array = [];
		var expense_price_array = [];
		if (!purchase_no) {
			alert('Please give a purchase no.');
			return false;
		}
		if (!item_id || item_id == 0) {
			alert('Please select an Item.');
			return false;
		}

		if (!quantity || quantity == 0) {
			alert('Quantity must be greated than zero.');
			return false;
		}

		if (!price || price == 0) {
			alert('Price must be greated than zero.');
			return false;
		}

		if (!supplier_id || supplier_id == 0) {
			alert('Please select a supplier.');
			return false;
		}
		$("select[name='expense_name[]']").each(function () {
			var expenses_name = $(this).val();

			if (expenses_name == 0) {
				check = false;
				alert('Expense Name must be selected.');
				return false;
			}
			expense_name_array.push(expenses_name);
		});

		if (!check) {
			return false;
		}
		$("input[name='expense_amount[]']").each(function () {
			var expense_amount = $(this).val();
			if (!expense_amount || expense_amount == 0) {
				check = false;
				alert('Expense Price must be greater than 0.');
				return false;
			}
			expense_price_array.push(expense_amount);
		});

		if (!check) {
			return false;
		}
		$.ajax({
			type: "POST",
			url: "inventory/purchase_save",
			data: { purchase_no: purchase_no, purchase_date: purchase_date, supplier_id: supplier_id, item_id: item_id, quantity: quantity, price: price, notes: notes, expense_name: expense_name_array, expense_price: expense_price_array },
			success: function (msg) {
				$("#purchase_details").children().remove();
				$("#purchase_details").html(msg);
				$("#datatables").dataTable();
			}
		});
		$('#purchase_no').attr('disabled', true);
		$('#purchase_date').attr('disabled', true);
		$('#quantity').val('');
		$('#price').val('');
		$('#item_id').val(0).trigger("change");
		$('#quantity').focus();
		$('#status').attr('disabled', true);
		$('#supplier_id').attr('disabled', true);
		$('.expense_name').attr('disabled', true);
		$('.expense_amount').attr('disabled', true);
		$('.remove_exp').remove();
		$('#add').remove();
	});

	//finish purchase item delete
	$(document).on('click', '.purchase_item_delete', function (event) {
		if (!confirm("Are you sure you want to delete this item?")) {
			return false;
		}
		var master_id = $('#master_id').val();
		var detail_id = $(this).prev().val();
		$.ajax({
			type: "POST",
			url: "inventory/purchase_item_delete",
			data: { master_id: master_id, detail_id: detail_id },
			success: function (msg) {
				$("#purchase_details").children().remove();
				$("#purchase_details").html(msg);
				$("#datatables").dataTable();
			}
		});
	});
	//finish purchase complete
	$(document).on('click', '#purchase_complete', function (event) {
		var totl_price = $('#totl_price').val();
		if (!totl_price || totl_price == 0) {
			alert('First Add Item.');
			return false;
		}
		var id = $('#master_id').val();
		var supplier_id = $('#supplier_id').val();
		var purchase_date = $('#purchase_date').val();
		var notes = $('#notes').val();
		$.ajax({
			type: "POST",
			url: "inventory/purchase_complete",
			data: { id: id, purchase_date: purchase_date, supplier_id: supplier_id, notes: notes },
			success: function (msg) {
				window.location.replace(msg);
			}
		});
	});
	//finish purchase return add item
	$(document).on('click', '#purchase_return_add_item', function (event) {
		var purchase_return_no = $('#purchase_return_no').val();
		var purchase_return_type = $('#purchase_return_type').val();
		var item_id = $('#item_id').val();
		var quantity = parseInt($('#quantity').val());
		var avail_quantity = parseInt($('#avail_quantity').val());
		var sq_weight = $('#sq_weight').val();
		var avail_sq_weight = $('#avail_sq_weight').val();
		var price = $('#price').val();
		var purchase_return_date = $('#purchase_return_date').val();
		var supplier_id = $('#supplier_id').val();
		var notes = $('#return_notes').val();
		if (!purchase_return_no) {
			alert('Please give a purchase return no.');
			return false;
		}
		if (!purchase_return_type) {
			alert('Please select Type.');
			return false;
		}
		if (!item_id) {
			alert('Please select an Item.');
			return false;
		}
		if (!quantity) {
			alert('Quantity must be greated than zero.');
			return false;
		}
		if (quantity > avail_quantity) {
			alert('Quantity cannot be greater than Available Quantity.');
			return false;
		}
		if (!sq_weight) {
			alert('Total Area must be greated than zero.');
			return false;
		}
		if (!price) {
			alert('Price must be greated than zero.');
			return false;
		}
		if (!supplier_id) {
			alert('Please select a supplier.');
			return false;
		}

		$.ajax({
			type: "POST",
			url: "inventory/purchase_return_save",
			data: { purchase_return_no: purchase_return_no, purchase_return_type: purchase_return_type, purchase_return_date: purchase_return_date, sq_weight: sq_weight, supplier_id: supplier_id, item_id: item_id, quantity: quantity, price: price, notes: notes },
			success: function (msg) {
				$("#purchase_return_details").children().remove();
				$("#purchase_return_details").html(msg);
				$("#datatables").dataTable();
			}
		});
		$('#purchase_return_no').attr('disabled', true);
		$('#purchase_return_date').attr('disabled', true);
		$('#supplier_id').attr('disabled', true);
		$('#quantity').val('');
		$('#price').val('');
		$('#quantity').focus();
		$('#sq_weight').val('');
		$('#avail_quantity').val('');
		$('#avail_sq_weight').val('');
		$('#purchase_return_type').val("0").trigger("change");
		$('#item_id').val("0").trigger("change");
	});
	//finish purchase return item delete
	$(document).on('click', '.purchase_return_item_delete', function (event) {
		if (!confirm("Are you sure you want to delete this item?")) {
			return false;
		}
		var purchase_return_no = $('#purchase_return_no').val();
		var item_id = $(this).prev().val();
		$.ajax({
			type: "POST",
			url: "inventory/purchase_return_item_delete",
			data: { purchase_return_no: purchase_return_no, item_id: item_id },
			success: function (msg) {
				$("#purchase_return_details").children().remove();
				$("#purchase_return_details").html(msg);
				$("#datatables").dataTable();
			}
		});
	});
	//finish purchase return complete
	$(document).on('click', '#purchase_return_complete', function (event) {
		var totl_price = $('#totl_price').val();
		if (!totl_price || totl_price == 0) {
			alert('First Add Item.');
			return false;
		}
		var purchase_return_no = $('#purchase_return_no').val();
		var paid_amount = $('#paid_amount').val();
		var supplier_id = $('#supplier_id').val();
		var purchase_return_date = $('#purchase_return_date').val();
		var notes = $('#return_notes').val();
		console.log(notes);
		$.ajax({
			type: "POST",
			url: "inventory/purchase_return_complete",
			data: { purchase_return_no: purchase_return_no, paid_amount: paid_amount, purchase_return_date: purchase_return_date, supplier_id: supplier_id, notes: notes },
			success: function (msg) {
				window.location.replace(msg);
			}
		});
	});
	//journal debit voucher add
	$("#voucher_type").change(function () {
		var selectedValue = $(this).find("option:selected").val();
		var result = selectedValue;
		$.ajax({
			url: "accounts/get_journal_value",
			type: "POST",
			data: { result: result },
			success: function (data) {
				var journal = $.parseJSON(data);
				$('#journal_no').val(journal.journal_no);
			}
		})

	});
	$(document).on('click', '.debit_voucher_update', function (event) {
		var memo = $(this).parent().prev().find("input[type=text]:first").val();
		var master_id = $('#hidden_master_id').val();
		var voucher_id = $(this).prev().val();
		$.ajax({
			type: "POST",
			url: "accounts/update_voucher/debit",
			data: { master_id: master_id, voucher_id: voucher_id, memo: memo },
			success: function (msg) {
				$("#debit_details").html(msg);
				$("p").text('Debit Data Updated Successfully . . .');
			}
		});
	});
	$(document).on('click', '.credit_voucher_update', function (event) {
		var memo = $(this).parent().prev().find("input[type=text]:first").val();
		var master_id = $('#hidden_master_id').val();
		var voucher_id = $(this).prev().val();
		// console.log(memo);
		$.ajax({
			type: "POST",
			url: "accounts/update_voucher/credit",
			data: { master_id: master_id, voucher_id: voucher_id, memo: memo },
			success: function (msg) {
				$("p").text('Credit Data Updated Successfully . . .');
			}
		});
	});
	$(document).on('click', '#debit_add', function (event) {
		var journal_no = $('#journal_no').val();
		var journal_date = $('#journal_date').val();
		var voucher_type = $('#voucher_type').val();
		var debit_chart_id = $('#debit_chart_id').val();
		var debit_amount = $('#debit_amount').val();
		var debit_memo = $('#debit_memo').val();

		if (!journal_no) {
			alert('Please select a Voucher Type');
			return false;
		}

		if (!debit_amount) {
			alert('Please Enter Debit Amount');
			return false;
		}
		// alert(debit_memo);
		// var journal_credit = $('#journal_credit').val();
		$.ajax({
			type: "POST",
			url: "accounts/debit_add",
			data: { journal_no: journal_no, journal_date: journal_date, voucher_type: voucher_type, debit_chart_id: debit_chart_id, debit_amount: debit_amount, debit_memo: debit_memo },
			success: function (msg) {
				$("#debit_details").children().remove();
				$("#debit_details").html(msg);
				var str = $('#debit_total').text();
				var total_debit = str.replace(",", "");
				$('#credit_amount').val(total_debit);
			}
		});
		$('#journal_no').attr('disabled', true);
		$('#journal_date').attr('disabled', true);
		$('#debit_amount').val('');
		$('#debit_memo').val('');
		$('#debit_amount').focus();
	});
	//journal credit voucher add
	$(document).on('click', '#credit_add', function (event) {
		var journal_no = $('#journal_no').val();
		var journal_date = $('#journal_date').val();
		var journal_credit = $('#journal_credit').val();
		var voucher_type = $('#voucher_type').val();
		var credit_chart_id = $('#credit_chart_id').val();
		var credit_amount = $('#credit_amount').val();
		var credit_memo = $('#credit_memo').val();
		if (!journal_no) {
			alert('Please select a Voucher Type');
			return false;
		}

		if (!credit_amount) {
			alert('Please Enter Credit Amount');
			return false;
		}
		$.ajax({
			type: "POST",
			url: "accounts/credit_add",
			data: { journal_no: journal_no, journal_date: journal_date, voucher_type: voucher_type, journal_credit: journal_credit, credit_chart_id: credit_chart_id, credit_amount: credit_amount, credit_memo: credit_memo },
			success: function (msg) {
				$("#credit_details").children().remove();
				$("#credit_details").html(msg);
				var debit = $('#debit_total').text();
				var total_debit = debit.replace(",", "");
				var credit = $('#credit_total').text();
				var total_credit = credit.replace(",", "");
				var rest_credit = total_debit - total_credit;
				$('#credit_amount').val(rest_credit);
			}
		});
		$('#journal_no').attr('disabled', true);
		$('#journal_date').attr('disabled', true);
		$('#credit_amount').val('');
		$('#credit_memo').val('');
		$('#credit_amount').focus();
	});
	//debit voucher delete
	$(document).on('click', '.debit_voucher_delete', function (event) {
		if (confirm("Are you sure you want to delete this item?")) {
			var master_id = $('#hidden_master_id').val();
			var voucher_id = $(this).prev().val();
			$.ajax({
				type: "POST",
				url: "accounts/delete_voucher/debit",
				data: { master_id: master_id, voucher_id: voucher_id },
				success: function (msg) {
					$("#debit_details").children().remove();
					$("#debit_details").html(msg);
				}
			});
		}
	});
	//credit voucher delete
	$(document).on('click', '.credit_voucher_delete', function (event) {
		if (confirm("Are you sure you want to delete this item?")) {
			var master_id = $('#hidden_master_id').val();
			var voucher_id = $(this).prev().val();
			$.ajax({
				type: "POST",
				url: "accounts/delete_voucher/credit",
				data: { master_id: master_id, voucher_id: voucher_id },
				success: function (msg) {
					$("#credit_details").children().remove();
					$("#credit_details").html(msg);
				}
			});
		}
	});
	//journal complete
	$(document).on('click', '#journal_complete', function (event) {
		var journal_no = $('#journal_no').val();
		var journal_date = $('#journal_date').val();
		var voucher_type = $('#voucher_type').val();
		var debit_total = $('#debit_total').text();
		var debit = Number(debit_total.replace(/[^0-9\.]+/g, ""));
		var credit_total = $('#credit_total').text();
		var credit = Number(credit_total.replace(/[^0-9\.]+/g, ""));
		var balance;
		if (debit == 0) {
			alert('Debit voucher need some amount.');
			return false;
		}
		if (credit == 0) {
			alert('Credit voucher need some amount.');
			return false;
		}
		if (debit !== credit) {
			if (debit > credit) {
				balance = debit - credit;
				alert('Credit voucher still need ' + balance + ' amount.');
				$("#credit_amount").focus();
			} else {
				balance = credit - debit;
				alert('Debit voucher still need ' + balance + ' amount.');
				$("#debit_amount").focus();
			}
			return false;
		}
		var master_id = $('#hidden_master_id').val();
		$.ajax({
			type: "POST",
			url: "accounts/journal_complete",
			data: { master_id: master_id, journal_no: journal_no, journal_date: journal_date, voucher_type: voucher_type },
			success: function (msg) {
				window.location.replace(msg);
			}
		});
	});
	// Add Supplier from Purchase
	$(document).on('click', '#add_supplier', function (event) {
		var code = $('#code').val();
		var name = $('#name').val();
		var address = $('#address').val();
		var contact_person = $('#contact_person').val();
		var phone_no = $('#phone_no').val();
		var notes = $('#notes').val();
		var status = 'Active';

		$.ajax({
			type: "POST",
			url: "inventory/add_new_supplier",
			data: { code: code, name: name, address: address, contact_person: contact_person, phone_no: phone_no, notes: notes, status: status },
			success: function (msg) {
				$("#supplier_id").select2("destroy");
				$("#supplier_id option").remove();
				$("#supplier_id").append(msg);
				$("#supplier_id").select2();
			}
		});
		$('#my_popup').popup('hide');
	});

	// Add Customer from Purchase
	$(document).on('click', '#add_customer', function (event) {
		var code = $('#code').val();
		var name = $('#name').val();
		var address = $('#address').val();
		var mobile = $('#mobile').val();
		var country = $('#country').val();
		var email = $('#email').val();
		var notes = $('#notes').val();
		var status = $('#status').val();

		$.ajax({
			type: "POST",
			url: "inventory/add_new_customer",
			data: { code: code, name: name, address: address, mobile: mobile, country: country, email: email, notes: notes, status: status },
			success: function (msg) {
				$("#customer_id").select2("destroy");
				$("#customer_id option").remove();
				$("#customer_id").append(msg);
				$("#customer_id").select2();
			}
		});
		$('#my_popup').popup('hide');
	});

	$(document).on('submit', '#form-modal', function (event) {
		event.preventDefault();

		var code = $('#code').val(); sil
		var name = $('#name').val();
		var company = $('#company').val();
		var address = $('#address').val();
		var phone = $('#phone').val();
		var email = $('#email').val();
		var web = $('#web').val();

		$.ajax({
			type: "POST",
			url: "invoice/add_customer",
			data: { code: code, name: name, company: company, address: address, phone: phone, email: email, web: web },
			success: function (msg) {
				$("#customer_id").html(msg);
			}
		});
		$('#myModal').modal('hide');

		// select2
		$.ajax({
			type: "POST",
			url: "invoice/latest_customer",
			success: function (msg) {
				alert("Customer Added.");
				$('#customer_id').select2("val", msg);
			}
		});

	});

	// datatables
	$('#datatables').dataTable({
		"iDisplayLength": 50,
		"bStateSave": true,
		"iCookieDuration": 60 * 60 * 24 * 30, // 30 days
		"bSort": false,
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		}
	});

	// datepicker
	$('[data-form=datepicker]').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });

	//delete confirmation
	$(".del").click(function () {
		if (!confirm("Are you sure you want to delete this item?")) {
			return false;
		}
	});

});