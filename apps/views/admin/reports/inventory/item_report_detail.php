<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Cybextech">
    <link rel="shortcut icon" href="assets/backend/img/cybex.png" type="image/x-icon" />
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />
    <link href="assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"/>
<!-- Datatables -->
    <script src='assets/vendors/echarts/test/lib/jquery.min.js'></script>
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="assets/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="assets/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
        @media print{
            p.muted{
                font-weight: bold;
            }
            small.small{
                font-weight: normal;
            }
        }
    </style>
</head>
<script>
      $(document).ready(function() {
  var table = $('#datatable-responsive').DataTable({
        "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
    });
 
 var buttons = new $.fn.dataTable.Buttons(table, {
     buttons: [
       'copy',
       'csv',
       'excel',
       'pdf',
     'print'
    ]

 }).container().appendTo($('#buttons'));
 });
 </script>
<body>
    <!-- section content -->
    <section class="section">
        <div class="container">
            <!-- span content -->
            <div class="span10 offset1">
                <!-- content -->
                <div class="content" style="border: 1px solid #d7d7d7;">
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <div class="pull-right">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="115" class="img">
                                </div>
                                <h3 class="left"><?php echo $this->session->userdata('company_name'); ?></h3>
                                <button id="buttons"></button>
                            </div>
                            <div class="row-fluid">
                                <div class="span12 center">
                                    <strong>Sales Report</strong>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span8">
                                    <p class="muted">Date From</p>
                                    <p><?php echo date('jS M, Y ', strtotime(date_to_db($start_date))); ?></p>
                                </div>
                                <div class="span4">
                                    <p class="muted">Date To</p>
                                    <p><?php echo date('jS M, Y ', strtotime(date_to_db($end_date))); ?></p>
                                </div>
                            </div>
                            <div class="invoice-table">
                                <table class="table table-bordered invoice responsive" id="datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th>SL No</th>
                                            <th>Sales No</th>
                                            <th>Sales Date</th>
                                            <th>Item Name</th>
                                            <th>Customer Name</th>
                                            <th>Sales Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        $quantity = 0;
                                        $price = 0;
                                        foreach ($sales as $key => $value) {

                                            var_dump($value);
                                            die;
                                            if ($value['item_quantity']) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?php echo $i; ?></td>
                                                    <td align="left"><?php echo $value['sales_no']; ?></td>
                                                    <td align="center"><?php echo $value['sales_date']; ?></td>
                                                    <td align="right"><?php echo number_format($value['item_id']); ?></td>
                                                    <td align="right"><?php echo number_format($value['customer_id'], 2); ?></td>
                                                    <td align="right"><?php echo ($value['notes'], 2); ?></td>
                                                </tr>
                                                <?php
                                                $quantity += $value['item_quantity'];
                                                $price += $value['total_price'];
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td align="center" colspan="3"><b>Grand Total</b></td>
                                            <td align="right"><?php echo number_format($quantity); ?></td>
                                            <td align="right"><?php echo number_format($price, 2); ?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="pull-left">
                        <b>Prepared By____________________</b>
                        </div>
                        <div class="center" style="margin-right:220px";>
                        <b>checked  By____________________</b>
                        </div>
                        <div class="pull-right" style="margin-top:-20px";>
                        <b>Approved By____________________</b>
                        </div>
                        <!--/invoice-->
                    </div><!--/content-body -->
                </div><!-- /content -->
            </div><!-- /span content -->

        </div><!-- /container -->
    </section>
</body>
</html>