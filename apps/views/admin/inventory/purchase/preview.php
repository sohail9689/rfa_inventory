<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<style type="text/css">
    @media print {
        a[href]:after {
            content: none !important;
        }

        #navbar,
        #footer,
        .noprint {
            /*display: none;*/
        }
    }
</style>

<head>
    <?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body>

    <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">

        <!-- BEGIN PAGE -->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN BLANK PAGE PORTLET-->
                <div class="widget grey">
                    <div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <h2 class="center" style="color: #000;">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                            </div>
                        </div>
                        <div class="space20"></div>
                        <div class="row-fluid invoice-list">
                            <div class="span4">
                                <h4>BILLING ADDRESS</h4>
                                <p>
                                    <?php echo $master[0]['supplier_name']; ?><br>
                                    <?php echo $master[0]['supplier_address']; ?><br>
                                    Mobile : <?php echo $master[0]['supplier_mobile']; ?><br>
                                    Notes : <?php echo $master[0]['notes']; ?><br>
                                </p>
                            </div>
                            <div class="span4">
                                <h4>INVOICE INFO</h4>
                                <ul class="unstyled">
                                    <li>Purchase Invoice Number : <strong><?php echo $master[0]['purchase_no']; ?></strong></li>
                                    <li>Purchase Invoice Date : <?php echo date('jS M, Y ', strtotime($master[0]['purchase_date'])); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="space20"></div>
                        <div class="space20"></div>
                        <div class="row-fluid">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>ITEM DESCRIPTIONS</th>
                                        <th class="right">QTY (pcs)</th>
                                        <th class="right">PRICE</th>
                                        <th class="right">TOTAL PRICE</th>
                                        <th class="right">Expense</th>
                                        <th class="right">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    // $qty = 0;
                                    $expense = 0;
                                    $tot_price = 0;
                                    $grand_total = 0;
                                    $i = 1;
                                    foreach ($details as $detail) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $detail['item_name']; ?></td>
                                            <td class="right"><?php echo $detail['quantity']; ?></td>
                                            <td class="right"><?php echo number_format($detail['purchase_price'], 2); ?></td>
                                            <td class="right"><?php echo number_format($detail['quantity'] * $detail['purchase_price'], 2); ?></td>
                                            <td class="right"><?php echo number_format($detail['total_expense'], 2); ?></td>
                                            <td class="right"><?php echo number_format($detail['total_price'], 2); ?></td>
                                        </tr>
                                    <?php
                                        $tot_price += $detail['quantity'] * $detail['purchase_price'];
                                        $expense += $detail['total_expense'];
                                        $grand_total += $detail['total_price'];
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="space20"></div>
                        <div class="row-fluid">
                            <div class="span4 invoice-block pull-right">
                                <ul class="unstyled amounts">
                                    <li><strong>Sub - Total amount :</strong> <?php echo number_format($tot_price, 2); ?> <?php echo $this->session->userdata('currency_symbol'); ?></li>
                                    <li><strong>Total Expense :</strong> <?php echo number_format($expense, 2); ?> <?php echo $this->session->userdata('currency_symbol'); ?></li>
                                    <li><strong>Grand Total :</strong> <?php echo number_format($grand_total, 2); ?> <?php echo $this->session->userdata('currency_symbol'); ?></li>
                                </ul>
                            </div>
                            <?php
                            if (sizeof($expenses) > 0) {

                            ?>
                                <div class="span4">
                                <h4>Expense Details</h4>
                                    <ul class="">
                                        <?php foreach ($expenses as $expense) { ?>
                                            <li><strong><?php echo $expense['expense_name']; ?> :</strong> <?php echo $expense['expense_price']; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                        <br>
                        <br>
                        <div class="row-fluid">
                            <div class="span12 center">
                                <div class="span9"></div>
                                <div class="span3">
                                    <b style="color: black">Received By____________________</b>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row-fluid">
                            <div class="span12 center">
                                <div class="span3">
                                    <b style="color: black">Prepared By____________________</b>
                                </div>
                                <div class="span3">
                                    <b style="color: black">Checked By____________________</b>
                                </div>
                                <div class="span3">
                                    <b style="color: black">Manager Account____________________</b>
                                </div>
                                <div class="span3">
                                    <b style="color: black">Director____________________</b>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid text-center">
                            <a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
                            <br>
                            <br>
                            <div style="text-align: center;">
                                <?php echo date('Y'); ?> &copy; Powered by <a href="http://cybextech.com/" target="_blank">CybexTech
                                    <!-- <br><i class="icon-mobile-phone"> +92-333-6308419</i> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END BLANK PAGE PORTLET-->
            </div>
        </div>
        <!-- END PAGE -->
    </div>
</body>
<!-- END BODY -->

</html>