<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">
    <title>Ledger From: <?php echo $start_date; ?> To: <?php echo $end_date; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Tapan Kumer Das : InnovativeBD">
    <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo'); ?>" type="image/x-icon" />

    <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />
    <style>
        @media print {
            p.muted {
                font-weight: bold;
            }

            small.small {
                font-weight: normal;
            }
        }
    </style>
</head>

<body>
    <!-- section content -->
    <section class="section">
        <div class="container">
            <!-- span content -->
            <div class="span12">
                <!-- content -->
                <div class="content" style="border: 1px solid #d7d7d7;">
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <h2 class="center" style="color: #000;">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                                <div class="row-fluid center">
                                    <strong>Aging Report</strong>
                                    <p><?php echo date('jS F Y ', strtotime(date_to_db($start_date))); ?> To <?php echo date('jS F Y ', strtotime(date_to_db($end_date))); ?></p>
                                </div>
                            </div>

                            <?php //print_r($opening_sum); print_r($previous); 
                            ?>
                            <div class="invoice-table">
                                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                                    <thead>
                                        <tr>
                                            <th class="span2">Journal Date</th>
                                            <th class="center span2">Journal No.</th>
                                            <th class="center span2">Name of A/C</th>
                                            <th class="center">0-30 Days</th>
                                            <th class="center">31-60 Days</th>
                                            <th class="center">61-90 Days</th>
                                            <th class="center">90+ Days</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $debit = 0;
                                    $credit = 0;
                                    foreach ($previous as $pre) {
                                        $debit += $pre['debit'];
                                        $credit += $pre['credit'];
                                    }
                                    if ($opening_sum['opening'] > 0) {
                                        $debit += $opening_sum['opening'];
                                    } else {
                                        $credit += $opening_sum['opening'];
                                    }
                                    $opening = $debit - $credit;
                                    ?>
                                    <tbody>
                                        <?php foreach ($charts as $chart) {

                                            $date1 = date_create($chart['journal_date']);
                                            $date2 = date_create(date('Y-m-d'));
                                            $diff = date_diff($date1, $date2);
                                            $data =  $diff->format("%a days");
                                            $amount = $chart['debit'] - $chart['credit'];
                                            // $diff->format("%R%a days");
                                        ?>
                                            <tr>
                                                <td><?php echo date('jS M, Y ', strtotime($chart['journal_date'])); ?></td>
                                                <td class="center"><b><a href="accounts/journal_preview/<?php echo $chart['journal_id']; ?>" target="_blank"><?php echo $chart['journal_no']; ?></a></b></td>
                                                <td><?php echo $chart['chart_name']; ?></td>
                                                <td class="left"><?php if ($data <= 30 && $data >= 0) {
                                                                        echo floor($amount) . ' - (' . $data . ')';
                                                                    } ?></td>
                                                <td class="left"><?php if ($data <= 60 && $data > 31) {
                                                                        echo floor($amount) . ' - (' . $data . ')';
                                                                    } ?></td>
                                                <td class="left">
                                                    <?php if ($data <= 90 && $data > 61) {
                                                        echo floor($amount) . ' - (' . $data . ')';
                                                    } ?>
                                                </td>
                                                <td class="left">
                                                    <?php if ($data > 90) {
                                                        echo floor($amount) . ' - (' . $data . ')';
                                                    } ?>
                                                </td>
                                            </tr>
                                            <?php
                                            if ($data <= 30 && $data >= 0) {
                                                $first += $amount;
                                            } elseif ($data <= 60 && $data > 31) {
                                                $second += $amount;
                                            } elseif ($data <= 90 && $data > 61) {
                                                $third += $amount;
                                            } elseif ($data > 90) {
                                                $fourth += $amount;
                                            }
                                            ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr style="font-weight: bold;">
                                            <td colspan="3"> Total</td>
                                            <td class="right"><?php echo floor($first); ?></td>
                                            <td class="right"><?php echo floor($second); ?></td>
                                            <td class="right"><?php echo floor($third); ?></td>
                                            <td class="right"><?php echo floor($fourth); ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td colspan="6"> Grand Total</td>
                                            <td class="right"><?php echo floor($first + $second + $third + $fourth); ?></td>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!--/invoice-->
                    </div>
                    <!--/content-body -->
                </div><!-- /content -->
            </div><!-- /span content -->

        </div><!-- /container -->
    </section>

</body>

</html>