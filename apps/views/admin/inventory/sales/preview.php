<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<style type="text/css">
            @media print {
                a[href]:after {
                content: none !important;
              }
                #navbar, #footer, .noprint
                {
                /*display: none;*/
                }
            }
        </style>
<head>
    <?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>
    <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
        <!-- BEGIN PAGE -->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN BLANK PAGE PORTLET-->
                <div class="widget grey">
                    <div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <h2 class="center" style="color: #000;">
                                <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                            </div>
                        </div>
                        <div class="space20"></div>
                        <div class="row-fluid invoice-list">
                        <div class="span4">
                                <h4>BILLING ADDRESS</h4>
                                <p>
                                    <?php echo $master['name']; ?><br>
                                    <?php echo $master['address']; ?><br>
                                    Mobile : <?php echo $master['mobile']; ?><br>
                                </p>
                            </div>
                            <div class="span4">
                                <h4>INVOICE INFO</h4>
                                <ul class="unstyled">

                                    <li style="color: black">Sale Invoice Number : <strong><?php echo $master['sales_no']; ?></strong></li>
                                    <li style="color: black">Sale Invoice Date : <?php echo date('jS M, Y ', strtotime($master['sales_date'])); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="space20"></div>
                        <div class="space20"></div>
                        <div class="row-fluid">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th style="border:1px solid #000; color: black">SL#</th>
                                        <th style="border:1px solid #000; color: black">Items</th>
                                        <th style="border:1px solid #000; color: black">QTY (pcs)</th>
                                        <th class="left" style="border:1px solid #000; color: black">Price</th>
                                        <th class="right" style="border:1px solid #000; color: black">Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $qty = 0;
                                    $price = 0;
                                    $i = 1;
                                    foreach ($details as $detail) {
                                        ?>
                                        <tr>
                                            <td style="border:1px solid #000; color: black"><?php echo $i; ?></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $detail['item_name']; ?></td>
                                            <td style="border:1px solid #000; color: black"><?php echo $detail['quantity']; ?></td>
                                            <td class="left" style="border:1px solid #000; color: black"><?php echo number_format($detail['sale_price'], 2); ?></td>
                                            <td class="right" style="border:1px solid #000; color: black"><?php echo number_format($detail['quantity']*$detail['sale_price'], 2); ?></td>
                                        </tr>
                                        <?php
                                        $qty += $detail['quantity'];
                                        $price += $detail['quantity']*$detail['sale_price'];

                                        $i++;
                                        // $percent = $detail['tax'];
                                        // $tax = ($price *$percent)/100;
                                        // $grand_total = $price + $tax;


// echo $words;
                                    }
                                     $words = $this->numbertowords->convert_number($price);
                                    ?>
                                </tbody>


                            </table></div>
                        <div class="space20"></div>
                        <div class="row-fluid">
                            <div class="span4 invoice-block pull-right">
                                <ul class="unstyled amounts">
                                    <li><strong>Sub - Total amount :</strong> <?php if ($this->session->userdata('currency_symbol_position') == 'Before') { echo $this->session->userdata('currency_symbol'); } ?> <?php echo number_format($price, 2); ?> <?php if ($this->session->userdata('currency_symbol_position') == 'After') { echo $this->session->userdata('currency_symbol'); } ?></li>
                                    <li><strong>Grand Total :</strong> <?php echo number_format($price, 2); ?> <?php echo $this->session->userdata('currency_symbol'); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-12 container hidden" style="margin-top:30px";>
                        <div class="row">
                        <div class="pull-left">
                        <b>Prepared By____________________</b>
                        </div>
                        <div class="center" style="margin-right:220px";>
                        <b>checked  By____________________</b>
                        </div>
                        <div class="pull-right" style="margin-top:-20px";>
                        <b>Approved By____________________</b>
                        </div>
                        </div>
                        </div>
                        <div class="space20"></div>
                         <div class="row-fluid text-center">
                            <a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
                            <br>
                            <br>
                            <div style="text-align: center;">
                                <?php echo date('Y'); ?> &copy; Powered by <a href="http://cybextech.com/" target="_blank">CybexTech
                                   <!--  <br><i class="icon-mobile-phone"> +92-333-6308419</i> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END BLANK PAGE PORTLET-->
            </div>
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->

</body>
<!-- END BODY -->

</html>