<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Expenses
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/expense_list">Expense List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php
						if (count($recipe) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Expense
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if ($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
			<!-- BEGIN Alert widget-->
			<div class="row-fluid">
				<div class="span12">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('info')) { ?>
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('error')) { ?>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
			<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/expense_save" method="post" class="form-horizontal form-bordered form-validate" id="frmCustomer">
							<div class="control-group">
								<label for="code" class="control-label">Sr #</label>
								<div class="controls">
									<input type="number" name="code" readonly id="code" value="<?php if (count($recipe) > 0) {
																									echo $recipe['code'];
																								} else {
																									echo $code;
																								} ?>" class="span5" placeholder="Code">
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Expense Name</label>
								<div class="controls">
									<input type="text" name="name" id="name" value="<?php if (count($recipe) > 0) {
																						echo $recipe['name'];
																					} ?>" class="span5" placeholder="Expense Name">
								</div>
							</div>

							<div class="control-group">
								<label for="status" class="control-label">Select Status</label>
								<div class="controls">
									<select name="status" id="status" class="span5 chzn-select" data-placeholder="Select Status">
										<option value="0" <?php if (count($recipe) > 0 && $recipe['status'] == '0') {
																echo 'selected';
															} ?>>Active</option>
										<option value="1" <?php if (count($recipe) > 0 && $recipe['status'] == '1') {
																echo 'selected';
															} ?>>Inactive</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if (count($recipe) > 0) { ?>
								<input type="hidden" name="id" id='id' value="<?php echo $recipe['id']; ?>" />
							<?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes" id="save">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	
	$('#save').click(function() {

		var name = $('#name').val();
		if (!name) {
			alert('Name cannot be empty.');
			return false;
		}
		
	});
</script>