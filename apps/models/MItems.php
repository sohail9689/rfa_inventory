<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MItems extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_id_grade($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items_grade');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_dates()
    {
        $this->db->select('start_date,end_date');
        $this->db->from('accounts_calander');
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $q = $this->db->get()->row_array();
        return $q;
    }

    // public function check_dates()
    // {
    //     $this->db->select('journal_date');
    //     $this->db->from('ac_journal_details');
    //     $this->db->where('journal_no', $journal_no);
    //     $q = $this->db->get()->row_array();
    //     return $q;
    // }

    public function get_latest()
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_code($code)
    {
        $data = array();
        $this->db->where('code', $code);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_name($name)
    {
        $data = array();
        $this->db->where('name', $name);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all($status = NULL, $type = NULL)
    {
        $data = array();
        if ($status)
        {
            $this->db->where('status', $status);
        }
        if ($type)
        {
            $this->db->where('type', $type);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function search_item($status = NULL, $type = NULL)
    {
        $data = array();
        if ($status)
        {
            $this->db->where('status', $status);
        }
        if ($type)
        {
            $this->db->where('type', $type);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all_grade($status = NULL)
    {
        $data = array();
        if ($status)
        {
            $this->db->where('status', $status);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items_grade');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all_dropdown($status = NULL)
    {
        if ($status)
        {
            $this->db->where('status', $status);
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[$row['id']] = $row['name'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function create()
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            're_order' => $this->input->post('re_order'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
            );
        $this->db->insert('items', $data);
    }
    public function create_by_production($type, $production_id)
    {
            $items = $this->get_latest();
            if (count($items) > 0)
            {
                $code = (int)$items['code'] + 1;
            }
            else
            {
                $code = 1001;
            }
            
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'code' => $code,
            'name' => $this->input->post('name'),
            // 'ac_id' => $ac_id,
            'type' => $type,
            // 'description' => $this->input->post('description'),
            // 'min_sale_price' => $this->input->post('min_sale_price'),
            'production_id' => $production_id,
            'status' => 'Active',
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
            );
        $this->db->insert('items', $data);
        return $this->db->insert_id();
    }
    public function create_grade()
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'grade_name' => $this->input->post('grade_name'),
            'status' => $this->input->post('status')
            );
        $this->db->insert('items_grade', $data);
    }
    public function update_grade()
    {
        $data = array(
           
            'grade_name' => $this->input->post('grade_name'),
            'status' => $this->input->post('status')
            );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('items_grade', $data);
    }
    public function delete_grade($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('items_grade');
    }
    public function update()
    {
        $data = array(
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            're_order' => $this->input->post('re_order'),
            'status' => $this->input->post('status'),
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
            );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('items', $data);
    }

    public function update_field($id, $field, $value)
    {
        $data = array(
            $field => $value,
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
            );
        $this->db->where('id', $id);
        $this->db->update('items', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('items');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('items');
    }

}
