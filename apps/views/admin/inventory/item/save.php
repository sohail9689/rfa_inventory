<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Item
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/item_list">Item List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($item) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Item
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if( validation_errors() ) { ?>
		<div class="row-fluid">
			<div class="span12">
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo validation_errors(); ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<!-- END Alert widget-->
<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/item_save" method="post" class="form-horizontal form-bordered form-validate" id="frmItem">
							<div class="control-group">
								<label for="code" class="control-label">Item Code</label>
								<div class="controls">
									<input type="text" name="code" value="<?php if (count($item) > 0) { echo $item['code']; } else { echo $code; } ?>" <?php if (count($item) > 0) { echo 'readonly'; } ?> id="code" class="span5" placeholder="Give your Custom Item/Product Code">
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Item Name</label>
								<div class="controls">
									<input type="text" name="name" value="<?php if (count($item) > 0) { echo $item['name']; } else { echo set_value('name'); } ?>" id="name" class="span5" placeholder="Item Name">
								</div>
							</div>
								
							<div class="control-group">
								<label for="description" class="control-label">Description</label>
								<div class="controls">
									<textarea name="description" id="description" class="span5" placeholder="Description"><?php if (count($item) > 0) { echo $item['description']; } else { echo set_value('description'); } ?></textarea>
								</div>
							</div>
							<div class="control-group">
								<label for="re_order" class="control-label">Re-Order Level</label>
								<div class="controls">
									<input type="text" name="re_order" value="<?php if (count($item) > 0) { echo $item['re_order']; } else { echo set_value('re_order'); } ?>" id="re_order" class="span5" placeholder="Re-Order Level">
								</div>
							</div>
							
							<div class="control-group">
								<label for="textfield" class="control-label">Select Status</label>
								<div class="controls">
									<select name="status" id="status" class="span5 chzn-select status_">
										<option value="Active" <?php if (count($item) > 0 && $item['status'] == 'Active') { echo 'selected'; }?>>Active</option>
										<option value="Inactive" <?php if (count($item) > 0 && $item['status'] == 'Inactive') { echo 'selected'; }?>>Inactive</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if (count($item) > 0) { ?><input type="hidden" name="id" value="<?php echo $item['id']; ?>" /><?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes" id="complete">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$('#code').focus();
	$(document).on('click', '#complete', function(event) {
		var code = $('#code').val();
		var name = $('#name').val();
		var type = $('#type').val();
		if(! code){
			alert('Code must be given.');
			return false;
		}
		if(! name){
			alert('Name must be given.');
			return false;
		}
		
	});
</script>