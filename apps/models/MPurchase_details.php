<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MPurchase_details extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_name'] = $item['name'];
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }


    public function get_by_master_id($id)
    {
        $data = array();
        $this->db->where('master_id', $id);
        $data = $this->db->get('purchase_details')->result_array();
        return $data;
    }

    public function get_by_barcode($barcode)
    {
        $data = array();
        $this->db->where('barcode', $barcode);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_latest()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit(1);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_purchase_no($master_id)
    {
        $data = array();
        $this->db->where('master_id', $master_id);
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_code'] = $item['code'];
                $row['item_name'] = $item['name'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_expense_details($master_id)
    {
        $this->db->select('purchase_meta_expenses.expense_price, purchase_expense.name as expense_name, purchase_expense.code as expense_code');
        $this->db->from('purchase_meta_expenses');
        $this->db->join('purchase_expense', 'purchase_expense.id = purchase_meta_expenses.expense_name');
        $this->db->where('purchase_meta_expenses.master_id', $master_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_quantity($master_id, $items = NULL)
    {
        $this->db->select_sum('quantity');
        if ($items) {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('master_id', $master_id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['quantity'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_price($master_id, $items = NULL)
    {
        $this->db->select_sum('total_price');
        if ($items) {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('master_id', $master_id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['total_price'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_total_sale_tax($purchase_no, $items = NULL, $stat_date, $en_date, $type)
    {
        $this->db->select_sum('vat_amount');
        if ($items) {
            $this->db->where_in('item_id', $items);
        }
        if ($type) {
            $this->db->where_in('item_type', $type);
        }
        $this->db->where('purchase_date >= ', ($stat_date));
        $this->db->where('purchase_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_no', $purchase_no);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['vat_amount'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all()
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $company = $this->MCompanies->get_by_id($row['company_id']);
                $row['company'] = $company['name'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_item_id($item_id)
    {
        $data = array();
        $this->db->where('item_id', $item_id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        return $data;
    }

    public function get_by_purchase_no_item_id($purchase_no, $item_id)
    {
        $data = null;
        $this->db->select('purchase.*, items.name as item_name, items.min_sale_price as item_sale');
        $this->db->from('purchase_details AS purchase');
        $this->db->join('items', 'items.id = purchase.item_id');
        $this->db->where('purchase.purchase_no', $purchase_no);
        $this->db->where('purchase.item_id', $item_id);
        $this->db->where('purchase.company_id', $this->session->userdata('user_company'));
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_item_with_purchase_no()
    {
        $data = array();
        $this->db->distinct();
        $this->db->select('sales_id');
        if ($this->input->post('item_id') != 'all') {
            $this->db->where('item_id', $this->input->post('item_id'));
        }
        $sdate = $this->input->post('s_date');
        $edate = $this->input->post('e_date');
        $this->db->where("edate BETWEEN '$sdate' AND '$edate'");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $this->MSales_master->get_by_id($row['sales_id']);
                //$data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_avco($item_id)
    {
        $this->db->select('SUM(quantity) as total_quantity, SUM(total_price) as total_price');
        $this->db->where('item_id', $item_id);
        $q = $this->db->get('purchase_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $temp = (float) $data[0]['total_price'] / (float) $data[0]['total_quantity'];
        $avco = $temp;
        $q->free_result();
        return $avco;
    }

    public function create($master_id, $total_expense)
    {

        // $total_price = round($this->input->post('quantity') * $this->input->post('price'));
        $data = array(
            'master_id' => $master_id,
            'item_id' => $this->input->post('item_id'),
            'quantity' => $this->input->post('quantity'),
            'vat_percent' => $this->input->post('vat_percent'),
            'purchase_price' => $this->input->post('price'),
            // 'total_price' => $total_price,
            // 'total_expense' => $total_expense,
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id'),
        );
        $this->db->insert('purchase_details', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $this->db->where('id', $this->input->post('item_id'));
            $item_data = $this->db->get('items')->row_array();
            if ($item_data) {
                $current_qty = $item_data['qty'] + $this->input->post('quantity');
                $this->update_field_item(trim($this->input->post('item_id')), 'qty', $current_qty);
            }
            $this->calculate_avgcos($master_id, $total_expense);
        }
    }

    public function calculate_avgcos($master_id, $total_expense)
    {
        $getTotalItemById = $this->getTotalItemById($master_id);
        if (sizeof($getTotalItemById) > 0) {
            $total_qty = $getTotalItemById[0]['total_quantity'];
            $exper_per_item = $total_expense / $total_qty;
        } else {
            $exper_per_item = 0;
        }
        $details_data = $this->get_by_master_id($master_id);
        for ($i = 0; $i < sizeof($details_data); $i++) {
            $detail_item = $details_data[$i];
            // print_r($detail_item);
            $total_expense_itms = $exper_per_item * $detail_item['quantity'];
            $total_price_item = $detail_item['quantity'] * $detail_item['purchase_price'];
            $total_price_with_expense = $total_price_item + $total_expense_itms;
            $update_data = array(
                'total_price' => number_format($total_price_with_expense, 2),
                'total_expense' => number_format($total_expense_itms, 2)
            );
            $this->db->where('id', $detail_item['id']);
            $this->db->update('purchase_details', $update_data);

            // set avco price

            $avco = $this->get_avco(trim($detail_item['item_id']));
            $this->update_field_item(trim($detail_item['item_id']), 'avco_price', number_format($avco, 2));
        }
    }
    public function update_field_item($id, $field, $value)
    {
        $data = array(
            $field => $value,
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );
        $this->db->where('id', $id);
        $this->db->update('items', $data);
    }
    public function getTotalItemById($master_id)
    {
        $this->db->select('SUM(quantity) as total_quantity');
        $this->db->where('master_id', $master_id);
        return $this->db->get('purchase_details')->result_array();
    }

    public function getExpenses($master_id)
    {
        $this->db->select_sum('expense_price');
        $this->db->where('master_id', $master_id);
        $q = $this->db->get('purchase_meta_expenses');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['expense_price'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function addExpense($master_id)
    {
        $keys = $this->input->post('expense_name');
        $values = $this->input->post('expense_price');
        $output = array();

        $size = sizeof($keys);
        for ($i = 0; $i < $size; $i++) {
            if (!isset($output[$keys[$i]])) {
                $output[$keys[$i]] = array();
            }
            $output[$keys[$i]][] = $values[$i];
        }
        $total = 0;
        foreach ($output as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $total += $value1;
                $data1 = array(
                    'master_id' => $master_id,
                    'expense_name' => $key,
                    'expense_price' => $value1
                );
                $this->db->insert('purchase_meta_expenses', $data1);
            }
        }
        return $total;
    }


    public function delete($id, $master_id)
    {
        $this->db->where('id', $id);
        $this->db->delete('purchase_details');
        $total_expense = $this->getExpenses($master_id);
        $this->calculate_avgcos($master_id, $total_expense);
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('purchase_details');
    }
}
