<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Dashboard
				</h3>
				<ul class="breadcrumb">
					<li class="active">
						Dashboard
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<!--BEGIN METRO STATES-->
			<div class="metro-nav">
				<?php if($privileges['sales'] == 1): ?>
					<div class="metro-nav-block nav-olive">
						<a data-original-title="" href="inventory/sales_list">
							<i class="icon-signout"></i>
							<div class="info">Sales</div>
							<div class="status">Sales</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['sales_return'] == 1): ?>
					<div class="metro-nav-block nav-olive">
						<a data-original-title="" href="inventory/sales_return_list">
							<i class="icon-signout"></i>
							<div class="info">Sales Return</div>
							<div class="status">Sales Return</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['purchase'] == 1): ?>
					<div class="metro-nav-block nav-block-yellow">
						<a data-original-title="" href="inventory/purchase_list">
							<i class="icon-signin"></i>
							<div class="info">Purchase</div>
							<div class="status">Purchase</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['purchase_return'] == 1): ?>
					<div class="metro-nav-block nav-block-yellow">
						<a data-original-title="" href="inventory/purchase_return_list">
							<i class="icon-signin"></i>
							<div class="info">Purchase Return</div>
							<div class="status">Purchase Return</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['customer'] == 1): ?>
					<div class="metro-nav-block nav-block-orange">
						<a data-original-title="" href="inventory/customer_list">
							<i class="icon-group"></i>
							<div class="info">Customer</div>
							<div class="status">Customer</div>
						</a>
					</div>
				<?php endif; ?>
				
				<?php if($privileges['supplier'] == 1): ?>
					<div class="metro-nav-block nav-block-red">
						<a data-original-title="" href="inventory/supplier_list">
							<i class="icon-male"></i>
							<div class="info">Supplier</div>
							<div class="status">Supplier</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['item'] == 1): ?>
					<div class="metro-nav-block nav-block-green">
						<a data-original-title="" href="inventory/item_list">
							<i class="icon-gift"></i>
							<div class="info">Item</div>
							<div class="status">Item</div>
						</a>
					</div>
				<?php endif; ?>
				
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				
				<?php if($privileges['ac_head'] == 1): ?>
					<div class="metro-nav-block nav-light-blue double">
						<a data-original-title="" href="accounts/chart_list">
							<i class="icon-tasks"></i>
							<div class="info">A/C Head</div>
							<div class="status">A/C Head</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['journal'] == 1): ?>
					<div class="metro-nav-block nav-light-green">
						<a data-original-title="" href="accounts/journal_list">
							<i class="icon-columns"></i>
							<div class="info">Journal</div>
							<div class="status">Journal</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['money_receipt'] == 1): ?>
					<div class="metro-nav-block nav-light-brown double">
						<a data-original-title="" href="accounts/mr_list">
							<i class="icon-dollar"></i>
							<div class="info">Money Receipt</div>
							<div class="status">Money Receipt</div>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if($privileges['user_section'] == 1): ?>
					<div class="metro-nav-block nav-block-grey double">
						<a data-original-title="" href="user">
							<i class="icon-user"></i>
							<div class="info">User</div>
							<div class="status">User</div>
						</a>
					</div>
				<?php endif; ?>
				<?php if($privileges['report_menu'] == 1): ?>
					<div class="metro-nav-block nav-light-brown">
						<a data-original-title="" href="report">
							<i class="icon-bar-chart"></i>
							<div class="info">Reports</div>
							<div class="status">Reports</div>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="space10"></div>
			<!--END METRO STATES-->
		</div>

		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>