<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MAc_charts extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('id', $id);
        $q = $this->db->get('ac_charts');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }
   
    public function get_by_code($code, $cmp_id)
    {
         
        $data = array();
        $this->db->where('company_id', $cmp_id);
        $this->db->where('code', $code);
        $q = $this->db->get('ac_charts');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        // var_dump($this->db->last_query());
        // die;
        return $data;
    }

    public function get_by_parent_id($parent_id)
    {
        $data = array();
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('ac_charts');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all()
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('parent_id', 'asc')->group_by('parent_id,id');
        $q = $this->db->get('ac_charts');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }


    public function get_sum_of_chart_id($chart_ids = NULL)
    {
        $data = array();
        $this->db->select('SUM(opening) as opening');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->from('ac_charts');
        $this->db->where('opening > ', '0');
        $this->db->where_in('id', $chart_ids);
        $q = $this->db->get();
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_with_child($parent_id)
    {
        $data = $this->MAc_charts->get_by_id($parent_id);
        $this->db->order_by('parent_id', 'asc');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('parent_id', $parent_id);
        $q = $this->db->get('ac_charts');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
 public function get_child($parent_id , $end_id)
    {
        // var_dump($end_id);
        // die;
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('parent_id', $parent_id);
        $data[] = $parent_id;
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            
            foreach ($q1->result_array() as $row1)
            {
                if($row1['id'] == $end_id){
                    return $data;
                }

               $data[] = $row1['id'];
               
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        if($row2['id'] == $end_id){
                                return $data;
                            }
                        $data[] = $row2['id'];
                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                if($row3['id'] == $end_id){
                                    return $data;
                                }
                               $data[] = $row3['id'];
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    { 
                                        if($row4['id'] == $end_id){
                                            return $data;
                                        }   
                                       $data[] = $row4['id'];
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                                if($row5['id'] == $end_id){
                                                        return $data;
                                                    }
                                               $data[] = $row5['id'];
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                        if($row6['id'] == $end_id){
                                                                return $data;
                                                            }
                                                        $data[] = $row6['id'];
                                                       
                                                        
                                                }
                                            // }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

        return $data;
    }

    public function get_child_level_down($parent_id)
    {
        // var_dump($parent_id);
        // die;
        $data = array();
        foreach ($parent_id as $key => $value) {
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $this->db->where('parent_id', $value);
            $data[] = $value;
            $q1 = $this->db->get('ac_charts');
            if ($q1->num_rows() > 0)
            {
            
            foreach ($q1->result_array() as $row1)
            {
               $data[] = $row1['id'];
               
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        if($row2['id'] == $end_id){
                                return $data;
                            }
                        $data[] = $row2['id'];
                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                if($row3['id'] == $end_id){
                                    return $data;
                                }
                               $data[] = $row3['id'];
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    { 
                                        if($row4['id'] == $end_id){
                                            return $data;
                                        }   
                                       $data[] = $row4['id'];
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                                if($row5['id'] == $end_id){
                                                        return $data;
                                                    }
                                               $data[] = $row5['id'];
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                        if($row6['id'] == $end_id){
                                                                return $data;
                                                            }
                                                        $data[] = $row6['id'];
                                                       
                                                        
                                                }
                                            // }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }   
        
        }
        return $data;
    }

    public function get_child_level_downs($parent_id)
    {
      
        $data = array();
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $this->db->where('parent_id', $parent_id);
            $data[] = $value;
            $q1 = $this->db->get('ac_charts');
            if ($q1->num_rows() > 0)
            {
            
            foreach ($q1->result_array() as $row1)
            {
               $data[] = $row1['id'];
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                       
                        $data[] = $row2['id'];
                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                               
                               $data[] = $row3['id'];
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    { 
                                         
                                       $data[] = $row4['id'];
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                               
                                               $data[] = $row5['id'];
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                       
                                                        $data[] = $row6['id'];
                                                       
                                                        
                                                }
                                            // }
                                            // }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }   
        
        }
        return $data;
    }
    // 10 tier COA Tree
    public function get_coa_tree($chart_id = NULL, $parent_id = 0)
    {
        $options = "";
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('parent_id', $parent_id);
        
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {
                if ($chart_id AND $row1['id'] == $chart_id)
                {
                    $options .= "<option  value=" . $row1['id'] . " selected><b>" . $row1['name'] . " (" . $row1['code'] . ")</b></option>\n";
                }
                else
                {
                    $options .= "<option value=" . $row1['id'] . " disabled><b>" . $row1['name'] . " (" . $row1['code'] . ")</b></option>\n";
                }
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        if ($chart_id AND $row2['id'] == $chart_id)
                        {
                            $options .= "<option value=" . $row2['id'] . " selected><b>&nbsp;&nbsp;--&nbsp;" . $row2['name'] . " (" . $row2['code'] . ")</b></option>\n";
                        }
                        else
                        {
                            $options .= "<option value=" . $row2['id'] . " disabled><b>&nbsp;&nbsp;--&nbsp;" . $row2['name'] . " (" . $row2['code'] . ")</b></option>\n";
                        }


                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                if ($chart_id AND $row3['id'] == $chart_id)
                                {
                                    $options .= "<option value=" . $row3['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row3['name'] . " (" . $row3['code'] . ")</b></option>\n";
                                }
                                else
                                {
                                    $options .= "<option value=" . $row3['id'] . " disabled><b>&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row3['name'] . " (" . $row3['code'] . ")</b></option>\n";
                                }
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    {
                                        if ($chart_id AND $row4['id'] == $chart_id)
                                        {
                                            $options .= "<option value=" . $row4['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row4['name'] . " (" . $row4['code'] . ")</b></option>\n";
                                        }
                                        else
                                        {
                                            $options .= "<option value=" . $row4['id'] . " ><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row4['name'] . " (" . $row4['code'] . ")</b></option>\n";
                                        }
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                                if ($chart_id AND $row5['id'] == $chart_id)
                                                {
                                                    $options .= "<option value=" . $row5['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row5['name'] . " (" . $row5['code'] . ")</b></option>\n";
                                                }
                                                else
                                                {
                                                    $options .= "<option value=" . $row5['id'] . " ><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row5['name'] . " (" . $row5['code'] . ")</b></option>\n";
                                                }
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                        if ($chart_id AND $row6['id'] == $chart_id)
                                                        {
                                                            $options .= "<option value=" . $row6['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row6['name'] . " (" . $row6['code'] . ")</b></option>\n";
                                                        }
                                                        else
                                                        {
                                                            $options .= "<option value=" . $row6['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row6['name'] . " (" . $row6['code'] . ")</b></option>\n";
                                                        }
                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                        $this->db->where('parent_id', $row6['id']);
                                                        $q7 = $this->db->get('ac_charts');
                                                        if ($q7->num_rows() > 0)
                                                        {
                                                            foreach ($q7->result_array() as $row7)
                                                            {
                                                                if ($chart_id AND $row7['id'] == $chart_id)
                                                                {
                                                                    $options .= "<option value=" . $row7['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row7['name'] . " (" . $row7['code'] . ")</b></option>\n";
                                                                }
                                                                else
                                                                {
                                                                    $options .= "<option value=" . $row7['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row7['name'] . " (" . $row7['code'] . ")</b></option>\n";
                                                                }
                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                $this->db->where('parent_id', $row7['id']);
                                                                $q8 = $this->db->get('ac_charts');
                                                                if ($q8->num_rows() > 0)
                                                                {
                                                                    foreach ($q8->result_array() as $row8)
                                                                    {
                                                                        if ($chart_id AND $row8['id'] == $chart_id)
                                                                        {
                                                                            $options .= "<option value=" . $row8['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row8['name'] . " (" . $row8['code'] . ")</b></option>\n";
                                                                        }
                                                                        else
                                                                        {
                                                                            $options .= "<option value=" . $row8['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row8['name'] . " (" . $row8['code'] . ")</b></option>\n";
                                                                        }
                                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                        $this->db->where('parent_id', $row8['id']);
                                                                        $q9 = $this->db->get('ac_charts');
                                                                        if ($q9->num_rows() > 0)
                                                                        {
                                                                            foreach ($q9->result_array() as $row9)
                                                                            {
                                                                                if ($chart_id AND $row9['id'] == $chart_id)
                                                                                {
                                                                                    $options .= "<option value=" . $row9['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row9['name'] . " (" . $row9['code'] . ")</b></option>\n";
                                                                                }
                                                                                else
                                                                                {
                                                                                    $options .= "<option value=" . $row9['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row9['name'] . " (" . $row9['code'] . ")</b></option>\n";
                                                                                }
                                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                                $this->db->where('parent_id', $row9['id']);
                                                                                $q10 = $this->db->get('ac_charts');
                                                                                if ($q10->num_rows() > 0)
                                                                                {
                                                                                    foreach ($q10->result_array() as $row10)
                                                                                    {
                                                                                        if ($chart_id AND $row10['id'] == $chart_id)
                                                                                        {
                                                                                            $options .= "<option value=" . $row10['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row10['name'] . " (" . $row10['code'] . ")</b></option>\n";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $options .= "<option value=" . $row10['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row10['name'] . " (" . $row10['code'] . ")</b></option>\n";
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $options;
    }
    public function get_basic_setting_coa_tree($chart_id = NULL, $parent_id = 0)
    {
        $options = "";
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('parent_id', $parent_id);
        
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {
                if ($chart_id AND $row1['id'] == $chart_id)
                {
                    $options .= "<option  value=" . $row1['id'] . " selected><b>" . $row1['name'] . " (" . $row1['code'] . ")</b></option>\n";
                }
                else
                {
                    $options .= "<option value=" . $row1['id'] . " disabled><b>" . $row1['name'] . " (" . $row1['code'] . ")</b></option>\n";
                }
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        if ($chart_id AND $row2['id'] == $chart_id)
                        {
                            $options .= "<option value=" . $row2['id'] . " selected><b>&nbsp;&nbsp;--&nbsp;" . $row2['name'] . " (" . $row2['code'] . ")</b></option>\n";
                        }
                        else
                        {
                            $options .= "<option value=" . $row2['id'] . " disabled><b>&nbsp;&nbsp;--&nbsp;" . $row2['name'] . " (" . $row2['code'] . ")</b></option>\n";
                        }


                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                if ($chart_id AND $row3['id'] == $chart_id)
                                {
                                    $options .= "<option value=" . $row3['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row3['name'] . " (" . $row3['code'] . ")</b></option>\n";
                                }
                                else
                                {
                                    $options .= "<option value=" . $row3['id'] . " disabled><b>&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row3['name'] . " (" . $row3['code'] . ")</b></option>\n";
                                }
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    {
                                        if ($chart_id AND $row4['id'] == $chart_id)
                                        {
                                            $options .= "<option value=" . $row4['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row4['name'] . " (" . $row4['code'] . ")</b></option>\n";
                                        }
                                        else
                                        {
                                            $options .= "<option value=" . $row4['id'] . " ><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row4['name'] . " (" . $row4['code'] . ")</b></option>\n";
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $options;
    }
   
    public function get_coa_treee($chart_id = NULL, $parent_id = 0)
    {
        $options = "";
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('parent_id', $parent_id);
        
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {
                if ($chart_id AND $row1['id'] == $chart_id)
                {
                    $options .= "<option  value=" . $row1['id'] . " selected><b>" . $row1['name'] . " (" . $row1['code'] . ")</b></option>\n";
                }
                else
                {
                    $options .= "<option value=" . $row1['id'] . "><b>" . $row1['name'] . " (" . $row1['code'] . ")</b></option>\n";
                }
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        if ($chart_id AND $row2['id'] == $chart_id)
                        {
                            $options .= "<option value=" . $row2['id'] . " selected><b>&nbsp;&nbsp;--&nbsp;" . $row2['name'] . " (" . $row2['code'] . ")</b></option>\n";
                        }
                        else
                        {
                            $options .= "<option value=" . $row2['id'] . "><b>&nbsp;&nbsp;--&nbsp;" . $row2['name'] . " (" . $row2['code'] . ")</b></option>\n";
                        }


                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                if ($chart_id AND $row3['id'] == $chart_id)
                                {
                                    $options .= "<option value=" . $row3['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row3['name'] . " (" . $row3['code'] . ")</b></option>\n";
                                }
                                else
                                {
                                    $options .= "<option value=" . $row3['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row3['name'] . " (" . $row3['code'] . ")</b></option>\n";
                                }
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    {
                                        if ($chart_id AND $row4['id'] == $chart_id)
                                        {
                                            $options .= "<option value=" . $row4['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row4['name'] . " (" . $row4['code'] . ")</b></option>\n";
                                        }
                                        else
                                        {
                                            $options .= "<option value=" . $row4['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row4['name'] . " (" . $row4['code'] . ")</b></option>\n";
                                        }
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                                if ($chart_id AND $row5['id'] == $chart_id)
                                                {
                                                    $options .= "<option value=" . $row5['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row5['name'] . " (" . $row5['code'] . ")</b></option>\n";
                                                }
                                                else
                                                {
                                                    $options .= "<option value=" . $row5['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row5['name'] . " (" . $row5['code'] . ")</b></option>\n";
                                                }
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                        if ($chart_id AND $row6['id'] == $chart_id)
                                                        {
                                                            $options .= "<option value=" . $row6['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row6['name'] . " (" . $row6['code'] . ")</b></option>\n";
                                                        }
                                                        else
                                                        {
                                                            $options .= "<option value=" . $row6['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row6['name'] . " (" . $row6['code'] . ")</b></option>\n";
                                                        }
                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                        $this->db->where('parent_id', $row6['id']);
                                                        $q7 = $this->db->get('ac_charts');
                                                        if ($q7->num_rows() > 0)
                                                        {
                                                            foreach ($q7->result_array() as $row7)
                                                            {
                                                                if ($chart_id AND $row7['id'] == $chart_id)
                                                                {
                                                                    $options .= "<option value=" . $row7['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row7['name'] . " (" . $row7['code'] . ")</b></option>\n";
                                                                }
                                                                else
                                                                {
                                                                    $options .= "<option value=" . $row7['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row7['name'] . " (" . $row7['code'] . ")</b></option>\n";
                                                                }
                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                $this->db->where('parent_id', $row7['id']);
                                                                $q8 = $this->db->get('ac_charts');
                                                                if ($q8->num_rows() > 0)
                                                                {
                                                                    foreach ($q8->result_array() as $row8)
                                                                    {
                                                                        if ($chart_id AND $row8['id'] == $chart_id)
                                                                        {
                                                                            $options .= "<option value=" . $row8['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row8['name'] . " (" . $row8['code'] . ")</b></option>\n";
                                                                        }
                                                                        else
                                                                        {
                                                                            $options .= "<option value=" . $row8['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row8['name'] . " (" . $row8['code'] . ")</b></option>\n";
                                                                        }
                                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                        $this->db->where('parent_id', $row8['id']);
                                                                        $q9 = $this->db->get('ac_charts');
                                                                        if ($q9->num_rows() > 0)
                                                                        {
                                                                            foreach ($q9->result_array() as $row9)
                                                                            {
                                                                                if ($chart_id AND $row9['id'] == $chart_id)
                                                                                {
                                                                                    $options .= "<option value=" . $row9['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row9['name'] . " (" . $row9['code'] . ")</b></option>\n";
                                                                                }
                                                                                else
                                                                                {
                                                                                    $options .= "<option value=" . $row9['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row9['name'] . " (" . $row9['code'] . ")</b></option>\n";
                                                                                }
                                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                                $this->db->where('parent_id', $row9['id']);
                                                                                $q10 = $this->db->get('ac_charts');
                                                                                if ($q10->num_rows() > 0)
                                                                                {
                                                                                    foreach ($q10->result_array() as $row10)
                                                                                    {
                                                                                        if ($chart_id AND $row10['id'] == $chart_id)
                                                                                        {
                                                                                            $options .= "<option value=" . $row10['id'] . " selected><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row10['name'] . " (" . $row10['code'] . ")</b></option>\n";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $options .= "<option value=" . $row10['id'] . "><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;" . $row10['name'] . " (" . $row10['code'] . ")</b></option>\n";
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $options;
    }
public function get_coa(){
 //    var_dump($parent_id = 4);
 // die;
// $this->db->where('parent_id',$parent_id);
        
$this->db->select('parent_id');
$this->db->where('id', '7');
$query = $this->db->get('ac_charts');
        return $query;
}
    // 10 tier COA List
    public function get_coa_list($parent_id = 0, $is_active)
    {     

        $options = array();
         $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('parent_id', $parent_id);
        if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
               $this->db->where('status', 'Active');
            }      
            
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {
                $options[] = $row1;
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
             if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
               $this->db->where('status', 'Active');
            }          
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        $options[] = $row2;
                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                 if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
               $this->db->where('status', 'Active');
            }        
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                $options[] = $row3;
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
           }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    {
                                        $options[] = $row4;
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                                $options[] = $row5;
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
           }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                        $options[] = $row6;
                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                        $this->db->where('parent_id', $row6['id']);
                                                        if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
           }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                                        $q7 = $this->db->get('ac_charts');
                                                        if ($q7->num_rows() > 0)
                                                        {
                                                            foreach ($q7->result_array() as $row7)
                                                            {
                                                                $options[] = $row7;
                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                $this->db->where('parent_id', $row7['id']);
                                                                if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                                                $q8 = $this->db->get('ac_charts');
                                                                if ($q8->num_rows() > 0)
                                                                {
                                                                    foreach ($q8->result_array() as $row8)
                                                                    {
                                                                        $options[] = $row8;
                                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                        $this->db->where('parent_id', $row8['id']);
                                                                        if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                                                        $q9 = $this->db->get('ac_charts');
                                                                        if ($q9->num_rows() > 0)
                                                                        {
                                                                            foreach ($q9->result_array() as $row9)
                                                                            {
                                                                                $options[] = $row9;
                                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                                $this->db->where('parent_id', $row9['id']);
                                                                                if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }elseif ($is_active == 'Active') {
                $this->db->where('status', 'Active');
            }         
                                                                                $q10 = $this->db->get('ac_charts');
                                                                                if ($q10->num_rows() > 0)
                                                                                {
                                                                                    foreach ($q10->result_array() as $row10)
                                                                                    {
                                                                                        $options[] = $row10;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $options;
    }


public function get_coa_list_level($chart_id = NULL, $parent = 0)
    {     
$options = array();

        $this->db->where('parent_id', $parent);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {
                $options[] = $row1;
                $this->db->where('company_id', $this->session->userdata('user_company'));
                $this->db->where('parent_id', $row1['id']);
                $q2 = $this->db->get('ac_charts');
                if ($q2->num_rows() > 0)
                {
                    foreach ($q2->result_array() as $row2)
                    {
                        $options[] = $row2;
                        $this->db->where('company_id', $this->session->userdata('user_company'));
                        $this->db->where('parent_id', $row2['id']);
                        $q3 = $this->db->get('ac_charts');
                        if ($q3->num_rows() > 0)
                        {
                            foreach ($q3->result_array() as $row3)
                            {
                                $options[] = $row3;
                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                $this->db->where('parent_id', $row3['id']);
                                $q4 = $this->db->get('ac_charts');
                                if ($q4->num_rows() > 0)
                                {
                                    foreach ($q4->result_array() as $row4)
                                    {
                                        $options[] = $row4;
                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                        $this->db->where('parent_id', $row4['id']);
                                        $q5 = $this->db->get('ac_charts');
                                        if ($q5->num_rows() > 0)
                                        {
                                            foreach ($q5->result_array() as $row5)
                                            {
                                                $options[] = $row5;
                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                $this->db->where('parent_id', $row5['id']);
                                                $q6 = $this->db->get('ac_charts');
                                                if ($q6->num_rows() > 0)
                                                {
                                                    foreach ($q6->result_array() as $row6)
                                                    {
                                                        $options[] = $row6;
                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                        $this->db->where('parent_id', $row6['id']);
                                                        $q7 = $this->db->get('ac_charts');
                                                        if ($q7->num_rows() > 0)
                                                        {
                                                            foreach ($q7->result_array() as $row7)
                                                            {
                                                                $options[] = $row7;
                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                $this->db->where('parent_id', $row7['id']);
                                                                $q8 = $this->db->get('ac_charts');
                                                                if ($q8->num_rows() > 0)
                                                                {
                                                                    foreach ($q8->result_array() as $row8)
                                                                    {
                                                                        $options[] = $row8;
                                                                        $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                        $this->db->where('parent_id', $row8['id']);
                                                                        $q9 = $this->db->get('ac_charts');
                                                                        if ($q9->num_rows() > 0)
                                                                        {
                                                                            foreach ($q9->result_array() as $row9)
                                                                            {
                                                                                $options[] = $row9;
                                                                                $this->db->where('company_id', $this->session->userdata('user_company'));
                                                                                $this->db->where('parent_id', $row9['id']);
                                                                                $q10 = $this->db->get('ac_charts');
                                                                                if ($q10->num_rows() > 0)
                                                                                {
                                                                                    foreach ($q10->result_array() as $row10)
                                                                                    {
                                                                                        $options[] = $row10;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $options;
    }
public function get_coa_list_ob($is_active = 'Active')
    {     
        $options = array();
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $this->db->where('opening <>', '0');
            if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }else{
                $this->db->where('status', 'Active');
            } 
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {   
                                $options[] = $row1;
                            
                
                              }
                }

        return $options;
    }

 

    public function get_coa_list_zob($is_active = 'Active')
    {     
        $options = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('opening =', '0'); 
        if($is_active == 'Inactive'){
                $this->db->where('status', 'Inactive');
            }else{
                $this->db->where('status', 'Active');
            }         
        $q1 = $this->db->get('ac_charts');
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {   
                $return = $this->MAc_journal_details->get_chrt($row1['id']);
                    
                            if(!$return){
                                $options[] = $row1;
                            }
                
                              }
                }

        return $options;
    }
public function get_level($level)
    {     
        $options = array();
        if($level == '1'){
            $this->db->where('parent_id', '0');
        }elseif($level == '2'){
            $parent = $this->get_level(1);
            $this->db->group_start();
            $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where("parent_id", $value['id']);
            }
            $this->db->group_end();
        }elseif($level == '3'){
            $parent = $this->get_level(2);
            $this->db->group_start();
            $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value['id']);

            }
            $this->db->group_end();
         }elseif($level == '4'){
            $parent = $this->get_level(3);
            $this->db->group_start();
            $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value['id']);

            }
            $this->db->group_end();
         }elseif($level == '5'){
            $parent = $this->get_level(4);
            $this->db->group_start();
            $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value['id']);

            }
            $this->db->group_end();
         }elseif($level == '6'){
            $parent = $this->get_level(5);
            $this->db->group_start();
            $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value['id']);

            }
            $this->db->group_end();
        }

        $this->db->order_by('code','ASC');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q1 = $this->db->get('ac_charts');
        // var_dump($this->db->last_query());
        
        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {   


                                $options[] = $row1;
                            
                
                              }
                }

        return $options;
    }

    public function get_level_ids($level)
    {     

        $options = array();
        if($level == '1'){
            $this->db->where('parent_id', '0');
        }elseif($level == '2'){
            $parent = $this->get_level_ids(1);
            // $this->db->where('parent_id', '0');

            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value);
            }
        }elseif($level == '3'){
            $parent = $this->get_level_ids(2);
            // $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value);

            }
         }elseif($level == '4'){
            $parent = $this->get_level_ids(3);
            // $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value);

            }
         }elseif($level == '5'){
            $parent = $this->get_level_ids(4);
            // $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value);

            }
         }elseif($level == '6'){
            $parent = $this->get_level_ids(5);
            // $this->db->where('parent_id', '0');
            foreach ($parent as $key => $value) {
                $this->db->or_where('parent_id', $value);

            }
        }
        
            $this->db->where('company_id', $this->session->userdata('user_company'));
        $q1 = $this->db->get('ac_charts');

        if ($q1->num_rows() > 0)
        {
            foreach ($q1->result_array() as $row1)
            {   


                                $options[] = $row1['id'];
                            
                
                              }
                }

        return $options;
    }

   
    public function basic_setup($cmp_id, $user_id)
    {
        $charts = $this->MAc_default_charts->get_all();
        foreach ($charts as $chart)
        {
            if ($chart['parent_id'] != 0)
            {
                $parent = $this->MAc_default_charts->get_by_id($chart['parent_id']);
                //print_r($parent);
                $parent_coa = $this->MAc_charts->get_by_code($parent['code'], $cmp_id);
                //print_r($parent_coa);
                $parent_id = $parent_coa['id'];
            }
            else
            {
                $parent_id = 0;
            }
            $data = array(
                'company_id' => $cmp_id,
                'parent_id' => $parent_id,
                'code' => $chart['code'],
                'name' => $chart['name'],
                'memo' => $chart['memo'],
                'created_at' => date('Y-m-d H:i:s', time()),
                'created_by' => $user_id
                );

            $this->db->insert('ac_charts', $data);
            $switch = $chart['type'];
            switch ($switch) {
                case "Receivable":
                $ac['receivable'] = $this->db->insert_id();
                break;
                case "Payable":
                $ac['payable'] = $this->db->insert_id();
                break;
                case "Cash":
                $ac['cash'] = $this->db->insert_id();
                break;
                case "Bank":
                $ac['bank'] = $this->db->insert_id();
                break;
                case "Sales":
                $ac['sales'] = $this->db->insert_id();
                break;
                case "Purchase":
                $ac['purchase'] = $this->db->insert_id();
                break;
                case "Inventory":
                $ac['inventory'] = $this->db->insert_id();
                break;
                case "COGS":
                $ac['cogs'] = $this->db->insert_id();
                break;
            }
        }
        $this->MSettings->create($cmp_id, $ac['receivable'], $ac['payable'], $ac['cash'], $ac['bank'], $ac['sales'], $ac['purchase'], $ac['inventory'], $ac['cogs']);
    }

    public function account_create($parent_id, $code, $name, $type = NULL)
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'parent_id' => $parent_id,
            'code' => $code,
            'name' => $name,
            'type' => $type,
            'created_at' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
            );

        $this->db->insert('ac_charts', $data);
        
        return $this->db->insert_id();
    }

    public function account_update($id, $name, $type = NULL)
    {
        $ac = $this->get_by_id($id);

        if($ac['type'] ==  $type){

            $data = array(
                'name' => $name,
                'type' => $type
                );
        }else{

            $inventory = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

            if($type == 'Raw'){

                    $chart = $this->get_by_id($inventory['ac_inventory_raw']);
                    $siblings = $this->get_by_parent_id($inventory['ac_inventory_raw']);
                    $parient_id = $inventory['ac_inventory_raw'];

                }else if($type == 'Wet Blue'){
                    $chart = $this->get_by_id($inventory['ac_inventory_wet_blue']);
                    $siblings = $this->get_by_parent_id($inventory['ac_inventory_wet_blue']);
                    $parient_id = $inventory['ac_inventory_wet_blue'];
                }else if($type == 'Chemical'){
                        $chart = $this->get_by_id($inventory['ac_inventory_chemical']);
                    $siblings = $this->get_by_parent_id($inventory['ac_inventory_chemical']);
                    $parient_id = $inventory['ac_inventory_chemical'];
                }else if($type == 'FG'){
                        $chart = $this->get_by_id($inventory['ac_inventory_fg']);
                    $siblings = $this->get_by_parent_id($inventory['ac_inventory_fg']);
                    $parient_id = $inventory['ac_inventory_fg'];
                }
                 if (count($siblings) > 0)
                    {

                        $ac_code_temp = explode('.', $siblings['code']);
                        $ac_last = count($ac_code_temp) - 1;
                        $ac_new = (int) $ac_code_temp[$ac_last] + 1;
                        $ac_code = $chart['code'] . '.' . $ac_new;
                    }
                    else
                    {
                        $ac_code = $chart['code'] . '.10';
                    }
            $data = array(
                'name' => $name,
                'type' => $type,
                'code' => $ac_code,
                'parent_id' => $parient_id
                );
        }
        
        $this->db->where('id', $id);
        $this->db->update('ac_charts', $data);
        return $this->db->insert_id();
    }
public function get_by_codeee($code)
    {
         
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('code', $code);
        $q = $this->db->get('ac_charts')->num_rows();
        return $q;
    }
    public function create()
    {
        // $code = $this->input->post('code');
        // $reslt = $this->get_by_codeee($code);
        
        // if($reslt > 0){
        //         return false;
        // }else{
            $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'parent_id' => $this->input->post('parent_id'),
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
            'memo' => $this->input->post('memo'),
            'opening' => $this->input->post('opening'),
            'status' => $this->input->post('status'),
            'created_at' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
            );
        $this->db->insert('ac_charts', $data);
        // return true;
        // }
        
    }

    public function update()
    {
        $data = array(    
            'parent_id' => $this->input->post('parent_id'),
            'code' => $this->input->post('code'),
            'name' => $this->input->post('name'),
            'memo' => $this->input->post('memo'),
            'opening' => $this->input->post('opening'),
            'status' => $this->input->post('status'),
            'modified_at' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
            );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('ac_charts', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('ac_charts');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('ac_charts');
    }

}