<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">
    <title>Trial Balance From: <?php echo $start_date; ?> To: <?php echo $end_date; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Cybextech">
     <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo');?>" type="image/x-icon" />
<link rel="stylesheet" href="assets/bootstrap.min.css">
    <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />
    <link href="assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"/>
<!-- Datatables -->
 <script src='assets/vendors/echarts/test/lib/jquery.min.js'></script>
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="assets/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <style>
      @media print{
        p.muted{
            font-weight: bold;
        }
        small.small{
          font-weight: normal;
      }
  }
</style>
</head>
<script>
      $(document).ready(function() {
  var table = $('#datatable-responsive').DataTable({
        "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
    });
 
 var buttons = new $.fn.dataTable.Buttons(table, {
     buttons: [
       'copy',
       'csv',
       'excel',
       'pdf',
     'print'
    ]

 }).container().appendTo($('#buttons'));
 });
 </script>
<body>
    <!-- section content -->
    <section class="section">
        <div class="container">
            <!-- span content -->
            <div class="span12">
                <!-- content -->
                <div class="content" style="border: 1px solid #d7d7d7;">
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                               <h2 class="center" style="color: #000;">
                                <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                                <button id="buttons"></button>
                            </div>
                            <div class="row-fluid center">
                                <strong>Trial Balance</strong>
                                <p><?php echo date('jS F Y ', strtotime(date_to_db($start_date))); ?> To <?php echo date('jS F Y ', strtotime(date_to_db($end_date))); ?></p>
                            </div>
                            <div class="invoice-table">
                                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="datatable-responsive">
                                    <thead>
                                    <tr>
                                       <th>#</th>
                                            <th>A/C Head Code</th>
                                            <th>Name of A/C</th>
                                            <th colspan="2" class="center">Opening</th>
                                            <th colspan="2" class="center">During the Period</th>
                                            <th colspan="2" class="center">Closing</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="center">Debit</th>
                                            <th class="center">Credit</th>
                                            <th class="center">Debit</th>
                                            <th class="center">Credit</th>
                                            <th class="center">Debit</th>
                                            <th class="center">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      $i=0;
                                      $debit = 0;
                                      $credit = 0;
                                      foreach ($trial_balance as $balance):
                                             
                                        $deb = $balance['total_debit'];
                                        $cre = $balance['total_credit'];
                                        $opn = $balance['chart_opening'];
                                        foreach ($previous_balance as $pr_balnace):
                                            if($balance['chart_id'] == $pr_balnace['chart_id']){
                                                $pre_deb = $pr_balnace['total_debit'];
                                            $pre_cre = $pr_balnace['total_credit'];
                                            $pre_bal = $pre_deb - $pre_cre;
                                            $opn = $balance['chart_opening'] + $pre_bal;
                                            }
                                        endforeach;
                                        $bal = $deb - $cre;
                                        $closing = $bal + $opn;
                                        
                                       
                                        $i++;
                                        if(($closing != '0' && $balance['report_typ'] == "with_baln") || ($closing == '0' && $balance['report_typ'] == "zero_baln")){
                                        ?>

                                    <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $balance['chart_code'];?></td>
                                    <td><?php echo $balance['chart_name']; ?></td>
                                    <td class="right"><?php if($opn > 0) echo number_format($opn, 2); ?></td>
                                     <td class="right">
                                     <?php if($opn < 0) echo number_format(-$opn, 2); ?></td> 
                                    
                                    <td class="right"><?php if($bal > 0) echo number_format($bal, 2); ?></td>
                                     <td class="right">
                                     <?php if($bal < 0) echo number_format(-$bal, 2); ?></td> 
                                      
                                      <td class="right"><?php if($closing > 0) echo number_format($closing, 2); ?></td>
                                     <td class="right">
                                     <?php if($closing < 0) echo number_format(-$closing, 2); ?></td> 
                                    </tr>
                                    <?php
                                }
                                    if($bal > 0){
                                        $debit += $bal;
                                    }
                                    if($bal < 0){
                                        $credit += -$bal;
                                    }
                                    if($closing > 0){
                                       $total_cls_debit += $closing;
                                    }
                                    if($closing < 0){
                                        $total_cls_credit += -$closing;
                                    }
                                    if($opn > 0){
                                       $total_opn_debit += $opn;
                                    }
                                    if($opn < 0){
                                        $total_opn_credit += -$opn;
                                    }
                                    endforeach;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"><b>Total</b></td>
                                        <td class="right" ><b><?php echo number_format($total_opn_debit, 2); ?></b></td>
                                        <td class="right"><b><?php echo number_format($total_opn_credit, 2); ?></b></td>
                                        <td class="right" ><b><?php echo number_format($debit, 2); ?></b></td>
                                        <td class="right"><b><?php echo number_format($credit, 2); ?></b></td>
                                        <td class="right" ><b><?php echo number_format($total_cls_debit, 2); ?></b></td>
                                        <td class="right"><b><?php echo number_format($total_cls_credit, 2); ?></b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!--/invoice-->
                </div><!--/content-body -->
            </div><!-- /content -->
        </div><!-- /span content -->

    </div><!-- /container -->
</section>

</body>
</html>