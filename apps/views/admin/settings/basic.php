<script type="text/javascript" src="assets/backend/js/select2.min.js"></script>
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Basic
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Basic
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Basic Settings </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->

						<form action="settings/basic" method="post" class="form-horizontal form-bordered form-validate" id="frm1">
							<div class="control-group">
								<label for="ac_payable" class="control-label ">Supplier on A/C Head</label>
								<select name="ac_payable" id="ac_payable" class="span5 chzn-select  chart_id">

									<option value="<?php if ($payable_tree['id']) {
														echo $payable_tree['id'];
													} ?>"><?php if ($payable_tree['id']) {
																echo $payable_tree['name'] . ' (' . $payable_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<div class="control-group">
								<label for="ac_receivable" class="control-label">Customer on A/C Head</label>
								<select name="ac_receivable" id="ac_receivable" class="span5 chzn-select chart_id">

									<option value="<?php if ($receivable_tree['id']) {
														echo $receivable_tree['id'];
													} ?>"><?php if ($receivable_tree['id']) {
																echo $receivable_tree['name'] . ' (' . $receivable_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<div class="control-group">
								<label for="ac_cash" class="control-label">Cash Book A/C Head</label>
								<select name="ac_cash" id="ac_cash" class="span5 chzn-select chart_id">
									<option value="<?php if ($cash_tree['id']) {
														echo $cash_tree['id'];
													} ?>"><?php if ($cash_tree['id']) {
																echo $cash_tree['name'] . ' (' . $cash_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<div class="control-group">
								<label for="ac_bank" class="control-label">Bank Book A/C Head</label>
								<select name="ac_bank" id="ac_bank" class="span5 chzn-select chart_id">
									<option value="<?php if ($bank_tree['id']) {
														echo $bank_tree['id'];
													} ?>"><?php if ($bank_tree['id']) {
																echo $bank_tree['name'] . ' (' . $bank_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<div class="control-group">
								<label for="ac_sales" class="control-label">Sales A/C Head</label>
								<select name="ac_sales" id="ac_sales" class="span5 chzn-select chart_id">
									<option value="<?php if ($sales_tree['id']) {
														echo $sales_tree['id'];
													} ?>"><?php if ($sales_tree['id']) {
																echo $sales_tree['name'] . ' (' . $sales_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<div class="control-group">
								<label for="ac_purchase" class="control-label">Purchase A/C Head</label>
								<select name="ac_purchase" id="ac_purchase" class="span5 chzn-select chart_id">
									<option value="<?php if ($purchase_tree['id']) {
														echo $purchase_tree['id'];
													} ?>"><?php if ($purchase_tree['id']) {
																echo $purchase_tree['name'] . ' (' . $purchase_tree['code'] . ")";
															} ?></option>
								</select>
							</div>

							
							<div class="control-group">
								<label for="ac_inventory" class="control-label">Cost of Goods Sold A/C Head</label>
								<select name="ac_cogs" id="ac_cogs" class="span5 chzn-select chart_id">
									<option value="<?php if ($cogs_tree['id']) {
														echo $cogs_tree['id'];
													} ?>"><?php if ($cogs_tree['id']) {
																echo $cogs_tree['name'] . ' (' . $cogs_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<div class="control-group">
								<label for="ac_inventory" class="control-label">Tax A/C Head</label>
								<select name="ac_tax" id="ac_tax" class="span5 chzn-select chart_id">
									<option value="<?php if ($tax_tree['id']) {
														echo $tax_tree['id'];
													} ?>"><?php if ($tax_tree['id']) {
																echo $tax_tree['name'] . ' (' . $tax_tree['code'] . ")";
															} ?></option>
								</select>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if (count($settings) > 0) { ?><input type="hidden" name="id" value="<?php echo $settings['id']; ?>"><?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes">
								<button type="reset" class="btn">Cancel</button>
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>
<script type="text/javascript">
	$('.chart_id').select2({
		placeholder: '--- Select A/C ---',
		ajax: {
			url: "inventory/get_ac_chart",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
</script>