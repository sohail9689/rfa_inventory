<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Accounts extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        if (!$this->session->userdata('user_id')) {
            redirect('', 'refresh');
        }
        $this->privileges = $this->MUser_privileges->get_by_ref_user($this->session->userdata('user_id'));
        eval(base64_decode("aWYoISR0aGlzLT5hY2NvdW50X3ByaXZpbGVnZXMtPmdldF9kYXRlKGRhdGUoJ1ktbS1kJykpKXsKICAgICAgICAgICAgICAgICR0aGlzLT5zZXNzaW9uLT5zZXRfZmxhc2hkYXRhKCd1c2VyX3ByaXZpbGVnZXMnLCdZb3VyIE1vbnRobHkgbGljZW5zZSBoYXMgYmVlbiBleHBpcmVkISBQbGVhc2UgQ29udGFjdCA8YSBocmVmPSJodHRwOi8vY3liZXh0ZWNoLmNvbS9jb250YWN0dXMuaHRtbCIgdGFyZ2V0PSJfYmxhbmsiPkN5YmV4IFRlY2g8L2E+LicpOwogICAgICAgICAgICAgICAgcmVkaXJlY3QgKCdkYXNoYm9hcmQnKTsKICAgICAgICAgICAgfQ=="));
        // $res = $this->MAc_journal_master->get_journal_num('BPV', '2020-02-01', "2020-02-29");
        // var_dump($res);
        // die;
    }
    public function index()
    {


        $data['title'] = 'POS System';
        $data['menu'] = 'accounts';
        $data['content'] = 'admin/accounts/home';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    //Chart of Accounts Start ---------------
    public function get_ac_code()
    {
        $combine = "";
        $ac_id = $this->input->post('result');
        $this->db->where('parent_id', $ac_id);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('code', "DESC");
        $query = $this->db->get("ac_charts")->row_array();
        if ($query <> '' || $query <> "") {
            if ($ac_id == 0) {
                $combine = $query['code'] + 10;
            } else {

                $a = explode(".", $query['code']);
                $last = end($a) + 10;
                array_pop($a);
                array_push($a, $last);
                $combine = implode(".", $a);
            }
        } else {
            $ac_id = $this->input->post('result');
            if ($ac_id == 0) {
                $combine = 10;
            } else {

                $this->db->where('id', $ac_id);
                $query2 = $this->db->get("ac_charts")->row_array();
                $combine = $query2['code'] . '.' . '10';
            }
        }
        echo json_encode($combine);
    }
    public function chart_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'accounts';
        $data['content'] = 'admin/accounts/chart/list';
        // $data['charts'] = $this->MAc_charts->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function chart_list_data()
    {
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('opening', $search, 'both');
            $this->db->or_like('created_at', $search, 'both');
            $this->db->or_like('status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $total = $this->db->get("ac_charts")->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'code';
            $order = 'ASC';
        }
        $this->db->order_by($sort, $order);
        $this->db->group_by('parent_id,id');
        if ($search) {
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('opening', $search, 'both');
            $this->db->or_like('created_at', $search, 'both');
            $this->db->or_like('status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get("ac_charts")->result_array();
        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'o_b' => '',
                'created' => '',
                'status' => '',
                'options' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['o_b'] = $row['opening'];
            $res['created'] = date('Y-m-d', strtotime($row['created_at']));
            $res['status'] = $row['status'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = " <a  class=\"btn btn-edit\" href=\"accounts/chart_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            } else {
                $res['options'] = "";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function chart_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MAc_charts->update();
            } else {
                $insert =  $this->MAc_charts->create();

                //    if(!$insert){
                //         echo '<script>alert("One or More A/C is added with this code. ");</script>';
                //         redirect('accounts/chart_save', 'refresh');
                //    }
            }
            redirect('accounts/chart_list', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'accounts';
            $data['content'] = 'admin/accounts/chart/save';

            if ($id) {
                // $data['ac_chart_tree'] = $this->MAc_charts->get_coa_treee($data['chart']['parent_id']);
                $data['chart'] = $this->MAc_charts->get_by_id($id);
                $data['ac_chart_parent'] = $this->MAc_charts->get_by_id($data['chart']['parent_id']);
                // var_dump($data['ac_chart_parent']);
                // die;
                // $data['ac_chart_parent'] = NULL;
            } else {
                // $data['ac_chart_tree'] = $this->MAc_charts->get_coa_treee();
                $data['ac_chart_parent'] = NULL;
                $data['chart'] = NULL;
            }
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function chart_delete($id)
    {
        $result = $this->MAc_journal_details->get_dataaa($id);

        $journal = $this->MAc_journal_details->ac_exist($id);
        if (($journal) > 0) {
            $data['error'] = true;
            $data['message'] = $journal . ' Journal Entry is recorded with this A/C Head. Remove Them First.';
        } elseif (($result) > 0) {
            $data['error'] = true;
            $data['message'] = $result . ' has child with this A/C Head. Remove Them First.';
        } else {
            $this->MAc_charts->delete($id);
            $data['error'] = false;
            $data['message'] = 'success';
        }

        echo json_encode($data);
    }

    //Chart of Accounts End ---------------
    //
    //Voucher Part Start ------------------

    public function journal_list()
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'accounts';
        $data['content'] = 'admin/accounts/journal/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function journal_list_data()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('ac_journal_master.journal_no', $search, 'both');
            $this->db->or_like('ac_journal_master.payment_type', $search, 'both');
            $this->db->or_like('ac_journal_master.journal_date', $search, 'both');
            $this->db->or_like('ac_journal_details.memo', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("ac_journal_master.id, ac_journal_master.payment_type, ac_journal_master.doc_type, ac_journal_master.journal_no, ac_journal_master.journal_date, sum(ac_journal_details.debit) as debit, sum(ac_journal_details.credit) as credit, ac_journal_details.memo");
        $this->db->from("ac_journal_master");
        $this->db->join("ac_journal_details", "ac_journal_master.id = ac_journal_details.master_id", "left");
        $this->db->where('ac_journal_master.journal_date >= ', $stat_date);
        $this->db->where('ac_journal_master.journal_date <= ', $en_date);
        $this->db->where('ac_journal_master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("ac_journal_master.journal_no, ac_journal_master.journal_date");
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'ac_journal_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('ac_journal_master.journal_no', $search, 'both');
            $this->db->or_like('ac_journal_master.payment_type', $search, 'both');
            $this->db->or_like('ac_journal_master.journal_date', $search, 'both');
            $this->db->or_like('ac_journal_details.memo', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("ac_journal_master.id, ac_journal_master.payment_type, ac_journal_master.doc_type, ac_journal_master.journal_no, ac_journal_master.journal_date, sum(ac_journal_details.debit) as debit, sum(ac_journal_details.credit) as credit, ac_journal_details.memo");
        $this->db->from("ac_journal_master");
        $this->db->join("ac_journal_details", "ac_journal_master.id = ac_journal_details.master_id", "left");
        $this->db->where('ac_journal_master.journal_date >= ', $stat_date);
        $this->db->where('ac_journal_master.journal_date <= ', $en_date);
        $this->db->where('ac_journal_master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("ac_journal_master.journal_no, ac_journal_master.journal_date");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {

            $res = array(
                'voucher_type' => '',
                'journal_no' => '',
                'journal_date' => '',
                'debit_amount' => '',
                'credit_amount' => '',
                'narration' => '',
                'option' => ''
            );

            $res['journal_no'] = "<a href=\"accounts/journal_preview/" . $row['id'] . "\"  target=\"_blank\">" . $row['journal_no'] . "</a>";
            $res['voucher_type'] = $row['payment_type'];
            $res['journal_date'] = $row['journal_date'];
            $res['debit_amount'] = $row['debit'];
            $res['credit_amount'] = $row['credit'];
            $res['narration'] = $row['memo'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                if ($row['doc_type'] == "") {
                    $res['options'] = " <a  class=\"btn btn-edit\" href=\"accounts/journal_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                                  <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
                }
            } else {
                $res['options'] = "";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function journal_save($id = NULL)
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'accounts';
        $data['content'] = 'admin/accounts/journal/save';
        $data['master'] = $this->MAc_journal_master->get_by_id($id);
        if ($id) {
            $data['journal_no'] = $data['master']['journal_no'];
            $data['details'] = $this->MAc_journal_details->get_by_journal_no($id);
        } else {
            $data['details'] = array();
        }
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function get_journal_value()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $voucher_type = $this->input->post('result');
        $json['journal_no'] = $this->MAc_journal_master->get_journal_num($voucher_type, $stat_date, $en_date);
        echo json_encode($json);
    }

    public function journal_complete()
    {
        $this->MAc_journal_master->update($this->input->post('master_id'));
        echo 'accounts/journal_preview_after_complete/' . $this->input->post('master_id');
    }

    public function journal_preview($id = NULL)
    {

        $data['title'] = 'POS System';
        $data['master'] = $this->MAc_journal_master->get_by_id($id);
        $data['details'] = $this->MAc_journal_details->get_by_journal_no($id);
        $data['privileges'] = $this->privileges;

        $this->load->view('admin/accounts/journal/preview', $data);
    }


    public function journal_preview_after_complete($id = NULL)
    {

        $data['title'] = 'POS System';
        $data['master'] = $this->MAc_journal_master->get_by_id($id);
        $data['details'] = $this->MAc_journal_details->get_by_journal_no($id);
        $data['privileges'] = $this->privileges;

        $this->load->view('admin/accounts/journal/preview_complete', $data);
    }

    public function journal_preview_after_purchase($id = NULL)
    {

        $data['title'] = 'POS System';
        $data['master'] = $this->MAc_journal_master->get_by_id($id);
        $data['details'] = $this->MAc_journal_details->get_by_journal_no($data['master']['journal_no']);
        $data['privileges'] = $this->privileges;

        $this->load->view('admin/accounts/journal/purchase_preview', $data);
    }

    public function debit_add()
    {

        $voucher_type = $this->input->post('voucher_type');
        $master = $this->MAc_journal_master->get_by_journal_no($this->input->post('journal_no'), $voucher_type);
        if (count($master) > 0) {
            // $this->MAc_journal_master->update($master['id']);
            $master_id = $master['id'];
        } else {

            $master_id = $this->MAc_journal_master->create($voucher_type);
        }
        $this->MAc_journal_details->create($this->input->post('debit_chart_id'), $this->input->post('debit_amount'), NULL, $this->input->post('debit_memo'), $master_id);
        $debit = $this->voucher_table($master_id, 'debit');
        echo $debit;
    }

    public function update_voucher($type)
    {
        $this->MAc_journal_details->update_memo($this->input->post('voucher_id'), $this->input->post('memo'));
        $details = $this->voucher_table($this->input->post('master_id'), $type);
        echo $details;
    }

    public function credit_add()
    {
        $master = $this->MAc_journal_master->get_by_journal_no($this->input->post('journal_no'), $this->input->post('voucher_type'));
        if (count($master) > 0) {
            // $this->MAc_journal_master->update($master['id']);
            $master_id = $master['id'];
        } else {
            $master_id = $this->MAc_journal_master->create();
        }
        $this->MAc_journal_details->create($this->input->post('credit_chart_id'), NULL, $this->input->post('credit_amount'), $this->input->post('credit_memo'), $master_id);
        $credit = $this->voucher_table($master_id, 'credit');
        echo $credit;
    }

    public function delete_voucher($type)
    {
        $this->MAc_journal_details->delete($this->input->post('voucher_id'));
        $details = $this->voucher_table($this->input->post('master_id'), $type);
        echo $details;
    }


    public function delete_journal($id)
    {
        $this->MAc_journal_master->delete($id);
        echo json_encode(true);
    }

    public function voucher_table($master_id, $type)
    {
        $details = $this->MAc_journal_details->get_by_journal_no($master_id);
        if ($type == 'debit') {
            $result = '<input type="hidden" value="' . $master_id . '" id="hidden_master_id"/>
            <table class="table table-bordered table-striped responsive">
                    <thead>
                        <tr>
                            <th class="center">Debit A/C Head</th>
                            <th class="center">Amount</th>
                            <th class="center">Memo</th>
                            <th class="center">Action</th>
                        </tr>
                    </thead>
                    <tbody>';
            $debit_amount = 0;
            foreach ($details as $list) {
                if ($list['debit']) {
                    $result .= '<tr>
                            <td>' . $list['chart_name'] . '</td>
                            <td class="center">' . $list['debit'] . '</td>
                            <td class="center">
                            <input name="memo" id="memo" type="text" value="' . $list['memo'] . '" /></td>
                            
                            <td class="center">
                                
                                <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger debit_voucher_delete"><i class="icon-trash icon-white"></i>Delete</span>
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn btn-primary debit_voucher_update"><i class="icon-info icon-white"></i>Update</span>
                            </td>
                        </tr>';
                    $debit_amount += $list['debit'];
                }
            }

            $result .= '</tbody>
                    <tfoot>
                        <tr>
                            <th class="left" colspan="4">&nbsp;</th>
                        </tr>
                        <tr>
                            <th colspan="2">Total </th>
                            <th colspan="2" class="right" id="debit_total">' . ($debit_amount) . '</th>
                        </tr>
                    </tfoot>
                </table>';
        } elseif ($type == 'credit') {
            $result = '<table class="table table-bordered table-striped responsive">
                <thead>
                    <tr>
                        <th class="center">Debit A/C Head</th>
                        <th class="center">Amount</th>
                        <th class="center">Memo</th>
                        <th class="center">Action</th>
                    </tr>
                </thead>
                <tbody>';
            $credit_amount = 0;
            foreach ($details as $list) {
                if ($list['credit']) {
                    $result .= '<tr>
                        <td>' . $list['chart_name'] . '</td>
                        <td class="center">' . $list['credit'] . '</td>
                        <td class="center"><input name="memo" id="memo" type="text" value="' . $list['memo'] . '" /></td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger credit_voucher_delete"><i class="icon-trash icon-white"></i>Delete</span>
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn btn-info credit_voucher_update"><i class="icon-primary icon-white"></i>Update</span>
                        </td>
                    </tr>';
                    $credit_amount += $list['credit'];
                }
            }

            $result .= '</tbody>
                <tfoot>
                    <tr>
                        <th class="left" colspan="4">&nbsp;</th>
                    </tr>
                    <tr>
                        <th colspan=2>Total </th>
                        <th colspan="2" class="right" id="credit_total">' . ($credit_amount) . '</th>
                    </tr>
                </tfoot>
            </table>';
        }

        return $result;
    }

    /* --------------- Voucher Part End ----------------- */

    /* -------------- Money Receipt Start ------------------ */

    public function mr_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'accounts';
        $data['content'] = 'admin/accounts/mr/list';
        $data['mrs'] = $this->MAc_money_receipts->get_all();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function mr_preview($id = NULL)
    {
        $data['title'] = 'POS System';
        $data['details'] = $this->MAc_money_receipts->get_by_id($id);
        $data['customer'] = $this->MCustomers->get_by_id($data['details']['customer_id']);
        $data['emp'] = $this->MEmps->get_by_id($data['details']['emp_id']);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/accounts/mr/preview', $data);
    }

    public function mr_save($id = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        if ($this->input->post()) {
            if ($this->input->post('id') == "") {
                $this->MAc_money_receipts->create();
                //Auto journal entry on money receipt entry
                $journal_no = 'mr_' . $this->input->post('mr_no');
                $this->MAc_journal_master->create_by_mr($journal_no, $this->input->post('mr_no'));

                $customer = $this->MCustomers->get_by_id($this->input->post('customer_id'));
                $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
                $this->MAc_journal_details->auto_create($journal_no, $customer['ac_id'], NULL, $this->input->post('amount'), $this->input->post('mr_date'));
                //Cash debit or bank debit
                if ($this->input->post('payment_type') == 'cash') {
                    $this->MAc_journal_details->auto_create($journal_no, $settings['ac_cash'], $this->input->post('amount'), NULL, $this->input->post('mr_date'));
                } else {
                    $this->MAc_journal_details->auto_create($journal_no, $settings['ac_bank'], $this->input->post('amount'), NULL, $this->input->post('mr_date'));
                }
            } else {
                $this->MAc_money_receipts->update();
                //Auto journal update on money receipt update
                $journal = $this->MAc_journal_master->get_by_doc('Receive', $this->input->post('mr_no'), $stat_date, $en_date);
                if (count($journal) > 0) {
                    $journal_no = $journal['journal_no'];
                    $this->MAc_journal_details->delete_by_journal_no($journal_no, $stat_date, $en_date);
                    $customer = $this->MCustomers->get_by_id($this->input->post('customer_id'));
                    $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
                    $this->MAc_journal_details->auto_create($journal_no, $customer['ac_id'], NULL, $this->input->post('amount'), $this->input->post('mr_date'));
                    //Cash debit or bank debit
                    if ($this->input->post('payment_type') == 'cash') {
                        $this->MAc_journal_details->auto_create($journal_no, $settings['ac_cash'], $this->input->post('amount'), NULL, $this->input->post('mr_date'));
                    } else {
                        $this->MAc_journal_details->auto_create($journal_no, $settings['ac_bank'], $this->input->post('amount'), NULL, $this->input->post('mr_date'));
                    }
                }
            }

            $this->session->set_flashdata('message', 'Money Receipt saved.');
            redirect('accounts/mr_list', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'accounts';
            $data['content'] = 'admin/accounts/mr/save';
            $mr = $this->MAc_money_receipts->get_latest($stat_date, $en_date);
            if (count($mr) > 0) {
                $data['mr_no'] = (int) $mr['mr_no'] + 1;
            } else {
                $data['mr_no'] = 1001;
            }
            $data['mr'] = $this->MAc_money_receipts->get_by_id($id);
            $data['customers'] = $this->MCustomers->get_all();
            $data['emps'] = $this->MEmps->get_all();
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function mr_delete($id)
    {
        $mr = $this->MAc_money_receipts->get_by_id($id);
        //Remove auto created journal
        $journal = $this->MAc_journal_master->get_by_doc('Receive', $mr['mr_no']);
        if (count($journal) > 0) {
            $this->MAc_journal_details->delete_by_journal_no($journal['journal_no']);
            $this->MAc_journal_master->delete_by_journal_no($journal['journal_no']);
        }
        //Remove money receipt
        $this->MAc_money_receipts->delete($id);
        redirect('accounts/mr_list', 'refresh');
    }

    /* -------------- Money Receipt End -------------------- */

    /* -------------- Payment Receipt Start ------------------ */

    public function payment_list()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $data['title'] = 'POS System';
        $data['menu'] = 'accounts';
        $data['content'] = 'admin/accounts/payment/list';
        $data['payments'] = $this->MAc_payment_receipts->get_all($stat_date, $en_date);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function payment_preview($id = NULL)
    {
        $data['title'] = 'POS System';
        $data['details'] = $this->MAc_payment_receipts->get_by_id($id);
        $data['supplier'] = $this->MSuppliers->get_by_id($data['details']['supplier_id']);
        $data['emp'] = $this->MEmps->get_by_id($data['details']['emp_id']);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/accounts/payment/preview', $data);
    }

    public function payment_save($id = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        if ($this->input->post()) {
            if ($this->input->post('id') == "") {
                $this->MAc_payment_receipts->create();
                //Auto journal entry on payment receipt entry
                $journal_no = 'payment_' . $this->input->post('payment_no');
                $this->MAc_journal_master->create_by_payment($journal_no, $this->input->post('payment_no'));

                $supplier = $this->MSuppliers->get_by_id($this->input->post('supplier_id'));
                $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
                $this->MAc_journal_details->auto_create($journal_no, $supplier['ac_id'], $this->input->post('amount'), NULL, $this->input->post('payment_date'));
                //Cash credit or bank credit
                if ($this->input->post('payment_type') == 'cash') {
                    $this->MAc_journal_details->auto_create($journal_no, $settings['ac_cash'], NULL, $this->input->post('amount'), $this->input->post('payment_date'));
                } else {
                    $this->MAc_journal_details->auto_create($journal_no, $settings['ac_bank'], NULL, $this->input->post('amount'), $this->input->post('payment_date'));
                }
            } else {
                $this->MAc_payment_receipts->update();
                //Auto journal update on payment receipt update
                $journal = $this->MAc_journal_master->get_by_doc('Payment', $this->input->post('payment_no'), $stat_date, $en_date);
                if (count($journal) > 0) {
                    $journal_no = $journal['journal_no'];
                    $this->MAc_journal_details->delete_by_journal_no($journal_no, $stat_date, $en_date);
                    $supplier = $this->MSuppliers->get_by_id($this->input->post('supplier_id'));
                    $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
                    $this->MAc_journal_details->auto_create($journal_no, $supplier['ac_id'], $this->input->post('amount'), NULL, $this->input->post('payment_date'));
                    $this->MAc_journal_details->auto_create($journal_no, $settings['ac_cash'], NULL, $this->input->post('amount'), $this->input->post('payment_date'));
                }
            }
            $this->session->set_flashdata('message', 'Customer created');
            redirect('accounts/payment_list', 'refresh');
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'accounts';
            $data['content'] = 'admin/accounts/payment/save';
            $payment = $this->MAc_payment_receipts->get_latest($stat_date, $en_date);
            if (count($payment) > 0) {
                $data['payment_no'] = (int) $payment['payment_no'] + 1;
            } else {
                $data['payment_no'] = 1001;
            }
            $data['payment'] = $this->MAc_payment_receipts->get_by_id($id);
            $data['suppliers'] = $this->MSuppliers->get_all();
            // $data['emps'] = $this->MEmps->get_all();
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }
    public function payment_delete($id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $payment = $this->MAc_payment_receipts->get_by_id($id);
        //Remove auto created journal
        $journal = $this->MAc_journal_master->get_by_doc('Payment', $payment['payment_no'], $stat_date, $en_date);
        if (count($journal) > 0) {
            $this->MAc_journal_details->delete_by_journal_no($journal['journal_no'], $stat_date, $en_date);
            $this->MAc_journal_master->delete_by_journal_no($journal['journal_no'], $stat_date, $en_date);
        }
        //Remove payment receipt
        $this->MAc_payment_receipts->delete($id);
        redirect('accounts/payment_list', 'refresh');
    }

    /* -------------- Payment Receipt End -------------------- */
}
