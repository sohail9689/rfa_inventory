<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Supplier
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
                    <li>
                        <a href="inventory/supplier_list">Supplier list</a>
                        <span class="divider">/</span>
                    </li>
					<li class="active">
						<?php if (count($supplier) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Supplier
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/supplier_save" method="post" class="form-horizontal form-bordered form-validate" id="frm1">
                            <div class="control-group">
                                <label for="code" class="control-label">Supplier Code</label>
                                <div class="controls">
                                    <input type="text" name="code" value="<?php if (count($supplier) > 0) { echo $supplier['code']; } else { echo $code; } ?>" id="code" class="span5" placeholder="Supplier Code"> Digit Only
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="name" class="control-label">Supplier Name</label>
                                <div class="controls">
                                    <input type="text" name="name" value="<?php if (count($supplier) > 0) { echo $supplier['name']; } ?>" id="name" class="span5" placeholder="Supplier Name">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="country" class="control-label">GST</label>
                                <div class="controls">
                                    <input type="text" name="gst" id="gst" value="<?php if(count($supplier) > 0){ echo $supplier['gst']; } ?>" class="span5" placeholder="GST">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="mobile" class="control-label">NTN No</label>
                                <div class="controls">
                                    <input type="text" name="ntn" id="ntn" value="<?php if(count($supplier) > 0){ echo $supplier['ntn']; } ?>" class="span5" placeholder="NTN No">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="email" class="control-label">Email</label>
                                <div class="controls">
                                    <input type="text" name="email" id="email" value="<?php if(count($supplier) > 0){ echo $supplier['email']; } ?>" class="span5" placeholder="Supplier Email Address">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="address" class="control-label">Address</label>
                                <div class="controls">
                                    <textarea type="text" name="address" id="address" class="span5" placeholder="Supplier Address"><?php if (count($supplier) > 0) { echo $supplier['address']; } ?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="contact_person" class="control-label">Contact Person</label>
                                <div class="controls">
                                    <input type="text" name="contact_person" value="<?php if (count($supplier) > 0) { echo $supplier['contact_person']; } ?>" id="contact_person" class="span5" placeholder="Contact Person">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="phone_no" class="control-label">Phone No</label>
                                <div class="controls">
                                    <input type="text" name="phone_no" value="<?php if (count($supplier) > 0) { echo $supplier['phone_no']; } ?>" id="phone_no" class="span5" placeholder="Phone No">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="notes" class="control-label">Notes</label>
                                <div class="controls">
                                    <textarea type="text" name="notes" id="notes" class="span5" placeholder="Supplier Notes"><?php if (count($supplier) > 0) { echo $supplier['notes']; } ?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Status</label>
                                <div class="controls">
                                    <select name="status" id="status" class="span5 chzn-select status_" data-placeholder="Select Status">
                                        <option value="Active" <?php if (count($supplier) > 0 && $supplier['status'] == 'Active') { echo 'selected'; }?>>Active</option>
                                        <option value="Inactive" <?php if (count($supplier) > 0 && $supplier['status'] == 'Inactive') { echo 'selected'; }?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <?php if (count($supplier) > 0) { ?>
                            <input type="hidden" name="id" value="<?php echo $supplier['id']; ?>" />
                            <?php } ?>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" value="Save Changes">
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>

<script>
  $('#code').focus();
</script>