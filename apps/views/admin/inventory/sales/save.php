<script type="text/javascript" src="assets/backend/js/select2.min.js"></script>
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Sales
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/sales_list">Sales List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($sales) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Sales
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if ($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
			<!-- BEGIN Alert widget-->
			<div class="row-fluid">
				<div class="span12">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('info')) { ?>
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('error')) { ?>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
			<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form name="sales" action="inventory/sales_save" method="post" class="form-horizontal">
							<div class="span6">
								<div class="control-group">
									<label class="control-label">Sales No</label>
									<div class="controls">
										<input type="text" name="sales_no" id="sales_no" class="required" readonly tabindex="1" class="span10" value="<?php if (count($sales) > 0) {
																																							echo $sales['sales_no'];
																																						} else {
																																							echo $sales_no;
																																						} ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Select Item</label>
									<div class="controls">
										<select name="product_id" id="product_id" tabindex="3" class="span10 chzn-select required product_id" data-form="select2">

										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Quantity (pcs)</label>
									<div class="controls">
										<input type="number" name="quantity" id="quantity" tabindex="5" class="span5 required" placeholder="0" />
										<input type="text" id="avail_quantity" tabindex="5" class="span5" placeholder="Available QTY" readonly />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Price per unit</label>
									<div class="controls">
										<input type="number" name="price" id="price" tabindex="6" class="span10" placeholder="Keep Blank for Default Price" data-validate="{required: true, messages:{required:'Please enter field required'}}" />
									</div>
								</div>
							</div>

							<div class="span6">
								<div class="control-group">
									<label class="control-label">Sales Date</label>
									<div class="controls">
										<div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
											<input name="sales_date" id="sales_date" tabindex="2" data-form="datepicker" size="16" type="text" value="<?php if (count($sales) > 0) {
																																							echo date_to_ui($sales['sales_date']);
																																						} else {
																																							echo date('d/m/Y');
																																						} ?>">
											<span class="add-on"><i class="icon-th"></i></span>
										</div>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Select Customer</label>
									<div class="controls">
										<select name="customer_id" id="customer_id" tabindex="4" class="span10 chzn-select customer_id">
											<?php if (count($sales) > 0) { ?>
												<option value="<?php echo $sales['customer_id']; ?>"><?php echo $sales['name']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Description</label>
									<div class="controls">
										<textarea name="des" id="des" tabindex="7" class="span10" rows="4" data-form="wysihtml5" placeholder="Description"><?php if (count($sales) > 0) {
																																								echo $sales['notes'];
																																							} ?></textarea>
									</div>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if (count($sales) > 0) { ?>
								<input type="hidden" name="id" value="<?php echo $sales['id']; ?>" />
							<?php } ?>
							<div style="clear: both;"></div>
							<div class="center">
								<input type="button" class="btn btn-success center" id="sales_add_item" tabindex="8" value="Add Item" />
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Item List</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<div id="sales_details">
							<table id="sample_1" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th class="center">Item Code</th>
										<th class="center">Item Name</th>
										<th class="center">Quantity (pcs)</th>
										<th class="center">Sales Price</th>
										<th class="center">Total Price</th>
										<th class="span3 center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$qty = 0;
									$price = 0;
									foreach ($details as $list) {
									?>
										<tr>
											<td><?php echo $list['item_code']; ?></td>
											<td><?php echo $list['item_name']; ?></td>
											<td class="center"><?php echo $list['quantity']; ?></td>
											<td class="center"><?php echo $list['sale_price']; ?></td>
											<td class="right"><?php echo round($list['quantity'] * $list['sale_price']); ?></td>
											<td class="center">
												<input type="hidden" value="<?php echo $list['id']; ?>" /><span class="btn del btn-danger sales_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
											</td>
										</tr>
									<?php
										$qty += $list['quantity'];
										$price += round($list['quantity'] * $list['sale_price']);
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th class="left" colspan="5">Order Totals</th>
									</tr>
									<tr>
										<td colspan=2>&nbsp;</td>
										<td class="center"><?php echo $qty; ?></td>
										<td></td>
										<td class="right"><input type="hidden" id="master_id" value="<?php echo $details[0]['master_id']; ?>"><input type="hidden" id="totl_price" value="<?php echo $price; ?>"><?php echo $price; ?></td>
										<td></td>
									</tr>
									<!-- <tr>
										<td colspan="4" class="right"> Total Paid Amount</td>
										<td class="right"><input type="text" name="paid_amount" id="paid_amount"></td>
										<td></td>
									</tr> -->
								</tfoot>
							</table>
						</div>
					</div>
					<div class="form-actions center">
						<input type="button" class="btn btn-success" id="sales_complete" value="Complete Sales Entry" />
					</div>
					<?php $accounts_date = $this->MItems->get_dates();
					$stat_date = date_to_ui($accounts_date['start_date']);
					$en_date = date_to_ui($accounts_date['end_date']);
					?>
					<input type="hidden" name="" id="stat_date" value="<?php echo $stat_date; ?>" />
					<input type="hidden" name="" id="en_date" value="<?php echo $en_date; ?>" />
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>


<script>
	$('.product_id').select2({
		placeholder: '--- Select Item ---',
		ajax: {
			url: "inventory/search_all_items",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
	$('.customer_id').select2({
		placeholder: '--- Select Customer ---',
		ajax: {
			url: "inventory/search_customers",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
	$(document).ready(function() {
		// Initialize the plugin
		$('#my_popup').popup({
			focusdelay: 400,
			outline: true,
			vertical: 'top'
		});
		var stat_date = $('#stat_date').val();
		var en_date = $('#en_date').val();
		$('[data-form=datepicker]').datepicker({
			autoclose: true,
			startDate: stat_date,
			endDate: en_date,
			format: 'dd/mm/yyyy'
		});

		$("#product_id").change(function() {
			var product_id = $(this).find("option:selected").val();
			$.ajax({
				url: "inventory/get_product_data",
				type: "POST",
				data: {
					product_id: product_id
				},
				success: function(data) {
					var res = $.parseJSON(data);
					// $('#avail_quantity').val(res['total_quantity']);
					$('#avail_quantity').val(res);
				}

			})
		});
	});

	$('#sales_no').focus();
	// $.validate();
</script>