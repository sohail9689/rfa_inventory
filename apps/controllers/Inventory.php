<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Inventory extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        if (!$this->session->userdata('user_id')) {
            redirect('', 'refresh');
        }
        $this->privileges = $this->MUser_privileges->get_by_ref_user($this->session->userdata('user_id'));

        eval(base64_decode("aWYoISR0aGlzLT5hY2NvdW50X3ByaXZpbGVnZXMtPmdldF9kYXRlKGRhdGUoJ1ktbS1kJykpKXsKICAgICAgICAgICAgICAgICR0aGlzLT5zZXNzaW9uLT5zZXRfZmxhc2hkYXRhKCd1c2VyX3ByaXZpbGVnZXMnLCdZb3VyIE1vbnRobHkgbGljZW5zZSBoYXMgYmVlbiBleHBpcmVkISBQbGVhc2UgQ29udGFjdCA8YSBocmVmPSJodHRwOi8vY3liZXh0ZWNoLmNvbS9jb250YWN0dXMuaHRtbCIgdGFyZ2V0PSJfYmxhbmsiPkN5YmV4IFRlY2g8L2E+LicpOwogICAgICAgICAgICAgICAgcmVkaXJlY3QgKCdkYXNoYm9hcmQnKTsKICAgICAgICAgICAgfQ=="));
        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        $this->output->set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    public function index()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/home';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function sales_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/sales/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function sales_list_data()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_master.sales_no', $search, 'both');
            $this->db->or_like('sales_master.sales_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_master.id as master_id, sales_master.sales_no, sales_master.sales_date,customers.name as customer_name, sum(sales_details.quantity) as item_qty, sum(sales_details.price_total) as price_total, sum(sales_details.total_profit) as total_profit');
        $this->db->from("sales_master");
        $this->db->join("customers", "sales_master.customer_id = customers.id", "left");
        $this->db->join("sales_details", "sales_master.id = sales_details.master_id", "left");
        $this->db->where('sales_master.sales_date >= ', $stat_date);
        $this->db->where('sales_master.sales_date <= ', $en_date);
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));

        $total = $this->db->get()->num_rows();

        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'sales_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_master.sales_no', $search, 'both');
            $this->db->or_like('sales_master.sales_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_master.id as master_id, sales_master.sales_no, sales_master.sales_date,customers.name as customer_name, sum(sales_details.quantity) as item_qty, sum(sales_details.price_total) as price_total, sum(sales_details.total_profit) as total_profit');
        $this->db->from("sales_master");
        $this->db->join("customers", "sales_master.customer_id = customers.id", "left");
        $this->db->join("sales_details", "sales_master.id = sales_details.master_id", "left");
        $this->db->where('sales_master.sales_date >= ', $stat_date);
        $this->db->where('sales_master.sales_date <= ', $en_date);
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("sales_master.sales_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {

            $res = array(
                'sales_no' => '',
                'sales_date' => '',
                'customer_name' => '',
                'item_qty' => '',
                'price_total' => '',
                'total_profit' => '',
                'option' => ''
            );

            $res['sales_no'] = "<a href=\"inventory/sales_preview/" . $row['master_id'] . "\"  target=\"_blank\">" . $row['sales_no'] . "</a>";
            $res['sales_date'] = date('Y-m-d', strtotime($row['sales_date']));
            $res['customer_name'] = $row['customer_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['price_total'] = $row['price_total'];
            $res['total_profit'] = $row['total_profit'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = " <a  class=\"btn btn-edit\" href=\"inventory/sales_save/" . $row['master_id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['master_id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            } else {
                $res['options'] = "";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function get_ac_chart()
    {
        $json = [];
        if (($this->input->get("q") != "")) {
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(name, " (" , code, ")") as text')
                ->limit(10)
                ->get("ac_charts");
            $json = $query->result();
        }

        $this->output->set_status_header(200, 'OK')
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        // echo json_encode($json);
    }


    public function search_all_items()
    {
        $json = [];
        if (($this->input->get("q") != "")) {
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                ->limit(10)
                ->get("items");
            $json = $query->result();
        }

        $this->output->set_status_header(200, 'OK')
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        // echo json_encode($json);
    }
    public function search_customers()
    {
        $json = [];
        if (($this->input->get("q") != "")) {
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                ->limit(10)
                ->get("customers");
            $json = $query->result();
        }

        $this->output->set_status_header(200, 'OK')
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        // echo json_encode($json);
    }

    public function search_suppliers()
    {
        $json = [];
        if (($this->input->get("q") != "")) {
            $this->db->group_start();
            $this->db->like('name', $this->input->get("q"));
            $this->db->or_like('code', $this->input->get("q"));
            $this->db->group_end();
            $this->db->where('status', 'Active');
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $query = $this->db->select('id, CONCAT(code, " " , name) as text')
                ->limit(10)
                ->get("suppliers");
            $json = $query->result();
        }

        $this->output->set_status_header(200, 'OK')
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        // echo json_encode($json);
    }

    public function get_product_stock()
    {

        $stock = $this->MReports->get_stock_balance($this->input->post('product_id'));
        echo json_encode($stock);
    }
    public function get_product_data()
    {

        $this->db->where('id', $this->input->post('product_id'));
        $item_data = $this->db->get('items')->row_array();
        echo json_encode($item_data['qty']);
    }

    public function sales_save($id = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        if ($this->input->post()) {

            $sales = $this->MSales_master->get_data_by_master($this->input->post('sales_no'), $stat_date, $en_date);
            if (count($sales) > 0) {
                $this->MSales_master->update($sales[0]['id']);
                $master_id =  $sales[0]['id'];
            } else {
                $master_id = $this->MSales_master->create();
            }
            $this->MSales_details->create($master_id);
            $msg = $this->sales_table($master_id);
            echo $msg;
        } else {
            if ($id) {
                $data['sales'] = $this->MSales_master->get_by_id($id);
                $data['details'] = $this->MSales_details->get_by_sales_no($id);
            } else {
                $data['details'] = array();
                $data['sales'] = NULL;
                $data['customers'] = NULL;
                $data['sales_no'] = $this->MSales_master->get_last_sales_no($stat_date, $en_date);
            }
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/sales/save';

            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function sales_preview($id)
    {

        $data['title'] = 'Sales Invoice Preview';
        $data['master'] = $this->MSales_master->get_by_id($id);
        $data['details'] = $this->MSales_details->get_by_sales_no($id);
        $data['privileges'] = $this->privileges;
        // $number = 1223588;
        // echo $this->numbertowords->convert_number($number);
        // die();
        $this->load->spark('barcodegen/0.0.1');
        $this->load->view('admin/inventory/sales/preview', $data);
        // print_r($data['details']);
        // die();
    }

    public function sales_complete()
    {

        $this->MSales_master->update($this->input->post('id'));
        echo 'inventory/sales_save/';
    }

    public function sales_delete($id)
    {


        $details = $this->MSales_details->get_by_master_id(trim($id));
        $this->MSales_master->delete($id);
        foreach ($details as $row) {
            $this->db->where('id', $row['item_id']);
            $item_data = $this->db->get('items')->row_array();
            $current_qty = $item_data['qty'] + $row['quantity'];
            $this->MItems->update_field(trim($row['item_id']), 'qty', $current_qty);
        }


        echo json_encode(true);
    }

    public function sales_item_delete()
    {
        $details = $this->MSales_details->get_by_id(trim($this->input->post('detail_id')));
        $this->MSales_details->delete($this->input->post('detail_id'));
        $msg = $this->sales_table($this->input->post('master_id'));

        $this->db->where('id', $details['item_id']);
        $item_data = $this->db->get('items')->row_array();
        $current_qty = $item_data['qty'] + $details['quantity'];
        $this->MItems->update_field(trim($details['item_id']), 'qty', $current_qty);

        echo $msg;
    }

    public function sales_table($master_id)
    {

        $details = $this->MSales_details->get_by_sales_no($master_id);

        $msg = '
        <table id="sample_1" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center" >Quantity (pcs)</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total Price</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
        $msg .= '<tbody>';
        $qty = 0;
        $price = 0;
        if (count($details) > 0) {
            foreach ($details as $list) {
                $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="right">' . $list['quantity'] . '</td>
                        <td class="right">' . $list['sale_price'] . '</td>
                        <td class="right">' . round($list['quantity'] * $list['sale_price']) . '</td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger sales_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                $qty += $list['quantity'];
                $price += round($list['quantity'] * $list['sale_price']);
            }
        }
        $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="5">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="right">' . $qty . '</td>
                    <td></td>
                    <td class="right"><input type="hidden" id="master_id" value="' . $master_id . '"><input type="hidden" id="totl_price" value="' . $price . '">' . $price . '</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }

    public function sales_return_list()
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/sales_return/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function sales_return_list_data()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_return_master.sales_return_no', $search, 'both');
            $this->db->or_like('sales_return_master.sales_return_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_return_master.id as master_id, sales_return_master.sales_return_no, sales_return_master.sales_return_date,customers.name as customer_name, sum(sales_return_details.quantity) as item_qty, sum(sales_return_details.sq_weight) as item_area, sum(sales_return_details.price_total) as price_total');
        $this->db->from("sales_return_master");
        $this->db->join("customers", "sales_return_master.customer_id = customers.id", "left");
        $this->db->join("sales_return_details", "sales_return_master.sales_return_no = sales_return_details.sales_return_no", "left");
        $this->db->where('sales_return_master.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_master.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_return_details.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_details.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_details.company_id', $this->session->userdata('user_company'));

        $total = $this->db->get()->num_rows();

        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'sales_return_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('sales_return_master.sales_return_no', $search, 'both');
            $this->db->or_like('sales_return_master.sales_return_date', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('sales_return_master.ac_id, sales_return_master.id as master_id, sales_return_master.sales_return_no, sales_return_master.sales_return_date,customers.name as customer_name, sum(sales_return_details.quantity) as item_qty, sum(sales_return_details.sq_weight) as item_area, sum(sales_return_details.price_total) as price_total');
        $this->db->from("sales_return_master");
        $this->db->join("customers", "sales_return_master.customer_id = customers.id", "left");
        $this->db->join("sales_return_details", "sales_return_master.sales_return_no = sales_return_details.sales_return_no", "left");
        $this->db->where('sales_return_master.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_master.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_return_details.sales_return_date >= ', $stat_date);
        $this->db->where('sales_return_details.sales_return_date <= ', $en_date);
        $this->db->where('sales_return_details.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("sales_return_master.sales_return_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {

            $res = array(
                'sales_return_no' => '',
                'voucher' => '',
                'sales_return_date' => '',
                'customer_name' => '',
                'item_qty' => '',
                'item_area' => '',
                'price_total' => '',
                'option' => ''
            );

            $res['sales_return_no'] = "<a href=\"inventory/sales_return_preview/" . $row['sales_return_no'] . "\"  target=\"_blank\">" . $row['sales_return_no'] . "</a>";
            $res['voucher'] = "<a href=\"accounts/journal_preview/" . $row['ac_id'] . "\"  target=\"_blank\">" . "Delivery_Return_" . $row['sales_return_no'] . "</a>";
            $res['sales_return_date'] = date('Y-m-d', strtotime($row['sales_return_date']));
            $res['customer_name'] = $row['customer_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['item_area'] = $row['item_area'];
            $res['price_total'] = $row['price_total'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = " <a  class=\"btn btn-edit\" href=\"inventory/sales_return_save/" . $row['master_id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['master_id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            } else {
                $res['options'] = "";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function sales_return_save($id = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        if ($this->input->post()) {
            $sales_return = $this->MSales_return_master->get_by_sales_return_no($this->input->post('sales_return_no'), $stat_date, $en_date);
            if (count($sales_return) > 0) {
                $this->MSales_return_master->update($sales_return[0]['id']);
            } else {
                $this->MSales_return_master->create();
            }
            //$stock = $this->MReports->get_stock_balance( $this->input->post( 'item_id' ) );
            $item = $this->MItems->get_by_id($this->input->post('item_id'));
            $this->MSales_return_details->create($this->input->post('sales_return_no'), $item);
            $msg = $this->sales_return_table($this->input->post('sales_return_no'), $stat_date, $en_date);
            //echo json_encode( array ( 'success' => $msg ) );
            echo $msg;
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/sales_return/save';
            $sales_return = $this->MSales_return_master->get_last_sales_return_no($stat_date, $en_date);
            if (count($sales_return) > 0) {
                $data['sales_return_no'] = (int) $sales_return['sales_return_no'] + 1;
            } else {
                $data['sales_return_no'] = 1001;
            }
            // $data['customers'] = $this->MCustomers->get_all();
            // $data['items'] = $this->MItems->get_all('active', 'FG');
            if ($id) {
                $data['sales_return'] = $this->MSales_return_master->get_by_id($id, $stat_date, $en_date);
                $sales_return_no = $data['sales_return']['sales_return_no'];
                $data['customers'] = $this->MCustomers->get_by_id($data['sales_return']['customer_id']);
            } else {
                $sales_return_no = $data['sales_return_no'];
                $data['sales_return'] = NULL;
                $data['customers'] = NULL;
            }
            // $customer = $this->MCustomers->get_latest();
            // if (count($customer) > 0)
            // {
            //     $data['code'] = (int)$customer['code'] + 1;
            // }
            // else
            // {
            //     $data['code'] = 1001;
            // }
            $data['details'] = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function sales_return_preview($sales_return_no = NULL)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $data['title'] = 'Sales Return Invoice Preview';
        $data['master'] = $this->MSales_return_master->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $data['details'] = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $data['company'] = $this->MCompanies->get_by_id($data['master'][0]['company_id']);
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/sales_return/preview', $data);
    }

    public function sales_return_complete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];

        $sales_return_no = trim($this->input->post('sales_return_no'));
        $sales_return_date = trim($this->input->post('sales_return_date'));

        $sales_return = $this->MSales_return_master->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        $this->MSales_return_master->update($sales_return[0]['id']);

        //Check if previous sales journal exist then Remove sales journal details else make new sales journal master
        $sales_journal = $this->MAc_journal_master->get_by_doc('Delivery Return', $sales_return_no, $stat_date, $en_date);
        if (count($sales_journal) > 0) {
            $insert_id = $sales_journal['id'];
            $sales_journal_no = $sales_journal['journal_no'];
            $this->MAc_journal_details->delete_by_journal_no($sales_journal_no, $stat_date, $en_date);
            $this->MAc_journal_master->delete($insert_id);
        } else {
            $sales_journal_no = 'Delivery_Return_' . $sales_return_no;
        }
        $insert_id = $this->MAc_journal_master->create_by_doc($sales_journal_no, $sales_return_date, 'Delivery Return', 'Delivery Return', $sales_return_no);

        // }

        $this->MSales_return_master->update_voucher_id($sales_return[0]['id'], $insert_id);
        $settings = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));

        //Determine Credit or Cash Sale and make journal
        $total_sales = $this->MSales_return_details->get_total_price_by_sales_return_no($sales_return_no, $stat_date, $en_date);
        // $customer = $this->MCustomers->get_by_id($this->input->post('customer_id'));
        $details = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);

        // $total_paid = $this->input->post('paid_amount');
        // $dues = (float)$total_sales - (float)$total_paid;

        // if ($dues == 0)
        // {
        //     $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cash'], NULL, $total_sales, $sales_return_date, $this->input->post('notes'));

        // }
        // else
        // {
        $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cogs'], NULL, $total_sales, $sales_return_date, $this->input->post('notes'));
        //     if ((float)$total_paid != 0)
        //     {
        //         $this->MAc_journal_details->create_by_inventory($sales_journal_no, $settings['ac_cash'], NULL, $total_paid, $sales_return_date, $this->input->post('notes'));
        //     }

        // }

        for ($i = 0; $i < sizeof($details); $i++) {
            // if($details[$i]['vat_amount'] <> 0){
            //     $this->MAc_journal_details->create_by_inventory($journal_no, $details[$i]['ac_id'], $details[$i]['total_price'] - $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
            //     $this->MAc_journal_details->create_by_inventory($journal_no, $settings['ac_tax'], $details[$i]['vat_amount'], NULL, $this->input->post('purchase_date'), $this->input->post('notes'));
            // }else{
            $this->MAc_journal_details->create_by_inventory($sales_journal_no, $details[$i]['ac_id'], $details[$i]['price_total'], NULL, $sales_return_date, $this->input->post('notes'));
            // }

        }


        echo 'inventory/sales_return_save/';
    }

    public function sales_return_delete($id)
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $sales_return = $this->MSales_return_master->get_by_id($id, $stat_date, $en_date);
        //Remove auto created sales journal
        $journal = $this->MAc_journal_master->get_by_doc('Delivery Return', $sales_return['sales_return_no'], $stat_date, $en_date);
        if (count($journal) > 0) {
            $this->MAc_journal_details->delete_by_journal_no($journal['journal_no'], $stat_date, $en_date);
            $this->MAc_journal_master->delete_by_journal_no($journal['journal_no'], $stat_date, $en_date);
        }
        //Remove auto created money receipt for partial or full cash sales
        // $mr = $this->MAc_money_receipts->get_by_doc( 'Sales', $sales['sales_no'] );
        // if( count($mr) > 0 ){
        //     $this->MAc_money_receipts->delete( $mr['id'] );
        // }
        //Remove sales
        $this->MSales_return_details->delete_by_sales_return_no($sales_return['sales_return_no'], $stat_date, $en_date);
        $this->MSales_return_master->delete($id);
        echo json_encode(true);
    }

    public function sales_return_item_delete()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $this->MSales_return_details->delete($this->input->post('item_id'));
        $msg = $this->sales_return_table($this->input->post('sales_return_no'), $stat_date, $en_date);
        echo $msg;
    }

    public function sales_return_table($sales_return_no, $stat_date, $en_date)
    {
        $details = $this->MSales_return_details->get_by_sales_return_no($sales_return_no, $stat_date, $en_date);

        $msg = '
        <table id="sample_1" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center">Quantity (pcs)</th>
                    <th class="center">Total Are (sq ft)</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total Price</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
        $msg .= '<tbody>';
        $qty = 0;
        $price = 0;
        $sq_weight = 0;
        if (count($details) > 0) {
            foreach ($details as $list) {
                $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="center">' . $list['quantity'] . '</td>
                        <td class="center">' . $list['sq_weight'] . '</td>
                        <td class="right">' . $list['sale_price'] . '</td>
                        <td class="right">' . round($list['sq_weight'] * $list['sale_price'], 2) . '</td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn del btn-danger sales_return_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                $qty += $list['quantity'];
                $sq_weight += $list['sq_weight'];
                $price += round($list['sq_weight'] * $list['sale_price']);
            }
        }
        $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="6">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="center">' . $qty . '</td>
                    <td class="center">' . $sq_weight . '</td>
                    <td></td>
                    <td class="right"><input type="hidden" id="totl_price" value="' . $price . '">' . $price . '</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4" class="right"> Total Paid Amount</td>
                    <td class="right"><input type="text" name="paid_amount" id="paid_amount"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }

    public function purchase_list()
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/purchase/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function purchase_list_data()
    {
        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_master.purchase_no', $search, 'both');
            $this->db->or_like('purchase_master.purchase_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_master.id as master_id, purchase_master.purchase_no, purchase_master.purchase_date,suppliers.name as supplier_name, sum(purchase_details.quantity) as item_qty, sum(purchase_details.total_price) as price_total');
        $this->db->from("purchase_master");
        $this->db->join("suppliers", "purchase_master.supplier_id = suppliers.id", "left");
        $this->db->join("purchase_details", "purchase_master.id = purchase_details.master_id", "left");
        $this->db->where('purchase_master.purchase_date >= ', $stat_date);
        $this->db->where('purchase_master.purchase_date <= ', $en_date);
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("purchase_master.purchase_no");

        $total = $this->db->get()->num_rows();

        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'purchase_master.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('purchase_master.purchase_no', $search, 'both');
            $this->db->or_like('purchase_master.purchase_date', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select('purchase_master.id as master_id, purchase_master.purchase_no, purchase_master.purchase_date,suppliers.name as supplier_name, sum(purchase_details.quantity) as item_qty, sum(purchase_details.total_price) as price_total');
        $this->db->from("purchase_master");
        $this->db->join("suppliers", "purchase_master.supplier_id = suppliers.id", "left");
        $this->db->join("purchase_details", "purchase_master.id = purchase_details.master_id", "left");
        $this->db->where('purchase_master.purchase_date >= ', $stat_date);
        $this->db->where('purchase_master.purchase_date <= ', $en_date);
        $this->db->where('purchase_master.company_id', $this->session->userdata('user_company'));
        $this->db->group_by("purchase_master.purchase_no");
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {

            $res = array(
                'purchase_no' => '',
                'purchase_date' => '',
                'supplier_name' => '',
                'item_qty' => '',
                'price_total' => '',
                'option' => ''
            );

            $res['purchase_no'] = "<a href=\"inventory/purchase_preview/" . $row['master_id'] . "\"  target=\"_blank\">" . $row['purchase_no'] . "</a>";
            $res['purchase_date'] = date('Y-m-d', strtotime($row['purchase_date']));
            $res['supplier_name'] = $row['supplier_name'];
            $res['item_qty'] = $row['item_qty'];
            $res['price_total'] = $row['price_total'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                // <a  class=\"btn btn-edit\" href=\"inventory/purchase_save/" . $row['master_id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                $res['options'] = " 
                
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['master_id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            } else {
                $res['options'] = "";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function purchase_save($id = NULL)
    {

        $accounts_date = $this->MItems->get_dates();
        $stat_date = $accounts_date['start_date'];
        $en_date = $accounts_date['end_date'];
        if ($this->input->post()) {
            $purchase = $this->MPurchase_master->get_by_purchase_no($this->input->post('purchase_no'), $stat_date, $en_date);
            if (count($purchase) > 0) {
                $this->MPurchase_master->update($purchase[0]['id']);
                $master_id = $purchase[0]['id'];
                $total_expense = $this->MPurchase_details->getExpenses($master_id);
            } else {
                $master_id = $this->MPurchase_master->create();
                $total_expense = $this->MPurchase_details->addExpense($master_id);
            }
            // Add purchase info on details table
            $this->MPurchase_details->create($master_id, $total_expense);
            // Update AVCO price in item table
            // $avco = $this->MPurchase_details->get_avco(trim($this->input->post('item_id')));
            // $this->MItems->update_field(trim($this->input->post('item_id')), 'avco_price', $avco);
            $msg = $this->purchase_table($master_id);
            echo $msg;
        } else {

            if ($id) {

                $data['purchase'] = $this->MPurchase_master->get_by_id($id);
                $data['purchase_no'] = $data['purchase']['purchase_no'];
                $data['details'] = $this->MPurchase_details->get_by_purchase_no($id);
                $data['suppliers'] = $this->MSuppliers->get_by_id($data['purchase']['supplier_id']);
                $data['items'] = $this->MItems->get_by_id($data['purchase']['item_id']);
            } else {
                $data['details'] = array();
                $data['suppliers'] = NULL;
                $data['items'] = NULL;
                $data['purchase'] = NULL;
                $data['purchase_no'] = $this->MPurchase_master->get_last_purchase_no($stat_date, $en_date);
            }
            $data['expenses'] = $this->MRecipe->get_by_all();
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/purchase/save';
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function purchase_preview($master_id = NULL)
    {
        $data['title'] = 'Purchase Invoice Preview';
        $data['master'] = $this->MPurchase_master->get_by_purchase_master($master_id);
        $data['details'] = $this->MPurchase_details->get_by_purchase_no($master_id);
        $data['expenses'] = $this->MPurchase_details->get_expense_details($master_id);
        // var_dump($details['expenses']);
        // die;
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/inventory/purchase/preview', $data);
    }

    public function purchase_complete()
    {

        $this->MPurchase_master->update($this->input->post('id'));
        echo 'inventory/purchase_save/';
    }

    public function purchase_delete($id)
    {

        $details = $this->MPurchase_details->get_by_master_id(trim($id));
        $this->MPurchase_master->delete($id);
        // $total_expense = $this->MPurchase_details->getExpenses($id);
        // $this->MPurchase_details->calculate_avgcos($id, $total_expense);
        foreach ($details as $row) {
            $avco = $this->MPurchase_details->get_avco(trim($row['item_id']));
            if (!is_nan($avco)) {
                $avco = $avco;
            } else {
                $avco = 0;
            }
            $this->MItems->update_field(trim($row['item_id']), 'avco_price', $avco);

            $this->db->where('id', $row['item_id']);
            $item_data = $this->db->get('items')->row_array();

            $current_qty = $item_data['qty'] - $row['quantity'];
            $this->MItems->update_field(trim($row['item_id']), 'qty', $current_qty);
        }

        echo json_encode(true);
    }

    public function purchase_item_delete()
    {
        $details = $this->MPurchase_details->get_by_id(trim($this->input->post('detail_id')));
        // Delete item from purchase details table
        $this->MPurchase_details->delete(trim($this->input->post('detail_id')), trim($this->input->post('master_id')));
        // Update AVCO price in item table
        $avco = $this->MPurchase_details->get_avco(trim($details['item_id']));
        if (!is_nan($avco)) {
            $avco = $avco;
        } else {
            $avco = 0;
        }
        $this->MItems->update_field(trim($details['item_id']), 'avco_price', $avco);
        $this->db->where('id', $details['item_id']);
        $item_data = $this->db->get('items')->row_array();
        $current_qty = $item_data['qty'] - $details['quantity'];
        $this->MItems->update_field(trim($details['item_id']), 'qty', $current_qty);
        $msg = $this->purchase_table($this->input->post('master_id'));
        echo $msg;
    }

    public function purchase_table($master_id)
    {
        $details = $this->MPurchase_details->get_by_purchase_no($master_id);

        $msg = '
        <table id="datatables" class="table table-bordered table-striped responsive">
            <thead>
                <tr>
                    <th class="left">Item Code</th>
                    <th class="left">Item Name</th>
                    <th class="center">Quantity</th>
                    <th class="center">Unit Price</th>
                    <th class="center">Total Price</th>
                    <th class="center">Total Expense</th>
					<th class="center">Grand Total</th>
                    <th class="span3 center">Action</th>
                </tr>
            </thead>';
        $msg .= '<tbody>';
        $qty = 0;
        $price = 0;
        $exp = 0;
        $gran_tot = 0;
        if (count($details) > 0) {
            foreach ($details as $list) {
                $to_price = ($list['quantity'] * $list['purchase_price']);
                $msg .= '
                    <tr>
                        <td>' . $list['item_code'] . '</td>
                        <td>' . $list['item_name'] . '</td>
                        <td class="center">' . $list['quantity'] . '</td>
                        <td class="right">' . $list['purchase_price'] . '</td>
                        <td class="right">' . number_format($to_price, 2) . '</td>
                        <td class="right">' . number_format($list['total_expense'], 2) . '</td>
                        <td class="right">' . number_format($list['total_price'], 2) . '</td>
                        <td class="center">
                            <input type="hidden" value="' . $list['id'] . '" /><span class="btn btn-danger purchase_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
                        </td>
                    </tr>';
                $qty += $list['quantity'];
                $price += ($to_price);
                $exp += ($list['total_expense']);
                $gran_tot += ($list['total_price']);
            }
        }
        $msg .= '</tbody>
            <tfoot>
                <tr>
                    <th class="left" colspan="8">Order Totals</th>
                </tr>
                <tr>
                    <td colspan="2">&nbsp</td>
                    <td class="center">' . $qty . '</td>
                    <td></td>
                    <td class="right">' . number_format($price, 2) . '</td>
                    <td class="right">' . number_format($exp, 2) . '</td>
                    <td class="right">' . number_format($gran_tot, 2) . '</td>
                    <td><input type="hidden" id="master_id" value="' . $master_id . '"><input type="hidden" id="totl_price" value="' . $price . '"></td>
                </tr>
            </tfoot>
        </table>';

        return $msg;
    }

    public function expense_delete($id)
    {

        $this->MRecipe->delete($id);
        $data['error'] = false;
        $data['message'] = 'success';

        echo json_encode($data);
    }

    public function expense_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/expense/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function expense_list_data()
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            if ($search == 'Active') {
                $search = "0";
            } else if ($search == 'Inactive') {
                $search = "1";
            }
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("id, code, name, status,");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $total = $this->db->get("purchase_expense")->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            if ($search == 'Active') {
                $search = "0";
            } else if ($search == 'Inactive') {
                $search = "1";
            }
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("id, code, name, status,");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get("purchase_expense")->result_array();

        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            if ($row['status'] == '0') {
                $res['status'] = "Active";
            } else if ($row['status'] == '1') {
                $res['status'] = "Inactive";
            }

            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = "
                                <a  class=\"btn btn-edit\" href=\"inventory/expense_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function expense_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {

                $this->MRecipe->update();
                $this->session->set_flashdata('success', 'Expense Updated successfully.');
                redirect('inventory/expense_list', 'refresh');
            } else {

                $this->MRecipe->insert_data();
                $this->session->set_flashdata('success', 'Expense saved successfully.');
                redirect('inventory/expense_save', 'refresh');
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/expense/save';
            $recipe = $this->MRecipe->get_latest();
            if (count($recipe) > 0) {
                $data['code'] = (int) $recipe['code'] + 1;
            } else {
                $data['code'] = 1001;
            }
            $data['recipe'] = $this->MRecipe->get_by_id($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function head_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {

                $this->MRecipe->update_ac();
                $this->session->set_flashdata('success', 'Head Updated successfully.');
                redirect('inventory/head_list', 'refresh');
            } else {

                $this->MRecipe->insert_data_ac();
                $this->session->set_flashdata('success', 'Head saved successfully.');
                redirect('inventory/head_save', 'refresh');
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/head/save';
            $recipe = $this->MRecipe->get_latest_ac();
            if (count($recipe) > 0) {
                $data['code'] = (int) $recipe['code'] + 1;
            } else {
                $data['code'] = 1001;
            }
            $data['recipe'] = $this->MRecipe->get_by_id_ac($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }


    public function head_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/head/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function head_list_data()
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {
            if ($search == 'Active') {
                $search = "0";
            } else if ($search == 'Inactive') {
                $search = "1";
            }
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("id, code, name, status,");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $total = $this->db->get("expense_ac")->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            if ($search == 'Active') {
                $search = "0";
            } else if ($search == 'Inactive') {
                $search = "1";
            }
            $this->db->group_start();
            $this->db->like('code', $search, 'both');
            $this->db->or_like('name', $search, 'both');
            $this->db->or_like('status', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("id, code, name, status,");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get("expense_ac")->result_array();

        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            if ($row['status'] == '0') {
                $res['status'] = "Active";
            } else if ($row['status'] == '1') {
                $res['status'] = "Inactive";
            }

            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = "
                                <a  class=\"btn btn-edit\" href=\"inventory/head_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function head_delete($id)
    {

        $this->MRecipe->delete_ac($id);
        $data['error'] = false;
        $data['message'] = 'success';

        echo json_encode($data);
    }

    public function expense_entries($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {

                $this->MRecipe->update_en();
                $this->session->set_flashdata('success', 'Expense Updated successfully.');
                redirect('inventory/expense_entries_list', 'refresh');
            } else {

                $this->MRecipe->insert_data_en();
                $this->session->set_flashdata('success', 'Expense saved successfully.');
                redirect('inventory/expense_entries', 'refresh');
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/entries/save';
            $recipe = $this->MRecipe->get_latest_en();
            if (count($recipe) > 0) {
                $data['code'] = (int) $recipe['code'] + 1;
            } else {
                $data['code'] = 1001;
            }
            $this->db->where('company_id', $this->session->userdata('user_company'));
            $this->db->where('status', 0);
            $data['account_heads'] = $this->db->get('expense_ac')->result_array();
            $data['recipe'] = $this->MRecipe->get_by_id_en($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function expense_entries_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/entries/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function expense_entries_list_data()
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {

            $this->db->group_start();
            $this->db->like('expense_en.code', $search, 'both');
            $this->db->or_like('expense_en.date', $search, 'both');
            $this->db->or_like('expense_en.amount', $search, 'both');
            $this->db->or_like('expense_ac.name', $search, 'both');
            $this->db->or_like('expense_ac.code', $search, 'both');
            $this->db->group_end();
        }

        $this->db->select("expense_en.code, expense_en.date, expense_en.amount, expense_en.ac_id, expense_ac.name, expense_ac.code as exp_code");
        $this->db->from('expense_en');
        $this->db->join('expense_ac', 'expense_en.ac_id = expense_ac.id', 'left');
        $this->db->where('expense_en.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'expense_en.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {
            $this->db->group_start();
            $this->db->like('expense_en.code', $search, 'both');
            $this->db->or_like('expense_en.date', $search, 'both');
            $this->db->or_like('expense_en.amount', $search, 'both');
            $this->db->or_like('expense_ac.name', $search, 'both');
            $this->db->or_like('expense_ac.code', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("expense_en.id, expense_en.code, expense_en.date, expense_en.amount, expense_ac.name, expense_ac.code as exp_code");
        $this->db->from('expense_en');
        $this->db->join('expense_ac', 'expense_en.ac_id = expense_ac.id', 'left');
        $this->db->where('expense_en.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'date' => '',
                'amount' => '',
                'name' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['date'] = $row['date'];
            $res['amount'] = $row['amount'];
            $res['name'] = $row['name'] . " (" . $row['exp_code'] . ")";


            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = "
                                <a  class=\"btn btn-edit\" href=\"inventory/expense_entries/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function entry_delete($id)
    {

        $this->MRecipe->delete_en($id);
        $data['error'] = false;
        $data['message'] = 'success';

        echo json_encode($data);
    }

    public function get_raw_stock_balance()
    {
        $item_id = $this->input->post('result');
        $data['raw'] = $this->MReports->get_raw_stock_balance($item_id);
        echo json_encode($data);
    }




    public function get_item_by_type()
    {
        $type = $this->input->post('type');
        $data['items'] = $this->MItems->get_all('active', $type);
        echo json_encode($data);
    }


    public function customer_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/customer/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function customer_list_data()
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {

            $this->db->group_start();
            $this->db->like('customers.code', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('customers.address', $search, 'both');
            $this->db->or_like('customers.mobile', $search, 'both');
            $this->db->or_like('customers.email', $search, 'both');
            $this->db->or_like('customers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('customers.id, customers.code, customers.name, customers.address,customers.mobile, customers.email, customers.status');
        $this->db->from("customers");
        $this->db->where('customers.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'customers.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {

            $this->db->group_start();
            $this->db->like('customers.code', $search, 'both');
            $this->db->or_like('customers.name', $search, 'both');
            $this->db->or_like('customers.address', $search, 'both');
            $this->db->or_like('customers.mobile', $search, 'both');
            $this->db->or_like('customers.email', $search, 'both');
            $this->db->or_like('customers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('customers.id, customers.code, customers.name, customers.address,customers.mobile, customers.email, customers.status');
        $this->db->from("customers");
        $this->db->where('customers.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'address' => '',
                'mobile' => '',
                'email' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['address'] = $row['address'];
            $res['mobile'] = $row['mobile'];
            $res['email'] = $row['email'];
            $res['status'] = $row['status'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/customer_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                             <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function customer_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MCustomers->update(trim($this->input->post('code')));
                $this->session->set_flashdata('success', 'Customer Updated successfully.');
                redirect('inventory/customer_list', 'refresh');
            } else {
                $this->MCustomers->create(trim($this->input->post('code')));
                $this->session->set_flashdata('success', 'Customer saved successfully.');
                redirect('inventory/customer_save', 'refresh');
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/customer/save';
            $customer = $this->MCustomers->get_latest();
            if (count($customer) > 0) {
                $data['code'] = (int) $customer['code'] + 1;
            } else {
                $data['code'] = 1001;
            }
            if ($id) {
                $data['customer'] = $this->MCustomers->get_by_id($id);
            } else {
                $data['customer'] = NULL;
            }

            // $chart_id = $data['customer']['ac_id'];
            // $data['charts'] = $this->MAc_charts->get_coa_tree($chart_id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }
    public function add_new_customer()
    {
        if ($this->input->post()) {
            $ac_receivable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            $chart = $this->MAc_charts->get_by_id($ac_receivable['ac_receivable']);
            $siblings = $this->MAc_charts->get_by_parent_id($ac_receivable['ac_receivable']);
            if (count($siblings) > 0) {
                $ac_code_temp = explode('.', $siblings['code']);
                $ac_last = count($ac_code_temp) - 1;
                $ac_new = (int) $ac_code_temp[$ac_last] + 10;
                $ac_code = $chart['code'] . '.' . $ac_new;
            } else {
                $ac_code = $chart['code'] . '.10';
            }

            $ac_id = $this->MAc_charts->account_create($ac_receivable['ac_receivable'], $ac_code, $this->input->post('name'));
            $insert_id = $this->MCustomers->create(trim($this->input->post('code')), $ac_id);
            $customers = $this->MCustomers->get_all();
            $html = '';
            foreach ($customers as $customer) {
                if ($insert_id == $customer['id']) {
                    $html .= '<option value="' . $customer['id'] . '" selected>' . $customer['name'] . '</option>';
                } else {
                    $html .= '<option value="' . $customer['id'] . '">' . $customer['name'] . '</option>';
                }
            }
            echo $html;
        }
    }

    public function customer_delete($id)
    {
        $sales = $this->MSales_master->get_by_customer_id($id);
        if (count($sales) > 0) {
            $data['error'] = true;
            $data['message'] = 'Customer can\'t delete, S/He is in Sales List.';
        } else {
            $data['error'] = false;
            $data['message'] = 'success';
            $this->MCustomers->delete($id);
        }

        echo json_encode($data);
    }

    /* ---------------------- Supplier Start -------------- */

    public function supplier_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/supplier/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function supplier_list_data()
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {

            $this->db->group_start();
            $this->db->like('suppliers.code', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('suppliers.address', $search, 'both');
            $this->db->or_like('suppliers.phone_no', $search, 'both');
            $this->db->or_like('suppliers.contact_person', $search, 'both');
            $this->db->or_like('suppliers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('suppliers.id, suppliers.code, suppliers.name, suppliers.address,suppliers.phone_no, suppliers.contact_person, suppliers.status');
        $this->db->from("suppliers");
        $this->db->where('suppliers.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'suppliers.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {

            $this->db->group_start();
            $this->db->like('suppliers.code', $search, 'both');
            $this->db->or_like('suppliers.name', $search, 'both');
            $this->db->or_like('suppliers.address', $search, 'both');
            $this->db->or_like('suppliers.phone_no', $search, 'both');
            $this->db->or_like('suppliers.contact_person', $search, 'both');
            $this->db->or_like('suppliers.status', $search, 'after');
            $this->db->group_end();
        }
        $this->db->select('suppliers.id, suppliers.code, suppliers.name, suppliers.address,suppliers.phone_no, suppliers.contact_person, suppliers.status');
        $this->db->from("suppliers");
        $this->db->where('suppliers.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'address' => '',
                'mobile' => '',
                'contact_person' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['address'] = $row['address'];
            $res['mobile'] = $row['phone_no'];
            $res['contact_person'] = $row['contact_person'];
            $res['status'] = $row['status'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/supplier_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }

    public function supplier_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MSuppliers->update(trim($this->input->post('code')));


                $this->session->set_flashdata('message', 'Supplier Updated successfully.');
                redirect('inventory/supplier_list', 'refresh');
            } else {
                $this->MSuppliers->create(trim($this->input->post('code')));
                $this->session->set_flashdata('message', 'Supplier saved successfully.');
                redirect('inventory/supplier_save', 'refresh');
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/supplier/save';
            $supplier = $this->MSuppliers->get_latest();
            if (count($supplier) > 0) {
                $data['code'] = (int) $supplier['code'] + 1;
            } else {
                $data['code'] = 1001;
            }
            if ($id) {
                $data['supplier'] = $this->MSuppliers->get_by_id($id);
            } else {
                $data['supplier'] = NULL;
            }

            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function add_new_supplier()
    {
        if ($this->input->post()) {
            $ac_payable = $this->MSettings->get_by_company_id($this->session->userdata('user_company'));
            $chart = $this->MAc_charts->get_by_id($ac_payable['ac_payable']);
            $siblings = $this->MAc_charts->get_by_parent_id($ac_payable['ac_payable']);
            if (count($siblings) > 0) {
                $ac_code_temp = explode('.', $siblings['code']);
                $ac_last = count($ac_code_temp) - 1;
                $ac_new = (int) $ac_code_temp[$ac_last] + 10;
                $ac_code = $chart['code'] . '.' . $ac_new;
            } else {
                $ac_code = $chart['code'] . '.10';
            }
            $ac_id = $this->MAc_charts->account_create($ac_payable['ac_payable'], $ac_code, $this->input->post('name'));
            $insert_id = $this->MSuppliers->create(trim($this->input->post('code')), $ac_id);
            $suppliers = $this->MSuppliers->get_all();
            $html = '';
            foreach ($suppliers as $supplier) {
                if ($insert_id == $supplier['id']) {
                    $html .= '<option value="' . $supplier['id'] . '" selected>' . $supplier['name'] . '</option>';
                } else {
                    $html .= '<option value="' . $supplier['id'] . '">' . $supplier['name'] . '</option>';
                }
            }
            echo $html;
        }
    }

    public function supplier_delete($id)
    {
        $purchase = $this->MPurchase_master->get_by_supplier_id($id);
        if (count($purchase) > 0) {
            $data['error'] = true;
            $data['message'] = 'Supplier can\'t delete, S/He is in Purchase List.';
        } else {
            $data['error'] = false;
            $data['message'] = 'success';
            $this->MSuppliers->delete($id);
        }

        echo json_encode($data);
    }

    public function item_grade_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MItems->update_grade();
                $this->session->set_flashdata('success', 'Item Grade updated successfully.');
                redirect('inventory/item_grade_list', 'refresh');
            } else {

                $this->form_validation->set_rules('grade_name', 'Grade Name', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'POS System';
                    $data['menu'] = 'inventory';
                    $data['content'] = 'admin/inventory/item_grade/save';
                    $data['item'] = $this->MItems->get_by_id_grade($id);
                    $data['privileges'] = $this->privileges;
                    $this->load->view('admin/template', $data);
                } else {

                    $this->MItems->create_grade();
                    $this->session->set_flashdata('success', 'Item Grade created successfully.');
                    redirect('inventory/item_grade_list', 'refresh');
                }
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/item_grade/save';
            $data['item'] = $this->MItems->get_by_id_grade($id);
            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function item_grade_list()
    {
        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/item_grade/list';
        $data['items'] = $this->MItems->get_all_grade();
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }

    public function item_grade_delete($id)
    {

        $this->MItems->delete_grade($id);
        $this->session->set_flashdata('success', 'Item Grade deleted successfully.');

        redirect('inventory/item_grade_list', 'refresh');
    }
    /* -------------------- Item ----------------- */

    public function item_list()
    {

        $data['title'] = 'POS System';
        $data['menu'] = 'inventory';
        $data['content'] = 'admin/inventory/item/list';
        $data['privileges'] = $this->privileges;
        $this->load->view('admin/template', $data);
    }
    public function item_list_data()
    {

        $limit = $this->input->get('limit');
        $search = $this->input->get('search');
        $order = $this->input->get('order');
        $offset = $this->input->get('offset');
        $sort = $this->input->get('sort');
        if ($search) {

            $this->db->group_start();
            $this->db->like('items.code', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->or_like('items.avco_price', $search, 'both');
            $this->db->or_like('items.qty', $search, 'both');
            $this->db->or_like('items.status', $search, 'after');
            $this->db->or_like('items.re_order', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("items.id, items.code,items.qty, items.name,items.avco_price,items.status, items.re_order ");
        $this->db->from("items");
        $this->db->where('items.company_id', $this->session->userdata('user_company'));
        $total = $this->db->get()->num_rows();
        $this->db->limit($limit);
        if ($sort == '') {
            $sort = 'items.id';
            $order = 'DESC';
        }
        $this->db->order_by($sort, $order);
        if ($search) {

            $this->db->group_start();
            $this->db->like('items.code', $search, 'both');
            $this->db->or_like('items.name', $search, 'both');
            $this->db->or_like('items.avco_price', $search, 'both');
            $this->db->or_like('items.qty', $search, 'both');
            $this->db->or_like('items.status', $search, 'after');
            $this->db->or_like('items.re_order', $search, 'both');
            $this->db->group_end();
        }
        $this->db->select("items.id, items.code,items.qty, items.name,items.avco_price,items.status, items.re_order ");
        $this->db->from("items");
        $this->db->where('items.company_id', $this->session->userdata('user_company'));
        $this->db->limit($limit, $offset);
        $products = $this->db->get()->result_array();

        $data = array();
        foreach ($products as $row) {
            $res = array(
                'code' => '',
                'name' => '',
                'avco_price' => '',
                'qty' => '',
                'total_price' => '',
                'status' => '',
                'option' => ''
            );
            $res['code'] = $row['code'];
            $res['name'] = $row['name'];
            $res['avco_price'] = $row['avco_price'];
            $res['qty'] = $row['qty'];
            $res['total_price'] = number_format($row['avco_price'] * $row['qty'], 2);
            $res['status'] = $row['status'];
            if ($this->session->userdata['user_type'] == 'Admin') {
                $res['options'] = "<a  class=\"btn btn-edit\" href=\"inventory/item_save/" . $row['id'] . "\"><i class=\"icon-edit icon-white\"></i> Edit</a>
                              <a  class=\"btn del btn-danger\" onclick=\"delete_confirm('" . $row['id'] . "')\"><i class=\"icon-trash icon-white\")\"></i> Delete</a>";
            }
            $data[] = $res;
        }
        $result = array(
            'total' => $total,
            'rows' => $data
        );
        echo json_encode($result);
    }
    public function item_save($id = NULL)
    {
        if ($this->input->post()) {
            if ($this->input->post('id')) {
                $this->MItems->update();
                $this->session->set_flashdata('success', 'Item updated successfully.');
                redirect('inventory/item_list', 'refresh');
            } else {
                $this->form_validation->set_rules('code', 'Item Code', 'callback_code_check');
                $this->form_validation->set_rules('name', 'Item Name', 'required');
                // $this->form_validation->set_rules('min_sale_price', 'Min. Sale Price', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'POS System';
                    $data['menu'] = 'inventory';
                    $data['content'] = 'admin/inventory/item/save';
                    $data['item'] = $this->MItems->get_by_id($id);
                    $data['privileges'] = $this->privileges;
                    $this->load->view('admin/template', $data);
                } else {
                    $this->MItems->create();
                    $this->session->set_flashdata('success', 'Item created successfully.');
                    redirect('inventory/item_save', 'refresh');
                }
            }
        } else {
            $data['title'] = 'POS System';
            $data['menu'] = 'inventory';
            $data['content'] = 'admin/inventory/item/save';
            $data['item'] = $this->MItems->get_by_id($id);
            $items = $this->MItems->get_latest();
            if (count($items) > 0) {
                $data['code'] = (int) $items['code'] + 1;
            } else {
                $data['code'] = 1001;
            }

            $data['privileges'] = $this->privileges;
            $this->load->view('admin/template', $data);
        }
    }

    public function code_check($code)
    {
        if ($this->MItems->get_by_code($code)) {
            $this->form_validation->set_message('code_check', 'Item code can\'t be duplicate. Please choose different item code.');
            return false;
        } else {
            return true;
        }
    }

    public function item_delete($id)
    {
        $purchase = $this->MPurchase_details->get_by_item_id($id);
        if (count($purchase) > 0) {
            $data['error'] = true;
            $data['message'] = 'Item Can\'t delete, It is in Purchase List.';
        } else {
            $data['error'] = false;
            $data['message'] = 'success';
            $this->MItems->delete($id);
        }

        echo json_encode($data);
    }

    public function test_avco()
    {
        echo $this->MPurchase_details->get_avco(1);
    }
}
