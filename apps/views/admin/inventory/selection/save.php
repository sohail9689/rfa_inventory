<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Selection Box
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/selection_list">Selection List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if(count($selection) > 0){ ?>Edit<?php }else{ ?>Add New<?php } ?> Selection item
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form action="inventory/selection_save" method="post" class="form-horizontal form-bordered " id="">
							<div class="control-group">
								<label for="textfield" class="control-label">Select Type</label>
								<div class="controls">
									<select name="type" id="type" class="span5 chzn-select">
									<option value="0">Select Type</option>
										<option value="Raw" <?php if (count($selection) > 0 && $selection['type'] == 'Raw') { echo 'selected'; }?>>Raw</option>
										<option value="Wet Blue" <?php if (count($selection) > 0 && $selection['type'] == 'Wet Blue') { echo 'selected'; }?>>Wet Blue</option>
										<option value="FG" <?php if (count($selection) > 0 && $selection['type'] == 'FG') { echo 'selected'; }?>>FG</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Selection item Name</label>
								<div class="controls">
									<input type="text" name="name" id="name" value="<?php if(count($selection) > 0){ echo $selection['name']; } ?>" class="span5" placeholder="Selection Name">
								</div>
							</div>
							<div class="control-group">
								<label for="name" class="control-label">Price</label>
								<div class="controls">
									<input type="number" name="price" id="price" value="<?php if(count($selection) > 0){ echo $selection['price']; } ?>" class="span5" placeholder="Price">
								</div>
							</div>
							<div class="control-group">
								<label for="status" class="control-label">Select Status</label>
								<div class="controls">
									<select name="status" id="status" class="span5 chzn-select" data-placeholder="Select Status">
										<option value="0" <?php if (count($selection) > 0 && $selection['status'] == '0') { echo 'selected'; }?>>Active</option>
										<option value="1" <?php if (count($selection) > 0 && $selection['status'] == '1') { echo 'selected'; }?>>Inactive</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<?php if(count($selection) > 0){ ?>
							<input type="hidden" name="id" value="<?php echo $selection['id']; ?>" />
							<?php } ?>
							<div class="form-actions">
								<input type="submit" class="btn btn-success" value="Save Changes" id="selection_save">
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
$(document).ready(function(){
	$(document).on('click', '#selection_save', function(event) {
	var name = $('#name').val();
	var type = $('#type').val();
	if(! type || type == 0){
			alert('Select Type.');
			return false;
		}
	if(!(name)){
		alert('Name can`t be empty');
		return false;
	}
	
});
});
	
</script>