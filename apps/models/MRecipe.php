<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MRecipe extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_by_id($id)
	{
		$data = array();
		$this->db->where('id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('purchase_expense');
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_latest()
	{
		$data = array();
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$this->db->order_by('code', 'DESC');
		$this->db->limit(1);
		$q = $this->db->get('purchase_expense');
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_by_all()
	{
		$this->db->where("company_id", $this->session->userdata('user_company'));
		$this->db->where("status", 0);
		return $this->db->get('purchase_expense')->result_array();
	}

	public function insert_data()
	{

		$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'code' => $this->input->post('code'),
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status'),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
		);
		$this->db->insert('purchase_expense', $data);
	}



	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('purchase_expense');
	}


	public function update()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status')
		);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('purchase_expense', $data);
	}


	//  Expenses Account Head 

	public function get_by_id_ac($id)
	{
		$data = array();
		$this->db->where('id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('expense_ac');
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}
	public function get_latest_ac()
	{
		$data = array();
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$this->db->order_by('code', 'DESC');
		$this->db->limit(1);
		$q = $this->db->get('expense_ac');
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function insert_data_ac()
	{

		$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'code' => $this->input->post('code'),
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status'),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
		);
		$this->db->insert('expense_ac', $data);
	}

	public function update_ac()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status')
		);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('expense_ac', $data);
	}

	public function delete_ac($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('expense_ac');
	}

	// Expense Entries

	public function get_latest_en()
	{
		$data = array();
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$this->db->order_by('code', 'DESC');
		$this->db->limit(1);
		$q = $this->db->get('expense_en');
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function get_by_id_en($id)
	{
		$data = array();
		$this->db->where('id', $id);
		$this->db->where('company_id', $this->session->userdata('user_company'));
		$q = $this->db->get('expense_en');
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				$data = $row;
			}
		}

		$q->free_result();
		return $data;
	}

	public function insert_data_en()
	{

		$data = array(
			'company_id' => $this->session->userdata('user_company'),
			'code' => $this->input->post('code'),
			'ac_id' => $this->input->post('ac_id'),
			'amount' => $this->input->post('amount'),
			'date' => date_to_db($this->input->post('date')),
			'created' => date('Y-m-d H:i:s', time()),
			'created_by' => $this->session->userdata('user_id')
		);
		$this->db->insert('expense_en', $data);
	}

	public function update_en()
	{
		$data = array(
			'ac_id' => $this->input->post('ac_id'),
			'date' => date_to_db($this->input->post('date')),
			'amount' => $this->input->post('amount')
		);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('expense_en', $data);
	}

	public function delete_en($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('expense_en');
	}
}
