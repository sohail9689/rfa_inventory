<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					User
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="user">User</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($user) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> User
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form id="user_entry" action="user/save" method="post" class="form-horizontal" autocomplete="off" novalidate>
							<fieldset>
								<div class="control-group">
									<label class="control-label" for="name">Full Name</label>
									<div class="controls">
										<input type="text" class="span5" name="name" value="<?php if (count($user) > 0) { echo $user['name']; } ?>" id="name" data-validation="length" data-validation-length="min5" data-validation-error-msg="Need min 5 charecter of your name." placeholder="Full name" />
									</div>
								</div>
								<?php if($this->session->userdata('user_type') == 'Admin'){ ?>
								<div class="control-group">
									<label class="control-label" for="company_id">Company</label>
									<div class="controls">
										<select name="company_id" id="company_id" class="span5 status_" data-placeholder="Company">
											<?php foreach ($companies as $company) { ?>
											<option value="<?php echo $company['id']; ?>" <?php if (count($user) > 0 && $company['id'] == $user['company_id']) { echo 'selected'; } ?>><?php echo $company['name']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<?php }?>
								<div class="control-group">
									<label class="control-label" for="email">Email</label>
									<div class="controls">
										<input type="text" class="span5" name="email" id="email" value="<?php
										if (count($user) > 0) {
											echo $user['email'];
										}
										?>" autocomplete=off data-validation="email" data-validation-error-msg="You did not enter a valid e-mail" placeholder="Email" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="password_confirmation">Password</label>
									<div class="controls">
										<input type="password" class="span5" name="password_confirmation" id="password_confirmation" <?php if ( ! count($user) > 0) { ?>data-validation="length" data-validation-length="min8"<?php } ?> placeholder="Password" /> <?php if (count($user) > 0) { echo 'Keep blank for unchange.'; } ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="password">Confirm Password</label>
									<div class="controls">
										<input type="password" class="span5" name="password" id="password" data-validation="confirmation" placeholder="Confirm Password" /> <?php if (count($user) > 0) { echo 'Keep blank for unchange.'; } ?>
									</div>
								</div>
								<?php if($this->session->userdata('user_type') != 'User'){ ?>
								<div class="control-group">
									<label class="control-label" for="type">User Type</label>
									<div class="controls">
										<select name="type" id="status" class="span5 status_" data-placeholder="Select User Type">
											<option value=""></option>
											<?php if($this->session->userdata('user_type') == 'Admin'){ ?>
											<option value="Admin" <?php if (count($user) > 0 && $user['type'] == 'Admin') { echo 'selected'; }?>>Admin User</option>
											<?php } ?>
											<option value="Power User" <?php if (count($user) > 0 && $user['type'] == 'Power User') { echo 'selected'; }?>>Power User</option>
											<option value="User" <?php if (count($user) > 0 && $user['type'] == 'User') { echo 'selected'; }?>>User</option>
										</select>
									</div>
								</div>
								<?php } ?>
								<?php if($this->session->userdata('user_type') != 'User'){ ?>
								<div class="control-group">
									<label class="control-label" for="status">Status</label>
									<div class="controls">
										<select name="status" id="status" class="span5 status_" data-placeholder="Select Status">
											<option value=""></option>
											<option value="Active" <?php if (count($user) > 0 && $user['status'] == 'Active') { echo 'selected'; }?>>Active</option>
											<option value="Inactive" <?php if (count($user) > 0 && $user['status'] == 'Inactive') { echo 'selected'; }?>>Inactive</option>
										</select>
									</div>
								</div>
								<?php } ?>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<?php if (count($user) > 0) { ?>
								<input type="hidden" name="id" value="<?php echo $user['id']; ?>" />
								<?php } ?>
								<div class="form-actions">
									<input type="submit" class="btn btn-success" value="Save Changes" />
									<button type="reset" class="btn">Cancel</button>
								</div>
							</fieldset>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$('#name').focus();
	$.validate({
		modules : 'security'
	});
</script>