<div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Sales
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="dashboard">Dashboard</a>
                        <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="inventory">Inventory</a>
                        <span class="divider">/</span>
                    </li>
                    <li class="active">
                        Sales List
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php if ($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
            <!-- BEGIN Alert widget-->
            <div class="row-fluid">
                <div class="span12">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <button class="close" data-dismiss="alert">×</button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
                            <button class="close" data-dismiss="alert">×</button>
                            <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('error')) { ?>
                        <div class="alert alert-error">
                            <button class="close" data-dismiss="alert">×</button>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- END Alert widget-->
        <?php } ?>
        <div class="row-fluid right">
            <a class="btn btn-primary" href="inventory/sales_save"><i class="icon-plus icon-white"></i> Add New</a>
        </div>
        <br>
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="table-responsive" id="demo_s">
            <table id="demo-table" class="table table-striped" data-url="inventory/sales_list_data" data-side-pagination="server" data-page-list="[5, 10, 20, 50, 100, 200, 500, 1000]" data-pagination="true" data-show-refresh="true" data-search="false" data-show-export="true" data-show-toggle="false" data-show-columns="true" data-filter-control="true" data-filter-show-clear="true" data-show-footer="true">

                <thead>
                    <tr>
                        <th data-field="sales_no" data-align="left" data-filter-control="input" data-sortable="true">
                            <?php echo "Sales #"; ?>
                        </th>
                        <th data-field="sales_date" data-filter-control="datepicker" data-filter-datepicker-options='{"format": "yyyy-mm-dd", "autoclose":true, "clearBtn": true, "todayHighlight": true, "orientation": "top"}' data-sortable="true">
                            <?php echo "Sales Date"; ?>
                        </th>
                        <th data-field="customer_name" data-align="left" data-filter-control="input" data-sortable="true">
                            <?php echo "Customer"; ?>
                        </th>
                        <th data-field="item_qty" data-align="center" data-sortable="true" data-footer-formatter="totalFormatter">
                            <?php echo "Qty"; ?>
                        </th>
                        <th data-field="price_total" data-align="center" data-sortable="true" data-footer-formatter="totalFormatter">
                            <?php echo "Amount"; ?>
                        </th>
                        <!-- <th data-field="total_profit" data-align="center" data-sortable="true" data-footer-formatter="totalFormatter">
                            <?php echo "Profit"; ?>
                        </th> -->
                        <th data-field="options" data-sortable="false">
                            <?php echo ('Options'); ?>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <span id="status"></span>
    <script type="text/javascript">
        var message1 = 'Active';
        var message2 = 'Inactive';
        var module = 'Sales';

        function totalFormatter(data) {

            var total = 0;

            if (data.length > 0) {

                var field = this.field;

                total = data.reduce(function(sum, row) {
                    return sum + (+row[field]);
                }, 0);

                return total.toLocaleString();
            }

            return '';
        };

        function delete_confirm(id) {
            if (confirm("Are you sure to delete this ?")) {
                var url = 'inventory/sales_delete/' + id;
                $.ajax({
                    type: 'get',
                    url: url,
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(data) {
                        alert("Successfully deleted !");
                        $('#demo-table').bootstrapTable('refresh');
                    }
                });
            } else {
                return false;
            }
        }
        $(document).ready(function() {
            $('#demo-table').bootstrapTable({}).on('all.bs.table', function(e, name, args) {

            }).on('click-row.bs.table', function(e, row, $element) {

            }).on('dbl-click-row.bs.table', function(e, row, $element) {

            }).on('sort.bs.table', function(e, name, order) {

            }).on('check.bs.table', function(e, row) {

            }).on('uncheck.bs.table', function(e, row) {

            }).on('check-all.bs.table', function(e) {

            }).on('uncheck-all.bs.table', function(e) {

            }).on('load-success.bs.table', function(e, data) {}).on('load-error.bs.table', function(e, status) {

            }).on('column-switch.bs.table', function(e, field, checked) {

            }).on('page-change.bs.table', function(e, size, number) {
                //alert('1');
                //set_switchery();
            }).on('search.bs.table', function(e, text) {

            });
        });
    </script>