<div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Report
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="dashboard">Dashboard</a>
                        <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="report">Report</a>
                        <span class="divider">/</span>
                    </li>
                    <li class="active">
                        Purchase
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="widget blue">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Purchase Report </h4>
                        <span class="tools">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <!-- BEGIN FORM-->
                        <form action="report/purchase_report" method="post" target="_blank" class="form-horizontal form-bordered form-validate" id="frm1">
                            <div class="row-fluid multiple-column">
                                <div class="span6">
                                    <?php $accounts_date = $this->MItems->get_dates();
                                    $stat_date = date_to_ui($accounts_date['start_date']);
                                    $en_date = date_to_ui($accounts_date['end_date']);
                                    ?>
                                    <div class="control-group">
                                        <label for="start_date" class="control-label">Start Date</label>
                                        <div class="controls">
                                            <div class="input-append date" data-form="datepicker" data-date="<?php echo date('01/m/Y');; ?>" data-date-format="dd/mm/yyyy">
                                                <input name="start_date" id="start_date" class="grd-white" data-form="datepicker" size="16" type="text" value="<?php echo date('01/m/Y'); ?>" />
                                                <span class="add-on"><i class="icon-th"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="end_date" class="control-label">End Date</label>
                                        <div class="controls">
                                            <div class="input-append date" data-form="datepicker" data-date="<?php echo date('t/m/Y'); ?>" data-date-format="dd/mm/yyyy">
                                                <input name="end_date" id="end_date" class="grd-white" data-form="datepicker" size="16" type="text" value="<?php echo date('t/m/Y'); ?>" />
                                                <span class="add-on"><i class="icon-th"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                            <input type="hidden" name="" id="stat_date" value="<?php echo $stat_date; ?>" />
                            <input type="hidden" name="" id="en_date" value="<?php echo $en_date; ?>" />
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Show Report</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>

                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<script type="text/javascript">
    // $('.supplier_id').select2({
    //         placeholder: '--- Select Supplier ---',
    //         ajax: {
    //           url:"inventory/search_suppliers",
    //           dataType: 'json',
    //           delay: 250,
    //           data: function (params) {
    //         return {
    //           q: params.term
    //         };
    //       },
    //           processResults: function (data) {
    //             return {
    //               results: data
    //             };
    //           },
    //           cache: true
    //         }
    //       });
    // $('.purchase_item').select2({
    //         placeholder: '--- Select Item ---',
    //         ajax: {
    //           url:"inventory/search_all_items",
    //           dataType: 'json',
    //           delay: 250,
    //           data: function (params) {
    //         return {
    //           q: params.term
    //         };
    //       },
    //           processResults: function (data) {
    //             return {
    //               results: data
    //             };
    //           },
    //           cache: true
    //         }
    //       });
    $('#frm1').submit(function(ev) {
        ev.preventDefault(); // to stop the form from submitting
        /* Validations go here */
        var purchase_type = $('#purchase_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if (!start_date) {
            alert('Please select a Date');
            return false;
        }
        if (!end_date) {
            alert('Please select a Date');
            return false;
        }
        this.submit(); // If all the validations succeeded
    });
    $(document).ready(function() {

        var stat_date = $('#stat_date').val();
        var en_date = $('#en_date').val();
        $('[data-form=datepicker]').datepicker({
            autoclose: true,
            startDate: stat_date,
            endDate: en_date,
            format: 'dd/mm/yyyy'
        });
    });
</script>