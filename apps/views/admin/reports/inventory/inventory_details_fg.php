<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Cybextech">
     <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo');?>" type="image/x-icon" />

    <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
        @media print{
            p.muted{
                font-weight: bold;
            }
            small.small{
                font-weight: normal;
            }
        }
    </style>
</head>

<body>
    <!-- section content -->
    <div id="main-content">
        <div class="container-fluid">
            <!-- span content -->
            <div class="row-fluid">
            <div class="span12">
                <!-- content -->
                <!-- <div class="content" style="border: 1px solid #d7d7d7;"> -->
                    <!-- content-body -->
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <h2 class="center" style="color: #000;">
                                <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                            </div>
                            <div class="row-fluid">
                                <div class="span12 center">
                                    <strong><?php echo $reprort_type; ?> Report</strong>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span8">
                                    <p class="muted">Date From</p>
                                    <p><?php echo date( 'Y-m-d', strtotime( date_to_db( $start_date ) ) ); ?></p>
                                </div>
                                <div class="span4">
                                    <p class="muted">Date To</p>
                                    <p><?php echo date( 'Y-m-d', strtotime( date_to_db( $end_date ) ) ); ?></p>
                                </div>
                            </div>
                            <div class="invoice-table">
                                <table class="table table-bordered invoice responsive">
                                    <thead>
                                        <tr>
                                            <th>SL #</th>
                                            <th>Date</th>
                                            <th>Opening Balance</th>
                                            <th>Produced in Production</th>
                                            <th>Sales Return</th>
                                            <th>Sales</th>
                                            <th>Purchase Return</th>
                                            <th>Closing Balance</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    if ( count( $reports['inv'] ) > 0 ) {
                                        $item = array();
                                        ?>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ( $reports['inv'] as $report ){
                                                ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td class="left"><?php echo ( $report['date'] ); ?></td>
                                                    <td class="left">
                                                        <?php
                                                        if ( $i == 1 ) {
                                                            $j = 0;
                                                            if ( is_array( $reports['open'] ) ) {
                                                                foreach ( $reports['open'] as $key => $open ) {
                                                                    echo $open['name'] . ' : ' . number_format($open['open'], 2) . ' --- '.number_format($open['open']*$open['avco_price'], 2).'<br />';
                                                                    $item[$j] = $open['open'];
                                                                    $j++;
                                                                }
                                                            } else {
                                                                $item[$j] = 0;
                                                            }
                                                        } else {
                                                            $j = 0;
                                                            foreach ( $items as $key => $value ) {
                                                                echo $value['name'] . ' : ' . number_format($item[$j], 2) . ' --- '.number_format($item[$j]*$value['avco_price'], 2). '<br />';
                                                                $j++;
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $j = 0;
                                                        foreach ( $report['details'] as $details ) {
                                                            echo $details['item_name'] . ' : ' . number_format($details['production_selection_qty'], 2) .  ' --- '.number_format($details['production_selection_qty']*$details['avco_price'], 2). '<br />';
                                                            $item[$j] = $item[$j] + $details['production_selection_qty'];
                                                            $j++;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $j = 0;
                                                        foreach ( $report['details'] as $details ) {
                                                            echo $details['item_name'] . ' : ' . number_format($details['sales_return_qty'], 2) .  ' --- '.number_format($details['sales_return_qty']*$details['avco_price'], 2). '<br />';
                                                            $item[$j] = $item[$j] + $details['sales_return_qty'];
                                                            $j++;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $j = 0;
                                                        foreach ( $report['details'] as $details ) {
                                                            echo $details['item_name'] . ' : ' . number_format($details['sales_qty'], 2) .  ' --- '.number_format($details['sales_qty']*$details['avco_price'], 2).  '<br />';
                                                            $item[$j] = $item[$j] - $details['sales_qty'];
                                                            $j++;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $j = 0;
                                                        foreach ( $report['details'] as $details ) {
                                                            echo $details['item_name'] . ' : ' . number_format($details['purchase_return_qty'], 2) .  ' --- '.number_format($details['purchase_return_qty']*$details['avco_price'], 2).  '<br />';
                                                            $item[$j] = $item[$j] - $details['purchase_return_qty'];
                                                            $j++;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class="left">
                                                        <?php
                                                        $j = 0;
                                                        foreach ( $items as $key => $value ) {
                                                            echo $value['name'] . ' : ' . number_format($item[$j], 2) . ' --- '.number_format($item[$j]*$value['avco_price'], 2).'<br />';
                                                            $j++;
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td align="center" colspan="8">&nbsp;</td>
                                            </tr>
                                        </tfoot>
                                        <?php
                                    } else {
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td align="center" colspan="8">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td align="center" colspan="8">&nbsp;</td>
                                            </tr>
                                        </tfoot>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="pull-left">
                        <b>Prepared By____________________</b>
                        </div>
                        <div class="center" style="margin-right:220px";>
                        <b>checked  By____________________</b>
                        </div>
                        <div class="pull-right" style="margin-top:-20px";>
                        <b>Approved By____________________</b>
                        </div>
                        <!--/invoice-->
                    </div><!--/content-body -->
                </div><!-- /content -->
            </div><!-- /span content -->

        </div><!-- /container -->
    </div>
</body>
</html>