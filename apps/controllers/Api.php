<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Karachi');
    }

    /* index of the admin. Default: Dashboard; On No Login Session: Back to login page. */

    function sync_db(){

        if($this->is_connected()){
            $this->db->order_by("id", "DESC");
            $sync_info = $this->db->get("sync_info")->row_array();
            $file_name = $sync_info['filename'];
            $filename = APPPATH . 'logs/'.$file_name.'.sql';

            if($sync_info['is_sent'] == 0){
                if(file_exists($filename)){

                        $target_url = URL."accept_db_file";
                        // $target_url = 'http://localhost/sms/index.php/auth/accept_db_file';
                        //This needs to be the full path to the file you want to send.
                        $file_name_with_full_path = realpath($filename);
                        // $full_path = realpath(__DIR__.'/../..').'\uploads\database/'.$name;
                        // $file_name_with_full_path = realpath($full_path);

                        /* curl will accept an array here too.
                         * Many examples I found showed a url-encoded string instead.
                         * Take note that the 'key' in the array will be the key that shows up in the
                         * $_FILES array of the accept script. and the at sign '@' is required before the
                         * file name.
                         */
                        $post =  array('file'=> new CurlFile($file_name_with_full_path));
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$target_url);
                        curl_setopt($ch, CURLOPT_POST,1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                        $httpCode = curl_getinfo($ch , CURLINFO_HTTP_CODE); 
                        $result=curl_exec($ch);
                        
                        // if ($result === false) 
                        //     $result = curl_error($ch);

                        // echo stripslashes($result);

                        // curl_close($ch);
                        // die;
                        $server_response = json_decode($result, TRUE);

                            $data = array(
                                'is_sent'=>1
                                );
                            $this->db->where("id", $sync_info['id']);
                            $this->db->update("sync_info",$data);
                        $error = $server_response['error'];
                        $messsage = $server_response['messsage'];
                        if (!is_dir(APPPATH.'../logs'))
                        mkdir(APPPATH.'../logs', 0777);

                        $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
                            "url: ".$target_url.PHP_EOL.
                            "result: ".$result.PHP_EOL.
                            "error: ".$error.PHP_EOL.
                            "messsage: ".$messsage.PHP_EOL.
                            "-------------------------".PHP_EOL;
                            //Save string to log, use FILE_APPEND to append.
                            file_put_contents('./logs/log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
                        
                            // $response['message'] = $server_response;
                            // $response['error'] = false;
                        if($server_response['error'] === false){
                            
                            $data = array(
                                'is_ack'=>1
                                );
                            $this->db->where("id", $sync_info['id']);
                            $this->db->update("sync_info",$data);
                            //unlink($filename);
                            curl_close ($ch);
                            $response['message'] = "Data uploaded Successfully";
                            $response['error'] = false;
                            $response['showButton'] = false;
                        }else if($server_response['error'] === true){
                            if($server_response['errorno'] == 1062){
                                //unlink($filename);
                                $response['message'] = "data already updated";
                                $response['showButton'] = false;
                                $response['error'] = true;
                            }else{
                                $response['showButton'] = false;
                                $response['message'] = "contact cybextech";
                                $response['error'] = true;
                            }
                        }else{
                            $response['showButton'] = true;
                            $response['message'] = "Data not uploaded. Please do not make other changes in system until sync successfully";
                            $response['error'] = true;
                        }   
                    
                }else{
                    $response['showButton'] = false;
                    $response['message'] = "file not found";
                    $response['error'] = true;
                            // $data['message'] = lang('file_not_exist');
                            // $this->load->view('temp/header');
                            // $this->load->view('backup', $data);
                 }
            }else{
                $response['showButton'] = false;
                $response['message'] = "Data Updated";
                $response['error'] = true;
                        // $data['message'] = lang('file_not_exist');
                        // $this->load->view('temp/header');
                        // $this->load->view('backup', $data);
             }             // $this->load->view('temp/footer');
        }else{
            $response['showButton'] = false;
            $response['message'] = "internet connection error! ";
            $response['error'] = true;
        }

                echo json_encode($response);
    }

    function is_connected()
    {
        $connected = @fsockopen("www.google.com", 80); 
                                            //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;

    }

     public function accept_db_file() {
        
        $file_name = $_FILES["file"]["name"];
        $path = APPPATH . 'local_logs/'.$file_name;
        move_uploaded_file($_FILES['file']['tmp_name'], $path);

        $resp = $this->run_queries($path, $file_name);

        // if($this->syn_db($name)){
        //     unlink($name);
        //     $data['resp'] = true;
        // }else{
        //     $data['resp'] = false;
           
        // }
         echo json_encode($resp);
    }

    public function run_queries($path, $filename){
            $this->db->order_by("id", "DESC");
            $sync_info = $this->db->get("sync_info")->row_array();
            if($filename <> $sync_info['filename']){

                if($path){
                    $mysql_host = $this->db->hostname;
                    $mysql_username = $this->db->username;
                    $mysql_password = $this->db->password;
                    $mysql_database = $this->db->database;
                    $mysqli = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);

                    $mysqli->query('SET foreign_key_checks = 1');
                    // $mysqli->close();
                    // $connection = mysqli_connect($mysql_host, $mysql_username, $mysql_password, $mysql_database);
                    // if (mysqli_connect_errno())
                    //     echo "Failed to connect to MySQL: " . mysqli_connect_error();
                    // Temporary variable, used to store current query
                    $templine = '';
                    $lines = file($path);
                        foreach ($lines as $line) {

                            if (substr($line, 0, 2) == '--' || $line == '')
                                continue;
                            $templine .= $line;
                            if (substr(trim($line), -1, 1) == ';') {
                                if(!mysqli_query($mysqli, $templine)){
                                    // print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error($mysqli) . ': '.mysqli_errno($mysqli).'<br /><br />');
                                    // print(mysqli_error($mysqli));
                                    $data['error'] = true;
                                    $data['errorno'] = mysqli_errno($mysqli);
                                    $data['messsage'] = mysqli_error($mysqli);
                                    
                                    return $data;
                                    break;
                                }
                                $templine = '';
                            }
                        }

                        mysqli_close($mysqli);
                        $data['error'] = false;
                        $data['messsage'] = 'success';
                        return $data;
                }else{
                    $data['error'] = true;
                    $data['errorno'] = 0;
                    $data['messsage'] = 'file not found';
                    return $data;
                }
            }else{
                    $data['error'] = true;
                    $data['errorno'] = 0;
                    $data['messsage'] = 'Data Already Updated';
                    return $data;
            }
    }

    function license_update(){
            if($this->is_connected()){
                $target_url = URL."get_date";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$target_url);
                curl_setopt($ch, CURLOPT_POST,1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                $httpCode = curl_getinfo($ch , CURLINFO_HTTP_CODE); 
                $result=curl_exec($ch);
               
                // if ($result === false) 
                //     $result = curl_error($ch);

                // echo stripslashes($result);

                // curl_close($ch);
                // die;
                $server_response = json_decode($result, TRUE);
                
                    $data = array(
                        'start_date_time'=>$server_response['start_date_time'],
                        'end_date_time'=>$server_response['end_date_time'],
                        'updated_date'=>$server_response['updated_date'],
                        );
                    $this->db->where("id", $server_response['id']);
                    curl_close ($ch);
                    if($this->db->update("account_privileges",$data)){
                        $response['message'] = "Licsnse Updated Successfully";
                        $response['error'] = false;
                        $response['showButton'] = false;
                    }else{
                        $response['message'] = "unSuccessfull";
                        $response['error'] = true;
                        $response['showButton'] = true;
                    }
            }else{
                $response['showButton'] = false;
                $response['message'] = "internet connection error! ";
                $response['error'] = true;
            }
                echo json_encode($response);
        }

        function get_date(){
            $data = $this->db->get('account_privileges')->row_array();
            echo json_encode($data);
        }

    function check_url(){
        // var_dump('expression');
        // die;
        $url  = $this->input->get('base_url');
        // // $exit = true;
        $url1 = rtrim($url, "/");
        $this->db->where('url', $url1);
        $exit = $this->db->get('urls')->num_rows();
       
        $res = false;
        if($exit > 0){
            $res  = true;
        }
        
        $data['resp'] = $res;
        header('Content-Type: application/javascript');
        echo $_GET['callback'] . "(" . json_encode($data) . ");";
        // echo json_encode($data);
    }

    function register() {
        $data['email'] = $this->input->post('email');
        $password = $this->input->post('password');
        $data['password'] = sha1($password);
        $data['phone'] = $this->input->post('phone');
        $data['creation_date'] = time();
        //user meta
        $meta_data['_first_name'] = $this->input->post('first_name');
        $meta_data['_last_name'] = $this->input->post('last_name');
        $meta_data['_age'] = $this->input->post('age');
        $meta_data['_province'] = $this->input->post('province');
        $meta_data['_canton'] = $this->input->post('canton');
        $meta_data['_parroquia'] = $this->input->post('parroquia');
        $meta_data['_device_type'] = $this->input->post('device_type');
        if ($meta_data['_device_type'] == 'ios')
            $meta_data['_ios_device_token'] = $this->input->post('device_token');
        else
            $meta_data['_android_device_token'] = $this->input->post('device_token');

        if (is_empty($data['email']) || is_empty($data['password']) || is_empty($data['phone']) ||
                is_empty($meta_data['_first_name']) || is_empty($meta_data['_last_name']) || is_empty($meta_data['_province']) ||
                is_empty($meta_data['_canton']) || is_empty($meta_data['_parroquia'])) {
            $message = "All Fields are Required";
            api_response($message, true);
        }
        if (!valid_email($data['email'])) {
            $message = "Please enter a valid email address";
            api_response($message, true);
        }


        if (is_already_email('user', $data['email'])) {
            $message = "Sorry, it looks like " . $data['email'] . " belongs to an existing account";
            api_response($message, true);
        }
        //return_json($data);
        $this->db->insert('user', $data);
        $user_id = $this->db->insert_id();
        if ($user_id) {
            $this->crud_model->insert_meta('dxm_users_meta', $user_id, $meta_data);
        }
        $user_data = $this->crud_model->get_table_by_id('user', $user_id);
        $user_meta = $this->crud_model->get_meta('dxm_users_meta', '', $user_id);

        $data = array_merge($user_data[0], $user_meta[0]);
        $result = array(
            'user_id' => $user_id,
            'email' => $data['email'],
            'phone' => $data['phone'],
            '_first_name' => $data['_first_name'],
            '_last_name' => $data['_last_name'],
            '_age' => $data['_age'],
            '_province' => $data['_province'],
            '_canton' => $data['_canton'],
            '_parroquia' => $data['_parroquia']
        );
        api_response('success', false, $result);
        $this->email_model->account_opening('user', $data['email'], $password);
    }

    /* Function Authenticate */

    function authenticate() {
        $user_email = $this->input->post('user');
        $password = $this->input->post('password');
        $password = sha1($password);
        if (is_empty($user_email) || is_empty($password)) {
            $message = "All Fields are Required";
            api_response($message, true);
        }

        $user_id = $this->crud_model->authenticate_user($user_email, $password);
        if ($user_id) {
            $user_data = $this->crud_model->get_table_by_id('user', $user_id);
            $user_meta = $this->crud_model->get_meta('dxm_users_meta', '', $user_id);
            $data = array_merge($user_data[0], $user_meta[0]);
            $result = array(
                'user_id' => $user_id,
                'email' => $data['email'],
                'phone' => $data['phone'],
                '_first_name' => $data['_first_name'],
                '_last_name' => $data['_last_name'],
                '_age' => $data['_age'],
                '_province' => $data['_province'],
                '_canton' => $data['_canton'],
                '_parroquia' => $data['_parroquia']
            );
            api_response('success', false, $result);
        } else {
            $message = "Incorrect Credentials";
            api_response($message, true);
        }
        exit();
    }

    /* Function Forget */

    function reset_password() {
        $data['email'] = $this->input->post('email');

        if (is_empty($data['email'])) {
            $message = "All Fields are Required";
            api_response($message, true);
        }
        if (!valid_email($data['email'])) {
            $message = "Please enter a valid email address";
            api_response($message, true);
        }
        $query = $this->db->get_where('user', array(
            'email' => $data['email']
        ));
        if ($query->num_rows() > 0) {
            $user_id = $query->row()->user_id;
            $password = substr(hash('sha512', rand()), 0, 12);
            $data['password'] = sha1($password);
            $this->db->where('user_id', $user_id);
            $this->db->update('user', $data);
            if ($this->email_model->password_reset_email('user', $user_id, $password)) {

                api_response('Email has sent.', false);
            } else {
                api_response('There is an error to send you email.', true);
            }
        } else {
            $message = "Sorry, it looks like " . $data['email'] . " don't belongs to any account";
            api_response($message, true);
        }
    }

    /* Get The Territories */

    public function territories_post() {
        $id = $this->input->post('id');
        if ($id != "") {
            $this->db->select('BaseTbl.id,BaseTbl.name, BaseTbl.parent_id, BaseTbl.is_parent');
            $this->db->where('parent_id', $id);
            $this->db->from('dxm_territories as BaseTbl');
            $query = $this->db->get();
            $get_data = $query->result();
            if (count($get_data) > 0) {
                api_response('success', false, $get_data);
            } else {
                $message = "Unsuccessfull! Not available";
                api_response($message, true);
            }
        } else {
            $message = "Unsuccessfull! Enter id";
            api_response($message, true);
        }
    }

    public function teritories_post() {
        $id = $this->input->post('id');
         if ($id != "") {
            $this->db->select('BaseTbl.id,BaseTbl.en_name,BaseTbl.ar_name, BaseTbl.parent_id, BaseTbl.isActive');
            $this->db->where('parent_id', $id);
            $this->db->from('services as BaseTbl');
            $query = $this->db->get();
            $get_data = $query->result();
            if (count($get_data) > 0) {
                api_response('success', false, $get_data);
            } else {
                $message = "Unsuccessfull! Not available";
                api_response($message, true);
            }
        } else {
            $message = "Unsuccessfull! Enter id";
            api_response($message, true);
        }
    }

     public function district_post() {
        $id = $this->input->post('id');
         if ($id != "") {
            $this->db->select('BaseTbl.id,BaseTbl.en_name,BaseTbl.ar_name, BaseTbl.parent_id, BaseTbl.is_district');
            $this->db->where('parent_id', $id);
            $this->db->from('territory as BaseTbl');
            $query = $this->db->get();
            $get_data = $query->result();
            if (count($get_data) > 0) {
                api_response('success', false, $get_data);
            } else {
                $message = "Unsuccessfull! Not available";
                api_response($message, true);
            }
        } else {
            $message = "Unsuccessfull! Enter id";
            api_response($message, true);
        }
    }

    /* Get the program list */

    public function program_list() {
        $main_cat = $this->input->post('cat');
        if ($main_cat != '') {
            $query = "SELECT `p`.`id` as `program_id`, `p`.`cat_id` as `cat`, "
                    . "(SELECT COUNT(pci.id) FROM dxm_program_cats pci, dxm_programs pi  "
                    . "WHERE pi.cat_id = pci.id and pci.parent_id = cat) AS `sub_cats`, "
                    . "`p`.`title`, `p`.`mobile_program_guide`, `p`.`mobile_desc`, `p`.`logo`, `p`.`pdf_file`, `p`.`video`, "
                    . "`pc`.`sort_by` as `position` FROM `dxm_programs` as `p` "
                    . "INNER JOIN `dxm_program_cats` as `pc` ON `p`.`cat_id` = `pc`.`id` "
                    . "WHERE `pc`.`parent_id` = '" . $main_cat . "' ORDER BY `position` ASC";
            $result = $this->db->query($query)->result_array();
            if (count($result) > 0) {
                $response_arr = array();
                $i = 0;
                foreach ($result as $single_result) {
                    $response_arr[$i]['program_id'] = (!is_empty($single_result['program_id'])) ? $single_result['program_id'] : 'null';
                    $response_arr[$i]['cat_id'] = (!is_empty($single_result['cat'])) ? $single_result['cat'] : 'null';
                    $response_arr[$i]['sub_cats'] = (!is_empty($single_result['sub_cats'])) ? $single_result['sub_cats'] : 'null';
                    $response_arr[$i]['detail'] = (!is_empty($single_result['mobile_desc'])) ? $single_result['mobile_desc'] : 'null';
                    $response_arr[$i]['position'] = (!is_empty($single_result['position'])) ? $single_result['position'] : 'null';
                    $response_arr[$i]['title'] = (!is_empty($single_result['title'])) ? $single_result['title'] : 'null';
                    $response_arr[$i]['program_guide'] = (!is_empty($single_result['mobile_program_guide'])) ? $single_result['mobile_program_guide'] : 'null';
                    if (!is_empty($single_result['logo'])) {
                        if (file_exists('uploads/program_logo_image/' . $single_result['logo'])) {
                            $response_arr[$i]['logo'] = base_url() . '/uploads/program_logo_image/' . $single_result['logo'];
                        } else
                            $response_arr[$i]['logo'] = null;
                    }
                    if (!is_empty($single_result['pdf_file'])) {
                        if (file_exists('uploads/program_file/' . $single_result['pdf_file'])) {
                            $response_arr[$i]['pdf_file'] = base_url() . '/uploads/program_file/' . $single_result['pdf_file'];
                        } else
                            $response_arr[$i]['pdf_file'] = null;
                    }
                    if (!is_empty($single_result['video'])) {
                        if (file_exists($single_result['video'])) {
                            $response_arr[$i]['video'] = base_url() . $single_result['video'];
                        } else
                            $response_arr[$i]['video'] = null;
                    }
                    $i++;
                }
                $message = "Success";
                api_response($message, false, $response_arr);
            } else {
                $message = "No, program available for this Category";
                api_response($message, true);
            }
        } else {
            $message = "Unsuccessfull! Enter id";
            api_response($message, true);
        }
    }

    /* Get the single program */

    public function program() {
        $program_id = $this->input->post('program_id');
        if (!is_empty($program_id)) {
            $program_data = $this->crud_model->get_table_by_id('dxm_programs', $program_id);

            if (count($program_data) > 0) {
                $single_result = $program_data[0];
                $response_arr = array();
                $response_arr['program_id'] = (!is_empty($single_result['id'])) ? $single_result['id'] : 'null';
                $response_arr['cat_id'] = (!is_empty($single_result['cat_id'])) ? $single_result['cat_id'] : 'null';
                $response_arr['detail'] = (!is_empty($single_result['mobile_desc'])) ? $single_result['mobile_desc'] : 'null';
                $response_arr['title'] = (!is_empty($single_result['title'])) ? $single_result['title'] : 'null';
                $response_arr['program_guide'] = (!is_empty($single_result['mobile_program_guide'])) ? $single_result['mobile_program_guide'] : 'null';
                if (!is_empty($single_result['logo'])) {
                    if (file_exists('uploads/program_logo_image/' . $single_result['logo'])) {
                        $response_arr['logo'] = base_url() . '/uploads/program_logo_image/' . $single_result['logo'];
                    } else
                        $response_arr['logo'] = null;
                }
                if (!is_empty($single_result['pdf_file'])) {
                    if (file_exists('uploads/program_file/' . $single_result['pdf_file'])) {
                        $response_arr['pdf_file'] = base_url() . '/uploads/program_file/' . $single_result['pdf_file'];
                    } else
                        $response_arr['pdf_file'] = null;
                }
                if (!is_empty($single_result['video'])) {
                    if (file_exists($single_result['video'])) {
                        $response_arr['video'] = base_url() . $single_result['video'];
                    } else
                        $response_arr['video'] = null;
                }

                $message = "Success";
                api_response($message, false, $response_arr);
            } else {
                $message = "No, program available";
                api_response($message, true);
            }
        } else {
            $message = "Unsuccessfull! Enter id";
            api_response($message, true);
        }
    }

    /* Get the Schedule */

    public function get_schedule() {
        $date = $this->input->post('date');
        $limit = $this->input->post('pL');
        $page = $this->input->post('pN');
        $featured = $this->input->post('featured');
        $user_id = $this->input->post('user_id');
        if (is_empty($page))
            $page = 0;
        if (is_empty($limit))
            $limit = 10;
        if (is_empty($date)) {

            $date = date('Y-m-d');
        } else {
            $date = date_create($date);
            $date = date_format($date, 'Y-m-d');
        }
        //fot count
        $this->db->select('*');
        $this->db->where('status', 1);
        if (!is_empty($featured))
            $this->db->where('featured', 1);
        $this->db->where('schedule_date >=', $date);
        $this->db->from('dxm_schedule');
        $total_result = $this->db->count_all_results();
        // for result
        $this->db->select('*');
        $this->db->where('status', 1);
        if (!is_empty($featured))
            $this->db->where('featured', 1);
        $this->db->where('schedule_date >=', $date);
        $this->db->from('dxm_schedule');
        $this->db->order_by('schedule_date', 'DESC');
        $this->db->limit($limit, $page);
//        echo $this->db->get_compiled_select('mytable');
//        exit();
        $results = $this->db->get()->result_array();


        if ($total_result > 0) {
            $response_data = array();
            $i = 0;
            foreach ($results as $single_result) {
                $schedule_meta = $data = array();
                $schedule_meta = $this->crud_model->get_meta('dxm_schedule_meta', '', $single_result['id']);
                $data = array_merge($single_result, $schedule_meta[0]);
                $response_data[$i]['id'] = $data['id'];
                $response_data[$i]['title'] = $data['title'];
                $response_data[$i]['detail'] = $data['mobile_description'];
                if (!is_empty($data['feature_image'])) {
                    if (file_exists('uploads/event_feature_image/' . $data['feature_image'])) {
                        $response_data[$i]['feature_image'] = base_url() . '/uploads/event_feature_image/' . $data['feature_image'];
                    } else
                        $response_data[$i]['feature_image'] = base_url() . '/uploads/event_feature_image/default.png';
                } else
                    $response_data[$i]['feature_image'] = base_url() . '/uploads/event_feature_image/default.png';
                $response_data[$i]['schedule_date'] = $data['schedule_date'];
                $response_data[$i]['schedule_time'] = $data['schedule_time'];

                //check if user registered
                if ($user_id) {
                    $query = $this->db->get_where('dxm_schedule_meta', array('type_id' => $data['id'], 'meta_key' => 'event_attendee', 'meta_value' => $user_id));
                    if ($query->num_rows() > 0) {
                        $response_data[$i]['is_user_registered'] = true;
                    } else {
                        $response_data[$i]['is_user_registered'] = false;
                    }
                }

                $i++;
            }
            $message = "Successfull!";
            $response_ar = array('total_record' => $total_result, 'results' => $response_data);
            api_response($message, false, $response_ar);
        } else {
            $message = "No, schedule available";
            api_response($message, true);
        }
        $message = "Unsuccessfull! Enter id";
        api_response($message, true);
    }

    /* Reguster User for an event */

    public function register_event_attendee() {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        if ($user_id && $event_id) {
            $query = $this->db->get_where('dxm_schedule_meta', array('type_id' => $event_id, 'meta_key' => 'event_attendee', 'meta_value' => $user_id));
            if ($query->num_rows() > 0) {
                $this->db->where('type_id', $event_id);
                $this->db->where('meta_key', 'event_attendee');
                $this->db->where('meta_value', $user_id);
                $this->db->delete('dxm_schedule_meta');
                $message = "You are unregistered successfully";
                api_response($message, true);
            } else {
                $data['type_id'] = $event_id;
                $data['meta_key'] = 'event_attendee';
                $data['meta_value'] = $user_id;
                $this->db->insert('dxm_schedule_meta', $data);
                $id = $this->db->insert_id();
                if ($id > 0) {
                    $message = "You are registered successfully";
                    api_response($message, true);
                }
            }
        } else {
            $message = "Unsuccessfull! Enter id";
            api_response($message, true);
        }
    }

    /* Get DM Team event schedule */

    public function dm_event_schedule_drop() {
        $date = date('Y-m-d');
        $this->db->select('*')
                ->where('status', 1)
                ->where('schedule_date >=', $date)
                ->where('relevant_program', 13)
                ->from('dxm_schedule')
                ->order_by('schedule_date', 'DESC');

        $results = $this->db->get()->result_array();
        if (count($results) > 0) {
            $response_data = array();
            $i = 0;
            foreach ($results as $single_result) {
                $schedule_meta = $data = array();
                $schedule_meta = $this->crud_model->get_meta('dxm_schedule_meta', '', $single_result['id']);
                $data = array_merge($single_result, $schedule_meta[0]);
                $response_data[$i]['event_id'] = $data['id'];
                $time = date('h:i:sa', strtotime($data['schedule_time']));
                $response_data[$i]['schedule'] = $data['schedule_date'] . ' ' . $time . ' at ' . $data['parroquia'] . ' ' . $data['canton'] . ',' . $data['province'];

                $i++;
            }
            $message = "Successfull!";

            api_response($message, false, $response_data);
        } else {
            $message = "No, schedule available";
            api_response($message, true);
        }
    }

    /* Function Submit Request */

    function send_request() {
        $data['first_name'] = $this->input->post('first_name');
        $data['last_name'] = $this->input->post('last_name');
        $data['phone'] = $this->input->post('phone');
        $data['program_id'] = $this->input->post('program_id');
        $data['status'] = 1;
        $data['creation_date'] = time();

        if (is_empty($data['program_id'])) {
            $message = "All Fields are Required";
            api_response($message, true);
        }
        //user meta
        $meta = $this->input->post('meta');
        $meta_data = array();
        if (!empty($meta) && is_array($meta)) {
            foreach ($meta as $key => $value) {
                $meta_data[$key] = $value;
            }
        } else {
            $message = "Meta is incorrect";
            api_response($message, true);
        }

        $this->db->insert('dxm_user_request', $data);
        $request_id = $this->db->insert_id();
        if ($request_id && !empty($meta_data)) {
            $this->crud_model->insert_meta('dxm_users_request_meta', $request_id, $meta_data);
        }
        $message = "Your request has been sent successfully";
        api_response($message, false);
        //$this->email_model->account_opening('user', $data['email'], $password);
    }

    //function settings
    function settings($para) {
        switch ($para) {
            case 'register_video':
                $response['video_link'] = base_url() . "/uploads/program_video/program_video_34.mp4";
                $message = "success";
                api_response($message, false, $response);
                break;
            case 'call_options':
                $response[0] = 'Early Morning';
                $response[1] = 'After Noon';
                $response[2] = 'Evening';
                $message = "success";
                api_response($message, false, $response);
                break;
        }
    }

    // function daniel media

    function get_media() {
        $cat = $this->input->post('cat');
        $type = $this->input->post('media_type');

        
                for ($i = 0; $i <= 20; $i++) {
                    $response['video_link'][$i] = base_url() . "/uploads/program_video/program_video_34.mp4";
                    $response['image_link'][$i] = base_url() . "/uploads/event_feature_image/default.png";
                }
                $message = "success";
                api_response($message, false, $response);
            
    }

}

/* End of file Api.php */
    /* Location: ./application/controllers/api.php */    