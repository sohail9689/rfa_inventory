<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Inventory
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if ($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
			<div class="row-fluid">
				<div class="span12">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('info')) { ?>
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('error')) { ?>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		<!-- END Alert widget-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<!--BEGIN METRO STATES-->
			<div class="metro-nav">
				<?php if ($privileges['sales'] == 1) { ?>
					<div class="metro-nav-block nav-olive double">
						<a data-original-title="" href="inventory/sales_save">
							<i class="icon-signout"></i>
							<div class="info">Add New</div>
							<div class="status">Sales</div>
						</a>
					</div>
					<div class="metro-nav-block nav-olive ">
						<a data-original-title="" href="inventory/sales_list">
							<i class="icon-signout"></i>
							<div class="info">List All</div>
							<div class="status">Sales</div>
						</a>
					</div>
				<?php } ?>
				<?php if ($privileges['purchase'] == 1) { ?>
					<div class="metro-nav-block nav-block-yellow">
						<a data-original-title="" href="inventory/purchase_save">
							<i class="icon-signin"></i>
							<div class="info">Add New</div>
							<div class="status">Purchase</div>
						</a>
					</div>
					<div class="metro-nav-block nav-block-yellow double">
						<a data-original-title="" href="inventory/purchase_list">
							<i class="icon-signin"></i>
							<div class="info">List All</div>
							<div class="status">Purchase</div>
						</a>
					</div>
				<?php } ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if ($privileges['sales_return'] == 1) { ?>
					<div class="metro-nav-block nav-block-blue double">
						<a data-original-title="" href="inventory/sales_return_save">
							<i class="icon-reply-all"></i>
							<div class="info">Add New</div>
							<div class="status">Delivery Notes Return</div>
						</a>
					</div>
					<div class="metro-nav-block nav-block-blue ">
						<a data-original-title="" href="inventory/sales_return_list">
							<i class="icon-reply-all"></i>
							<div class="info">List All</div>
							<div class="status">Delivery Notes Return</div>
						</a>
					</div>
				<?php } ?>

				<?php if ($privileges['purchase_return'] == 1) { ?>
					<div class="metro-nav-block nav-block-purple">
						<a data-original-title="" href="inventory/purchase_return_save">
							<i class="icon-arrow-left"></i>
							<div class="info">Add New</div>
							<div class="status">Purchase Return</div>
						</a>
					</div>
					<div class="metro-nav-block nav-block-purple double">
						<a data-original-title="" href="inventory/purchase_return_list">
							<i class="icon-arrow-left"></i>
							<div class="info">List All</div>
							<div class="status">Purchase Return</div>
						</a>
					</div>
				<?php } ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if ($privileges['customer'] == 1) { ?>
					<div class="metro-nav-block nav-block-orange double">
						<a data-original-title="" href="inventory/customer_save">
							<i class="icon-group"></i>
							<div class="info">Add New</div>
							<div class="status">Customer</div>
						</a>
					</div>
					<div class="metro-nav-block nav-block-orange ">
						<a data-original-title="" href="inventory/customer_list">
							<i class="icon-group"></i>
							<div class="info">List All</div>
							<div class="status">Customer</div>
						</a>
					</div>
				<?php } ?>
				<?php if ($privileges['supplier'] == 1) { ?>
					<div class="metro-nav-block nav-block-red">
						<a data-original-title="" href="inventory/supplier_save">
							<i class="icon-male"></i>
							<div class="info">Add New</div>
							<div class="status">Supplier</div>
						</a>
					</div>
					<div class="metro-nav-block nav-block-red double">
						<a data-original-title="" href="inventory/supplier_list">
							<i class="icon-male"></i>
							<div class="info">List All</div>
							<div class="status">Supplier</div>
						</a>
					</div>
				<?php } ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if ($privileges['item'] == 1) { ?>
					<div class="metro-nav-block nav-light-blue double">
						<a data-original-title="" href="inventory/item_save">
							<i class="icon-gift"></i>
							<div class="info">Add New</div>
							<div class="status">Item</div>
						</a>
					</div>
					<div class="metro-nav-block nav-light-blue">
						<a data-original-title="" href="inventory/item_list">
							<i class="icon-gift"></i>
							<div class="info">List All</div>
							<div class="status">Item</div>
						</a>
					</div>

				<?php }
				 ?>
					<div class="metro-nav-block nav-light-blue double">
						<a data-original-title="" href="inventory/expense_save">
							<i class="icon-gift"></i>
							<div class="info">Add New</div>
							<div class="status">Purchase Expenses</div>
						</a>
					</div>
					<div class="metro-nav-block nav-light-blue">
						<a data-original-title="" href="inventory/expense_list">
							<i class="icon-gift"></i>
							<div class="info">List All</div>
							<div class="status">Expense</div>
						</a>
					</div>
					<div class="metro-nav-block nav-light-blue double">
						<a data-original-title="" href="inventory/head_save">
							<i class="icon-arrow-left"></i>
							<div class="info">Add New</div>
							<div class="status">Account Heads</div>
						</a>
					</div>
					<div class="metro-nav-block nav-light-blue">
						<a data-original-title="" href="inventory/head_list">
							<i class="icon-arrow-left"></i>
							<div class="info">List All</div>
							<div class="status">Account Heads</div>
						</a>
					</div>
					<div class="metro-nav-block nav-light-blue double">
						<a data-original-title="" href="inventory/expense_entries">
							<i class="icon-signin"></i>
							<div class="info">Add New</div>
							<div class="status">Expense Entries</div>
						</a>
					</div>
					<div class="metro-nav-block nav-light-blue">
						<a data-original-title="" href="inventory/expense_entries_list">
							<i class="icon-signin"></i>
							<div class="info">List All</div>
							<div class="status">Expenses List</div>
						</a>
					</div>
				<?php  ?>
			</div>
			<div class="space10"></div>
			<!--END METRO STATES-->
		</div>

		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>