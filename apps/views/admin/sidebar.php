<div class="sidebar-scroll">
	<div id="sidebar" class="nav-collapse collapse">

		<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
		<div class="navbar-inverse">
			<form class="navbar-search visible-phone">
				<input type="text" class="search-query" placeholder="Search" />
			</form>
		</div>
		<!-- END RESPONSIVE QUICK SEARCH FORM -->
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="sidebar-menu">
			<li class="sub-menu <?php if($menu == 'dashboard'){ ?>active<?php } ?>">
				<a class="" href="dashboard">
					<i class="icon-dashboard"></i>
					<span>Dashboard</span>
				</a>
			</li>
			<?php if($privileges['inventory_menu'] == 1){ ?>
			<li class="sub-menu <?php if($menu == 'inventory'){ ?>active<?php } ?>">
				<a href="inventory" class="">
					<i class="icon-gift"></i>
					<span>Inventory</span>
				</a>
			</li>
			<?php } ?>
			<?php if($privileges['accounts_menu'] == 1){ ?>
			<li class="sub-menu <?php if($menu == 'accounts'){ ?>active<?php } ?>">
				<a class="" href="accounts">
					<i class="icon-money"></i>
					<span>Accounts</span>
				</a>
			</li>
			<?php } ?>
		
			<?php if($privileges['report_menu'] == 1){ ?>
			<li class="sub-menu <?php if($menu == 'report'){ ?>active<?php } ?>">
				<a class="" href="report">
					<i class="icon-bar-chart"></i>
					<span>Reports</span>
				</a>
			</li>
			<?php } ?>
			<?php if($privileges['settings_menu'] == 1){ ?>
			<li class="sub-menu <?php if($menu == 'settings'){ ?>active<?php } ?>">
				<a class="" href="settings">
					<i class="icon-cogs"></i>
					<span>Settings</span>
				</a>
			</li>
			<?php } ?>
			<?php if($privileges['user_menu'] == 1){ ?>
			<li class="sub-menu <?php if($menu == 'user'){ ?>active<?php } ?>">
				<a href="user" class="">
					<i class="icon-user"></i>
					<span>Users</span>
				</a>
			</li>
			<?php } ?>
			
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>