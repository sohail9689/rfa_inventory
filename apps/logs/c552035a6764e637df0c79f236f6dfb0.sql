INSERT INTO `sync_info` (`filename`) VALUES ('c552035a6764e637df0c79f236f6dfb0');
DELETE FROM `emps`
WHERE `id` = '64';
UPDATE `sync_info` SET `is_sent` = 1
WHERE `id` = '9';
UPDATE `sync_info` SET `is_ack` = 1
WHERE `id` = '9';
UPDATE `accounts_calander` SET `start_date` = '2018-07-01', `end_date` = '2019-06-30', `created` = '2019-06-17 20:18:36'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2019-11-17 11:20:36'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2019-11-17 11:21:54'
WHERE `user_id` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '39', '1715', '12', '12', '2019-11-17', 'sdsad', '1', '2019-11-17 11:29:41', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1715', '44', 'Raw', '577', '10', '12', 'sq ft', '12', 17.28, '2019-11-17', '12', 144, '2019-11-17 11:29:41', '8');
UPDATE `items` SET `avco_price` = 12, `modified` = '2019-11-17 11:29:41', `modified_by` = '8'
WHERE `id` = '44';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '39', `purchase_no` = '1715', `supplier_no` = '12', `gate_no` = '12', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'sdsad', `modified` = '2019-11-17 11:29:43', `modified_by` = '8'
WHERE `id` = '981';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '2019-11-17', 'Direct Purchase', 'Purchase', '1715', '2019-11-17 11:29:43', '8');
UPDATE `purchase_master` SET `ac_id` = 3230
WHERE `id` = '981';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '970', NULL, 161, 'sdsad', '2019-11-17', '2019-11-17 11:29:43', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '577', '144', NULL, 'sdsad', '2019-11-17', '2019-11-17 11:29:43', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '1034', 17, NULL, 'sdsad', '2019-11-17', '2019-11-17 11:29:43', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `type`, `created_at`, `created_by`) VALUES ('2', NULL, '.10', 'Other chemical', 'Other Items', '2019-11-17 11:42:31', '8');
INSERT INTO `items` (`company_id`, `code`, `name`, `ac_id`, `type`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1503', 'Other chemical', 1292, 'Other Items', 'asd', '', 'Active', '2019-11-17 11:42:31', '8');
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '42', '1716', '12', '23', '2019-11-17', 'sdsd', '1', '2019-11-17 11:44:54', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1716', '537', 'Other Items', '1292', 'asd', 'asd', 'sq ft', '', 0, '2019-11-17', 'asd', 0, '2019-11-17 11:44:54', '8');
UPDATE `items` SET `avco_price` = NAN, `modified` = '2019-11-17 11:44:54', `modified_by` = '8'
WHERE `id` = '537';
DELETE FROM `purchase_details`
WHERE `id` = '2920';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2019-11-17 11:45:02', `modified_by` = '8'
WHERE `id` = '2920';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '42', `purchase_no` = '1716', `supplier_no` = '12', `gate_no` = '23', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'asd', `modified` = '2019-11-17 11:45:25', `modified_by` = '8'
WHERE `id` = '982';
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1716', '537', 'Other Items', '1292', '12', '13', 'sq ft', '12', 18.72, '2019-11-17', '12', 156, '2019-11-17 11:45:25', '8');
UPDATE `items` SET `avco_price` = 12, `modified` = '2019-11-17 11:45:25', `modified_by` = '8'
WHERE `id` = '537';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '42', '1717', '12', 'sd', '2019-11-17', 'asd', '1', '2019-11-17 12:09:31', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1717', '537', 'Other Items', '1292', '10', '10', 'Kg', '10', 10, '2019-11-17', '10', 100, '2019-11-17 12:09:31', '8');
UPDATE `items` SET `avco_price` = 11.130434782609, `modified` = '2019-11-17 12:09:31', `modified_by` = '8'
WHERE `id` = '537';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '42', `purchase_no` = '1717', `supplier_no` = '12', `gate_no` = 'sd', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'asd', `modified` = '2019-11-17 12:09:34', `modified_by` = '8'
WHERE `id` = '983';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '2019-11-17', 'Direct Purchase', 'Purchase', '1717', '2019-11-17 12:09:34', '8');
UPDATE `purchase_master` SET `ac_id` = 3231
WHERE `id` = '983';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '967', NULL, 110, 'asd', '2019-11-17', '2019-11-17 12:09:34', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '1292', '100', NULL, 'asd', '2019-11-17', '2019-11-17 12:09:34', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1717', '1034', 10, NULL, 'asd', '2019-11-17', '2019-11-17 12:09:34', '8');
DELETE FROM `ac_journal_details`
WHERE `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30'
AND `journal_no` = 'Purchase_1717';
DELETE FROM `ac_journal_master`
WHERE `journal_no` = 'Purchase_1717'
AND `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1717';
DELETE FROM `purchase_master`
WHERE `id` = '983';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1716';
DELETE FROM `purchase_master`
WHERE `id` = '982';
DELETE FROM `ac_journal_details`
WHERE `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30'
AND `journal_no` = 'Purchase_1715';
DELETE FROM `ac_journal_master`
WHERE `journal_no` = 'Purchase_1715'
AND `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1715';
DELETE FROM `purchase_master`
WHERE `id` = '981';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1714';
DELETE FROM `purchase_master`
WHERE `id` = '774';
DELETE FROM `purchase_details`
WHERE `purchase_date` >= '2019-07-01'
AND `purchase_date` <= '2020-06-30'
AND `purchase_no` = '1654';
DELETE FROM `purchase_master`
WHERE `id` = '708';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `supplier_no`, `gate_no`, `purchase_date`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '42', '1715', 'asd', 'asd', '2019-11-17', 'asd', '1', '2019-11-17 12:14:21', '8');
INSERT INTO `purchase_details` (`company_id`, `purchase_no`, `item_id`, `item_type`, `ac_id`, `quantity`, `sq_weight`, `unit`, `vat_percent`, `vat_amount`, `purchase_date`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('2', '1715', '537', 'Other Items', '1292', '10', '10', 'Kg', '', 0, '2019-11-17', '10', 100, '2019-11-17 12:14:21', '8');
UPDATE `items` SET `avco_price` = 10, `modified` = '2019-11-17 12:14:21', `modified_by` = '8'
WHERE `id` = '537';
UPDATE `purchase_master` SET `company_id` = '2', `supplier_id` = '42', `purchase_no` = '1715', `supplier_no` = 'asd', `gate_no` = 'asd', `draft` = 0, `purchase_date` = '2019-11-17', `notes` = 'asd', `modified` = '2019-11-17 12:14:22', `modified_by` = '8'
WHERE `id` = '984';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '2019-11-17', 'Direct Purchase', 'Purchase', '1715', '2019-11-17 12:14:22', '8');
UPDATE `purchase_master` SET `ac_id` = 3232
WHERE `id` = '984';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '967', NULL, 100, 'asd', '2019-11-17', '2019-11-17 12:14:22', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `chart_id`, `debit`, `credit`, `memo`, `journal_date`, `created`, `created_by`) VALUES ('2', 'Purchase_1715', '1292', '100', NULL, 'asd', '2019-11-17', '2019-11-17 12:14:22', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 15:12:12'
WHERE `user_id` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '0', `sales` = '0', `sales_return` = '0', `purchase` = '0', `purchase_return` = '0', `supplier` = '0', `item` = '0', `customer` = '0', `hr_menu` = '1', `employee` = '1', `accounts_menu` = '1', `journal` = '1', `ac_head` = '1', `chart_of_account` = '1', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '1', `sales_report` = '1', `sales_return_report` = '1', `inventory_report` = '1', `production_report` = '1', `ledger_report` = '1', `trial_balance_report` = '1', `balance_sheet_report` = '1', `income_statement_report` = '1', `bills_receivable_report` = '1', `bills_payable_report` = '1', `cash_book_report` = '1', `bank_book_report` = '1', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '0', `production` = '0', `recipe_detail` = '0', `production_details` = '0', `selection_box` = '0', `modified` = '2020-01-11 15:15:42', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '0', `sales` = '0', `sales_return` = '0', `purchase` = '0', `purchase_return` = '0', `supplier` = '0', `item` = '0', `customer` = '0', `hr_menu` = '1', `employee` = '1', `accounts_menu` = '1', `journal` = '1', `ac_head` = '1', `chart_of_account` = '1', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '0', `purchase_return_report` = '0', `sales_report` = '0', `sales_return_report` = '0', `inventory_report` = '0', `production_report` = '0', `ledger_report` = '1', `trial_balance_report` = '1', `balance_sheet_report` = '1', `income_statement_report` = '1', `bills_receivable_report` = '1', `bills_payable_report` = '1', `cash_book_report` = '1', `bank_book_report` = '1', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '0', `production` = '0', `recipe_detail` = '0', `production_details` = '0', `selection_box` = '0', `modified` = '2020-01-11 15:16:28', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 15:19:18'
WHERE `user_id` = '8';
UPDATE `companies` SET `name` = 'Cybex Tech', `address` = '97 B Gulberg - II', `area` = 'Main Market', `city` = 'Lahore', `zip` = '54400', `country` = 'Pakistan', `phone` = '+92 (42) 0000000', `email` = 'info@cybextech.com', `contact_person` = 'Umair Iqbal', `mobile_no` = '+92310000000', `currency_id` = '87', `currency_symbol_position` = 'Before', `status` = 'Active', `modified` = '2020-01-11 15:21:55', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 15:22:07'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 17:56:28'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', 'BPV', '2020-01-11 18:01:43', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '553', '1000', NULL, '', '2020-01-11 18:01:43', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:01:45', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '546', NULL, '1000', '', '2020-01-11 18:01:45', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '23876';
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:00', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '546', NULL, '200', '', '2020-01-11 18:02:00', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:10', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '524', NULL, '500', '', '2020-01-11 18:02:10', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:13', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '524', NULL, '300', '', '2020-01-11 18:02:13', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '23879';
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:02:57', `modified_by` = '8'
WHERE `id` = '3623';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'BPV_100165', '2020-01-11', '524', NULL, '300', '', '2020-01-11 18:02:57', '8');
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23875';
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23877';
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23878';
UPDATE `ac_journal_details` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `created` = '2020-01-11 18:03:00', `created_by` = '8'
WHERE `id` = '23880';
UPDATE `ac_journal_master` SET `journal_no` = 'BPV_100165', `journal_date` = '2020-01-11', `modified` = '2020-01-11 18:03:00', `modified_by` = '8'
WHERE `id` = '3623';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-01-11 22:10:52'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', 'JV_100004', '2020-01-11', 'JV', '2020-01-11 22:11:15', '8');
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'JV_100004', '2020-01-11', '510', '100', NULL, '', '2020-01-11 22:11:15', '8');
UPDATE `ac_journal_master` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `modified` = '2020-01-11 22:11:19', `modified_by` = '8'
WHERE `id` = '3624';
INSERT INTO `ac_journal_details` (`company_id`, `journal_no`, `journal_date`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', 'JV_100004', '2020-01-11', '516', NULL, '100', '', '2020-01-11 22:11:19', '8');
UPDATE `ac_journal_details` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `created` = '2020-01-11 22:11:22', `created_by` = '8'
WHERE `id` = '23881';
UPDATE `ac_journal_details` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `created` = '2020-01-11 22:11:22', `created_by` = '8'
WHERE `id` = '23882';
UPDATE `ac_journal_master` SET `journal_no` = 'JV_100004', `journal_date` = '2020-01-11', `modified` = '2020-01-11 22:11:22', `modified_by` = '8'
WHERE `id` = '3624';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 13:33:28'
WHERE `user_id` = '8';
DELETE FROM `users`
WHERE `id` = '9';
DELETE FROM `user_privileges`
WHERE `ref_user` = '9';
UPDATE `users` SET `email` = 'alw@gmail.com', `name` = 'Hamid Riaz', `status` = 'Active', `modified` = '2020-02-08 13:34:13', `modified_by` = '8', `company_id` = '2', `password` = '40bd001563085fc3', `type` = 'Admin'
WHERE `id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 13:34:22'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_001', '2020-02-08', 'JV', '2020-02-08 15:14:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (1, '513', '10', NULL, 'expenses 10 debit', '2020-02-08 15:14:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('1', '516', '20', NULL, 'non current 20', '2020-02-08 15:17:01', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_001', '2020-02-08', 'JV', '2020-02-08 15:17:36', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (2, '510', '20', NULL, 'asdasd', '2020-02-08 15:17:36', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('2', '510', '10', NULL, 'sadasd', '2020-02-08 15:18:48', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_001', '2020-02-08', 'JV', '2020-02-08 15:19:22', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (3, '510', '10', NULL, 'liablities 10', '2020-02-08 15:19:22', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '516', '20', NULL, 'asdas', '2020-02-08 15:21:07', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '515', NULL, '30', 'asdsa', '2020-02-08 15:21:15', '8');
UPDATE `ac_journal_details` SET `memo` = 'liablities 102'
WHERE `id` = '5';
UPDATE `ac_journal_details` SET `memo` = 'asdsa2'
WHERE `id` = '7';
UPDATE `ac_journal_details` SET `memo` = 'asdsa2'
WHERE `id` = '7';
UPDATE `ac_journal_details` SET `memo` = 'asdsa2'
WHERE `id` = '7';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '516', '2', NULL, 'wa', '2020-02-08 15:53:44', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('3', '510', '1', NULL, '', '2020-02-08 15:55:32', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-08', 'CPV', '2020-02-08 16:01:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (4, '510', '12', NULL, 'adasd', '2020-02-08 16:01:54', '8');
UPDATE `ac_journal_details` SET `memo` = 'adasd'
WHERE `id` = '10';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '510', '12', NULL, 'sad', '2020-02-08 16:04:47', '8');
UPDATE `ac_journal_details` SET `memo` = 'sad'
WHERE `id` = '11';
UPDATE `ac_journal_details` SET `memo` = 'sadsss'
WHERE `id` = '11';
DELETE FROM `ac_journal_details`
WHERE `id` = '11';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-02-08', 'CPV', '2020-02-08 16:07:39', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (5, '510', '12', NULL, 'asasasd', '2020-02-08 16:07:39', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('5', '515', NULL, '12', 'sdsd', '2020-02-08 16:07:44', '8');
UPDATE `ac_journal_details` SET `memo` = 'asasasd1111111'
WHERE `id` = '12';
UPDATE `ac_journal_details` SET `memo` = 'sdsd1111111111'
WHERE `id` = '13';
DELETE FROM `ac_journal_details`
WHERE `id` = '13';
DELETE FROM `ac_journal_details`
WHERE `id` = '12';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 16:08:43'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-02-08', 'CPV', '2020-02-08 16:08:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (6, '510', '12', NULL, 'dsds', '2020-02-08 16:08:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('6', '517', NULL, '12', 'asdsad', '2020-02-08 16:09:01', '8');
UPDATE `ac_journal_details` SET `memo` = 'dsds'
WHERE `id` = '14';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-08 16:12:16'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-02-08', 'CPV', '2020-02-08 16:13:23', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (7, '509', '12', NULL, 'sad', '2020-02-08 16:13:23', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('7', '513', NULL, '12', 'sads', '2020-02-08 16:13:28', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('7', '513', NULL, '2', '', '2020-02-08 16:13:50', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-02-08', 'CPV', '2020-02-08 16:20:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (8, '', '12', NULL, '', '2020-02-08 16:20:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '', '13', NULL, '', '2020-02-08 16:20:19', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '513', '13', NULL, 'sadsd', '2020-02-08 16:20:30', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '', NULL, '13', '', '2020-02-08 16:20:39', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('8', '513', NULL, '13', 'asdasd', '2020-02-08 16:20:47', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1006', '2020-02-08', 'CPV', '2020-02-08 16:28:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (9, '513', '12', NULL, 'sdad', '2020-02-08 16:28:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('9', '531', NULL, '12', 'sadsa', '2020-02-08 16:28:47', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1007', '2020-02-08', 'CPV', '2020-02-08 16:29:55', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (10, '513', '23', NULL, 'asdasd', '2020-02-08 16:29:55', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '516', NULL, '23', 'sad', '2020-02-08 16:29:58', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1008', '2020-02-08', 'CPV', '2020-02-08 16:31:19', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (11, '513', '23', NULL, 'asdsa', '2020-02-08 16:31:19', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('11', '515', NULL, '23', 'asd', '2020-02-08 16:31:22', '8');
DELETE FROM `ac_journal_details`
WHERE `journal_date` >= '2019-07-01'
AND `journal_date` <= '2020-06-30'
AND `journal_no` = '1920_1008';
DELETE FROM `ac_journal_master`
WHERE `id` = '11';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '515', '12', NULL, 'sdsa', '2020-02-08 16:59:10', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '516', NULL, '35', 'sdas', '2020-02-08 16:59:17', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('10', '516', NULL, '-23', 'sda', '2020-02-08 16:59:19', '8');
DELETE FROM `ac_journal_details`
WHERE `id` = '32';
DELETE FROM `ac_journal_details`
WHERE `id` = '27';
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '515', '10.10.90', 'test asset', 'sadas', '', 'Active', '2020-02-08 17:03:11', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-09 12:10:33'
WHERE `user_id` = '8';
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '515', NULL, '12', 'ssd', '2020-02-09 12:16:00', '8');
INSERT INTO `emps` (`company_id`, `code`, `name`, `father_name`, `cnic`, `joining`, `resign`, `present_address`, `permanent_address`, `voter_id`, `department`, `designation`, `mobile`, `email`, `salary`, `house_rent`, `account_no`, `notes`, `status`, `created`, `created_by`) VALUES ('2', 1001, 'sohaill', '', '', '2020-02-09', '--', '', '', NULL, '', '', '', '', '', '', '', '', '', '2020-02-09 12:17:13', '8');
UPDATE `emps` SET `name` = 'sohaill', `father_name` = '', `cnic` = '', `joining` = '2020-02-09', `resign` = '0000-00-00', `present_address` = '', `permanent_address` = '', `voter_id` = NULL, `department` = '', `designation` = '', `mobile` = '', `email` = '', `salary` = '0', `house_rent` = '0', `account_no` = '', `notes` = '', `status` = 'Active', `modified` = '2020-02-09 12:17:27', `modified_by` = '8'
WHERE `id` = '63';
INSERT INTO `emps_salary` (`company_id`, `code`, `emps_id`, `basic_salary`, `days`, `date`, `arrears`, `bonus`, `i_others`, `eobi`, `pessi`, `advance`, `d_others`, `amount_type`, `salary_date`, `created`, `created_by`) VALUES ('2', 1001, '63', '0', '30', '2020-02-29', '1200', '100', '200', '10', '50', '30', '', 'Cash', '2020-02-01', '2020-02-09 12:18:08', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '515', '100', NULL, 'sdasd', '2020-02-09 12:48:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '510', NULL, '112', '', '2020-02-09 12:48:54', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '510', NULL, '-12', '', '2020-02-09 12:48:56', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1008', '2020-02-09', 'CPV', '2020-02-09 12:51:09', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (12, '517', '12', NULL, 'sda', '2020-02-09 12:51:09', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('12', '513', NULL, '12', 'asd', '2020-02-09 12:51:13', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '513', '122', NULL, 'asd', '2020-02-09 12:52:29', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('4', '513', '10', NULL, 'sadsa', '2020-02-09 13:02:02', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', 'BPV', '2020-02-09 13:05:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (13, '513', '12', NULL, 'sdasd', '2020-02-09 13:05:48', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:05:52', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', 'sdas', '2020-02-09 13:05:52', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:05:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', '', '2020-02-09 13:05:56', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:05:57', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', '', '2020-02-09 13:05:57', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-09', NULL, '2020-02-09 13:06:52', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '515', NULL, '12', '', '2020-02-09 13:06:52', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-02-09', 'BPV', '2020-02-09 13:07:00', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (14, '515', '23', NULL, 'sad', '2020-02-09 13:07:00', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-02-09', NULL, '2020-02-09 13:07:03', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (0, '516', NULL, '23', 'asd', '2020-02-09 13:07:03', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-02-09', 'BPV', '2020-02-09 13:08:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (15, '516', '23', NULL, 'asd', '2020-02-09 13:08:46', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('15', '516', NULL, '23', 'asd', '2020-02-09 13:08:49', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-02-09', 'BPV', '2020-02-09 13:11:12', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (16, '526', '100', NULL, 'sdas', '2020-02-09 13:11:12', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('16', '510', NULL, '100', 'sada', '2020-02-09 13:11:17', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-02-09', 'BPV', '2020-02-09 13:18:59', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (17, '513', '100', NULL, 'asdasd', '2020-02-09 13:18:59', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('17', '514', NULL, '100', 'asdas', '2020-02-09 13:19:41', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `journal_date` = '2020-02-06', `modified` = '2020-02-09 13:20:09', `modified_by` = '8'
WHERE `id` = '17';
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'CRV', `journal_date` = '2020-02-06', `modified` = '2020-02-09 13:21:22', `modified_by` = '8'
WHERE `id` = '17';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1005', '2020-02-09', 'BPV', '2020-02-09 14:33:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (18, '515', '23', NULL, 'asdsd', '2020-02-09 14:33:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('18', '515', NULL, '23', 'asdas', '2020-02-09 14:33:05', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1005', `payment_type` = 'BPV', `journal_date` = '2020-02-09', `modified` = '2020-02-09 14:33:06', `modified_by` = '8'
WHERE `id` = '18';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-15 11:55:36'
WHERE `user_id` = '8';
UPDATE `companies` SET `name` = 'Cybex Tech', `address` = '97 B Gulberg - II', `area` = 'Main Market', `city` = 'Lahore', `zip` = '54400', `country` = 'Pakistan', `phone` = '+92 (42) 0000000', `email` = 'info@cybextech.com', `contact_person` = 'Umair Iqbal', `mobile_no` = '+92310000000', `currency_id` = '87', `currency_symbol_position` = 'Before', `status` = 'Active', `modified` = '2020-02-15 11:57:26', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `companies` SET `logo` = '2.png'
WHERE `id` = '2';
UPDATE `users` SET `email` = 'admin@admin.com', `name` = 'Hamid Riaz', `status` = 'Active', `modified` = '2020-02-15 11:57:56', `modified_by` = '8', `company_id` = '2', `type` = 'Admin'
WHERE `id` = '8';
DELETE FROM `users`
WHERE `id` = '5';
DELETE FROM `user_privileges`
WHERE `ref_user` = '5';
DELETE FROM `users`
WHERE `id` = '10';
DELETE FROM `user_privileges`
WHERE `ref_user` = '10';
DELETE FROM `users`
WHERE `id` = '11';
DELETE FROM `user_privileges`
WHERE `ref_user` = '11';
DELETE FROM `users`
WHERE `id` = '12';
DELETE FROM `user_privileges`
WHERE `ref_user` = '12';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_2', '2020-02-15', 'JV', '2020-02-15 12:07:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (19, '515', '30', NULL, 'asdsad', '2020-02-15 12:07:14', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('19', '516', NULL, '30', 'asdasd', '2020-02-15 12:07:20', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_2', `payment_type` = 'JV', `journal_date` = '2020-02-15', `modified` = '2020-02-15 12:07:33', `modified_by` = '8'
WHERE `id` = '19';
UPDATE `ac_journal_master` SET `journal_no` = '1920_2', `payment_type` = 'JV', `journal_date` = '2020-01-28', `modified` = '2020-02-15 12:08:01', `modified_by` = '8'
WHERE `id` = '19';
DELETE FROM `ac_journal_master`
WHERE `id` = '19';
DELETE FROM `ac_journal_master`
WHERE `id` = '18';
DELETE FROM `ac_journal_master`
WHERE `id` = '17';
DELETE FROM `ac_journal_master`
WHERE `id` = '16';
DELETE FROM `ac_journal_master`
WHERE `id` = '3';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-15 12:11:03'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-16 12:24:29'
WHERE `user_id` = '8';
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '515', '10.10.100', 'projects', '', '', 'Active', '2020-02-16 12:38:13', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1334', '10.10.100.10', 'descon', '', '', 'Active', '2020-02-16 12:38:27', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1335', '10.10.100.10.10', 'expenses', '', '', 'Active', '2020-02-16 12:38:38', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1335', '10.10.100.10.20', 'operating expenses', '', '', 'Active', '2020-02-16 12:39:26', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1334', '10.10.100.20', 'project 2', '', '', 'Active', '2020-02-16 12:40:19', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1338', '10.10.100.20.10', 'operating expense', '', '', 'Active', '2020-02-16 12:40:34', '8');
INSERT INTO `ac_charts` (`company_id`, `parent_id`, `code`, `name`, `memo`, `opening`, `status`, `created_at`, `created_by`) VALUES ('2', '1338', '10.10.100.20.20', 'material', '', '', 'Active', '2020-02-16 12:40:48', '8');
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1009', '2020-02-16', 'CPV', '2020-02-16 12:41:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (20, '1336', '100', NULL, '', '2020-02-16 12:41:56', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('20', '526', NULL, '100', '', '2020-02-16 12:42:03', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1009', `payment_type` = 'CPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:42:21', `modified_by` = '8'
WHERE `id` = '20';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1010', '2020-02-16', 'CPV', '2020-02-16 12:43:16', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (21, '1336', '50', NULL, 'asds', '2020-02-16 12:43:16', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('21', '526', NULL, '50', '', '2020-02-16 12:43:23', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1010', `payment_type` = 'CPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:44:28', `modified_by` = '8'
WHERE `id` = '21';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1011', '2020-02-16', 'CPV', '2020-02-16 12:44:58', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (22, '1339', '123', NULL, 'asd', '2020-02-16 12:44:58', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('22', '526', NULL, '123', '', '2020-02-16 12:45:10', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1011', `payment_type` = 'CPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:45:14', `modified_by` = '8'
WHERE `id` = '22';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-02-16', 'BPV', '2020-02-16 12:45:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (23, '1340', '1000', NULL, 'asd', '2020-02-16 12:45:48', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('23', '553', NULL, '1000', 'asd', '2020-02-16 12:45:55', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1004', `payment_type` = 'BPV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 12:46:03', `modified_by` = '8'
WHERE `id` = '23';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `payment_type`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-02-16', 'JV', '2020-02-16 13:12:00', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES (24, '526', '400', NULL, '', '2020-02-16 13:12:01', '8');
INSERT INTO `ac_journal_details` (`master_id`, `chart_id`, `debit`, `credit`, `memo`, `created`, `created_by`) VALUES ('24', '1340', NULL, '400', '', '2020-02-16 13:12:02', '8');
UPDATE `ac_journal_master` SET `journal_no` = '1920_1001', `payment_type` = 'JV', `journal_date` = '2020-02-16', `modified` = '2020-02-16 13:12:03', `modified_by` = '8'
WHERE `id` = '24';
UPDATE `emps` SET `name` = 'sohaill', `father_name` = '', `cnic` = '', `joining` = '2020-02-09', `resign` = '0000-00-00', `present_address` = '', `permanent_address` = '', `voter_id` = NULL, `department` = '', `designation` = '', `mobile` = '', `email` = '', `salary` = '10000', `house_rent` = '0', `account_no` = '', `notes` = '', `status` = 'Active', `modified` = '2020-02-16 13:23:25', `modified_by` = '8'
WHERE `id` = '63';
INSERT INTO `emps_salary` (`company_id`, `code`, `emps_id`, `basic_salary`, `days`, `date`, `arrears`, `bonus`, `i_others`, `eobi`, `pessi`, `advance`, `d_others`, `amount_type`, `salary_date`, `created`, `created_by`) VALUES ('2', 1002, '63', '10000', '30', '2020-02-16', '', '', 2666.6666666667, '', '', '', '', 'Cash', '2020-02-16', '2020-02-16 13:23:43', '8');
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = '8.00', `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:27:05', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 341.33333333333, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:29:33', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674.3333333333, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:00', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:05', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:10', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 2674, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:30:15', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-29', `arrears` = '1200', `bonus` = '100', `i_others` = 200, `eobi` = '10', `pessi` = '50', `advance` = '30', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-01', `modified` = '2020-02-16 13:30:23', `modified_by` = '8'
WHERE `id` = '3';
INSERT INTO `emps_salary` (`company_id`, `code`, `emps_id`, `basic_salary`, `days`, `date`, `arrears`, `bonus`, `i_others`, `eobi`, `pessi`, `advance`, `d_others`, `amount_type`, `salary_date`, `created`, `created_by`) VALUES ('2', 1003, '63', '10000', '30', '2020-02-16', '', '', 3333.3333333333, '', '', '', '', 'Cash', '2020-02-16', '2020-02-16 13:30:44', '8');
UPDATE `emps_salary` SET `basic_salary` = '10000', `days` = '30', `date` = '2020-02-16', `arrears` = '0', `bonus` = '0', `i_others` = 3333, `eobi` = '0', `pessi` = '0', `advance` = '0', `d_others` = '0', `amount_type` = 'Cash', `salary_date` = '2020-02-16', `modified` = '2020-02-16 13:31:05', `modified_by` = '8'
WHERE `id` = '5';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-16 13:59:28'
WHERE `user_id` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '1', `purchase` = '1', `purchase_return` = '1', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = '1', `journal` = '1', `ac_head` = '1', `chart_of_account` = '1', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = NULL, `purchase_return_report` = NULL, `sales_report` = NULL, `sales_return_report` = NULL, `inventory_report` = NULL, `production_report` = NULL, `ledger_report` = '1', `trial_balance_report` = '1', `balance_sheet_report` = '1', `income_statement_report` = '1', `bills_receivable_report` = '1', `bills_payable_report` = '1', `cash_book_report` = '1', `bank_book_report` = '1', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:00:50', `modified_by` = '8'
WHERE `ref_user` = '8';
INSERT INTO `users` (`company_id`, `email`, `password`, `name`, `type`, `status`, `code`, `created`, `created_by`) VALUES ('2', 'sohail@gmail.com', '7c222fb2927d828a', 'sohail', 'Admin', 'Active', 'b85762436e75e672b2d0a2533ce2bfe4', '2020-02-16 14:01:22', '8');
DELETE FROM `users`
WHERE `id` = '9';
DELETE FROM `user_privileges`
WHERE `ref_user` = '9';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '1', `purchase` = '1', `purchase_return` = '1', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = '0', `journal` = '0', `ac_head` = '0', `chart_of_account` = '1', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = NULL, `purchase_return_report` = NULL, `sales_report` = NULL, `sales_return_report` = NULL, `inventory_report` = NULL, `production_report` = NULL, `ledger_report` = '1', `trial_balance_report` = '1', `balance_sheet_report` = '1', `income_statement_report` = '1', `bills_receivable_report` = '1', `bills_payable_report` = '1', `cash_book_report` = '1', `bank_book_report` = '1', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:02:32', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '1', `purchase` = '1', `purchase_return` = '1', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = NULL, `journal` = NULL, `ac_head` = NULL, `chart_of_account` = '0', `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = NULL, `purchase_return_report` = NULL, `sales_report` = NULL, `sales_return_report` = NULL, `inventory_report` = NULL, `production_report` = NULL, `ledger_report` = '0', `trial_balance_report` = '0', `balance_sheet_report` = '0', `income_statement_report` = '0', `bills_receivable_report` = '0', `bills_payable_report` = '0', `cash_book_report` = '0', `bank_book_report` = '0', `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:03:40', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '1', `purchase` = '1', `purchase_return` = '1', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = NULL, `journal` = NULL, `ac_head` = NULL, `chart_of_account` = NULL, `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '1', `sales_report` = '1', `sales_return_report` = '1', `inventory_report` = '1', `production_report` = '1', `ledger_report` = NULL, `trial_balance_report` = NULL, `balance_sheet_report` = NULL, `income_statement_report` = NULL, `bills_receivable_report` = NULL, `bills_payable_report` = NULL, `cash_book_report` = NULL, `bank_book_report` = NULL, `settings_menu` = '1', `basic_settings` = '1', `company_settings` = '1', `default_ac_head_settings` = '1', `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:10:35', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '1', `purchase` = '1', `purchase_return` = '1', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = NULL, `journal` = NULL, `ac_head` = NULL, `chart_of_account` = NULL, `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '1', `sales_report` = '1', `sales_return_report` = '1', `inventory_report` = '1', `production_report` = '1', `ledger_report` = NULL, `trial_balance_report` = NULL, `balance_sheet_report` = NULL, `income_statement_report` = NULL, `bills_receivable_report` = NULL, `bills_payable_report` = NULL, `cash_book_report` = NULL, `bank_book_report` = NULL, `settings_menu` = '0', `basic_settings` = '0', `company_settings` = '1', `default_ac_head_settings` = '0', `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:11:02', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '1', `purchase` = '1', `purchase_return` = '1', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = NULL, `journal` = NULL, `ac_head` = NULL, `chart_of_account` = NULL, `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '1', `sales_report` = '1', `sales_return_report` = '1', `inventory_report` = '1', `production_report` = '1', `ledger_report` = NULL, `trial_balance_report` = NULL, `balance_sheet_report` = NULL, `income_statement_report` = NULL, `bills_receivable_report` = NULL, `bills_payable_report` = NULL, `cash_book_report` = NULL, `bank_book_report` = NULL, `settings_menu` = '1', `basic_settings` = '0', `company_settings` = '1', `default_ac_head_settings` = '0', `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:11:17', `modified_by` = '8'
WHERE `ref_user` = '8';
INSERT INTO `items` (`company_id`, `code`, `name`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1001', 'item 1', '', '', 'Active', '2020-02-16 14:37:04', '8');
INSERT INTO `items` (`company_id`, `code`, `name`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1002', 'item 2', '', '', 'Active', '2020-02-16 14:37:09', '8');
INSERT INTO `items` (`company_id`, `code`, `name`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1003', 'item 3', '', '', 'Active', '2020-02-16 14:37:13', '8');
INSERT INTO `items` (`company_id`, `code`, `name`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1004', 'item 4', '', '', 'Active', '2020-02-16 14:37:17', '8');
UPDATE `items` SET `code` = '1004', `name` = 'item 04', `description` = '', `re_order` = '0', `status` = 'Active', `modified` = '2020-02-16 14:37:50', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `items` SET `code` = '1004', `name` = 'item 4', `description` = '', `re_order` = '0', `status` = 'Active', `modified` = '2020-02-16 14:37:55', `modified_by` = '8'
WHERE `id` = '4';
INSERT INTO `items` (`company_id`, `code`, `name`, `description`, `re_order`, `status`, `created`, `created_by`) VALUES ('2', '1005', 'item 5', '', '', 'Active', '2020-02-16 14:38:29', '8');
DELETE FROM `items`
WHERE `id` = '5';
INSERT INTO `suppliers` (`company_id`, `code`, `name`, `address`, `contact_person`, `gst`, `ntn`, `email`, `phone_no`, `status`, `created_at`, `created_by`) VALUES ('2', '1001', 'supplier 1', '', '', '', '', '', '', 'Active', '2020-02-16 14:46:08', '8');
INSERT INTO `suppliers` (`company_id`, `code`, `name`, `address`, `contact_person`, `gst`, `ntn`, `email`, `phone_no`, `status`, `created_at`, `created_by`) VALUES ('2', '1002', 'supplier 2', '', '', '', '', '', '', 'Active', '2020-02-16 14:46:16', '8');
INSERT INTO `suppliers` (`company_id`, `code`, `name`, `address`, `contact_person`, `gst`, `ntn`, `email`, `phone_no`, `status`, `created_at`, `created_by`) VALUES ('2', '1003', 'supplier 3', '', '', '', '', '', '', 'Active', '2020-02-16 14:46:22', '8');
INSERT INTO `suppliers` (`company_id`, `code`, `name`, `address`, `contact_person`, `gst`, `ntn`, `email`, `phone_no`, `status`, `created_at`, `created_by`) VALUES ('2', '1004', 'supplier 4', '', '', '', '', '', '', 'Active', '2020-02-16 14:46:28', '8');
UPDATE `suppliers` SET `code` = '1004', `name` = 'supplier 41', `address` = '', `contact_person` = '', `phone_no` = '', `status` = 'Active', `modified_at` = '2020-02-16 14:48:34', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `suppliers`
WHERE `id` = '4';
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1005', 'customer 1', 'asd', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:54:32', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1006', 'customer 2', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:54:38', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1007', 'customer 3', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:54:44', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1008', 'customer 4', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:54:50', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1001', 'customer 1', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:55:35', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1002', 'customer 2', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:55:39', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1003', 'customer 3', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:55:44', '8');
INSERT INTO `customers` (`company_id`, `code`, `name`, `address`, `city`, `zip`, `country`, `ntn`, `mobile`, `phone`, `email`, `dob`, `web`, `notes`, `status`, `created`, `created_by`) VALUES ('2', '1004', 'customer 4', '', NULL, NULL, '', '', '', NULL, '', NULL, NULL, '', 'active', '2020-02-16 14:55:48', '8');
UPDATE `customers` SET `code` = '1004', `name` = 'customer 14', `address` = '', `city` = NULL, `zip` = NULL, `country` = '', `ntn` = '', `mobile` = '', `phone` = NULL, `email` = '', `dob` = NULL, `status` = 'active', `web` = NULL, `notes` = '', `modified` = '2020-02-16 14:56:48', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `customers`
WHERE `id` = '4';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = '0', `purchase` = '1', `purchase_return` = '0', `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = NULL, `journal` = NULL, `ac_head` = NULL, `chart_of_account` = NULL, `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '1', `sales_report` = '1', `sales_return_report` = '1', `inventory_report` = '1', `production_report` = '1', `ledger_report` = NULL, `trial_balance_report` = NULL, `balance_sheet_report` = NULL, `income_statement_report` = NULL, `bills_receivable_report` = NULL, `bills_payable_report` = NULL, `cash_book_report` = NULL, `bank_book_report` = NULL, `settings_menu` = '1', `basic_settings` = NULL, `company_settings` = '1', `default_ac_head_settings` = NULL, `user_menu` = '1', `user_section` = '1', `recipe` = '1', `production` = '1', `recipe_detail` = '1', `production_details` = '1', `selection_box` = '1', `modified` = '2020-02-16 14:57:22', `modified_by` = '8'
WHERE `ref_user` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-16', '', '2020-02-16 16:53:56', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (1, '1', '10', NULL, '10', 100, '2020-02-16 16:53:56', '8');
UPDATE `items` SET `avco_price` = 10, `modified` = '2020-02-16 16:53:56', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-16', `notes` = '', `modified` = '2020-02-16 16:54:57', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('1', '2', '10', NULL, '20', 200, '2020-02-16 16:54:57', '8');
UPDATE `items` SET `avco_price` = 20, `modified` = '2020-02-16 16:54:57', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-17', `notes` = '', `modified` = '2020-02-16 16:55:46', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `ac_journal_master` (`company_id`, `journal_no`, `journal_date`, `memo`, `doc_type`, `doc_no`, `created`, `created_by`) VALUES ('2', 'Purchase_1920_1001', '2020-02-17', 'Direct Purchase', 'Purchase', '1920_1001', '2020-02-16 16:55:46', '8');
UPDATE `purchase_master` SET `ac_id` = 25
WHERE `id` = '1';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-16 16:56:00'
WHERE `user_id` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1002', '2020-02-16', '', '2020-02-16 17:05:07', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (2, '1', '10', NULL, '15', 150, '2020-02-16 17:05:07', '8');
UPDATE `items` SET `avco_price` = 12.5, `modified` = '2020-02-16 17:05:07', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1003', '2020-02-16', '', '2020-02-16 17:10:06', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (3, '1', '10', NULL, '10', 100, '2020-02-16 17:10:06', '8');
UPDATE `items` SET `avco_price` = 11.666666666667, `modified` = '2020-02-16 17:10:06', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1004', '2020-02-16', '', '2020-02-16 17:16:20', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (4, '1', '1', NULL, '12', 12, '2020-02-16 17:16:20', '8');
UPDATE `items` SET `avco_price` = 11.677419354839, `modified` = '2020-02-16 17:16:20', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-16', `notes` = '', `modified` = '2020-02-16 17:16:22', `modified_by` = '8'
WHERE `id` = '4';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1005', '2020-02-16', '', '2020-02-16 17:33:07', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (5, '1', '10', NULL, '10', 100, '2020-02-16 17:33:07', '8');
UPDATE `items` SET `avco_price` = 11.268292682927, `modified` = '2020-02-16 17:33:07', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-16', `notes` = '', `modified` = '2020-02-16 17:33:16', `modified_by` = '8'
WHERE `id` = '5';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('5', '2', '10', NULL, '10', 100, '2020-02-16 17:33:16', '8');
UPDATE `items` SET `avco_price` = 15, `modified` = '2020-02-16 17:33:16', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-16', `notes` = '', `modified` = '2020-02-16 17:33:34', `modified_by` = '8'
WHERE `id` = '5';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-16', `notes` = '', `modified` = '2020-02-16 17:44:12', `modified_by` = '8'
WHERE `id` = '5';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('5', '2', '10', NULL, '10', 100, '2020-02-16 17:44:12', '8');
UPDATE `items` SET `avco_price` = 13.333333333333, `modified` = '2020-02-16 17:44:12', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-16', `notes` = '', `modified` = '2020-02-16 17:44:23', `modified_by` = '8'
WHERE `id` = '5';
UPDATE `purchase_master` SET `supplier_id` = '2', `purchase_date` = '2020-02-04', `notes` = 'supplier', `modified` = '2020-02-16 17:44:47', `modified_by` = '8'
WHERE `id` = '5';
DELETE FROM `purchase_details`
WHERE `id` = '6';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-16 17:45:02', `modified_by` = '8'
WHERE `id` = '6';
DELETE FROM `purchase_details`
WHERE `id` = '7';
UPDATE `items` SET `avco_price` = 15, `modified` = '2020-02-16 18:03:00', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '2', `purchase_date` = '2020-02-04', `notes` = 'supplier', `modified` = '2020-02-16 18:04:01', `modified_by` = '8'
WHERE `id` = '5';
DELETE FROM `purchase_master`
WHERE `id` = '4';
UPDATE `items` SET `avco_price` = 11.666666666667, `modified` = '2020-02-16 18:16:14', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-29 11:49:15'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-02-29 11:59:01'
WHERE `user_id` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1006', '2020-02-29', 'asdasd', '2020-02-29 11:59:30', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (6, '1', '12', NULL, '12', 144, '2020-02-29 11:59:30', '8');
UPDATE `items` SET `avco_price` = 11.761904761905, `modified` = '2020-02-29 11:59:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = 'asdasd', `modified` = '2020-02-29 12:03:53', `modified_by` = '8'
WHERE `id` = '6';
DELETE FROM `purchase_master`
WHERE `id` = '6';
UPDATE `items` SET `avco_price` = 11.666666666667, `modified` = '2020-02-29 12:04:17', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1001', 'expense 1', '0', '2020-02-29 12:26:44', '8');
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1002', 'asdasd', '0', '2020-02-29 12:27:56', '8');
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1003', 'asdsad', '0', '2020-02-29 12:28:20', '8');
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1004', 'asd', '1', '2020-02-29 12:29:28', '8');
DELETE FROM `recipe_chemical`
WHERE `recipe_id` = '1';
DELETE FROM `recipe`
WHERE `id` = '1';
DELETE FROM `recipe_chemical`
WHERE `recipe_id` = '1';
DELETE FROM `recipe`
WHERE `id` = '1';
DELETE FROM `purchase_expense`
WHERE `id` = '1';
DELETE FROM `purchase_expense`
WHERE `id` = '2';
DELETE FROM `purchase_expense`
WHERE `id` = '3';
UPDATE `purchase_expense` SET `name` = 'asd1', `status` = '0'
WHERE `id` = '4';
UPDATE `purchase_expense` SET `name` = 'asd11', `status` = '1'
WHERE `id` = '4';
UPDATE `purchase_expense` SET `name` = 'expense 1', `status` = '0'
WHERE `id` = '4';
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1005', 'expense 2', '0', '2020-02-29 12:40:00', '8');
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1006', 'expense 3', '0', '2020-02-29 12:40:05', '8');
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1007', 'expense 4', '0', '2020-02-29 12:40:09', '8');
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1008', 'sadas', '1', '2020-02-29 12:40:12', '8');
UPDATE `purchase_expense` SET `name` = 'sadas', `status` = '0'
WHERE `id` = '8';
UPDATE `purchase_expense` SET `name` = 'sadas', `status` = '1'
WHERE `id` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1006', '2020-02-29', '', '2020-02-29 13:03:47', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (7, '1', '10', NULL, '10', 100, '2020-02-29 13:03:47', '8');
UPDATE `items` SET `avco_price` = 11.25, `modified` = '2020-02-29 13:03:47', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1007', '2020-02-29', '', '2020-02-29 13:08:49', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (8, '1', '12', NULL, '12', 144, '2020-02-29 13:08:49', '8');
UPDATE `items` SET `avco_price` = 11.423076923077, `modified` = '2020-02-29 13:08:49', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 13:09:06', `modified_by` = '8'
WHERE `id` = '8';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('8', '1', '12', NULL, '12', 144, '2020-02-29 13:09:06', '8');
UPDATE `items` SET `avco_price` = 11.53125, `modified` = '2020-02-29 13:09:06', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1008', '2020-02-29', '', '2020-02-29 13:11:01', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (9, '1', '12', NULL, '12', 144, '2020-02-29 13:11:01', '8');
UPDATE `items` SET `avco_price` = 11.605263157895, `modified` = '2020-02-29 13:11:01', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1009', '2020-02-29', '', '2020-02-29 13:12:46', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (10, '1', '12', NULL, '12', 144, '2020-02-29 13:12:46', '8');
UPDATE `items` SET `avco_price` = 11.659090909091, `modified` = '2020-02-29 13:12:46', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 13:13:14', `modified_by` = '8'
WHERE `id` = '10';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES ('10', '1', '12', NULL, '12', 144, '2020-02-29 13:13:14', '8');
UPDATE `items` SET `avco_price` = 11.7, `modified` = '2020-02-29 13:13:14', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1010', '2020-02-29', '', '2020-02-29 13:36:15', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (11, '2', '12', NULL, '12', 144, '2020-02-29 13:36:15', '8');
UPDATE `items` SET `avco_price` = 13.875, `modified` = '2020-02-29 13:36:15', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1011', '2020-02-29', '', '2020-02-29 13:39:23', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (12, '1', '12', NULL, '12', 144, '2020-02-29 13:39:23', '8');
UPDATE `items` SET `avco_price` = 11.732142857143, `modified` = '2020-02-29 13:39:23', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '3', '1920_1012', '2020-02-29', '', '2020-02-29 14:31:35', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`, `created`, `created_by`) VALUES (13, 4, '10', '2020-02-29 14:31:35', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`, `created`, `created_by`) VALUES (13, 5, '5', '2020-02-29 14:31:35', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (13, '4', '12', NULL, '10', 120, '2020-02-29 14:31:35', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (13, '4', '12', NULL, '10', '2020-02-29 14:31:35', '8');
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1013', '2020-02-29', '', '2020-02-29 14:32:53', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (14, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (14, 5, '5');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `total_price`, `created`, `created_by`) VALUES (14, '2', '10', NULL, '10', 100, '2020-02-29 14:32:53', '8');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (14, '2', '10', NULL, '10', '2020-02-29 14:32:53', '8');
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1014', '2020-02-29', '', '2020-02-29 14:35:21', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (15, 4, '12');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (15, 5, '11');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (15, '1', '12', NULL, '12', '2020-02-29 14:35:21', '8');
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1015', '2020-02-29', '', '2020-02-29 14:39:25', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (16, 4, '14');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (16, 5, '11');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (16, '1', '13', NULL, '13', '2020-02-29 14:39:25', '8');
UPDATE `purchase_details` SET `total_price` = 143, `total_expense` = 26
WHERE `id` = 1;
UPDATE `items` SET `avco_price` = 9.9051094890511, `modified` = '2020-02-29 14:39:25', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1016', '2020-02-29', '', '2020-02-29 14:41:49', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (17, 4, '2');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (17, 6, '3');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (17, '1', '12', NULL, '12', '2020-02-29 14:41:49', '8');
UPDATE `purchase_details` SET `total_price` = 144, `total_expense` = 0
WHERE `id` = 1;
UPDATE `items` SET `avco_price` = 9.1140939597315, `modified` = '2020-02-29 14:41:49', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1017', '2020-02-29', '', '2020-02-29 14:43:18', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (18, 5, '23');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (18, 5, '4');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (18, '1', '12', NULL, '12', '2020-02-29 14:43:18', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 24
WHERE `id` = 1;
UPDATE `items` SET `avco_price` = 8.2857142857143, `modified` = '2020-02-29 14:43:18', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1018', '2020-02-29', '', '2020-02-29 14:45:18', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (19, 5, '12');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (19, 6, '1');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (19, '2', '23', NULL, '23', '2020-02-29 14:45:18', '8');
UPDATE `purchase_details` SET `total_price` = 506, `total_expense` = 23
WHERE `id` = 1;
UPDATE `items` SET `avco_price` = 7.2533333333333, `modified` = '2020-02-29 14:45:18', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '19';
UPDATE `items` SET `avco_price` = 10.461538461538, `modified` = '2020-02-29 14:48:05', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '18';
UPDATE `items` SET `avco_price` = 11.543624161074, `modified` = '2020-02-29 14:48:07', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '17';
UPDATE `items` SET `avco_price` = 12.554744525547, `modified` = '2020-02-29 14:48:09', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '16';
UPDATE `items` SET `avco_price` = 13.870967741935, `modified` = '2020-02-29 14:48:11', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '15';
UPDATE `items` SET `avco_price` = 15.357142857143, `modified` = '2020-02-29 14:48:13', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '14';
UPDATE `items` SET `avco_price` = 13.875, `modified` = '2020-02-29 14:48:15', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 13.875, `modified` = '2020-02-29 14:48:15', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '13';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 14:48:18', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 14:48:18', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `purchase_master`
WHERE `id` = '12';
UPDATE `items` SET `avco_price` = 15.76, `modified` = '2020-02-29 14:48:20', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '11';
UPDATE `items` SET `avco_price` = 15, `modified` = '2020-02-29 14:48:22', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '10';
UPDATE `items` SET `avco_price` = 16.947368421053, `modified` = '2020-02-29 14:48:24', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 16.947368421053, `modified` = '2020-02-29 14:48:24', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '9';
UPDATE `items` SET `avco_price` = 17.875, `modified` = '2020-02-29 14:48:26', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '8';
UPDATE `items` SET `avco_price` = 21.4, `modified` = '2020-02-29 14:48:28', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 21.4, `modified` = '2020-02-29 14:48:28', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '7';
UPDATE `items` SET `avco_price` = 25.2, `modified` = '2020-02-29 14:48:30', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '5';
UPDATE `items` SET `avco_price` = 20, `modified` = '2020-02-29 14:48:32', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '3';
UPDATE `items` SET `avco_price` = 32.8, `modified` = '2020-02-29 14:48:34', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 50.6, `modified` = '2020-02-29 14:48:36', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 14:48:38', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 14:48:38', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 14:49:30', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (20, 4, '5');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (20, 5, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (20, 6, '15');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (20, '4', '10', NULL, '10', '2020-02-29 14:49:30', '8');
UPDATE `purchase_details` SET `total_price` = 70, `total_expense` = 30
WHERE `id` = 27;
UPDATE `items` SET `avco_price` = 7, `modified` = '2020-02-29 14:49:30', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 14:50:23', `modified_by` = '8'
WHERE `id` = '20';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('20', '1', '10', NULL, '20', '2020-02-29 14:50:23', '8');
UPDATE `purchase_details` SET `total_price` = 200, `total_expense` = 0
WHERE `id` = 28;
UPDATE `items` SET `avco_price` = 20, `modified` = '2020-02-29 14:50:23', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1002', '2020-02-29', '', '2020-02-29 15:01:33', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (21, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (21, 5, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (21, '2', '12', NULL, '12', '2020-02-29 15:01:33', '8');
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-02-29 15:01:33', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1003', '2020-02-29', '', '2020-02-29 15:02:02', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (22, 4, '12');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (22, 6, '14');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (22, '1', '12', NULL, '12', '2020-02-29 15:02:02', '8');
UPDATE `items` SET `avco_price` = 9.0909090909091, `modified` = '2020-02-29 15:02:02', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 15:02:20', `modified_by` = '8'
WHERE `id` = '22';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('22', '4', '13', NULL, '12', '2020-02-29 15:02:20', '8');
UPDATE `items` SET `avco_price` = 3.0434782608696, `modified` = '2020-02-29 15:02:20', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `purchase_master`
WHERE `id` = '22';
UPDATE `items` SET `avco_price` = 20, `modified` = '2020-02-29 15:08:35', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 7, `modified` = '2020-02-29 15:08:35', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `purchase_master`
WHERE `id` = '21';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:08:48', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '20';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:08:52', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:08:52', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 15:11:52', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (23, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (23, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (23, '3', '10', NULL, '10', '2020-02-29 15:11:53', '8');
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 30
WHERE `id` = '32';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 15:11:53', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 15:12:57', `modified_by` = '8'
WHERE `id` = '23';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('23', '2', '20', NULL, '20', '2020-02-29 15:12:57', '8');
UPDATE `purchase_details` SET `total_price` = 110, `total_expense` = 10
WHERE `id` = '32';
UPDATE `items` SET `avco_price` = 11, `modified` = '2020-02-29 15:12:57', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `purchase_details` SET `total_price` = 420, `total_expense` = 20
WHERE `id` = '33';
UPDATE `items` SET `avco_price` = 21, `modified` = '2020-02-29 15:12:57', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 15:15:58', `modified_by` = '8'
WHERE `id` = '23';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('23', '4', '12', NULL, '12', '2020-02-29 15:15:58', '8');
UPDATE `purchase_details` SET `total_price` = 100, `total_expense` = 0
WHERE `id` = '32';
UPDATE `items` SET `avco_price` = 10, `modified` = '2020-02-29 15:15:58', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `purchase_details` SET `total_price` = 400, `total_expense` = 0
WHERE `id` = '33';
UPDATE `items` SET `avco_price` = 20, `modified` = '2020-02-29 15:15:58', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 144, `total_expense` = 0
WHERE `id` = '34';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 15:15:58', `modified_by` = '8'
WHERE `id` = '4';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1002', '2020-02-29', '', '2020-02-29 15:20:50', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (24, 4, '12');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (24, 6, '34');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (24, '1', '10', NULL, '10', '2020-02-29 15:20:50', '8');
UPDATE `purchase_details` SET `total_price` = 150, `total_expense` = 50
WHERE `id` = '35';
UPDATE `items` SET `avco_price` = 15, `modified` = '2020-02-29 15:20:50', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '2', `purchase_date` = '2020-02-29', `notes` = 'asd', `modified` = '2020-02-29 15:21:18', `modified_by` = '8'
WHERE `id` = '24';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('24', '4', '14', NULL, '23', '2020-02-29 15:21:18', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '35';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 15:21:18', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 350, `total_expense` = 28
WHERE `id` = '36';
UPDATE `items` SET `avco_price` = 19, `modified` = '2020-02-29 15:21:18', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `purchase_master` SET `supplier_id` = '2', `purchase_date` = '2020-02-29', `notes` = 'asd', `modified` = '2020-02-29 15:21:35', `modified_by` = '8'
WHERE `id` = '24';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('24', '4', '23', NULL, '12', '2020-02-29 15:21:35', '8');
UPDATE `purchase_details` SET `total_price` = 110, `total_expense` = 10
WHERE `id` = '35';
UPDATE `items` SET `avco_price` = 11, `modified` = '2020-02-29 15:21:35', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 336, `total_expense` = 14
WHERE `id` = '36';
UPDATE `items` SET `avco_price` = 9.7959183673469, `modified` = '2020-02-29 15:21:35', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `purchase_details` SET `total_price` = 299, `total_expense` = 23
WHERE `id` = '37';
UPDATE `items` SET `avco_price` = 15.897959183673, `modified` = '2020-02-29 15:21:35', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `purchase_master` SET `supplier_id` = '2', `purchase_date` = '2020-02-29', `notes` = 'asd', `modified` = '2020-02-29 15:22:00', `modified_by` = '8'
WHERE `id` = '24';
DELETE FROM `purchase_master`
WHERE `id` = '24';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:22:13', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 15:22:13', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 15:22:13', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `purchase_master`
WHERE `id` = '23';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:22:22', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:22:22', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:22:22', `modified_by` = '8'
WHERE `id` = '4';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 15:23:15', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (25, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (25, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (25, 7, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (25, '1', '10', NULL, '10', '2020-02-29 15:23:15', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '38';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 15:23:15', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 15:23:51', `modified_by` = '8'
WHERE `id` = '25';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1002', '2020-02-29', '', '2020-02-29 15:31:04', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (26, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (26, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (26, 6, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (26, '1', '10', NULL, '10', '2020-02-29 15:31:04', '8');
UPDATE `purchase_details` SET `total_price` = 150, `total_expense` = 50
WHERE `id` = '39';
UPDATE `items` SET `avco_price` = 15.5, `modified` = '2020-02-29 15:31:04', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 15:32:11', `modified_by` = '8'
WHERE `id` = '26';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('26', '2', '10', NULL, '10', '2020-02-29 15:32:11', '8');
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 30
WHERE `id` = '39';
UPDATE `items` SET `avco_price` = 14.5, `modified` = '2020-02-29 15:32:11', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 30
WHERE `id` = '40';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 15:32:11', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '26';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 15:37:17', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:37:17', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '25';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 15:37:19', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:13:59', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (27, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (27, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (27, 6, '10');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (27, '1', '10', NULL, '10', '2020-02-29 16:13:59', '8');
UPDATE `purchase_details` SET `total_price` = 140, `total_expense` = 40
WHERE `id` = '41';
UPDATE `items` SET `avco_price` = 14, `modified` = '2020-02-29 16:13:59', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:14:53', `modified_by` = '8'
WHERE `id` = '27';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('27', '2', '10', NULL, '20', '2020-02-29 16:14:53', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '41';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:14:53', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 220, `total_expense` = 20
WHERE `id` = '42';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:14:53', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:15:09', `modified_by` = '8'
WHERE `id` = '27';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('27', '4', '5', NULL, '13', '2020-02-29 16:15:09', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '41';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:15:09', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 220, `total_expense` = 20
WHERE `id` = '42';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:15:09', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 75, `total_expense` = 10
WHERE `id` = '43';
UPDATE `items` SET `avco_price` = 15, `modified` = '2020-02-29 16:15:09', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:15:49', `modified_by` = '8'
WHERE `id` = '27';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('27', '3', '5', NULL, '16', '2020-02-29 16:15:49', '8');
UPDATE `purchase_details` SET `total_price` = 110, `total_expense` = 10
WHERE `id` = '41';
UPDATE `items` SET `avco_price` = 11, `modified` = '2020-02-29 16:15:49', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 210, `total_expense` = 10
WHERE `id` = '42';
UPDATE `items` SET `avco_price` = 21, `modified` = '2020-02-29 16:15:49', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 70, `total_expense` = 5
WHERE `id` = '43';
UPDATE `items` SET `avco_price` = 14, `modified` = '2020-02-29 16:15:49', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `purchase_details` SET `total_price` = 85, `total_expense` = 5
WHERE `id` = '44';
UPDATE `items` SET `avco_price` = 17, `modified` = '2020-02-29 16:15:49', `modified_by` = '8'
WHERE `id` = '3';
DELETE FROM `purchase_details`
WHERE `id` = '41';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:19:46', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:19:48', `modified_by` = '8'
WHERE `id` = '27';
DELETE FROM `purchase_master`
WHERE `id` = '27';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:19:52', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:19:52', `modified_by` = '8'
WHERE `id` = '4';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:19:52', `modified_by` = '8'
WHERE `id` = '3';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:23:17', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (28, 4, '12');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (28, 5, '13');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (28, '1', '12', NULL, '12', '2020-02-29 16:23:17', '8');
UPDATE `purchase_details` SET `total_price` = 168.96, `total_expense` = 24.96
WHERE `id` = '45';
UPDATE `items` SET `avco_price` = 14.08, `modified` = '2020-02-29 16:23:17', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:24:06', `modified_by` = '8'
WHERE `id` = '28';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('28', '4', '13', NULL, '13', '2020-02-29 16:24:06', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '45';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:24:06', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 182, `total_expense` = 13
WHERE `id` = '46';
UPDATE `items` SET `avco_price` = 14, `modified` = '2020-02-29 16:24:06', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `purchase_details`
WHERE `id` = '46';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:24:31', `modified_by` = '8'
WHERE `id` = '4';
DELETE FROM `purchase_master`
WHERE `id` = '28';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:33:18', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (29, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (29, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (29, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (29, '1', '10', NULL, '10', '2020-02-29 16:33:18', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '47';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:33:18', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:33:50', `modified_by` = '8'
WHERE `id` = '29';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('29', '2', '20', NULL, '20', '2020-02-29 16:33:50', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '47';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:33:50', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 440, `total_expense` = 40
WHERE `id` = '48';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:33:50', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '48';
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '47';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:34:10', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '47';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:37:40', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '29';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:38:45', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (30, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (30, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (30, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (30, '1', '10', NULL, '10', '2020-02-29 16:38:45', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '49';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:38:45', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:39:10', `modified_by` = '8'
WHERE `id` = '30';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('30', '2', '20', NULL, '20', '2020-02-29 16:39:10', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '49';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:39:10', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 440, `total_expense` = 40
WHERE `id` = '50';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:39:10', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '50';
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '49';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:39:24', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:39:24', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:41:05', `modified_by` = '8'
WHERE `id` = '30';
DELETE FROM `purchase_master`
WHERE `id` = '30';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:41:28', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '30';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:42:11', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (31, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (31, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (31, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (31, '1', '10', NULL, '10', '2020-02-29 16:42:11', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '51';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:42:11', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:42:25', `modified_by` = '8'
WHERE `id` = '31';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('31', '2', '20', NULL, '20', '2020-02-29 16:42:25', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '51';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:42:25', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 440, `total_expense` = 40
WHERE `id` = '52';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:42:25', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '52';
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '51';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:42:36', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:42:36', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:44:06', `modified_by` = '8'
WHERE `id` = '31';
DELETE FROM `purchase_master`
WHERE `id` = '31';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:44:10', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:44:42', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (32, 6, '10');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (32, '1', '1', NULL, '10', '2020-02-29 16:44:42', '8');
UPDATE `purchase_details` SET `total_price` = 20, `total_expense` = 10
WHERE `id` = '53';
UPDATE `items` SET `avco_price` = 20, `modified` = '2020-02-29 16:44:42', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '53';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:44:48', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '32';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:47:34', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (33, 5, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (33, '1', '12', NULL, '12', '2020-02-29 16:47:34', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '54';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:47:34', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '54';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:47:37', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1002', '2020-02-29', '', '2020-02-29 16:49:19', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (34, 4, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (34, '2', '12', NULL, '12', '2020-02-29 16:49:19', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '55';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:49:19', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '55';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:49:21', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1003', '2020-02-29', '', '2020-02-29 16:49:50', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (35, 4, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (35, '1', '12', NULL, '12', '2020-02-29 16:49:50', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '56';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:49:50', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '56';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:49:53', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '2', '1920_1004', '2020-02-29', '', '2020-02-29 16:50:17', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (36, 6, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (36, '1', '12', NULL, '12', '2020-02-29 16:50:17', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '57';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:50:17', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '57';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:50:19', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1005', '2020-02-29', '', '2020-02-29 16:50:52', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (37, 4, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (37, '1', '12', NULL, '12', '2020-02-29 16:50:52', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '58';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:50:52', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '58';
UPDATE `items` SET `avco_price` = NAN, `modified` = '2020-02-29 16:50:54', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1006', '2020-02-29', '', '2020-02-29 16:52:50', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (38, 6, '12');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (38, '1', '12', NULL, '12', '2020-02-29 16:52:50', '8');
UPDATE `purchase_details` SET `total_price` = 156, `total_expense` = 12
WHERE `id` = '59';
UPDATE `items` SET `avco_price` = 13, `modified` = '2020-02-29 16:52:50', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '59';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-02-29 16:52:52', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '38';
DELETE FROM `purchase_master`
WHERE `id` = '37';
DELETE FROM `purchase_master`
WHERE `id` = '36';
DELETE FROM `purchase_master`
WHERE `id` = '35';
DELETE FROM `purchase_master`
WHERE `id` = '34';
DELETE FROM `purchase_master`
WHERE `id` = '33';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-02-29', '', '2020-02-29 16:54:25', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (39, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (39, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (39, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (39, '1', '10', NULL, '10', '2020-02-29 16:54:25', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '60';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:54:25', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:54:38', `modified_by` = '8'
WHERE `id` = '39';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('39', '2', '20', NULL, '20', '2020-02-29 16:54:38', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '60';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:54:38', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 440, `total_expense` = 40
WHERE `id` = '61';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:54:38', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '61';
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '60';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-02-29 16:54:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-02-29 16:54:43', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:54:56', `modified_by` = '8'
WHERE `id` = '39';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('39', '2', '20', NULL, '20', '2020-02-29 16:54:56', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '60';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-02-29 16:54:56', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 440, `total_expense` = 40
WHERE `id` = '62';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-02-29 16:54:56', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:55:08', `modified_by` = '8'
WHERE `id` = '39';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('39', '3', '3', NULL, '24', '2020-02-29 16:55:08', '8');
UPDATE `purchase_details` SET `total_price` = 118.2, `total_expense` = 18.2
WHERE `id` = '60';
UPDATE `items` SET `avco_price` = 11.82, `modified` = '2020-02-29 16:55:08', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 436.4, `total_expense` = 36.4
WHERE `id` = '62';
UPDATE `items` SET `avco_price` = 21.82, `modified` = '2020-02-29 16:55:08', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 77.46, `total_expense` = 5.46
WHERE `id` = '63';
UPDATE `items` SET `avco_price` = 25.82, `modified` = '2020-02-29 16:55:08', `modified_by` = '8'
WHERE `id` = '3';
DELETE FROM `purchase_details`
WHERE `id` = '62';
UPDATE `purchase_details` SET `total_price` = 146.2, `total_expense` = 46.2
WHERE `id` = '60';
UPDATE `items` SET `avco_price` = 14.62, `modified` = '2020-02-29 16:55:28', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 85.86, `total_expense` = 13.86
WHERE `id` = '63';
UPDATE `items` SET `avco_price` = 28.62, `modified` = '2020-02-29 16:55:28', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-02-29 16:55:28', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-02-29', `notes` = '', `modified` = '2020-02-29 16:55:34', `modified_by` = '8'
WHERE `id` = '39';
DELETE FROM `purchase_master`
WHERE `id` = '39';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-02-29 16:56:00', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-02-29 16:56:00', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-01 12:17:25'
WHERE `user_id` = '8';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-01', '', '2020-03-01 12:35:44', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (40, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (40, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (40, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (40, '1', '10', NULL, '10', '2020-03-01 12:35:45', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 60
WHERE `id` = '64';
UPDATE `items` SET `avco_price` = 16, `modified` = '2020-03-01 12:35:45', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:36:14', `modified_by` = '8'
WHERE `id` = '40';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('40', '2', '15', NULL, '20', '2020-03-01 12:36:14', '8');
UPDATE `purchase_details` SET `total_price` = 124, `total_expense` = 24
WHERE `id` = '64';
UPDATE `items` SET `avco_price` = 12.4, `modified` = '2020-03-01 12:36:14', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 336, `total_expense` = 36
WHERE `id` = '65';
UPDATE `items` SET `avco_price` = 22.4, `modified` = '2020-03-01 12:36:14', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:37:03', `modified_by` = '8'
WHERE `id` = '40';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('40', '3', '5', NULL, '15', '2020-03-01 12:37:03', '8');
UPDATE `purchase_details` SET `total_price` = 120, `total_expense` = 20
WHERE `id` = '64';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-03-01 12:37:03', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 330, `total_expense` = 30
WHERE `id` = '65';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-03-01 12:37:04', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 85, `total_expense` = 10
WHERE `id` = '66';
UPDATE `items` SET `avco_price` = 17, `modified` = '2020-03-01 12:37:04', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:37:25', `modified_by` = '8'
WHERE `id` = '40';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1002', '2020-03-01', '', '2020-03-01 12:39:20', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (41, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (41, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (41, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (41, '1', '10', NULL, '15', '2020-03-01 12:39:21', '8');
UPDATE `purchase_details` SET `total_price` = 210, `total_expense` = 60
WHERE `id` = '67';
UPDATE `items` SET `avco_price` = 16.5, `modified` = '2020-03-01 12:39:22', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:40:07', `modified_by` = '8'
WHERE `id` = '41';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('41', '2', '5', NULL, '18', '2020-03-01 12:40:07', '8');
UPDATE `purchase_details` SET `total_price` = 190, `total_expense` = 40
WHERE `id` = '67';
UPDATE `items` SET `avco_price` = 15.5, `modified` = '2020-03-01 12:40:08', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 110, `total_expense` = 20
WHERE `id` = '68';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-03-01 12:40:10', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '68';
UPDATE `purchase_details` SET `total_price` = 210, `total_expense` = 60
WHERE `id` = '67';
UPDATE `items` SET `avco_price` = 16.5, `modified` = '2020-03-01 12:40:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-03-01 12:40:30', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:40:46', `modified_by` = '8'
WHERE `id` = '41';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('41', '2', '5', NULL, '22', '2020-03-01 12:40:46', '8');
UPDATE `purchase_details` SET `total_price` = 190, `total_expense` = 40
WHERE `id` = '67';
UPDATE `items` SET `avco_price` = 15.5, `modified` = '2020-03-01 12:40:48', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 20
WHERE `id` = '69';
UPDATE `items` SET `avco_price` = 23, `modified` = '2020-03-01 12:40:49', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:43:31', `modified_by` = '8'
WHERE `id` = '41';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('41', '3', '10', NULL, '20', '2020-03-01 12:43:31', '8');
UPDATE `purchase_details` SET `total_price` = 174, `total_expense` = 24
WHERE `id` = '67';
UPDATE `items` SET `avco_price` = 14.7, `modified` = '2020-03-01 12:43:31', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 122, `total_expense` = 12
WHERE `id` = '69';
UPDATE `items` SET `avco_price` = 22.6, `modified` = '2020-03-01 12:43:31', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 224, `total_expense` = 24
WHERE `id` = '70';
UPDATE `items` SET `avco_price` = 20.6, `modified` = '2020-03-01 12:43:31', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:48:24', `modified_by` = '8'
WHERE `id` = '41';
DELETE FROM `purchase_master`
WHERE `id` = '41';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-03-01 12:48:38', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-03-01 12:48:38', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 17, `modified` = '2020-03-01 12:48:38', `modified_by` = '8'
WHERE `id` = '3';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1002', '2020-03-01', '', '2020-03-01 12:49:20', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (42, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (42, 5, '20');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (42, 6, '30');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (42, '1', '10', NULL, '12', '2020-03-01 12:49:20', '8');
UPDATE `purchase_details` SET `total_price` = 180, `total_expense` = 60
WHERE `id` = '71';
UPDATE `items` SET `avco_price` = 15, `modified` = '2020-03-01 12:49:20', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:49:34', `modified_by` = '8'
WHERE `id` = '42';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('42', '2', '5', NULL, '22', '2020-03-01 12:49:34', '8');
UPDATE `purchase_details` SET `total_price` = 160, `total_expense` = 40
WHERE `id` = '71';
UPDATE `items` SET `avco_price` = 14, `modified` = '2020-03-01 12:49:34', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 20
WHERE `id` = '72';
UPDATE `items` SET `avco_price` = 23, `modified` = '2020-03-01 12:49:34', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:49:47', `modified_by` = '8'
WHERE `id` = '42';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('42', '3', '10', NULL, '20', '2020-03-01 12:49:47', '8');
UPDATE `purchase_details` SET `total_price` = 144, `total_expense` = 24
WHERE `id` = '71';
UPDATE `items` SET `avco_price` = 13.2, `modified` = '2020-03-01 12:49:47', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 122, `total_expense` = 12
WHERE `id` = '72';
UPDATE `items` SET `avco_price` = 22.6, `modified` = '2020-03-01 12:49:47', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 224, `total_expense` = 24
WHERE `id` = '73';
UPDATE `items` SET `avco_price` = 20.6, `modified` = '2020-03-01 12:49:47', `modified_by` = '8'
WHERE `id` = '3';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-01', `notes` = '', `modified` = '2020-03-01 12:50:24', `modified_by` = '8'
WHERE `id` = '42';
UPDATE `suppliers` SET `code` = '1001', `name` = 'supplier 1', `address` = '', `contact_person` = '03041234567', `phone_no` = '', `status` = 'Active', `modified_at` = '2020-03-01 13:03:07', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `suppliers` SET `code` = '1001', `name` = 'supplier 1', `address` = '', `contact_person` = '', `phone_no` = '03041234567', `status` = 'Active', `modified_at` = '2020-03-01 13:03:18', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `suppliers` SET `code` = '1001', `name` = 'supplier 1', `address` = '', `contact_person` = '', `phone_no` = '03041234567', `status` = 'Active', `modified_at` = '2020-03-01 13:03:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `suppliers` SET `code` = '1001', `name` = 'supplier 1', `address` = 'cavlery, walton', `contact_person` = '', `phone_no` = '03041234567', `status` = 'Active', `modified_at` = '2020-03-01 13:04:08', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1001', '2020-03-01', '1', '', '2020-03-01 13:51:02', '8');
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1001', '2020-03-01', '1', '12', '', NULL, '20', 0, 0, '2020-03-01 13:51:02', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1002', '2020-03-01', '1', '', '2020-03-01 13:56:11', '8');
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1002', '2020-03-01', '1', '12', '', NULL, '1', 0, 0, '2020-03-01 13:56:11', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1003', '2020-03-01', '1', '', '2020-03-01 13:57:30', '8');
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1003', '2020-03-01', '1', '1', '', NULL, '1', 0, 0, '2020-03-01 13:57:30', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1004', '2020-03-01', '1', '', '2020-03-01 13:59:02', '8');
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1004', '2020-03-01', '1', '1', '', NULL, '1', 0, 0, '2020-03-01 13:59:02', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1005', '2020-03-01', '1', '', '2020-03-01 14:02:11', '8');
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1005', '2020-03-01', '1', '12', '', NULL, '1', 0, 0, '2020-03-01 14:02:11', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1006', '2020-03-01', '1', '', '2020-03-01 14:05:58', '8');
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1006', '2020-03-01', '1', '2', '', NULL, '2', 0, 0, '2020-03-01 14:05:58', '8');
UPDATE `sales_master` SET `sales_no` = '1006', `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = '', `month` = NULL, `modified` = '2020-03-01 14:06:06', `modified_by` = '8'
WHERE `id` IS NULL;
INSERT INTO `sales_details` (`company_id`, `sales_no`, `sales_date`, `item_id`, `sale_price`, `notes`, `sq_weight`, `quantity`, `price_total`, `cogs_total`, `created_at`, `created_by`) VALUES ('2', '1006', '2020-03-01', '2', '2', '', NULL, '2', 0, 0, '2020-03-01 14:06:06', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-01', '1', '', '2020-03-01 14:56:20', '8');
INSERT INTO `sales_details` (`master_id`, `sales_date`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (1, '2020-03-01', '1', '2', '', '2', 4, 26, -22, '2020-03-01 14:56:21', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-01', '1', '', '2020-03-01 14:58:54', '8');
INSERT INTO `sales_details` (`master_id`, `sales_date`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (2, '2020-03-01', '1', '2', '', '2', 4, 26, -22, '2020-03-01 14:58:54', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-03-01', '1', 'sdas', '2020-03-01 15:00:27', '8');
INSERT INTO `sales_details` (`master_id`, `sales_date`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (3, '2020-03-01', '1', '2', 'sdas', '2', 4, 26, -22, '2020-03-01 15:00:27', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-03-01', '1', 'sdas', '2020-03-01 15:01:29', '8');
INSERT INTO `sales_details` (`master_id`, `sales_date`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (5, '2020-03-01', '1', '2', 'sdas', '2', 4, 26, -22, '2020-03-01 15:01:29', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-03-01', '1', 'sdas', '2020-03-01 15:02:48', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (6, '1', '2', 'sdas', '2', 4, 26, -22, '2020-03-01 15:02:48', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-01', '1', 'sdas', '2020-03-01 15:07:57', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (7, '1', '2', 'sdas', '2', 4, 26, -22, '2020-03-01 15:07:57', '8');
UPDATE `sales_master` SET `sales_no` = '1920_1001', `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'sdas', `month` = NULL, `modified` = '2020-03-01 15:08:51', `modified_by` = '8'
WHERE `id` IS NULL;
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('7', '2', '10', 'sdas', '1', 10, 23, -13, '2020-03-01 15:08:51', '8');
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-03-01', '1', 'sdas', '2020-03-01 15:10:41', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (8, '2', '20', 'sdas', '4', 80, 90, -10, '2020-03-01 15:10:41', '8');
UPDATE `sales_master` SET `sales_no` = '1920_1002', `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'sdas', `month` = NULL, `modified` = '2020-03-01 15:11:03', `modified_by` = '8'
WHERE `id` IS NULL;
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('8', '2', '2', 'sdas', '15', 30, 339, -309, '2020-03-01 15:11:03', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-01 15:29:01'
WHERE `user_id` = '8';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1003', '2020-03-01', '1', 'this is a dummy text', '2020-03-01 15:30:05', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (9, '1', '2', 'this is a dummy text', '2', 4, 26, -22, '2020-03-01 15:30:05', '8');
UPDATE `sales_master` SET `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'this is a dummy text', `modified` = '2020-03-01 15:30:21', `modified_by` = '8'
WHERE `id` = '9';
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `notes`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('9', '1', '3', 'this is a dummy text', '3', 9, 40, -31, '2020-03-01 15:30:21', '8');
DELETE FROM `sales_details`
WHERE `id` = '6';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1004', '2020-03-01', '1', 'this is a dummy text', '2020-03-01 15:37:36', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (10, '1', '2', '2', 4, 26, -22, '2020-03-01 15:37:36', '8');
UPDATE `sales_master` SET `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'this is a dummy text', `modified` = '2020-03-01 15:37:39', `modified_by` = '8'
WHERE `id` = '10';
UPDATE `sales_master` SET `sales_date` = '2020-03-01', `customer_id` = '2', `notes` = 'sdas', `modified` = '2020-03-01 16:17:11', `modified_by` = '8'
WHERE `id` = '';
UPDATE `sales_master` SET `sales_date` = '2020-01-29', `customer_id` = '3', `notes` = 'sdas', `modified` = '2020-03-01 16:17:55', `modified_by` = '8'
WHERE `id` = '';
UPDATE `sales_master` SET `sales_date` = '2020-02-27', `customer_id` = '2', `notes` = 'sdas here', `modified` = '2020-03-01 16:20:27', `modified_by` = '8'
WHERE `id` = '7';
UPDATE `sales_master` SET `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'this is a dummy text', `modified` = '2020-03-01 16:21:01', `modified_by` = '8'
WHERE `id` = '9';
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('9', '3', '23', '2', 46, 41, 5, '2020-03-01 16:21:01', '8');
UPDATE `sales_master` SET `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'this is a dummy text', `modified` = '2020-03-01 16:21:08', `modified_by` = '8'
WHERE `id` = '9';
DELETE FROM `sales_details`
WHERE `id` = '7';
UPDATE `sales_master` SET `sales_date` = '2020-03-01', `customer_id` = '1', `notes` = 'this is a dummy text', `modified` = '2020-03-01 16:21:23', `modified_by` = '8'
WHERE `id` = '9';
DELETE FROM `sales_master`
WHERE `id` = '10';
UPDATE `customers` SET `code` = '1001', `name` = 'customer 1', `address` = 'Bwp', `city` = NULL, `zip` = NULL, `country` = '', `ntn` = '', `mobile` = '03001234567', `phone` = NULL, `email` = '', `dob` = NULL, `status` = 'active', `web` = NULL, `notes` = '', `modified` = '2020-03-01 16:28:00', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `user_privileges` SET `company_id` = '2', `ref_user` = '8', `inventory_menu` = '1', `sales` = '1', `sales_return` = NULL, `purchase` = '1', `purchase_return` = NULL, `supplier` = '1', `item` = '1', `customer` = '1', `hr_menu` = '1', `employee` = '1', `accounts_menu` = NULL, `journal` = NULL, `ac_head` = NULL, `chart_of_account` = NULL, `money_receipt` = NULL, `payment_receipt` = NULL, `report_menu` = '1', `purchase_report` = '1', `purchase_return_report` = '0', `sales_report` = '1', `sales_return_report` = '0', `inventory_report` = '1', `production_report` = '0', `ledger_report` = NULL, `trial_balance_report` = NULL, `balance_sheet_report` = NULL, `income_statement_report` = NULL, `bills_receivable_report` = NULL, `bills_payable_report` = NULL, `cash_book_report` = NULL, `bank_book_report` = NULL, `settings_menu` = '1', `basic_settings` = NULL, `company_settings` = '1', `default_ac_head_settings` = NULL, `user_menu` = '1', `user_section` = '1', `recipe` = '0', `production` = '0', `recipe_detail` = '0', `production_details` = '0', `selection_box` = '0', `modified` = '2020-03-01 17:30:52', `modified_by` = '8'
WHERE `ref_user` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-07 10:38:05'
WHERE `user_id` = '8';
DELETE FROM `sales_master`
WHERE `id` = '9';
DELETE FROM `sales_master`
WHERE `id` = '8';
DELETE FROM `sales_master`
WHERE `id` = '7';
DELETE FROM `purchase_master`
WHERE `id` = '42';
UPDATE `items` SET `avco_price` = 12, `modified` = '2020-03-07 10:58:28', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 22, `modified` = '2020-03-07 10:58:28', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 17, `modified` = '2020-03-07 10:58:28', `modified_by` = '8'
WHERE `id` = '3';
DELETE FROM `purchase_master`
WHERE `id` = '40';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 10:58:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 10:58:30', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 10:58:30', `modified_by` = '8'
WHERE `id` = '3';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 10:59:06', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (43, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (43, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (43, '1', '12', NULL, '10', '2020-03-07 10:59:06', '8');
UPDATE `purchase_details` SET `total_price` = 150, `total_expense` = 30
WHERE `id` = '74';
UPDATE `items` SET `avco_price` = '6.25', `modified` = '2020-03-07 10:59:06', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 12, `modified` = '2020-03-07 10:59:06', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = 'sda', `modified` = '2020-03-07 11:06:39', `modified_by` = '8'
WHERE `id` = '43';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('43', '2', '20', NULL, '10', '2020-03-07 11:06:39', '8');
UPDATE `purchase_details` SET `total_price` = 131.28, `total_expense` = 11.28
WHERE `id` = '74';
UPDATE `items` SET `avco_price` = '8.60', `modified` = '2020-03-07 11:06:40', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 24, `modified` = '2020-03-07 11:06:40', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 218.8, `total_expense` = 18.8
WHERE `id` = '75';
UPDATE `items` SET `avco_price` = '5.47', `modified` = '2020-03-07 11:06:40', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:06:40', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '75';
UPDATE `purchase_details` SET `total_price` = 150, `total_expense` = 30
WHERE `id` = '74';
UPDATE `items` SET `avco_price` = '10.55', `modified` = '2020-03-07 11:15:51', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 36, `modified` = '2020-03-07 11:15:51', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:15:51', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '74';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:15:53', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_master`
WHERE `id` = '43';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 11:16:42', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (44, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (44, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (44, '1', '10', NULL, '10', '2020-03-07 11:16:42', '8');
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 30
WHERE `id` = '76';
UPDATE `items` SET `avco_price` = '6.50', `modified` = '2020-03-07 11:16:42', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:16:42', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:17:08', `modified_by` = '8'
WHERE `id` = '44';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('44', '2', '10', NULL, '10', '2020-03-07 11:17:08', '8');
UPDATE `purchase_details` SET `total_price` = 115, `total_expense` = 15
WHERE `id` = '76';
UPDATE `items` SET `avco_price` = '9.00', `modified` = '2020-03-07 11:17:08', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:17:08', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 115, `total_expense` = 15
WHERE `id` = '77';
UPDATE `items` SET `avco_price` = '5.75', `modified` = '2020-03-07 11:17:08', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:17:08', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:21:28', `modified_by` = '8'
WHERE `id` = '44';
DELETE FROM `purchase_master`
WHERE `id` = '44';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:21:31', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:21:31', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', 'sda', '2020-03-07 11:22:05', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (45, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (45, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (45, '1', '10', NULL, '10', '2020-03-07 11:22:05', '8');
UPDATE `purchase_details` SET `total_price` = 130, `total_expense` = 30
WHERE `id` = '78';
UPDATE `items` SET `avco_price` = '6.50', `modified` = '2020-03-07 11:22:05', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:22:05', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = 'sda', `modified` = '2020-03-07 11:22:20', `modified_by` = '8'
WHERE `id` = '45';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('45', '2', '10', NULL, '10', '2020-03-07 11:22:20', '8');
UPDATE `purchase_details` SET `total_price` = 115, `total_expense` = 15
WHERE `id` = '78';
UPDATE `items` SET `avco_price` = '9.00', `modified` = '2020-03-07 11:22:20', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 115, `total_expense` = 15
WHERE `id` = '79';
UPDATE `items` SET `avco_price` = '5.75', `modified` = '2020-03-07 11:22:20', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:22:20', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = 'sda', `modified` = '2020-03-07 11:22:42', `modified_by` = '8'
WHERE `id` = '45';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('45', '1', '20', NULL, '12', '2020-03-07 11:22:42', '8');
UPDATE `purchase_details` SET `total_price` = 107.5, `total_expense` = 7.5
WHERE `id` = '78';
UPDATE `items` SET `avco_price` = '9.88', `modified` = '2020-03-07 11:22:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:22:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = 107.5, `total_expense` = 7.5
WHERE `id` = '79';
UPDATE `items` SET `avco_price` = '8.25', `modified` = '2020-03-07 11:22:43', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = 255, `total_expense` = 15
WHERE `id` = '80';
UPDATE `items` SET `avco_price` = '11.32', `modified` = '2020-03-07 11:22:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 40, `modified` = '2020-03-07 11:22:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = 'sda', `modified` = '2020-03-07 11:35:50', `modified_by` = '8'
WHERE `id` = '45';
DELETE FROM `purchase_master`
WHERE `id` = '45';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:35:54', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:35:54', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:35:54', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 11:36:35', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (46, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (46, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (46, '1', '10', NULL, '10', '2020-03-07 11:36:35', '8');
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '81';
UPDATE `items` SET `avco_price` = '6.50', `modified` = '2020-03-07 11:36:35', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:36:35', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:38:41', `modified_by` = '8'
WHERE `id` = '46';
DELETE FROM `purchase_master`
WHERE `id` = '46';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:38:45', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 11:39:32', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (47, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (47, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (47, '1', '10', NULL, '10', '2020-03-07 11:39:32', '8');
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '82';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 11:39:32', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:39:32', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:39:57', `modified_by` = '8'
WHERE `id` = '47';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('47', '2', '10', NULL, '10', '2020-03-07 11:39:57', '8');
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '82';
UPDATE `items` SET `avco_price` = '12.25', `modified` = '2020-03-07 11:39:57', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '83';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:39:57', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:39:57', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:45:40', `modified_by` = '8'
WHERE `id` = '47';
DELETE FROM `purchase_master`
WHERE `id` = '47';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:45:44', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:45:44', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 11:46:17', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (48, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (48, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (48, '1', '10', NULL, '10', '2020-03-07 11:46:17', '8');
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '84';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 11:46:17', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:46:17', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:46:35', `modified_by` = '8'
WHERE `id` = '48';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('48', '2', '10', NULL, '10', '2020-03-07 11:46:35', '8');
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '84';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:46:35', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '85';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:46:35', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:46:35', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:47:01', `modified_by` = '8'
WHERE `id` = '48';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('48', '1', '10', NULL, '12', '2020-03-07 11:47:01', '8');
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '84';
UPDATE `items` SET `avco_price` = '5.50', `modified` = '2020-03-07 11:47:01', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:47:01', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '85';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 11:47:01', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '10.00'
WHERE `id` = '86';
UPDATE `items` SET `avco_price` = '12.00', `modified` = '2020-03-07 11:47:01', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 11:47:01', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:53:09', `modified_by` = '8'
WHERE `id` = '48';
DELETE FROM `purchase_master`
WHERE `id` = '48';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:53:14', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:53:14', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:53:14', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 11:53:51', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (49, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (49, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (49, '1', '10', NULL, '10', '2020-03-07 11:53:51', '8');
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '87';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 11:53:51', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:53:51', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:54:02', `modified_by` = '8'
WHERE `id` = '49';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('49', '2', '10', NULL, '10', '2020-03-07 11:54:02', '8');
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '87';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:54:02', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:54:02', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:54:02', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:54:16', `modified_by` = '8'
WHERE `id` = '49';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('49', '1', '10', NULL, '10', '2020-03-07 11:54:16', '8');
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '87';
UPDATE `items` SET `avco_price` = '5.50', `modified` = '2020-03-07 11:54:16', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:54:16', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 11:54:16', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '89';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 11:54:16', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 11:54:16', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '89';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '87';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:54:55', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:54:55', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 11.5, `modified` = '2020-03-07 11:54:55', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:54:55', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '87';
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 11:55:01', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:55:01', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:55:01', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:55:30', `modified_by` = '8'
WHERE `id` = '49';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('49', '1', '10', NULL, '10', '2020-03-07 11:55:30', '8');
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:55:30', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '90';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:55:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:55:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:55:47', `modified_by` = '8'
WHERE `id` = '49';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('49', '2', '12', NULL, '12', '2020-03-07 11:55:47', '8');
UPDATE `purchase_details` SET `total_price` = '109.38', `total_expense` = '9.38'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '4.97', `modified` = '2020-03-07 11:55:47', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:55:47', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '109.38', `total_expense` = '9.38'
WHERE `id` = '90';
UPDATE `items` SET `avco_price` = '10.94', `modified` = '2020-03-07 11:55:47', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '155.25', `total_expense` = '11.25'
WHERE `id` = '91';
UPDATE `items` SET `avco_price` = '12.03', `modified` = '2020-03-07 11:55:47', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 32, `modified` = '2020-03-07 11:55:47', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '91';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '88';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:56:23', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '90';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:56:23', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 11.5, `modified` = '2020-03-07 11:56:23', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 11:56:23', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:59:02', `modified_by` = '8'
WHERE `id` = '49';
DELETE FROM `purchase_master`
WHERE `id` = '49';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:59:06', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 11:59:06', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 11:59:42', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (50, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (50, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (50, '1', '10', NULL, '10', '2020-03-07 11:59:42', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:59:42', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '92';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 11:59:42', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 11:59:55', `modified_by` = '8'
WHERE `id` = '50';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('50', '2', '10', NULL, '10', '2020-03-07 11:59:55', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 11:59:55', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '92';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:59:55', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '93';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 11:59:55', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:01:13', `modified_by` = '8'
WHERE `id` = '50';
DELETE FROM `purchase_master`
WHERE `id` = '50';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:01:17', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:01:17', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 12:01:43', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (51, 4, '10');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (51, '1', '10', NULL, '10', '2020-03-07 12:01:43', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:01:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '94';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 12:01:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:01:59', `modified_by` = '8'
WHERE `id` = '51';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('51', '2', '10', NULL, '10', '2020-03-07 12:01:59', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:01:59', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '105.00', `total_expense` = '5.00'
WHERE `id` = '94';
UPDATE `items` SET `avco_price` = '10.50', `modified` = '2020-03-07 12:01:59', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '105.00', `total_expense` = '5.00'
WHERE `id` = '95';
UPDATE `items` SET `avco_price` = '10.50', `modified` = '2020-03-07 12:01:59', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:02:15', `modified_by` = '8'
WHERE `id` = '51';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('51', '1', '20', NULL, '10', '2020-03-07 12:02:15', '8');
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:02:15', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '102.50', `total_expense` = '2.50'
WHERE `id` = '94';
UPDATE `items` SET `avco_price` = '3.42', `modified` = '2020-03-07 12:02:15', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '102.50', `total_expense` = '2.50'
WHERE `id` = '95';
UPDATE `items` SET `avco_price` = '10.25', `modified` = '2020-03-07 12:02:16', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '205.00', `total_expense` = '5.00'
WHERE `id` = '96';
UPDATE `items` SET `avco_price` = '10.25', `modified` = '2020-03-07 12:02:16', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '96';
UPDATE `purchase_details` SET `total_price` = '105.00', `total_expense` = '5.00'
WHERE `id` = '94';
UPDATE `items` SET `avco_price` = '10.50', `modified` = '2020-03-07 12:02:36', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '105.00', `total_expense` = '5.00'
WHERE `id` = '95';
UPDATE `items` SET `avco_price` = '10.50', `modified` = '2020-03-07 12:02:36', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 10.5, `modified` = '2020-03-07 12:02:36', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:02:36', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '94';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '95';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 12:02:46', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:02:46', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:02:46', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:03:00', `modified_by` = '8'
WHERE `id` = '51';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1002', '2020-03-07', '', '2020-03-07 12:03:30', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (52, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (52, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (52, '1', '10', NULL, '20', '2020-03-07 12:03:30', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:03:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '230.00', `total_expense` = '30.00'
WHERE `id` = '97';
UPDATE `items` SET `avco_price` = '23.00', `modified` = '2020-03-07 12:03:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:03:54', `modified_by` = '8'
WHERE `id` = '52';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('52', '2', '10', NULL, '10', '2020-03-07 12:03:54', '8');
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:03:54', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '215.00', `total_expense` = '15.00'
WHERE `id` = '97';
UPDATE `items` SET `avco_price` = '21.50', `modified` = '2020-03-07 12:03:54', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '98';
UPDATE `items` SET `avco_price` = '11.25', `modified` = '2020-03-07 12:03:54', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_details`
WHERE `id` = '98';
UPDATE `purchase_details` SET `total_price` = '230.00', `total_expense` = '30.00'
WHERE `id` = '97';
UPDATE `items` SET `avco_price` = '23.00', `modified` = '2020-03-07 12:04:15', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 11, `modified` = '2020-03-07 12:04:15', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:04:15', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:04:36', `modified_by` = '8'
WHERE `id` = '52';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('52', '2', '10', NULL, '10', '2020-03-07 12:04:36', '8');
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:04:36', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '215.00', `total_expense` = '15.00'
WHERE `id` = '97';
UPDATE `items` SET `avco_price` = '21.50', `modified` = '2020-03-07 12:04:36', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '99';
UPDATE `items` SET `avco_price` = '11.25', `modified` = '2020-03-07 12:04:36', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:04:41', `modified_by` = '8'
WHERE `id` = '52';
DELETE FROM `purchase_master`
WHERE `id` = '52';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:06:47', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:06:47', `modified_by` = '8'
WHERE `id` = '';
UPDATE `items` SET `avco_price` = 11, `modified` = '2020-03-07 12:06:47', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:06:47', `modified_by` = '8'
WHERE `id` = '';
DELETE FROM `purchase_master`
WHERE `id` = '51';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:07:09', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:07:09', `modified_by` = '8'
WHERE `id` = '';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 12:08:59', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (53, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (53, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (53, '1', '10', NULL, '10', '2020-03-07 12:08:59', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:08:59', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '100';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 12:08:59', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:09:28', `modified_by` = '8'
WHERE `id` = '53';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('53', '2', '10', NULL, '10', '2020-03-07 12:09:28', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:09:28', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '100';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 12:09:28', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '101';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 12:09:28', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:09:47', `modified_by` = '8'
WHERE `id` = '53';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('53', '1', '10', NULL, '10', '2020-03-07 12:09:47', '8');
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:09:47', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '100';
UPDATE `items` SET `avco_price` = '5.50', `modified` = '2020-03-07 12:09:47', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '101';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 12:09:47', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '102';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 12:09:47', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `purchase_details`
WHERE `id` = '102';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '100';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 12:09:56', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '101';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 12:09:56', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `avco_price` = 11.5, `modified` = '2020-03-07 12:09:56', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:09:56', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:10:04', `modified_by` = '8'
WHERE `id` = '53';
DELETE FROM `purchase_master`
WHERE `id` = '53';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:11:20', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:11:20', `modified_by` = '8'
WHERE `id` = '';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:11:20', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:11:20', `modified_by` = '8'
WHERE `id` = '';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 12:12:10', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (54, 4, '10');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (54, '1', '10', NULL, '10', '2020-03-07 12:12:10', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:12:10', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '110.00', `total_expense` = '10.00'
WHERE `id` = '103';
UPDATE `items` SET `avco_price` = '11.00', `modified` = '2020-03-07 12:12:10', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:12:16', `modified_by` = '8'
WHERE `id` = '54';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:14:13', `modified_by` = '8'
WHERE `id` = '';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:15:05', `modified_by` = '8'
WHERE `id` = '';
DELETE FROM `purchase_master`
WHERE `id` = '54';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-03-07 12:16:06', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:16:06', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1001', '2020-03-07', '', '2020-03-07 12:16:53', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (55, 4, '10');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (55, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (55, '1', '10', NULL, '10', '2020-03-07 12:16:53', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:16:53', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '130.00', `total_expense` = '30.00'
WHERE `id` = '104';
UPDATE `items` SET `avco_price` = '13.00', `modified` = '2020-03-07 12:16:53', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:17:02', `modified_by` = '8'
WHERE `id` = '55';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('55', '2', '10', NULL, '10', '2020-03-07 12:17:02', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:17:02', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '104';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 12:17:02', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '115.00', `total_expense` = '15.00'
WHERE `id` = '105';
UPDATE `items` SET `avco_price` = '11.50', `modified` = '2020-03-07 12:17:02', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:17:04', `modified_by` = '8'
WHERE `id` = '55';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1002', '2020-03-07', '', '2020-03-07 12:17:30', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (56, 4, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (56, '1', '20', NULL, '10', '2020-03-07 12:17:30', '8');
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:17:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '220.00', `total_expense` = '20.00'
WHERE `id` = '106';
UPDATE `items` SET `avco_price` = '11.17', `modified` = '2020-03-07 12:17:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:17:40', `modified_by` = '8'
WHERE `id` = '56';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('56', '2', '20', NULL, '20', '2020-03-07 12:17:40', '8');
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:17:40', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '210.00', `total_expense` = '10.00'
WHERE `id` = '106';
UPDATE `items` SET `avco_price` = '10.83', `modified` = '2020-03-07 12:17:40', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '410.00', `total_expense` = '10.00'
WHERE `id` = '107';
UPDATE `items` SET `avco_price` = '17.50', `modified` = '2020-03-07 12:17:40', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:17:46', `modified_by` = '8'
WHERE `id` = '56';
INSERT INTO `purchase_master` (`company_id`, `supplier_id`, `purchase_no`, `purchase_date`, `notes`, `created`, `created_by`) VALUES ('2', '1', '1920_1003', '2020-03-07', '', '2020-03-07 12:18:20', '8');
INSERT INTO `purchase_meta_expenses` (`master_id`, `expense_name`, `expense_price`) VALUES (57, 5, '20');
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES (57, '1', '10', NULL, '10', '2020-03-07 12:18:20', '8');
UPDATE `items` SET `qty` = 40, `modified` = '2020-03-07 12:18:20', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '120.00', `total_expense` = '20.00'
WHERE `id` = '108';
UPDATE `items` SET `avco_price` = '11.13', `modified` = '2020-03-07 12:18:20', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:18:32', `modified_by` = '8'
WHERE `id` = '57';
INSERT INTO `purchase_details` (`master_id`, `item_id`, `quantity`, `vat_percent`, `purchase_price`, `created`, `created_by`) VALUES ('57', '2', '20', NULL, '10', '2020-03-07 12:18:32', '8');
UPDATE `items` SET `qty` = 50, `modified` = '2020-03-07 12:18:32', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_details` SET `total_price` = '106.67', `total_expense` = '6.67'
WHERE `id` = '108';
UPDATE `items` SET `avco_price` = '10.79', `modified` = '2020-03-07 12:18:32', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `purchase_details` SET `total_price` = '213.33', `total_expense` = '13.33'
WHERE `id` = '109';
UPDATE `items` SET `avco_price` = '14.77', `modified` = '2020-03-07 12:18:32', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `purchase_master` SET `supplier_id` = '1', `purchase_date` = '2020-03-07', `notes` = '', `modified` = '2020-03-07 12:18:35', `modified_by` = '8'
WHERE `id` = '57';
DELETE FROM `purchase_master`
WHERE `id` = '57';
UPDATE `items` SET `avco_price` = 10.833333333333, `modified` = '2020-03-07 12:19:11', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:19:11', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 17.5, `modified` = '2020-03-07 12:19:11', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:19:11', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-07', '1', '', '2020-03-07 12:34:50', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (10, '1', '15', '10', 150, 108, 42, '2020-03-07 12:34:50', '8');
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:34:50', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:35:18', `modified_by` = '8'
WHERE `id` = '10';
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('10', '1', '10', '5', 50, 54, -4, '2020-03-07 12:35:18', '8');
UPDATE `items` SET `qty` = 15, `modified` = '2020-03-07 12:35:18', `modified_by` = '8'
WHERE `id` = '1';
DELETE FROM `sales_details`
WHERE `id` = '11';
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:35:43', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:36:04', `modified_by` = '8'
WHERE `id` = '10';
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('10', '1', '12', '20', 240, 217, 23, '2020-03-07 12:36:04', '8');
UPDATE `items` SET `qty` = 0, `modified` = '2020-03-07 12:36:04', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:36:21', `modified_by` = '8'
WHERE `id` = '10';
DELETE FROM `sales_master`
WHERE `id` = '10';
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:36:28', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:36:28', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-07', '1', '', '2020-03-07 12:36:45', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (11, '1', '20', '10', 200, 108, 92, '2020-03-07 12:36:45', '8');
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:36:45', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:36:53', `modified_by` = '8'
WHERE `id` = '11';
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('11', '2', '15', '12', 180, 210, -30, '2020-03-07 12:36:53', '8');
UPDATE `items` SET `qty` = 18, `modified` = '2020-03-07 12:36:53', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:36:56', `modified_by` = '8'
WHERE `id` = '11';
DELETE FROM `sales_master`
WHERE `id` = '11';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:37:05', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 30, `modified` = '2020-03-07 12:37:05', `modified_by` = '8'
WHERE `id` = '2';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1001', '2020-03-07', '1', '', '2020-03-07 12:37:35', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (12, '1', '20', '10', 200, 108, 92, '2020-03-07 12:37:35', '8');
UPDATE `items` SET `qty` = 20, `modified` = '2020-03-07 12:37:35', `modified_by` = '8'
WHERE `id` = '1';
INSERT INTO `sales_master` (`company_id`, `sales_no`, `sales_date`, `customer_id`, `notes`, `created`, `created_by`) VALUES ('2', '1920_1002', '2020-03-07', '1', '', '2020-03-07 12:38:46', '8');
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES (13, '1', '20', '10', 200, 108, 92, '2020-03-07 12:38:46', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:38:46', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:38:54', `modified_by` = '8'
WHERE `id` = '13';
INSERT INTO `sales_details` (`master_id`, `item_id`, `sale_price`, `quantity`, `price_total`, `cogs_total`, `total_profit`, `created_at`, `created_by`) VALUES ('13', '2', '30', '20', 600, 350, 250, '2020-03-07 12:38:54', '8');
UPDATE `items` SET `qty` = 10, `modified` = '2020-03-07 12:38:54', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `sales_master` SET `sales_date` = '2020-03-07', `customer_id` = '1', `notes` = '', `modified` = '2020-03-07 12:38:56', `modified_by` = '8'
WHERE `id` = '13';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-07 17:46:57'
WHERE `user_id` = '8';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-03-08 13:52:40'
WHERE `user_id` = '8';
INSERT INTO `purchase_expense` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1001', 'head 1', '0', '2020-03-08 14:07:43', '8');
INSERT INTO `expense_ac` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1001', 'head 1', '0', '2020-03-08 14:10:16', '8');
INSERT INTO `expense_ac` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1002', 'head 2', '1', '2020-03-08 14:10:24', '8');
UPDATE `expense_ac` SET `name` = 'head 2', `status` = '0'
WHERE `id` = '2';
UPDATE `expense_ac` SET `name` = 'head 3', `status` = '0'
WHERE `id` = '2';
DELETE FROM `expense_ac`
WHERE `id` = '2';
INSERT INTO `expense_ac` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1002', 'head 2', '0', '2020-03-08 14:15:56', '8');
INSERT INTO `expense_ac` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1003', 'head 3', '0', '2020-03-08 14:16:01', '8');
INSERT INTO `expense_ac` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1004', 'head 4', '0', '2020-03-08 14:16:09', '8');
INSERT INTO `expense_ac` (`company_id`, `code`, `name`, `status`, `created`, `created_by`) VALUES ('2', '1005', 'head 5', '1', '2020-03-08 14:16:22', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `created`, `created_by`) VALUES ('2', '1001', '3', '100', '2020-03-08 14:32:21', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1002', '4', '100', '03/03/2020', '2020-03-08 14:34:17', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1003', '4', '11', '2020-03-01', '2020-03-08 14:35:10', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1004', '1', '123', '2020-03-08', '2020-03-08 14:35:32', '8');
UPDATE `expense_en` SET `ac_id` = '1', `amount` = '1231'
WHERE `id` = '4';
UPDATE `expense_en` SET `ac_id` = '1', `date` = '2020-03-09', `amount` = '1231'
WHERE `id` = '4';
DELETE FROM `expense_en`
WHERE `id` = '2';
DELETE FROM `expense_en`
WHERE `id` = '1';
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1005', '1', '100', '2020-02-10', '2020-03-08 15:01:08', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1006', '5', '100', '2020-03-11', '2020-03-08 15:01:15', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1007', '3', '100', '2020-02-22', '2020-03-08 15:01:53', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1008', '1', '12', '2020-03-08', '2020-03-08 15:15:02', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1009', '3', '123123', '2020-03-08', '2020-03-08 15:15:05', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1010', '4', '322', '2020-03-08', '2020-03-08 15:15:09', '8');
INSERT INTO `expense_en` (`company_id`, `code`, `ac_id`, `amount`, `date`, `created`, `created_by`) VALUES ('2', '1011', '5', '343', '2020-03-08', '2020-03-08 15:15:12', '8');
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-05-04 14:19:37'
WHERE `user_id` = '8';
DELETE FROM `purchase_master`
WHERE `id` = '56';
UPDATE `items` SET `avco_price` = 11.5, `modified` = '2020-05-04 14:20:27', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = -10, `modified` = '2020-05-04 14:20:27', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 11.5, `modified` = '2020-05-04 14:20:27', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = -10, `modified` = '2020-05-04 14:20:27', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `purchase_master`
WHERE `id` = '55';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-05-04 14:20:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = -20, `modified` = '2020-05-04 14:20:30', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `avco_price` = 0, `modified` = '2020-05-04 14:20:30', `modified_by` = '8'
WHERE `id` = '2';
UPDATE `items` SET `qty` = -20, `modified` = '2020-05-04 14:20:30', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `sales_master`
WHERE `id` = '13';
UPDATE `items` SET `qty` = -10, `modified` = '2020-05-04 14:20:34', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `items` SET `qty` = 0, `modified` = '2020-05-04 14:20:34', `modified_by` = '8'
WHERE `id` = '2';
DELETE FROM `sales_master`
WHERE `id` = '12';
UPDATE `items` SET `qty` = 0, `modified` = '2020-05-04 14:20:36', `modified_by` = '8'
WHERE `id` = '1';
UPDATE `accounts_calander` SET `start_date` = '2019-07-01', `end_date` = '2020-06-30', `created` = '2020-05-04 14:52:06'
WHERE `user_id` = '8';
