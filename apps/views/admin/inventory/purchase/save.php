<script type="text/javascript" src="assets/backend/js/select2.min.js"></script>
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Purchase
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory">Inventory</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="inventory/purchase_list">Purchase List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($purchase) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Purchase
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if ($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
			<!-- BEGIN Alert widget-->
			<div class="row-fluid">
				<div class="span12">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('info')) { ?>
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('error')) { ?>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
			<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">

						<!-- BEGIN FORM-->
						<form action="inventory/purchase_save" method="post" class="form-horizontal" id="form-validate">
							<fieldset>
								<div class="span6">
									<div class="control-group">
										<label class="control-label" for="purchase_no">Purchase No</label>
										<div class="controls">
											<input type="text" name="purchase_no" id="purchase_no" tabindex="1" readonly class="span10" value="<?php if (count($purchase) > 0) {
																																					echo $purchase['purchase_no'];
																																				} else {
																																					echo $purchase_no;
																																				} ?>" data-validate="{required: true, messages:{required:'Please enter field required'}}" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="item_id">Select Item</label>
										<div class="controls">
											<select name="item_id" id="item_id" tabindex="3" class="span10 chzn-select" data-form="select2">
												<?php if ($items) { ?>
													<option value="<?php echo $items['id']; ?>"><?php echo $items['code'] . ' ' . $items['name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="quantity">Quantity (pcs)</label>
										<div class="controls">
											<input type="text" name="quantity" id="quantity" tabindex="7" class="span10" placeholder="0" data-validate="{required: true, messages:{required:'Please enter field required'}}" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="price">Price per Unit</label>
										<div class="controls">
											<input type="text" name="price" id="price" tabindex="7" class="span10" placeholder="0" data-validate="{required: true, messages:{required:'Please enter field required'}}" />
										</div>
									</div>
									<div class="control-group">
										<label for="name" class="span4">Expense Name</label>
										<label for="name" class="span5">Expense Price</label>
										<!-- <select name="expense_name[]" id="expense_name[]" class="span4 chzn-select expense_name" data-placeholder="Select Expense">
											<option value="0">Select Expense</option>
											<?php foreach ($expenses as $expen) { ?>
												<option value="<?php echo $expen['id']; ?>" <?php
																							if (count($recipe) > 0) {
																								if ($expen['id'] == $chemical[0]['chemical_id']) {
																									echo "selected";
																								}
																							}
																							?>><?php echo $expen['name']; ?></option>
											<?php } ?>
										</select>
										<input type="number" name="expense_amount[]" id="name" value="<?php echo $chemical[0]['quantity'] ?>" min="0" class="span3" placeholder="Expense Amount" step="any"> -->
									</div>

									<div id="child"></div>
									<a id="add" class="btn btn-default">Add Expense <i class="icon-plus"></i></a>
									<br>
									<br>
								</div>
								<div class="span6">
									<div class="control-group">
										<label class="control-label" for="purchase_date">Purchase Date</label>
										<div class="controls">
											<div class="input-append date" data-form="datepicker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy">
												<input name="purchase_date" id="purchase_date" tabindex="2" data-form="datepicker" size="16" type="text" value="<?php if (count($purchase) > 0) {
																																									echo date_to_ui($purchase['purchase_date']);
																																								} else {
																																									echo date('d/m/Y');
																																								} ?>">
												<span class="add-on"><i class="icon-th"></i></span>
											</div>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="supplier_id">Select Supplier</label>
										<div class="controls">
											<select name="supplier_id" id="supplier_id" tabindex="3" class="span10 chzn-select supplier_id" data-form="select2">
												<?php if ($suppliers) { ?>
													<option value="<?php echo $suppliers['id']; ?>"><?php echo $suppliers['code'] . ' ' . $suppliers['name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="notes">Notes</label>
										<div class="controls">
											<textarea name="notes" id="notes" tabindex="5" class="span10" rows="3" data-form="wysihtml5" placeholder="Notes"><?php if (count($purchase) > 0) {
																																									echo $purchase['notes'];
																																								} ?></textarea>
										</div>
									</div>
								</div>
								<?php if (count($purchase) > 0) { ?>
									<input type="hidden" name="id" value="<?php echo $purchase['id']; ?>" />
								<?php } ?>
							</fieldset>
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<div style="clear: both;"></div>
							<div class="center">
								<input type="button" class="btn btn-success" id="purchase_add_item" tabindex="8" value="Add Item" />
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Item List</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<div id="purchase_details">
							<table id="sample_1" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th class="center">Item Code</th>
										<th class="center">Item Name</th>
										<th class="center">Quantity</th>
										<th class="center">Unit Price</th>
										<th class="center">Total Price</th>
										<th class="center">Total Expense</th>
										<th class="center">Grand Total</th>
										<th class="span3 center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$qty = 0;
									$price = 0;
									$exp = 0;
									$gran_tot = 0;
									foreach ($details as $list) {
										$to_price = ($list['quantity'] * $list['purchase_price']);
									?>
										<tr>
											<td><?php echo $list['item_code']; ?></td>
											<td><?php echo $list['item_name']; ?></td>
											<td class="center"><?php echo $list['quantity']; ?></td>
											<td class="center"><?php echo $list['purchase_price']; ?></td>
											<td class="right"><?php echo number_format($to_price, 2) ?></td>
											<td class="center"><?php echo number_format($list['total_expense'], 2); ?></td>
											<td class="center"><?php echo number_format($list['total_price'], 2); ?></td>
											<td class="center">
												<input type="hidden" value="<?php echo $list['id']; ?>" /><span class="btn btn-danger purchase_item_delete"><i class="icon-trash icon-white"></i>Delete</span>
											</td>
										</tr>
									<?php
										$qty += $list['quantity'];
										$price += ($to_price);
										$exp += ($list['total_expense']);
										$gran_tot += ($list['total_price']);
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th class="left" colspan="8">Order Totals</th>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
										<td class="center"><?php echo $qty; ?></td>
										<td></td>
										<td class="right"><?php echo number_format($price, 2); ?></td>
										<td class="right"><?php echo number_format($exp, 2); ?></td>
										<td class="right"><?php echo number_format($gran_tot, 2); ?></td>
										<td><input type="hidden" id="master_id" value="<?php echo $details[0]['master_id']; ?>"><input type="hidden" id="totl_price" value="<?php echo $price; ?>"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="form-actions center">
						<input type="button" class="btn btn-success" id="purchase_complete" value="Complete Purchase Entry" />
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$(function() {
		$('#add').click(function() {
			var newDiv = $('<div ><div class="control-group " ><select name="expense_name[]" id="expense_name[]" class="span4 select chzn-select expense_name" data-placeholder="Select Expense"><option value="0">Select Expense</option><?php foreach ($expenses as $expen) { ?><option value="<?php echo $expen['id']; ?>"><?php echo $expen['name']; ?></option><?php } ?></select><input type="number" name="expense_amount[]" min="0" id="name" value="" step="any" class="span3 expense_amount" placeholder="Expense Amount"><a href="javascript:void(0);" class="remove_exp"> Remove Expense <i  class="icon-minus icon-white"></i></a></div></div>');

			newDiv.on("click", "a", function() {
				$(this).parent().remove();
			});
			$('#child').append(newDiv);
			$('.select').select2();
		});
	});
	$('.supplier_id').select2({
		placeholder: '--- Select Supplier ---',
		ajax: {
			url: "inventory/search_suppliers",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});

	$('#item_id').select2({
		placeholder: '--- Select Item ---',
		ajax: {
			url: "inventory/search_all_items",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
	$(document).ready(function() {
		// Initialize the plugin
		$('#my_popup').popup({
			focusdelay: 400,
			outline: true,
			vertical: 'top'
		});
	});
	$(document).on('click', '#save_draft', function(event) {
		var totl_price = $('#totl_price').val();
		if (!totl_price || totl_price == 0) {
			alert('First Add Item.');
			return false;
		}
		var purchase_no = $('#purchase_no').val();
		$.ajax({
			type: "POST",
			url: "inventory/purchase_draft",
			data: {
				purchase_no: purchase_no
			},
			success: function(msg) {
				window.location.replace(msg);
			}
		});
		// window.location.replace('inventory/purchase_save');
	});
	$('#purchase_no').focus();
</script>