<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MSales_details extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_master_id($id)
    {
        $data = array();
        $this->db->where('master_id', $id);
        $data = $this->db->get('sales_details')->result_array();
        return $data;
    }
    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_sales_no_item_id($sales_no, $item_id)
    {
        $data = null;
        $this->db->select('sales.*, items.name as item_name, items.min_sale_price as item_sale, items.code as item_code');
        $this->db->from('sales_details AS sales');
        $this->db->join('items', 'items.id = sales.item_id');
        $this->db->where('sales.sales_no', $sales_no);
        $this->db->where('sales.item_id', $item_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_sales_no($master_id)
    {
        $data = array();
        $this->db->where('master_id', $master_id);
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_code'] = $item['code'];
                $row['item_name'] = $item['name'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_sales_no2($sales_no = 0, $stat_date, $en_date)
    {
        $data = array();
        $this->db->select('sales_details.sq_weight, items.ac_id, items.avco_price');
        $this->db->from('sales_details');
        $this->db->where('sales_details.sales_no', $sales_no);
        $this->db->where('sales_details.sales_date >= ', $stat_date);
        $this->db->where('sales_details.sales_date <= ', $en_date);
        $this->db->join('items', 'items.id = sales_details.item_id', 'left');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_price_by_sales_no($sales_no = 0, $stat_date, $en_date)
    {
        //$data = array();
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $this->db->select_sum('price_total');
        $this->db->where('sales_no', $sales_no);
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['price_total'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_qty_by_sales_no($sales_no = 0, $stat_date, $en_date)
    {
        //$data = array();
        $this->db->select_sum('quantity');
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $this->db->where('sales_no', $sales_no);
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['quantity'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_total_area_by_sales_no($sales_no = 0, $stat_date, $en_date)
    {
        //$data = array();
        $this->db->select_sum('sq_weight');
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $this->db->where('sales_no', $sales_no);
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['sq_weight'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_by_item_between_date()
    {
        $data = array();
        $this->db->distinct();
        $this->db->select('sales_id');
        if ($this->input->post('item_id') != 'all') {
            $this->db->where('item_id', $this->input->post('item_id'));
        }
        $sdate = $this->input->post('s_date');
        $edate = $this->input->post('e_date');
        $this->db->where("sales_date BETWEEN '$sdate' AND '$edate'");
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $this->MSales_master->get_by_id($row['sales_id']);
                //$data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_quantity($master_id, $items = NULL)
    {
        $this->db->select_sum('quantity');
        if ($items) {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('master_id', $master_id);
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['quantity'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_price($master_id, $items = NULL)
    {
        $this->db->select_sum('price_total');
        if ($items) {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('master_id', $master_id);
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['price_total'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_total_cogs($sales_no, $items = NULL, $stat_date, $en_date)
    {
        $this->db->select_sum('cogs_total');
        if ($items) {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('sales_no', $sales_no);
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row['cogs_total'];
            }
        }

        $q->free_result();
        return $data;
    }

    public function create($master_id)
    {
        $price = $this->input->post('price');
        $price_total = $price * $this->input->post('quantity');

        // Get item avco price
        $cogs = $this->MItems->get_by_id($this->input->post('item_id'));
        $cogs_total = $cogs['avco_price'] * $this->input->post('quantity');
        $data = array(
            'master_id' => $master_id,
            'item_id' => $this->input->post('item_id'),
            'sale_price' => $price,
            'quantity' => $this->input->post('quantity'),
            'price_total' => round($price_total),
            'cogs_total' => round($cogs_total),
            'total_profit' => round($price_total - $cogs_total),
            'created_at' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );

        $this->db->insert('sales_details', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $this->db->where('id', $this->input->post('item_id'));
            $item_data = $this->db->get('items')->row_array();
            if ($item_data) {
                $current_qty = $item_data['qty'] - $this->input->post('quantity');
                $this->update_field_item(trim($this->input->post('item_id')), 'qty', $current_qty);
            }
        }
        return $insert_id;
    }

    public function update_field_item($id, $field, $value)
    {
        $data = array(
            $field => $value,
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );
        $this->db->where('id', $id);
        $this->db->update('items', $data);
    }
    public function delete_by_sales_no($sales_no, $stat_date, $en_date)
    {
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $this->db->where('sales_no', $sales_no);
        $this->db->delete('sales_details');
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sales_details');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('sales_details');
    }
}
