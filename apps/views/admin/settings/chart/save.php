<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					A/C Head
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($chart) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> Default A/C Head
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" action="settings/chart_save" method="post">
							<fieldset>
								<div class="control-group">
									<label for="typeahead" class="control-label">Under A/C of</label>
									<div class="controls">
										<select name="parent_id" id="parent_id" class="span5 chzn-select">
											<option value="">Root</option>
											<?php echo $ac_chart_tree; ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label for="typeahead" class="control-label">Account Class</label>
									<div class="controls">
										<select name="type" id="parent_id" class="span5 chzn-select">
											<option value="">Default</option>
											<option value="Receivable" <?php if(count($chart) > 0 && $chart['type'] == 'Receivable'){ echo 'selected'; } ?>>Receivable</option>
											<option value="Payable" <?php if(count($chart) > 0 && $chart['type'] == 'Payable'){ echo 'selected'; } ?>>Payable</option>
											<option value="Cash" <?php if(count($chart) > 0 && $chart['type'] == 'Cash'){ echo 'selected'; } ?>>Cash</option>
											<option value="Bank" <?php if(count($chart) > 0 && $chart['type'] == 'Bank'){ echo 'selected'; } ?>>Bank</option>
											<option value="Sales" <?php if(count($chart) > 0 && $chart['type'] == 'Sales'){ echo 'selected'; } ?>>Sales</option>
											<option value="Purchase" <?php if(count($chart) > 0 && $chart['type'] == 'Purchase'){ echo 'selected'; } ?>>Purchase</option>
											<option value="Inventory" <?php if(count($chart) > 0 && $chart['type'] == 'Inventory'){ echo 'selected'; } ?>>Inventory</option>
											<option value="COGS" <?php if(count($chart) > 0 && $chart['type'] == 'COGS'){ echo 'selected'; } ?>>Cost of Goods Sold</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label for="name" class="control-label">A/C Code</label>
									<div class="controls">
										<input type="text" id="name" name="code" placeholder="Enter A/C Code ..." class="span5" value="<?php
										if (count($chart) > 0) {
											echo $chart['code'];
										}
										?>" />
									</div>
								</div>
								<div class="control-group">
									<label for="date01" class="control-label">A/C Name</label>
									<div class="controls">
										<input type="text" id="date01" name="name" placeholder="Enter A/C Name ..." class="span5" value="<?php
										if (count($chart) > 0) {
											echo $chart['name'];
										}
										?>" />
									</div>
								</div>
								<div class="control-group">
									<label for="typeahead" class="control-label">Memo</label>
									<div class="controls">
										<textarea id="notes" name="memo" placeholder="Enter Memo..." class="span5"><?php
											if (count($chart) > 0) {
												echo $chart['memo'];
											}
											?></textarea>
										</div>
									</div>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<?php if (count($chart) > 0) { ?>
									<input type="hidden" name="id" value="<?php echo $chart['id']; ?>" />
									<?php } ?>
									<div class="form-actions">
										<input type="submit" name="submit" class="btn btn-success" value="Save changes" />
									</div>
								</fieldset>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>

			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>

	<script>
		$('#parent_id').focus();
	</script>