<!DOCTYPE html>
<style type="text/css" media="print">
            @media print {
                a[href]:after {
                content: none !important;
              }
                #navbar, #footer, .noprint
                {
                /*display: none;*/
                }
            }
            @page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
        </style>
<head>
	<?php $this->load->view('admin/head'); ?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>

	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
	<!-- BEGIN PAGE -->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN BLANK PAGE PORTLET-->
				<div class="widget grey">
					<div class="widget-body">
						<div class="row-fluid">
							<div class="span12">
								<h2 class="center" style="color: #000;">
                                <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                                <h2 class="center" style="color: black"><?php 
                                if($master['payment_type'] == 'JV'){
				                   $voucher = 'Journal Voucher';
				                }
							    if ($master['payment_type'] == 'CPV') 
							                {
							                  $voucher = 'Cash Payment Voucher';
							                }
							    elseif ($master['payment_type'] == 'BPV')
							                 {
							        
							                    $voucher ='Bank Payment Voucher';
							                }
							    elseif($master['payment_type'] == 'CRV')
							                            {
							                    $voucher = 'Cash Receipt Voucher';

							    			}
							    elseif($master['payment_type'] == 'BRV')
							                            {
							                    $voucher = 'Bank Receipt Voucher';

							                }
				                elseif($master['payment_type'] == '')
	                            {
	                    					$voucher = $master['doc_type']." Voucher";

	                			}
							    elseif($master['payment_type'] == '-1')
							                            {
							                    $voucher = '';

							                }
							                echo $voucher;
                                 ?></h2>
							</div>
						</div>
						<div class="row-fluid invoice-list">
							<div class="span12 right">
								<h4 style="color: black">Voucher # <?php echo $master['journal_no']; ?></h4>
								<h4 style="color: black">Date : <?php echo date('jS F Y ', strtotime($master['journal_date'])); ?></h4>
								<!-- <h4>Narration : <?php echo $master['memo']; ?></h4> -->
							</div>
						</div>
						<div class="row-fluid">
							<?php
							$d = 0;
							$c = 0;
							foreach ($details as $list) {
								if ($list['debit']) {
									$d++;
								} else {
									$c++;
								}
							}
							?>
							<div class="invoice-table">
								<fieldset>
									<div class="span12">
										<div id="debit_details">
											<table class="table  table-striped responsive" style="border: 2px solid #000000;">
												<thead>
													<tr>
														<!-- <th class="center">Details</th> -->
														<th class="left" style="border: 1px solid #000000; color: black">Narration</th>
														<th class="left" style="border: 1px solid #000000; color: black">A/C Head</th>
														<th class="right" style="border: 1px solid #000000; color: black">Dr</th>
														<th class="right" style="border: 1px solid #000000; color: black">Cr</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$debit_amount = 0;
													$credit_amount = 0;
													foreach ($details as $list) {
														if ($list['debit']) {
															?>
															<tr>
															<td class="left" style="border: 1px solid #000000; color: black"><?php echo $list['memo']; ?></td>
																<td style="border: 1px solid #000000; color: black"><?php echo $list['chart_name']; ?></td>
																<td class="right" style="border: 1px solid #000000; color: black"><?php echo number_format($list['debit'], 2); ?></td>
																<td style="border: 1px solid #000000; color: black"></td>

															</tr>
															<?php
															$debit_amount += $list['debit'];
														}
													
														if ($list['credit']) {
															?>
															<tr>
															<td class="left" style="border: 1px solid #000000; color: black"><?php echo $list['memo']; ?></td>
															<td style="border: 1px solid #000000; color: black"><?php echo $list['chart_name']; ?></td>
																<td style="border: 1px solid #000000; color: black"></td>
																<td class="right" style="border: 1px solid #000000; color: black"><?php echo number_format($list['credit'], 2); ?></td>
															</tr>
															<?php
															$credit_amount += $list['credit'];
															 $words = $this->numbertowords->convert_number($credit_amount);
														}
													}
													?>
													<tr>
														<th class="left" colspan="2" style="border: 1px solid #000000; color: black">Total</th>
														<th class="right" colspan="1" style="border: 1px solid #000000; color: black"><?php echo number_format($debit_amount, 2); ?></th>
														<th class="right" colspan="1" style="border: 1px solid #000000; color: black"><?php echo number_format($credit_amount, 2); ?></th>
													</tr>
													<tr>
														<th class="left" colspan="4" style="border: 1px solid #000000; color: black">Amount in Words: <?php echo $words; ?> </th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									</fieldset>
							</div>
						</div>
						<br>
						<br>
						<div class="row-fluid">
	                        <div class="span12">
	                        	<div class="span9"></div>
	                        	<div class="span3">
	                        		<b style="color: black">Received By____________________</b>
	                        	</div>
	                        </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="row-fluid">
	                        <div class="span12">
	                        	<div class="span3">
	                        		<b style="color: black">Prepared By____________________</b>
	                        	</div>
	                        	<div class="span3">
	                        		<b style="color: black">Checked By____________________</b>
	                        	</div>
	                        	<div class="span3">
	                        		<b style="color: black">Manager Account____________________</b>
	                        	</div>
	                        	<div class="span3">
	                        		<b style="color: black">Director_______________________</b>
	                        	</div>
	                        </div>
                        </div>
						<!-- <div class="space20"></div> -->
						<div class="row-fluid text-center">
							<a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
							<br>
							<br>
							<div style="text-align: center;">
				                <?php echo date('Y'); ?> &copy; Powered by <a href="http://cybextech.com/" target="_blank">CybexTech
				                    <!-- <br><i class="icon-mobile-phone"> +92-333-6308419</i> -->
				            </div>
						</div>
					</div>
				</div>
				<!-- END BLANK PAGE PORTLET-->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
</body>
<!-- END BODY -->

</html>
<script type="text/javascript">
window.print();
    (function() {

    var afterPrint = function() {
    	window.location.replace("inventory/purchase_save");
        // location.href = base_url + "index.php/admin/sales";
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function(mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }
    window.onafterprint = afterPrint;

}());
</script>