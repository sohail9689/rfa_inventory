<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MPurchase_return_details extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->where('id', $id);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_name'] = $item['name'];
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_barcode($barcode)
    {
        $data = array();
        $this->db->where('barcode', $barcode);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }
public function get_by_purchase_no_item_id($purchase_no, $item_id)
    {
        $data = null;
        $this->db->select('purchase.*, items.name as item_name, items.min_sale_price as item_sale');
        $this->db->from('purchase_return_details AS purchase');
        $this->db->join('items', 'items.id = purchase.item_id');
        $this->db->where('purchase.purchase_return_no', $purchase_no);
        $this->db->where('purchase.item_id', $item_id);
        $this->db->where( 'purchase.company_id', $this->session->userdata( 'user_company' ) );
        $q = $this->db->get();
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_latest()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->limit(1);
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_purchase_return_no($purchase_return_no = 0, $stat_date, $en_date)
    {
        $data = array();
        $this->db->where('purchase_return_no', $purchase_return_no);
        $this->db->where('purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_date <= ', $en_date);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $item = $this->MItems->get_by_id($row['item_id']);
                $row['item_code'] = $item['code'];
                $row['item_name'] = $item['name'];
                $row['item_price'] = $item['min_sale_price'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
public function get_by_purchase_return_no2($purchase_return_no = 0, $stat_date, $en_date)
    {
        $data = array();
        $this->db->select('purchase_return_details.sq_weight, items.ac_id, items.avco_price');
        $this->db->from('purchase_return_details');
        $this->db->where('purchase_return_details.purchase_return_no', $purchase_return_no);
        $this->db->where('purchase_return_details.purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_details.purchase_return_date <= ', $en_date);
        $this->db->join('items', 'items.id = purchase_return_details.item_id', 'left');
        $q = $this->db->get();
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_total_quantity($purchase_return_no, $items = NULL, $stat_date, $en_date)
    {
        $this->db->select_sum('quantity');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_date <= ', $en_date);
        $this->db->where('purchase_return_no', $purchase_return_no);
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['quantity'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_total_area($purchase_return_no, $items = NULL, $stat_date, $en_date)
    {
        $this->db->select_sum('sq_weight');
        $this->db->where('company_id', $this->session->userdata('user_company'));
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_date <= ', $en_date);
        $this->db->where('purchase_return_no', $purchase_return_no);
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['sq_weight'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_total_price($purchase_return_no, $items = NULL, $stat_date, $en_date)
    {
        $this->db->select_sum('total_price');
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('purchase_return_date >= ', ($stat_date));
        $this->db->where('purchase_return_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('purchase_return_no', $purchase_return_no);
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['total_price'];
            }
        }

        $q->free_result();
        return $data;
    }
public function get_total_cogs($purchase_return_no, $items = NULL,$stat_date, $en_date)
    {
        $this->db->select_sum('cogs_total');
        if ($items)
        {
            $this->db->where_in('item_id', $items);
        }
        $this->db->where('purchase_return_no', $purchase_return_no);
        $this->db->where('purchase_return_date >= ', ($stat_date));
        $this->db->where('purchase_return_date <= ', ($en_date));
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row['cogs_total'];
            }
        }

        $q->free_result();
        return $data;
    }
    public function get_all()
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('items');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $company = $this->MCompanies->get_by_id($row['company_id']);
                $row['company'] = $company['name'];
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_item_id($item_id)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('item_id', $item_id);
        $q = $this->db->get('purchase_return_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        return $data;
    }

    public function get_by_purchase_return_no_item_id($purchase_return_no, $item_id)
    {
        $data = null;
        $this->db->select('purchase.*, items.name as item_name, items.min_sale_price as item_sale');
        $this->db->from('purchase_return_details AS purchase');
        $this->db->join('items', 'items.id = purchase.item_id');
        $this->db->where('purchase.purchase_return_no', $purchase_return_no);
        $this->db->where('purchase.item_id', $item_id);
        $this->db->where( 'purchase.company_id', $this->session->userdata( 'user_company' ) );
        $q = $this->db->get();
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_item_with_purchase_return_no()
    {
        $data = array();
        $this->db->distinct();
        $this->db->select('sales_id');
        if ($this->input->post('item_id') != 'all')
        {
            $this->db->where('item_id', $this->input->post('item_id'));
        }
        $sdate = $this->input->post('s_date');
        $edate = $this->input->post('e_date');
        $this->db->where("edate BETWEEN '$sdate' AND '$edate'");
        $this->db->where( 'company_id', $this->session->userdata( 'user_company' ) );
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('sales_details');
        if ($q->num_rows() > 0)
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $this->MSales_master->get_by_id($row['sales_id']);
                //$data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function create($purchase_return_no)
    {
        $item = $this->MItems->get_by_id($this->input->post('item_id'));
        $cogs_total = $item['avco_price'] * $this->input->post('sq_weight');

        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'purchase_return_no' => $purchase_return_no,
            'purchase_return_type' => $this->input->post('purchase_return_type'),
            'purchase_return_date' => date_to_db($this->input->post('purchase_return_date')),
            'item_id' => $this->input->post('item_id'),
            'quantity' => $this->input->post('quantity'),
            'sq_weight' => $this->input->post('sq_weight'),
            'ac_id' => $item['ac_id'],
            'purchase_price' => $this->input->post('price'),
            'cogs_total' => round($cogs_total),
            'total_price' => $this->input->post('sq_weight') * $this->input->post('price'),
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id'),
            );
        $this->db->insert('purchase_return_details', $data);
    }

    public function delete_by_purchase_return_no($purchase_return_no, $stat_date, $en_date)
    {
        $this->db->where('purchase_return_no', $purchase_return_no);
        $this->db->where('purchase_return_date >= ', $stat_date);
        $this->db->where('purchase_return_date <= ', $en_date);
        $this->db->delete('purchase_return_details');
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('purchase_return_details');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('purchase_return_details');
    }

}