<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MSales_master extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $data = array();
        $this->db->select('sales_master.*, customers.name, customers.address, customers.mobile');
        $this->db->from('sales_master');
        $this->db->join('customers', 'sales_master.customer_id = customers.id', 'left');
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $this->db->where('sales_master.id', $id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $item_qty = $this->MSales_details->get_total_quantity($id, NULL);
                $row['item_qty'] = $item_qty;
                $amount = $this->MSales_details->get_total_price($id, NULL);
                $row['amount'] = $amount;
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }


    public function get_last_sales_no($stat_date, $en_date)
    {
        $data = array();
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('sales_master');
        $start_y = date('y', strtotime($stat_date));
        $end_y = date('y', strtotime($en_date));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
            $string = $data['sales_no'];
            $delimiter = "_";
            $array = explode($delimiter, $string);
            $aa = (int) $array[1] + 1;
            $number =  $start_y . $end_y . '_' . $aa;
        } else {
            $number = $start_y . $end_y . '_1001';
        }

        $q->free_result();
        return $number;
    }
    public function get_latest()
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all($stat_date, $en_date)
    {
        $data = array();
        $this->db->order_by('id', 'DESC');
        $this->db->where('sales_date >= ', ($stat_date));
        $this->db->where('sales_date <= ', ($en_date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                if ($row['customer_id'] == 0) {
                    $customer['name'] = 'Annynimous';
                } else {
                    $customer = $this->MCustomers->get_by_id($row['customer_id']);
                }
                $row['customer_name'] = $customer['name'];
                $item_qty = $this->MSales_details->get_total_qty_by_sales_no($row['sales_no'], $stat_date, $en_date);
                $row['item_qty'] = $item_qty;
                $item_qty = $this->MSales_details->get_total_area_by_sales_no($row['sales_no'], $stat_date, $en_date);
                $row['item_area'] = $item_qty;
                $amount = $this->MSales_details->get_total_price_by_sales_no($row['sales_no'], $stat_date, $en_date);
                $row['amount'] = $amount;
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_customer_id($customer_id)
    {
        $data = array();
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->where('customer_id', $customer_id);
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data = $row;
            }
        }

        return $data;
    }


    public function get_data_by_master($sales_no, $stat_date, $en_date)
    {
        $data = array();
        $this->db->select('sales_master.*, customers.name as customer_name, customers.address as customer_address, customers.mobile as customer_mobile, customers.ntn as customer_ntn, customers.country as customer_country');
        $this->db->from('sales_master');
        $this->db->join('customers', 'sales_master.customer_id = customers.id', 'left');
        $this->db->where('sales_master.sales_no', $sales_no);
        $this->db->where('sales_master.sales_date >= ', ($stat_date));
        $this->db->where('sales_master.sales_date <= ', ($en_date));
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_sales_no($sales_no = NULL, $stat_date, $en_date)
    {
        $data = array();
        $this->db->select('sales_master.*, SUM(sales_details.quantity) as quantity, customers.name as customer_name, customers.address as customer_address, customers.mobile as customer_mobile, customers.ntn as customer_ntn, customers.country as customer_country');
        $this->db->from('sales_master');
        $this->db->join('sales_details', 'sales_master.sales_no = sales_details.sales_no', 'left');
        $this->db->join('customers', 'sales_master.customer_id = customers.id', 'left');
        if ($sales_no) {
            $this->db->where('sales_master.sales_no', $sales_no);
        }
        $this->db->where('sales_master.sales_date >= ', ($stat_date));
        $this->db->where('sales_master.sales_date <= ', ($en_date));
        $this->db->group_by("sales_master.sales_no");
        $this->db->order_by('sales_master.id', 'ASC');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_before_date($date)
    {
        $data = array();
        $this->db->where('sales_date <', date_to_db($date));
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_by_date($date)
    {
        $data = array();
        $this->db->where('sales_date', $date);
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all_between_date($items = NULL, $customer_id = NULL, $stat_date, $en_date)
    {
        $data = array();
        $start_date = date_to_db($this->input->post('start_date'));
        $end_date = date_to_db($this->input->post('end_date'));
        if ($customer_id) {
            $this->db->where('customer_id', $customer_id);
        }
        $this->db->where("sales_date BETWEEN '$start_date' AND '$end_date'");
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('sales_no', 'ASC');
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                if ($items) {
                    $row['item_quantity'] = $this->MSales_details->get_total_quantity($row['sales_no'], $items, $stat_date, $en_date);
                    $row['total_price'] = $this->MSales_details->get_total_price($row['sales_no'], $items, $stat_date, $en_date);
                } else {
                    $row['item_quantity'] = $this->MSales_details->get_total_quantity($row['sales_no'], NULL, $stat_date, $en_date);
                    $row['total_price'] = $this->MSales_details->get_total_price($row['sales_no'], NULL, $stat_date, $en_date);
                }

                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_all_between_date_item_report($items = NULL, $customer_id = NULL)
    {
        $data = array();
        $start_date = date_to_db($this->input->post('start_date'));
        $end_date = date_to_db($this->input->post('end_date'));
        if ($customer_id) {
            $this->db->where('customer_id', $customer_id);
        }
        // $this->db->select('sales_master, sales_master.sales_date, customers.full_name');
        $this->db->join('sales_details', 'sales_master.sales_no=sales_details.sales_no');
        $this->db->where("sales_master.sales_date BETWEEN '$start_date' AND '$end_date'");
        $this->db->where('sales_master.company_id', $this->session->userdata('user_company'));
        $this->db->from('sales_master');
        // $this->db->order_by('sales_no', 'ASC');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                // if ($items)
                // {
                //     $row['item_quantity'] = $this->MSales_details->get_total_quantity($row['sales_no'], $items);
                //     $row['total_price'] = $this->MSales_details->get_total_price($row['sales_no'], $items);
                // }
                // else
                // {
                //     $row['item_quantity'] = $this->MSales_details->get_total_quantity($row['sales_no']);
                //     $row['total_price'] = $this->MSales_details->get_total_price($row['sales_no']);
                // }

                $data[] = $row;
            }
        }

        $data = $q->free_result();

        return $data;
    }

    public function get_by_customer_between_date()
    {
        $data = array();
        $sdate = $this->input->post('s_date');
        $edate = $this->input->post('e_date');
        $this->db->where("sales_date BETWEEN '$sdate' AND '$edate'");
        if ($this->input->post('customer_id') != 'all') {
            $this->db->where('customer_id', $this->input->post('customer_id'));
        }
        $this->db->where('company_id', $this->session->userdata('user_company'));
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('sales_master');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                if ($row['customer_id'] == 0) {
                    $customer['name'] = 'Annynimous';
                } else {
                    $customer = $this->MCustomers->get_by_id($row['customer_id']);
                }
                $row['customer_name'] = $customer['name'];
                $user = $this->MUsers->get_by_id($row['user_id']);
                $row['user_name'] = $user['full_name'];
                $item_qty = $this->MSales_details->get_total_qty_by_sales_id($row['id']);
                $row['item_qty'] = $item_qty;
                $amount = $this->MSales_details->get_total_price_by_sales_id($row['id']);
                $row['amount'] = $amount;
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function get_sales_customer()
    {
        $data = array();
        $this->db->select('sales_master.id, sales_master.sales_date, customers.full_name');
        $this->db->from('sales_master');
        $this->db->join('customers', 'sales_master.customer_id=customers.id');
        $this->db->where('sales_master.company_id', $this->session->userdata('company_id'));
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    public function create()
    {
        $data = array(
            'company_id' => $this->session->userdata('user_company'),
            'sales_no' => $this->input->post('sales_no'),
            'sales_date' => date_to_db($this->input->post('sales_date')),
            'customer_id' => $this->input->post('customer_id'),
            'notes' => $this->input->post('des'),
            'created' => date('Y-m-d H:i:s', time()),
            'created_by' => $this->session->userdata('user_id')
        );
        $this->db->insert('sales_master', $data);
        return $this->db->insert_id();
    }

    public function update($id)
    {
        $data = array(
            'sales_date' => date_to_db($this->input->post('sales_date')),
            'customer_id' => $this->input->post('customer_id'),
            'notes' => $this->input->post('des'),
            'modified' => date('Y-m-d H:i:s', time()),
            'modified_by' => $this->session->userdata('user_id')
        );

        $this->db->where('id', $id);
        $this->db->update('sales_master', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sales_master');
    }

    public function delete_by_cmp($cmp_id)
    {
        $this->db->where('company_id', $cmp_id);
        $this->db->delete('sales_master');
    }
}
