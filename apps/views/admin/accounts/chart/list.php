
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->   
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					A/C Head
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts">Accounts</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						A/C Head List
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<!-- BEGIN Alert widget-->
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- END Alert widget-->
		<?php } ?>
		<!-- BEGIN ADVANCED TABLE widget-->
		<div class="row-fluid right">
			<a class="btn btn-primary" href="accounts/chart_save"><i class="icon-plus icon-white"></i> Add New</a>
		</div>
		<br>
		
<!-- <div class="widget-body"> -->
<div class="table-responsive" id="demo_s">
    <table id="demo-table" class="table table-striped" data-url="accounts/chart_list_data" data-side-pagination="server"  data-page-list="[5, 10, 20, 50, 100, 200, 500, 1000]" data-pagination="true"  data-show-refresh="true" data-search="false" data-show-export="true" data-show-toggle="false" data-show-columns="true" data-filter-control="true" data-filter-show-clear="true" data-show-footer="true">

        <thead>
            <tr>
                <th data-field="code" data-align="center"  data-filter-control="input" data-sortable="true">
                    <?php echo "Code";?>
                </th>
                <th data-field="name" data-align="center"  data-filter-control="input" data-sortable="true">
                    <?php echo "Name";?>
                </th>
                <th data-field="o_b" data-align="center"  data-filter-control="input" data-sortable="true" data-footer-formatter="totalFormatter">
                    <?php echo "Opening";?>
                </th>
                <th data-field="created" data-filter-control="datepicker" data-filter-datepicker-options='{"format": "yyyy-mm-dd", "autoclose":true, "clearBtn": true, "todayHighlight": true, "orientation": "top"}' data-sortable="true">
                    <?php echo "Created";?>
                </th>
                <th data-field="status" data-align="center"  data-filter-control="input" data-sortable="true">
                    <?php echo "Status";?>
                </th>
                <th data-field="options" data-sortable="false">
                    <?php echo ('Options');?>
                </th>
            </tr>
        </thead>
    </table>
<!-- </div> -->
						<!-- <table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th>Code of A/C</th>
									<th>Name of A/C</th>
									<th class="center">Opening Balance</th>
									<th class="center">Created</th>
									<th class="center">Status</th>
									<?php if($this->session->userdata['user_type'] == 'Admin'){?>		<th class="center span3">Action</th><?php }?>
								</tr>
							</thead>   
							<tbody>
								<?php foreach($charts as $chart): ?>
									<tr>
										<td><?php echo $chart['code']; ?></td>
										<td><?php echo $chart['name']; ?></td>
										<td class="right"><?php echo number_format($chart['opening'], 2); ?></td>
										<td class="right"><?php echo date('jS M, Y ', strtotime($chart['created_at'])); ?></td>
										<td class="center"><?php if($chart['status']== 'Active'){ ?><span class="label label-success">Active</span><?php }else{ ?><span class="label label-important">Inactive</span><?php } ?></td>
										<?php if($this->session->userdata['user_type'] == 'Admin'){?>
										<td class="center">
											<a class="btn btn-edit" href="accounts/chart_save/<?php echo $chart['id']; ?>"><i class="icon-edit icon-white"></i> Edit</a>
											<a class="btn del btn-danger del" href="accounts/chart_delete/<?php echo $chart['id']; ?>"><i class="icon-trash icon-white"></i> Delete</a>
										</td>
										<?php }?>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table> -->
			

		<!-- END ADVANCED TABLE widget-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>
<span id="status"></span>
<script type="text/javascript">
var message1 = 'Active';
var message2 = 'Inactive';
var module = 'Account Heads';
function totalFormatter(data) {

  var total = 0;

  if (data.length > 0) {

    var field = this.field;

    total = data.reduce(function(sum, row) {
      return sum + (+row[field]);
    }, 0);

    return  total.toLocaleString();
  }

  return '';
};
 function delete_confirm(id){
    if(confirm("Are you sure to delete this ?")){
        var url= 'accounts/chart_delete/'+id;
                $.ajax({
                    type: 'get',
                    url : url,
                    data : {id:id},
                     dataType : 'json',
                     success:function(data){
                        if(data.error == false){
                            alert("Successfully deleted !");
                            $('#demo-table').bootstrapTable('refresh');
                        }else{
                            alert(data.message);
                            return false;
                        }
                    }
                });
    }else{
        return false;
    }
}
    $(document).ready(function(){
        $('#demo-table').bootstrapTable({
        }).on('all.bs.table', function (e, name, args) {

        }).on('click-row.bs.table', function (e, row, $element) {
            
        }).on('dbl-click-row.bs.table', function (e, row, $element) {
            
        }).on('sort.bs.table', function (e, name, order) {
            
        }).on('check.bs.table', function (e, row) {
            
        }).on('uncheck.bs.table', function (e, row) {
            
        }).on('check-all.bs.table', function (e) {
            
        }).on('uncheck-all.bs.table', function (e) {
            
        }).on('load-success.bs.table', function (e, data) {
        }).on('load-error.bs.table', function (e, status) {
            
        }).on('column-switch.bs.table', function (e, field, checked) {
            
        }).on('page-change.bs.table', function (e, size, number) {
            //alert('1');
            //set_switchery();
        }).on('search.bs.table', function (e, text) {
            
        });
    });
$(".del").click(function() {
		if (!confirm("Are you sure you want to delete this item?")) {
			return false;
		}
	});
</script>