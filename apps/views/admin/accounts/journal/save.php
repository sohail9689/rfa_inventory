<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script type="text/javascript" src="assets/backend/js/select2.min.js"></script>
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Vouchers
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts">Accounts</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts/journal_list">Journal List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($master) > 0) : ?>Edit<?php else : ?>Add New<?php endif; ?> Voucher
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal">
							<fieldset>
								<!-- 							<input type="hidden" name="journal_n" id="journal_n" placeholder="Enter Journal No ..." class="span5" value="<?php echo $journal_no; ?>" />
 -->
								<div class="span8 pull-left">
									<div class="control-group">
										<label class="control-label" for="journal_no">Voucher No</label>
										<div class="controls">
											<select name="voucher_type" id="voucher_type" class="span5 chzn-select voucher_type ">
												<option value="-1">Select Type</option>
												<option value="JV" <?php if (count($master) > 0) {
																		if ($master['payment_type'] == 'JV') {
																			echo 'selected';
																		}
																	} ?>>JV</option>
												<option value="CPV" <?php if (count($master) > 0) {
																		if ($master['payment_type'] == 'CPV') {
																			echo 'selected';
																		}
																	} ?>>CPV</option>
												<option value="BPV" <?php if (count($master) > 0) {
																		if ($master['payment_type'] == 'BPV') {
																			echo 'selected';
																		}
																	} ?>>BPV</option>
												<option value="CRV" <?php if (count($master) > 0) {
																		if ($master['payment_type'] == 'CRV') {
																			echo 'selected';
																		}
																	} ?>>CRV</option>
												<option value="BRV" <?php if (count($master) > 0) {
																		if ($master['payment_type'] == 'BRV') {
																			echo 'selected';
																		}
																	} ?>>BRV</option>
												
											</select>
											<input type="text" name="journal_no" id="journal_no" placeholder="Enter Journal No ..." class="span5 " readonly value="<?php echo $journal_no; ?>" />
										</div>
									</div>
									<?php $accounts_date = $this->MItems->get_dates();

									$stat_date = date_to_ui($accounts_date['start_date']);
									$en_date = date_to_ui($accounts_date['end_date']);

									$today = date('Y-m-d');
									$today = date('Y-m-d', strtotime($today));
									$start_date = date('Y-m-d', strtotime($accounts_date['start_date']));
									$end_date = date('Y-m-d', strtotime($accounts_date['end_date']));


									?>


									<div class="control-group">
										<label class="control-label" for="journal_date">voucher Date</label>
										<div class="controls">

											<div class="input-append date" data-form="datepicker" data-date="<?php if (($today > $start_date) && ($today < $end_date)) {
																													echo date('d/m/Y');
																												} else {
																													echo $stat_date;
																												} ?>" data-date-format="dd/mm/yyyy">
												<input type="text" name="journal_date" id="journal_date" data-form="datepicker" class="span12" value="<?php if (count($master) > 0) {
																																							echo date_to_ui($master['journal_date']);
																																						} else {
																																							if (($today > $start_date) && ($today < $end_date)) {
																																								echo date('d/m/Y');
																																							} else {
																																								echo $stat_date;
																																							}
																																						} ?>">
												<span class="add-on"><i class="icon-th"></i></span>
											</div>
										</div>
									</div>


									<!-- <div class="control-group">
                                    <label class="control-label" for="voucher_type">Voucher Type</label>
                                    <div class="controls">
                                        
                                    </div>
									</div>  -->
								</div>
							</fieldset>
							<!-- <div class="clear"></div> -->
							<fieldset>
								<div class="span6 pull-left">
									<div class="control-group">
										<legend class="span11">Debit Voucher</legend>
										<label class="control-label" for="debit_chart_id">Select A/C Head</label>
										<div class="controls">
											<select name="debit_chart_id" id="debit_chart_id" class="span10 chzn-select debit_chart_id">
												<!-- <?php echo $ac_chart_tree; ?> -->
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="debit_amount">Debit Amount</label>
										<div class="controls">
											<input type="text" name="debit_amount" id="debit_amount" placeholder="00.00" class="span10" value="<?php echo set_value('debit'); ?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="journal_memo">Narration</label>
										<div class="controls">
											<textarea name="debit_memo" id="debit_memo" placeholder="Narration..." class="span12"><?php
																																	if (count($master) > 0) {
																																		echo $master['memo'];
																																	} else {
																																		echo set_value('memo');
																																	}
																																	?></textarea>
										</div>
									</div>
									<div class="form-actions">
										<div class="span12 center">
											<input type="button" name="debit_add" id="debit_add" class="btn btn-success" value="Add Debit" />
										</div>
									</div>
								</div>
								<div class="span6 pull-left">
									<div class="control-group">
										<legend class="span11">Credit Voucher</legend>
										<label class="control-label" for="credit_chart_id">Select A/C Head</label>
										<div class="controls">
											<select name="credit_chart_id" id="credit_chart_id" class="span10 chzn-select credit_chart_id">
												<!-- <?php echo $ac_chart_tree; ?> -->
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="credit_amount">Credit Amount</label>
										<div class="controls">
											<input type="text" name="credit_amount" id="credit_amount" placeholder="00.00" class="span10" value="<?php echo set_value('credit'); ?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="credit_memo">Narration</label>
										<div class="controls">
											<textarea name="credit_memo" id="credit_memo" placeholder="Narration..." class="span12"><?php
																																	if (count($master) > 0) {
																																		echo $master['credit'];
																																	} else {
																																		echo set_value('credit');
																																	}
																																	?></textarea>
										</div>
									</div>
									<div class="form-actions">
										<div class="span12 center">
											<input type="button" name="credit_add" id="credit_add" class="btn btn-success" value="Add Credit" />
										</div>
									</div>
								</div>
							</fieldset>
							<div class="clear"></div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE widget-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Voucher List</h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<b>
							<p style="color: green"></p>
						</b>
						<fieldset>
							<div class="span6 pull-left">
								<div id="debit_details">
									<input type="hidden" value="<?php if (count($master) > 0) {
																	echo $master['id'];
																} ?>" id="hidden_master_id" />
									<table class="table table-bordered table-striped responsive">
										<thead>
											<tr>
												<th class="center">Debit A/C Head</th>
												<th class="center">Amount</th>
												<th class="center">Memo</th>
												<th class="center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$debit_amount = 0;
											foreach ($details as $list) {
												if ($list['debit']) {
											?>
													<tr>
														<td><?php echo $list['chart_name']; ?></td>
														<td class="center"><?php echo $list['debit']; ?>

															<!-- <input type="text" name="debit" size="5" id="debit" value="<?php echo $list['debit']; ?>" /> -->

														</td>

														<td class="center">
															<input type="text" name="memo" id="memo" value="<?php echo $list['memo']; ?>" />

														</td>
														<td class="center">
															<input type="hidden" value="<?php echo $list['id']; ?>" /><span class="btn btn-danger debit_voucher_delete"><i class="icon-trash icon-white"></i>Delete</span>
															<input type="hidden" value="<?php echo $list['id']; ?>" /><span class="btn btn-primary debit_voucher_update"><i class="icon-info icon-white"></i>Update</span>

														</td>
													</tr>
											<?php
													$debit_amount += $list['debit'];
												}
											}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th class="left" colspan="4">&nbsp;</th>
											</tr>
											<tr>
												<th colspan="2">Total </th>
												<th colspan="2" class="right" id="debit_total"><?php echo number_format($debit_amount, 2); ?></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>

							<div class="span6 pull-right">
								<div id="credit_details">
									<table class="table table-bordered table-striped responsive">
										<thead>
											<tr>
												<th class="center">Credit A/C Head</th>
												<th class="center">Amount</th>
												<th class="center">Memo</th>
												<th class="center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$credit_amount = 0;
											foreach ($details as $list) {
												if ($list['credit']) {
											?>
													<tr>
														<td><?php echo $list['chart_name']; ?></td>
														<td class="center"><?php echo $list['credit']; ?>
															<!-- <input type="text" name="credit" id="credit" value="<?php echo $list['credit']; ?>" /> -->
														</td>
														<td class="center">
															<input type="text" name="memo" id="memo" value="<?php echo $list['memo']; ?>" />

														</td>
														<td class="center">
															<input type="hidden" value="<?php echo $list['id']; ?>" /><span class="btn btn-danger credit_voucher_delete"><i class="icon-trash icon-white"></i>Delete</span>
															<input type="hidden" value="<?php echo $list['id']; ?>" /><span class="btn btn-primary credit_voucher_update"><i class="icon-info icon-white"></i>Update</span>
														</td>
													</tr>
											<?php
													$credit_amount += $list['credit'];
												}
											}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th class="left" colspan="4">&nbsp;</th>
											</tr>
											<tr>
												<th colspan="2">Total </th>
												<th colspan="2" class="right" id="credit_total"><?php echo number_format($credit_amount, 2); ?></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>

						</fieldset>
						<input type="hidden" name="start_date" id="stat_date" value="<?php echo $stat_date; ?>" />
						<input type="hidden" name="end_date" id="en_date" value="<?php echo $en_date; ?>" />
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

					</div>
					<div class="form-actions center" style="margin-bottom:100px;">
						<input type="button" class="btn btn-success" id="journal_complete" value="Complete Entry" />
					</div>
				</div>
				<!-- END EXAMPLE TABLE widget-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$('.debit_chart_id').select2({
		placeholder: '--- Select A/C ---',
		ajax: {
			url: "inventory/get_ac_chart",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
	$('.credit_chart_id').select2({
		placeholder: '--- Select A/C ---',
		ajax: {
			url: "inventory/get_ac_chart",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
	$('#journal_no').focus();
	$(document).ready(function() {

		var stat_date = $('#stat_date').val();
		var en_date = $('#en_date').val();
		$('[data-form=datepicker]').datepicker({
			autoclose: true,
			startDate: stat_date,
			endDate: en_date,
			format: 'dd/mm/yyyy'
		});
	});
</script>