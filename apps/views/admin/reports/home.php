<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Reports
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="report">Reports</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						Home
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		<!-- END Alert widget-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<!--BEGIN METRO STATES-->
			<div class="metro-nav">
				<?php if($privileges['purchase_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-yellow">
					<a data-original-title="" href="report/purchase">
						<div class="info">Purchase</div>
						<div class="status">Purchase Report</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['purchase_return_report'] == 1){ ?>
				<div class="metro-nav-block nav-light-blue">
					<a data-original-title="" href="report/purchase_return">
						<div class="info">Purchase Return</div>
						<div class="status">Purchase Return Report</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['sales_report'] == 1){ ?>
				<div class="metro-nav-block nav-olive">
					<a data-original-title="" href="report/sales">
						<div class="info">Sales</div>
						<div class="status">Sales Report</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['sales_return_report'] == 1){ ?>
				<div class="metro-nav-block nav-light-green">
					<a data-original-title="" href="report/sales_return">
						<div class="info">Sales Return</div>
						<div class="status">Sales Return Report</div>
					</a>
				</div>
				<?php } ?>
			
				<?php if($privileges['inventory_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-purple">
					<a data-original-title="" href="report/inventory">
						<div class="info">Profit</div>
						<div class="status">Profit Report</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['voucher_report'] == 1){ ?>
				<div class="metro-nav-block nav-light-brown">
					<a data-original-title="" href="report/accounts_reports	">
						<div class="info">Voucher Reports</div>
						<div class="status">Voucher Reports</div>
					</a>
				</div>
				<?php } ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if($privileges['ledger_report'] == 1){ ?>
				<div class="metro-nav-block nav-light-purple">
					<a data-original-title="" href="report/ledger">
						<div class="info">Ledger</div>
						<div class="status">Ledger</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['trial_balance_report'] == 1){ ?>
				<div class="metro-nav-block nav-light-brown double">
					<a data-original-title="" href="report/trial_balance">
						<div class="info">Trial Balance</div>
						<div class="status">Trial Balance</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['balance_sheet_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-orange">
					<a data-original-title="" href="report/balance_sheet">
						<div class="info">Balance Sheet</div>
						<div class="status">Balance Sheet</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['income_statement_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-red double">
					<a data-original-title="" href="report/income_statement">
						<div class="info">Income Statement</div>
						<div class="status">Income Statement</div>
					</a>
				</div>
				<div class="metro-nav-block nav-block-red double">
					<a data-original-title="" href="report/income_statement_pro">
						<div class="info">Project Wise Statement</div>
						<div class="status">Project Wise Statement</div>
					</a>
				</div>
				<?php } ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if($privileges['bills_receivable_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-blue double">
					<a data-original-title="" href="report/bills_receivable">
						<div class="info">Aging Report</div>
						<div class="status">Aging Report</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['bills_payable_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-grey">
					<a data-original-title="" href="report/bills_payable">
						<div class="info">Bills Payable</div>
						<div class="status">Bills Payable</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['cash_book_report'] == 1){ ?>
				<div class="metro-nav-block nav-deep-thistle double">
					<a data-original-title="" href="report/cash_book">
						<div class="info">Cash Book</div>
						<div class="status">Cash Book</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['bank_book_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-green">
					<a data-original-title="" href="report/bank_book">
						<div class="info">Bank Book</div>
						<div class="status">Bank Book</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['item_report'] == 1){ ?>
				<div class="metro-nav-block nav-block-red">
					<a data-original-title="" href="report/item_report">
						<div class="info">Item Report</div>
						<div class="status">Item Report</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['chart_of_account'] == 1){ ?>
				<div class="metro-nav-block nav-block-orange">
					<a data-original-title="" href="report/chart_of_account">
						<div class="info">Chart Of Account</div>
						<div class="status">Chart Of Account</div>
					</a>
				</div>
				<?php } ?>
			</div>
			<div class="space10"></div>
			<!--END METRO STATES-->
		</div>

		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>