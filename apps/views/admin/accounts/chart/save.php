<script type="text/javascript" src="assets/backend/js/select2.min.js"></script>
<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					A/C Head
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts">Accounts</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="accounts/chart_list">Accounts List</a>
						<span class="divider">/</span>
					</li>
					<li class="active">
						<?php if (count($chart) > 0) { ?>Edit<?php } else { ?>Add New<?php } ?> A/C Head
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="widget blue">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i> Entry Form </h4>
						<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
						</span>
					</div>
					<div class="widget-body">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" action="accounts/chart_save" method="post">
							<fieldset>
								<div class="control-group">
									<label for="typeahead" class="control-label">Under A/C of</label>
									<div class="controls">

										<select name="parent_id" id="parent_id" class="span5 chzn-select parent_id">
											<!-- <option></option> -->
											<?php if ($ac_chart_parent) { ?>
												<option value="<?php echo $ac_chart_parent['id'] ?>"><?php echo $ac_chart_parent['name'] . ' (' . $ac_chart_parent['code'] . ")" ?></option>
											<?php } else { ?>
												<option value="0">Root</option>
											<?php } ?>
											<!-- <?php echo $ac_chart_tree; ?> -->
										</select>
									</div>
								</div>
								<div class="control-group">
									<label for="name" class="control-label">A/C Code</label>
									<div class="controls">

										<input type="text" id="ac_code" name="code" readonly placeholder="Enter A/C Code ..." class="span5" value="<?php if (count($chart) > 0) {
																																						echo $chart['code'];
																																					} ?>" />

									</div>
								</div>
								<div class="control-group">
									<label for="date01" class="control-label">A/C Name</label>
									<div class="controls">
										<input type="text" id="date01" name="name" placeholder="Enter A/C Name ..." class="span5" value="<?php if (count($chart) > 0) {
																																				echo $chart['name'];
																																			} ?>" />
									</div>
								</div>
								<div class="control-group">
									<label for="typeahead" class="control-label">Memo</label>
									<div class="controls">
										<textarea id="notes" name="memo" placeholder="Enter Memo..." class="span5"><?php if (count($chart) > 0) {
																														echo $chart['memo'];
																													} ?></textarea>
									</div>
								</div>
								<div class="control-group">
									<label for="name" class="control-label">Opening Balance</label>
									<div class="controls">
										<input type="text" id="name" name="opening" placeholder="Enter Opening Balance ..." class="span5" value="<?php if (count($chart) > 0) {
																																						echo $chart['opening'];
																																					} ?>" />
									</div>
								</div>
								<div class="control-group">
									<label for="textfield" class="control-label">Select Status</label>
									<div class="controls">
										<select name="status" id="status" class="span5 chzn-select status_">
											<option value="Active" <?php if (count($chart) > 0 && $chart['status'] == 'Active') {
																		echo 'selected';
																	} ?>>Active</option>
											<option value="Inactive" <?php if (count($chart) > 0 && $chart['status'] == 'Inactive') {
																			echo 'selected';
																		} ?>>Inactive</option>
										</select>
									</div>
								</div>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<?php if (count($chart) > 0) { ?>
									<input type="hidden" name="id" value="<?php echo $chart['id']; ?>" />
								<?php } ?>
								<div class="form-actions">
									<input type="submit" name="submit" class="btn btn-success" value="Save changes" />
								</div>
							</fieldset>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
		</div>

		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

<script>
	$('.parent_id').select2({
		placeholder: '--- Select A/C ---',
		ajax: {
			url: "inventory/get_ac_chart",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});
	$('#parent_id').focus();
</script>