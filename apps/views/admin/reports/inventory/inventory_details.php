<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Cybextech">
    <link rel="shortcut icon" href="<?php echo $this->session->userdata('company_logo'); ?>" type="image/x-icon" />

    <!-- styles -->
    <link href="assets/backend/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/backend/css/stilearn.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
        @media print {
            p.muted {
                font-weight: bold;
            }

            small.small {
                font-weight: normal;
            }
        }
    </style>
</head>

<body>
    <!-- section content -->
    <div id="main-content">
        <div class="container-fluid">
            <!-- span content -->
            <div class="row-fluid">
                <div class="span12">
                    <!-- content -->
                    <!-- <div class="content" style="border: 1px solid #d7d7d7;"> -->
                    <!-- content-body -->
                    <div class="content-body">
                        <!-- invoice -->
                        <div id="invoice-container" class="invoice-container">
                            <div class="page-header">
                                <h2 class="center" style="color: #000;">
                                    <img src="<?php echo $this->session->userdata('company_logo'); ?>" width="50" class="img"><b><?php echo $this->session->userdata('company_name'); ?><b></h2>
                                <br>
                            </div>
                            <div class="row-fluid">
                                <div class="span12 center">
                                    <strong><?php echo $reprort_type; ?>Profit Report</strong>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span8">
                                    <p class="muted">Date From</p>
                                    <p><?php echo date('Y-m-d', strtotime(date_to_db($start_date))); ?></p>
                                </div>
                                <div class="span4">
                                    <p class="muted">Date To</p>
                                    <p><?php echo date('Y-m-d', strtotime(date_to_db($end_date))); ?></p>
                                </div>
                            </div>
                            <div class="invoice-table">
                                <table class="table table-bordered invoice responsive">
                                    <thead>
                                        <tr>
                                            <th>SL #</th>
                                            <th>Items</th>
                                            <th>Profit</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $i = 1;
                                        $total = 0;
                                        foreach ($products as $product) {
                                        ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td class="left"><?php echo $product['item_name']; ?></td>
                                                <td><?php echo $product['total_profit']; ?></td>
                                            </tr>
                                        <?php
                                            $total += $product['total_profit'];
                                            $i++;
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">Total Profit</td>
                                            <td><?php echo number_format($total, 2); ?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="invoice-table">
                                <table class="table table-bordered invoice responsive">
                                    <thead>
                                        <tr>
                                            <th>SL #</th>
                                            <th>Account Heads</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $i = 1;
                                        $total_ex = 0;
                                        foreach ($expenses as $expense) {
                                        ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td class="left"><?php echo $expense['name'] . " (" . $expense['exp_code'] . ")"; ?></td>
                                                <td><?php echo $expense['amount']; ?></td>
                                            </tr>
                                        <?php
                                            $total_ex += $expense['amount'];
                                            $i++;
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">Total Expense</td>
                                            <td><?php echo number_format($total_ex, 2); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Net Profit</td>
                                            <td><?php echo number_format($total - $total_ex, 2); ?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="pull-left">
                            <b>Prepared By____________________</b>
                        </div>
                        <div class="center" style="margin-right:220px" ;>
                            <b>checked By____________________</b>
                        </div>
                        <div class="pull-right" style="margin-top:-20px" ;>
                            <b>Approved By____________________</b>
                        </div>
                        <!--/invoice-->
                    </div>
                    <!--/content-body -->
                    <!-- </div> -->
                    <!-- /content -->
                </div><!-- /span content -->
            </div>
        </div><!-- /container -->
    </div>
</body>

</html>