<div id="main-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Settings
				</h3>
				<ul class="breadcrumb">
					<li>
						<a href="dashboard">Dashboard</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="settings">Settings</a>
					</li>

				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN Alert widget-->
		<?php if($this->session->flashdata('success') || $this->session->flashdata('info') || $this->session->flashdata('error')) { ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('error')) { ?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		<!-- END Alert widget-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<!--BEGIN METRO STATES-->
			<?php if($this->account_privileges->get_date(date('Y-m-d'))){ ?>
			<div class="metro-nav">
				<?php if($privileges['basic_settings'] == 1){ ?>
				<div class="metro-nav-block nav-block-green double">
					<a data-original-title="" href="settings/currency_save">
						<i class="icon-usd"></i>
						<div class="info">Add New</div>
						<div class="status">Currency</div>
					</a>
				</div>
				<?php } ?>
				<?php if($privileges['basic_settings'] == 1){ ?>
				<div class="metro-nav-block nav-block-green">
					<a data-original-title="" href="settings/currency_list">
						<i class="icon-usd"></i>
						<div class="info">List All</div>
						<div class="status">Currency</div>
					</a>
				</div>
				<?php } ?>
				<!-- <?php if($privileges['basic_settings'] == 1){ ?>
				<div class="metro-nav-block nav-block-purple double">
					<a data-original-title="" href="settings/basic">
						<i class="icon-cog"></i>
						<div class="info">Basic</div>
						<div class="status">Settings</div>
					</a>
				</div>
				<?php } ?> -->
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<?php if($this->session->userdata('user_type') == 'Admin'){ ?>
				<?php if($privileges['company_settings'] == 1){ ?>
				<div class="metro-nav-block nav-block-blue double">
					<a data-original-title="" href="settings/cmp_list">
						<i class="icon-list"></i>
						<div class="info">List All</div>
						<div class="status">Company</div>
					</a>
				</div>
				<?php } ?>
			
				<?php } ?>
			</div>
			<div class="space10"></div>
			<div class="metro-nav">
				<div class="metro-nav-block nav-block-orange">
					<a data-original-title="" href="settings/db_backup">
						<i class="icon-file-text"></i>
						<div class="info">Backup</div>
						<div class="status">Backup</div>
					</a>
				</div>
			
				</div>
				<?php } ?>
			<div class="space10"></div>
			<div class="metro-nav">
               </div>
			<!--END METRO STATES-->
		</div>

		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>
<script>
    jQuery(document).ready(function() {
        $(".loading").hide();
        $(".loading_date").hide();
    });

$(".sync").click(function() {
    $(".sync").hide();
    $(".loading").show();

    $.ajax({
        type: "POST",
        url: "api/sync_db",
        data: '',
        cache: false,
        success: function (response) {
            $("#msg").empty();
            var res = JSON.parse(response);
            var error = res.error;
            var message = res.message;
            if(error){
                if(res.showButton){
                    $(".sync").show();
                    $(".loading").hide();
                    $("#msg").append(message);
                }else{
                    $(".sync").hide();
                    $(".loading").hide();
                    $("#msg").append(message);
                }
            }else{
                $(".sync").show();
                $(".loading").hide();
                $("#msg").append(message);
            }
            

        }
    });
});
$(".date").click(function() {
    $(".date").hide();
    $(".loading_date").show();

    $.ajax({
        type: "POST",
        url: "api/license_update",
        data: '',
        cache: false,
        success: function (response) {
        	console.log(response)
            $("#license_msg").empty();
            var res = JSON.parse(response);
            var error = res.error;
            var message = res.message;
            if(error){
                if(res.showButton){
                    $(".date").show();
                    $(".loading_date").hide();
                    $("#license_msg").append(message);
                }else{
                    $(".date").hide();
                    $(".loading_date").hide();
                    $("#license_msg").append(message);
                }
            }else{
                $(".date").show();
                $(".loading_date").hide();
                $("#license_msg").append(message);
            }
            

        }
    });
})
</script>